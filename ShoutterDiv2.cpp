#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
struct ShoutterDiv2 {int count(vector <int> s, vector <int> t);};

int ShoutterDiv2::count(vector <int> s, vector <int> t) {
	int res = 0;
	rep(i, s.size()) REP(j, i+1, s.size()) res += max(s[i], s[j]) <= min(t[i], t[j]);
	return res;
}