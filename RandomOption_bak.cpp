#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct RandomOption {double getProbability(int keyCount, vector <int> badLane1, vector <int> badLane2);};

int K;
double dp[1<<15][15];
bool invalid[55][55];

double dfs(int used, int last_ = -1) {
	auto last = last_ == -1 ? 14 : last_;
	auto& ret = dp[used][last];
	if(ret >= 0) { return ret; }
	if(used == (1<<K)-1) { return ret = 1.0; }

	ret = 0;
	rep(i, K) {
		if(used >> i & 1) { continue; }
		if(invalid[last][i]) { continue; }
		ret += dfs(used|(1<<i), i);
	}
	ret /= K - __builtin_popcount(used);
	return ret;
}

double RandomOption::getProbability(int keyCount, vector <int> badLane1, vector <int> badLane2) {
  K = keyCount;
  minus(dp);
  zero(invalid);
  rep(i, badLane1.size()) {
  	invalid[badLane1[i]][badLane2[i]] = invalid[badLane2[i]][badLane1[i]] = 1;
  }

  return dfs(0, -1);
}
