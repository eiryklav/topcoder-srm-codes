#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

// ------------- input start -------------
vector<string> cost;
// ------------- input end  --------------
int solve(); struct GameOnABoard { int optimalChoice(vector<string> _cost) { cost = _cost; return solve(); }};

int N, M;

int dijkstra(int sy, int sx) {

  vector<vector<int>> dist(N, vector<int>(M, inf));

  priority_queue<tuple<int, int, int>> pq;
  dist[sy][sx] = 0;
  pq.emplace(0, sy, sx);
  while(!pq.empty()) {
    int c, y, x; tie(c, y, x) = pq.top(); pq.pop();
    c *= -1;
    rep(i, 4) {
      int const ny = y+dy[i], nx = x+dx[i];
      if(!in_range(ny, nx, N, M)) { continue; }
      int const nc = c + ::cost[ny][nx] - '0';

      if(dist[ny][nx] <= nc) { continue; }
      dist[ny][nx] = nc;
      pq.emplace(-nc, ny, nx);
    }
  }

  int ans = 0;
  rep(i, N) rep(j, M) {
    if(sy == i && sx == j) { continue; }
    maximize(ans, dist[i][j]);
  }
  return ans;
}

int solve() {
  N = cost.size();
  M = cost[0].size();
  int ans = inf;
  rep(i, N) rep(j, M) {
    minimize(ans, dijkstra(i, j) + cost[i][j] - '0');
  }

  return ans;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> cost;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              cost = { "11","10" };
                              
                    __expected = 2;
                    break;
        }
                case 1: {
                              cost = { "01","10" };
                              
                    __expected = 1;
                    break;
        }
                case 2: {
                              cost = { "111001","001000","111111","001111","001100","001011","111001","010011" };
                              
                    __expected = 3;
                    break;
        }
                case 3: {
                              cost = { "001001101011","110011001101","111111000001","111101010001","011100101111","110010111000","111111110101","111011110111","111100100011","000000000110","101011011110","011111000111","101111001011" };
                              
                    __expected = 5;
                    break;
        }
                case 4: {
                              cost = { "110010100101010110100010001100111011","001000000110100011010100000001001000","011000110111101001011101110111000100","111001011000100101111010100110110011","111000011101001010000100001010000010","111001110010100101000001001100011011","111110100111010101100000100111000111","011111111100100111111110000001110111","110000010101001111100011110000001000","010010110111111100011101100000011010","110001100001111001101000101110110001","110010000111011110000010110111010101","100100110101001001101000001101101101","001011101101001100111110101111001110","111010111111111100110100000011111100","110101101000001001000100101011100000","011011001011010001001000100000110101","011111111100000011010111010011010100","111001111110001110001110010100111010","000001111000001100101010000001101110","010000110000010010111110111000010101","100010010100110011000111101001101011","111010110001101011010001111101111100","000111110000110000000101100101000110","110000010111001001110001101010111100","011111101101001011011010011111100010","110101111101010100110010000011001101","101101111001010100101111100001110001","000110010100101111011011110010010010","110101010011101000111011100000010011","110001010001110011010100110000010001","111010101100111100100011001101010100","011000000000100001011010000100010001","100000110110000001010001001111010000","100011111110010011011011001110011111","101100001111100101001101100000100001","010000111011010110011001110011111000","100010100111110111001010100101111010","000110011110111011111000101000001000" };
                              
                    __expected = 7;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    cost = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(cost, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> cost, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << cost  << "]" << endl;
   
    GameOnABoard *__instance = new GameOnABoard();
  int __result = __instance->optimalChoice(cost);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "GameOnABoard: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
