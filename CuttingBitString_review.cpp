#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()

int constexpr inf = 1<<30;

typedef long long ll;

struct CuttingBitString {int getmin(string S);};

ll to_integer(string const& s) {
  ll ret = 0;
  rep(i, s.size()) {
    ret *= 2;
    ret += s[i] - '0';
  }
  return ret;
}

int CuttingBitString::getmin(string S) {
  int N = S.size();
  vector<string> fs;
  ll five = 1;
  fs.push_back("1");
  REP(i, 1, 30) {
    five *= 5;
    ll temp = five;
    string tar;
    while(temp > 0) {
      tar += char(temp % 2 + '0');
      temp /= 2;
    }
    reverse(all(tar));
    fs.push_back(tar);
  }

  int dp[55]; fill(dp, dp+55, inf);
  dp[0] = 0;
  rep(i, N) {
    if(dp[i] == inf) continue;
    for(auto && e: fs) {
      if(S.size() >= i + e.size() && S.substr(i, e.size()) == e) {
        dp[i+e.size()] = min(dp[i+e.size()], dp[i] + 1);
      }
    }
  }
  return dp[N] == inf ? -1 : dp[N];
}
/*
int main() {
  CuttingBitString c;
  string s;
  while(cin >> s) {
    cout << c.getmin(s) << endl;
  }
}
*/