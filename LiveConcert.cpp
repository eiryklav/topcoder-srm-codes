#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct LiveConcert {int maxHappiness(vector <int> h, vector <string> s);};

int LiveConcert::maxHappiness(vector <int> h, vector <string> s) {
  int res = 0;
  vector<pair<int, string>> v;
  rep(i, h.size()) {
    v.emplace_back(h[i], s[i]);
  }
  sort(all(v)); reverse(all(v));
  set<string> used;
  rep(i, v.size()) {
    if(used.count(v[i].second)) { continue; }
    used.insert(v[i].second);
    res += v[i].first;
  }
  return res;
}
