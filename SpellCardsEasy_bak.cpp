#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct SpellCardsEasy {int maxDamage(vector <int> level, vector <int> damage);};

int dp[55][55];

int SpellCardsEasy::maxDamage(vector <int> level, vector <int> damage) {
	int N = level.size();
	rep(i, N) level[i] --;
	minus(dp);
	dp[N][0] = 0;
	for(int i=N-1; i>=0; i--) {
		rep(j, N) {
			if(dp[i+1][j] >= 0) { dp[i][j+1] = max(dp[i][j+1], dp[i+1][j]); }
			REP(k, i+1, N+1) {
				if(dp[k][j] < 0) { continue; }
				dp[i][j] = max(dp[i][j], dp[k][j]);
				if(j-level[i] < 0) { continue; }
				dp[i][j-level[i]] = max(dp[i][j-level[i]], dp[k][j] + damage[i]);
			}
		}
	}
	return *max_element(dp[0], dp[0] + N);
}