#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> allA;
vector<string> allB;
vector<string> allC;
// ------------- input end  --------------
int solve(); struct WinterAndReindeers {int getNumber(vector<string> _allA, vector<string> _allB, vector<string> _allC){allA = _allA, allB = _allB, allC = _allC;return solve();}};

vector<tuple<int, int, char>> memo[55][55];

vector<string> dfs(int s, int t) {

  if(s == 0 || t == 0) { return {""}; }

  vector<string> ret;

  for(auto& e: memo[s][t]) {
    int ns, nt; char c; tie(ns, nt, c) = e;
//    cout << ns << ", " << nt << ", " << c << endl;
    auto v = dfs(ns, nt);
    if(c != '-') {
      for(auto& u: v) {
        u.push_back(c);
      }
    }
    
    copy(all(v), back_inserter(ret));
  }

  return ret;
}

int dp1[2505][2505];
int dp[2505][2505];

int solve() {

  rep(i, 55) rep(j, 55) memo[i][j].clear();

  string A; for(auto& e: allA) A += e;
  string B; for(auto& e: allB) B += e;
  string C; for(auto& e: allC) C += e;

  int n = A.size(), m = B.size(), l = C.size();

  zero(dp1);
  rep(i, n) rep(j, m) {
    if(A[i] == B[j]) {
      dp1[i+1][j+1] = dp1[i][j] + 1;
      memo[i+1][j+1].push_back(make_tuple(i, j, A[i]));
    }
    else {
      if(dp1[i+1][j] > dp1[i][j+1]) {
        dp1[i+1][j+1] = dp1[i+1][j];
        memo[i+1][j+1].push_back(make_tuple(i+1, j, '-'));
      }
      else {
        dp1[i+1][j+1] = dp1[i][j+1];
        memo[i+1][j+1].push_back(make_tuple(i, j+1, '-'));
      }
    }
  }

//  string AB;
  auto ABs = dfs(n, m);

  int max = 0;

  for(auto& AB: ABs) {

    cout << AB << endl;

    int abn = AB.size();

    zero(dp);
    rep(i, abn) rep(j, l) {
      if(AB[i] == C[j]) {
        dp[i+1][j+1] = dp[i][j] + 1;
      }
      else {
        dp[i+1][j+1] = std::max(dp[i+1][j], dp[i][j+1]);
      }
    }
    maximize(max, dp[abn][l] >= l ? dp[abn][l] : 0);
  }

  return max;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,vector<string>,vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> allA;
            vector<string> allB;
            vector<string> allC;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              allA = { "X" };
                                        allB = { "X","Y" };
                                        allC = { "X" };
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              allA = { "ABCXD" };
                                        allB = { "BCDEF" };
                                        allC = { "CD" };
                              
                    __expected = 3;
                    break;
        }
                case 2: {
                              allA = { "WE","LOVE" };
                                        allB = { "WORKING","FOR","SANTA" };
                                        allC = { "JK" };
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              allA = { "ABCDE" };
                                        allB = { "CDEAB" };
                                        allC = { "B" };
                              
                    __expected = 2;
                    break;
        }
                case 4: {
                              allA = { "ABCABDEGAXAHDJBAAHFJDXBB","ABHFHCDALFDJDBBA","BABBAXAXXX" };
                                        allB = { "ABLOCBAXBAHAJDXBIJKA","JBABCDAHKFIUDALPJAH","AABACX" };
                                        allC = { "AXBADXBBAB","CDD" };
                              
                    __expected = 23;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    allA = ;
                  allB = ;
                  allC = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(allA, allB, allC, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> allA, vector<string> allB, vector<string> allC, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << allA << ","<< allB << ","<< allC  << "]" << endl;
   
    WinterAndReindeers *__instance = new WinterAndReindeers();
  int __result = __instance->getNumber(allA, allB, allC);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "WinterAndReindeers: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
