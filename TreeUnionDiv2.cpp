#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define chmin(a, x) a = min(a, x)
#define chmax(a, x) a = max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> tree1;
vector<string> tree2;
int K;
// ------------- input end  --------------
int solve(); struct TreeUnionDiv2 { int maximumCycles(vector<string> _tree1, vector<string> _tree2, int _K) { tree1 = _tree1, tree2 = _tree2, K = _K; return solve(); }};

int dist[2][10][10];
int N;

int solve() {
  rep(i, 2) rep(j, 10) rep(k, 10) dist[i][j][k] = inf;

  N = tree1.size();
  rep(i, N) dist[0][i][i] = dist[1][i][i] = 0;
  rep(i, N) rep(j, N) {
    dist[0][i][j] = tree1[i][j] == 'X' ? 1 : inf;
    dist[1][i][j] = tree2[i][j] == 'X' ? 1 : inf;
  }

  rep(l, 2) rep(k, N) rep(i, N) rep(j, N) {
    if(dist[l][i][k] < inf && dist[l][k][j] < inf) {
      chmin(dist[l][i][j], dist[l][i][k] + dist[l][k][j]);
    }
  }

  int res = 0;
  int perm[10]; iota(perm, perm+N, 0);
  do {
    int loopCnt = 0;
    rep(i, N) REP(j, i+1, N) {
      int k = perm[i], l = perm[j];
      if(dist[0][i][j] + dist[1][k][l] + 2 == K) {
        loopCnt ++;
      }
    }
    chmax(res, loopCnt);
  } while(next_permutation(perm, perm+N));

  return res;
}