#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
vector<int> pos;
// ------------- input end  --------------
int solve(); struct PermutationCountsDiv2 {int countPermutations(int _N, vector<int> _pos){N = _N, pos = _pos;return ::solve();}};

namespace math { namespace integer {
template<class value_type> value_type mod_mul(value_type x, value_type k, ll m) { if(k == 0) { return 0; } if(k % 2 == 0) { return mod_mul((x+x) % m, k/2, m); } else { return (x + mod_mul(x, k-1, m)) % m; } }
template<class value_type> value_type mod_pow(value_type x, value_type n, ll mod) { if(n == 0) { return 1; } if(n % 2 == 0) { return mod_pow(mod_mul(x, x, mod) % mod, n / 2, mod); } else { return mod_mul(x, mod_pow(x, n - 1, mod), mod); } }
template<class value_type> value_type mod_inverse(value_type x, ll mod) { return mod_pow(x, mod-2, mod); /* use fermat */ }
}}
using namespace math::integer;

constexpr int MOD = 1e9+7;

namespace math {

constexpr int MaxFact = 1000100;

struct FactRevFactList {

  array<long long, MaxFact+1> fact_;
  array<long long, MaxFact+1> rev_fact_;

  FactRevFactList() {
    fact_[0] = 1;
    for(int i=1; i<MaxFact; i++) {
      fact_[i] = fact_[i-1] * i;
      fact_[i] %= MOD;
    }

    rev_fact_[MaxFact-1] = mod_pow(fact_[MaxFact-1], (long long)MOD-2, MOD);

    for(int i=MaxFact-2; i>=0; i--) {
      rev_fact_[i] = rev_fact_[i+1] * (i+1);
      rev_fact_[i] %= MOD;
    }
  }

  long long fact(int x) const { return fact_[x]; }
  long long fact_inv(int x) const { return rev_fact_[x]; }

};

}

math::FactRevFactList fr;

int solve() {

  pos.push_back(0);
  pos.push_back(N);
  sort(all(pos));

  ll dp[3333] = {};
  dp[0] = fr.fact(N);
  // 包除原理で解く。
  // [j, i]を条件を満たさない連続降下する区間として、全てのiを重ならないように掛け合わせる
  rep(i, pos.size()) rep(j, i) {
    ll an_interval_pattern = dp[j] * fr.fact_inv(pos[i] - pos[j]) % MOD;
    if((i - j) % 2) {
      dp[i] += an_interval_pattern;
    } else {
      dp[i] += MOD - an_interval_pattern;
    }
    dp[i] %= MOD;
  }
  return dp[pos.size() - 1];
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            vector<int> pos;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 5;
                                        pos = { 3 };
                              
                    __expected = 9;
                    break;
        }
                case 1: {
                              N = 13;
                                        pos = { 12,11,10,9,8,7,6,5,4,3,2,1 };
                              
                    __expected = 1;
                    break;
        }
                case 2: {
                              N = 13;
                                        pos = {  };
                              
                    __expected = 1;
                    break;
        }
                case 3: {
                              N = 9;
                                        pos = { 2,4,5 };
                              
                    __expected = 1421;
                    break;
        }
                case 4: {
                              N = 80;
                                        pos = { 31,41,59,26,53,58,9,79,32,3,8,46 };
                              
                    __expected = 82650786;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  pos = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, pos, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, vector<int> pos, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< pos  << "]" << endl;
   
    PermutationCountsDiv2 *__instance = new PermutationCountsDiv2();
  int __result = __instance->countPermutations(N, pos);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "PermutationCountsDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
