#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> d;
// ------------- input end  --------------
double solve(); struct RandomPancakeStackDiv2 {double expectedDeliciousness(vector<int> _d){d = _d;return ::solve();}};

double solve() {

  int fact[11]; fact[0] = 1;
  REP(i, 1, 11) fact[i] = fact[i-1] * i;

  vector<int> p; rep(i, d.size()) p.push_back(i);

  double res = 0.0;
  do {
    int curw = inf;
    rep(i, d.size()) {
      int wid = p[i] + 1;
      int del = d[p[i]];
      if(curw < wid) { break; }
      res += del;
      curw = wid;
    }

  } while(next_permutation(all(p)));

  return res / fact[d.size()];
}

// CUT begin
using namespace std;
    char do_test(vector<int>,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> d;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              d = { 1,1,1 };
                              
                    __expected = 1.6666666666666667;
                    break;
        }
                case 1: {
                              d = { 10,20 };
                              
                    __expected = 20.0;
                    break;
        }
                case 2: {
                              d = { 3,6,10,9,2 };
                              
                    __expected = 9.891666666666667;
                    break;
        }
                case 3: {
                              d = { 10,9,8,7,6,5,4,3,2,1 };
                              
                    __expected = 10.999999724426809;
                    break;
        }
                case 4: {
                              d = { 1,2,3,4,5,6,7,8,9,10 };
                              
                    __expected = 7.901100088183421;
                    break;
        }
                case 5: {
                              d = { 1,1,1,1,1,1,1,1,1,1 };
                              
                    __expected = 1.7182818011463845;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    d = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(d, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> d, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << d  << "]" << endl;
   
    RandomPancakeStackDiv2 *__instance = new RandomPancakeStackDiv2();
  double __result = __instance->expectedDeliciousness(d);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "RandomPancakeStackDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
