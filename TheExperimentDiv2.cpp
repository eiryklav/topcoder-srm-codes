#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct TheExperimentDiv2 {vector <int> determineHumidity(vector <int> intensity, int L, vector <int> leftEnd);};

vector <int> TheExperimentDiv2::determineHumidity(vector <int> intensity, int L, vector <int> leftEnd) {
	vector<int> ret;
	set<int> used;
	rep(i, leftEnd.size()) {
		int l = leftEnd[i], r = leftEnd[i] + L - 1;
		int cnt = 0;
		rep(j, intensity.size()) {
			if(l <= j && j <= r) {
				if(used.count(j)) { continue; }
				used.insert(j);
				cnt += intensity[j];
			}
		}
		ret.push_back(cnt);
	}
	return ret;
}
