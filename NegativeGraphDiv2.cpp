#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e18;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
vector<int> s;
vector<int> t;
vector<int> weight;
int charges;
// ------------- input end  --------------
ll solve(); struct NegativeGraphDiv2 {ll findMin(int _N, vector<int> _s, vector<int> _t, vector<int> _weight, int _charges){N = _N, s = _s, t = _t, weight = _weight, charges = _charges;return solve();}};

ll dist[55][55];
ll dp[55][1010];  // iからN-1まで最大j回negative edgeにしたときの最小コスト

ll solve() {

  rep(i, s.size()) s[i]--, t[i]--;

  rep(i, 55) rep(j, 55) {
    dist[i][j] = Inf;
    if(i == j) dist[i][j] = 0;
  }

  rep(i, s.size()) {
    minimize(dist[s[i]][t[i]], (ll)weight[i]);
  }

  rep(k, N) rep(i, N) rep(j, N) {
    if(dist[i][k] < Inf && dist[k][j] < Inf) {
      minimize(dist[i][j], dist[i][k] + dist[k][j]);
    }
  }

  rep(i, 55) rep(j, 1010) {
    dp[i][j] = Inf;
  }

  rep(i, N) {
    dp[i][0] = dist[i][N-1];
  }

  REP(k, 1, charges+1) {
    rep(i, N) {
      dp[i][k] = dp[i][k-1];
      rep(e, s.size()) {
        minimize(dp[i][k], dist[i][s[e]] - weight[e] + dp[t[e]][k-1]);
      }
    }
  }

  return dp[0][charges];
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,vector<int>,int,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            vector<int> s;
            vector<int> t;
            vector<int> weight;
            int charges;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 3;
                                        s = { 1,1,2,2,3,3 };
                                        t = { 2,3,1,3,1,2 };
                                        weight = { 1,5,1,10,1,1 };
                                        charges = 1;
                              
                    __expected = -9LL;
                    break;
        }
                case 1: {
                              N = 1;
                                        s = { 1 };
                                        t = { 1 };
                                        weight = { 100 };
                                        charges = 1000;
                              
                    __expected = -100000LL;
                    break;
        }
                case 2: {
                              N = 2;
                                        s = { 1,1,2 };
                                        t = { 2,2,1 };
                                        weight = { 6,1,4 };
                                        charges = 2;
                              
                    __expected = -9LL;
                    break;
        }
                case 3: {
                              N = 2;
                                        s = { 1 };
                                        t = { 2 };
                                        weight = { 98765 };
                                        charges = 100;
                              
                    __expected = -98765LL;
                    break;
        }
                case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = 50;
                  s = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 21, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 35, 37, 38, 39, 40, 41, 42, 43, 42, 44, 45, 46, 45};
                  t = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 1, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 23, 37, 38, 39, 40, 41, 42, 43, 37, 44, 45, 46, 44, 50};
                  weight = {99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 99999, 0, 99998, 99998, 99998, 99998, 99998, 99998, 99998, 99998, 99998, 99998, 99998, 99998, 99998, 99998, 0, 99997, 99997, 99997, 99997, 99997, 99997, 99997, 0, 92734, 92734, 92734, 0};
                  charges = 1000;
              
                  __expected = -99991587;
          break;
        }
    
    default: return 'm';
  }
  return do_test(N, s, t, weight, charges, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, vector<int> s, vector<int> t, vector<int> weight, int charges, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< s << ","<< t << ","<< weight << ","<< pretty_print(charges)  << "]" << endl;
   
    NegativeGraphDiv2 *__instance = new NegativeGraphDiv2();
  ll __result = __instance->findMin(N, s, t, weight, charges);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "NegativeGraphDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
