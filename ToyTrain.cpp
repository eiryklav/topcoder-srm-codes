#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct ToyTrain {int getMinCost(vector <string> field);};

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int N, M;
bool paid[10];

int ToyTrain::getMinCost(vector <string> field) {

  N = field.size();
  M = field[0].size();

  bool loop_once = 0;

  bool vis[55][55] = {};
  bool paid[10] = {};
  int cost = 0;

  rep(i, N) {
  	int next = -1; // 0: A, 1: B
  	int num = 0;
  	rep(j, M) {
  		if(next == -1 && field[i][j] == 'A') {
  			loop_once = 1;
  			next = 1;
  			num ++;
  		}
  		else if(next == -1 && field[i][j] == 'B') {
  			loop_once = 1;
  			next = 0;
  			num ++;
  		}
  		else if(next == 0 && field[i][j] == 'A') {
  			loop_once = 1;
  			next = -1;
  			num ++;
  		}
  		else if(next == 1 && field[i][j] == 'B') {
  			loop_once = 1;
  			next = -1;
  			num ++;
  		}
  		else if(field[i][j] == '.' || field[i][j] == 'S') {
  			if(next >= 0) vis[i][j] = 1;
  		}
  		else if(isdigit(field[i][j])) {
  			if(next >= 0) {
	  			int nc = field[i][j] - '0';
	  			if(!paid[nc]) {
	  				paid[nc] = 1;
	  				cost += nc;
	  			}
	  			vis[i][j] = 1;
	  		}
  		}
  		else {
  			return -1;
  		}
		}
		if(num % 2) { return -1; }
  }

  rep(j, M) {
  	int next = -1; // 0: A, 1: B
  	int num = 0;
  	rep(i, N) {
  		if(next == -1 && field[i][j] == 'A') {
  			next = 1;
  			num ++;
  		}
  		else if(next == -1 && field[i][j] == 'B') {
  			next = 0;
  			num ++;
  		}
  		else if(next == 0 && field[i][j] == 'A') {
  			next = -1;
  			num ++;
  		}
  		else if(next == 1 && field[i][j] == 'B') {
  			next = -1;
  			num ++;
  		}
  		else if(field[i][j] == '.' || field[i][j] == 'S') {
  			if(next >= 0) {
  				if(vis[i][j]) { return -1; }
  				vis[i][j] = 1;
  			}
  		}
  		else if(isdigit(field[i][j])) {
  			if(next >= 0) {
  				if(vis[i][j]) { return -1; }
	  			int nc = field[i][j] - '0';
	  			if(!paid[nc]) {
	  				paid[nc] = 1;
	  				cost += nc;
	  			}
	  			vis[i][j] = 1;
	  		}
  		}
  		else {
  			return -1;
  		}
		}
		if(num % 2) { return -1; }
  }

  rep(i, N) rep(j, M) {
  	if(field[i][j] == 'S' && !vis[i][j]) {
  		return -1;
  	}
  }

  return loop_once ? cost : -1;
}
