#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int st1;
int p1;
int t1;
int st2;
int p2;
int t2;
// ------------- input end  --------------
string solve(); struct ElectronicPetEasy {string isDifficult(int _st1, int _p1, int _t1, int _st2, int _p2, int _t2){st1 = _st1, p1 = _p1, t1 = _t1, st2 = _st2, p2 = _p2, t2 = _t2;return ::solve();}};

string solve1() {
  rep(i, t1) rep(j, t2) {
    if(st1 + p1 * i == st2 + p2 * j) { return "Difficult"; }
  }
  return "Easy";
}

unordered_set<int> st;

string solve2() {
  rep(i, t1) st.insert(st1 + p1 * i);
  rep(i, t2) if(st.count(st2 + p2 * i)) { return "Difficult"; }
  return "Easy";
}

string solve() {
  return solve2();
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,int,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int st1;
            int p1;
            int t1;
            int st2;
            int p2;
            int t2;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              st1 = 3;
                                        p1 = 3;
                                        t1 = 3;
                                        st2 = 5;
                                        p2 = 2;
                                        t2 = 3;
                              
                    __expected = "Difficult";
                    break;
        }
                case 1: {
                              st1 = 3;
                                        p1 = 3;
                                        t1 = 3;
                                        st2 = 5;
                                        p2 = 2;
                                        t2 = 2;
                              
                    __expected = "Easy";
                    break;
        }
                case 2: {
                              st1 = 1;
                                        p1 = 4;
                                        t1 = 7;
                                        st2 = 1;
                                        p2 = 4;
                                        t2 = 7;
                              
                    __expected = "Difficult";
                    break;
        }
                case 3: {
                              st1 = 1;
                                        p1 = 1000;
                                        t1 = 1000;
                                        st2 = 2;
                                        p2 = 1000;
                                        t2 = 1000;
                              
                    __expected = "Easy";
                    break;
        }
                case 4: {
                              st1 = 1;
                                        p1 = 1;
                                        t1 = 1;
                                        st2 = 2;
                                        p2 = 2;
                                        t2 = 2;
                              
                    __expected = "Easy";
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    st1 = ;
                  p1 = ;
                  t1 = ;
                  st2 = ;
                  p2 = ;
                  t2 = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(st1, p1, t1, st2, p2, t2, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int st1, int p1, int t1, int st2, int p2, int t2, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(st1) << ","<< pretty_print(p1) << ","<< pretty_print(t1) << ","<< pretty_print(st2) << ","<< pretty_print(p2) << ","<< pretty_print(t2)  << "]" << endl;
   
    ElectronicPetEasy *__instance = new ElectronicPetEasy();
  string __result = __instance->isDifficult(st1, p1, t1, st2, p2, t2);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ElectronicPetEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
