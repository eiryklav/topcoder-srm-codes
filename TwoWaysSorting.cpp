#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> stringList;
// ------------- input end  --------------
string solve(); struct TwoWaysSorting {string sortingMethod(vector<string> _stringList){stringList = _stringList;return solve();}};

string solve() {

  auto K = stringList;
  auto L = stringList;

  sort(all(K));
  sort(all(L), [](string const& a, string const& b) { return a.size() < b.size(); });

  if(stringList == K && stringList == L) {
    return "both";
  }
  if(stringList == K) {
    return "lexicographically";
  }
  if(stringList == L) {
    return "lengths";
  }

  return "none";
}

// CUT begin
using namespace std;
    char do_test(vector<string>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> stringList;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              stringList = { "a","aa","bbb" };
                              
                    __expected = "both";
                    break;
        }
                case 1: {
                              stringList = { "c","bb","aaa" };
                              
                    __expected = "lengths";
                    break;
        }
                case 2: {
                              stringList = { "etdfgfh","aio" };
                              
                    __expected = "none";
                    break;
        }
                case 3: {
                              stringList = { "aaa","z" };
                              
                    __expected = "lexicographically";
                    break;
        }
                case 4: {
                              stringList = { "z" };
                              
                    __expected = "both";
                    break;
        }
                case 5: {
                              stringList = { "abcdef","bcdef","cdef","def","ef","f","topcoder" };
                              
                    __expected = "lexicographically";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    stringList = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(stringList, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> stringList, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << stringList  << "]" << endl;
   
    TwoWaysSorting *__instance = new TwoWaysSorting();
  string __result = __instance->sortingMethod(stringList);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TwoWaysSorting: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
