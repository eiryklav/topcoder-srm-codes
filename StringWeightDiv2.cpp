#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int L;
// ------------- input end  --------------
int solve(); struct StringWeightDiv2 { int countMinimums(int _L) { L = _L; return solve(); }};

int const MOD9 = 1e9+9;

ll comb[1111][1111];
void make_comb(int n) {
  rep(i, n+1) {
    comb[i][0] = 1;
    REP(j, 1, i+1) {
      comb[i][j] = comb[i-1][j-1] + comb[i-1][j];
      comb[i][j] %= MOD9;
    }
  }
}

int solve() {
  make_comb(1100);

  ll f[27]; f[0] = 1;
  REP(i, 1, 27) {
    f[i] = f[i-1] * i;
    f[i] %= MOD9;
  }

  if(L < 27) {
    return (comb[26][L] * f[L]) % MOD9;
  }
  else {
    return (f[26] * comb[L-26+25][25]) % MOD9;
  }

}
