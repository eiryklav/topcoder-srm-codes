#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
ll n;
int M;
int K;
// ------------- input end  --------------
double solve(); struct JanuszInTheCasino {double findProbability(ll _n, int _m, int _k){n = _n, M = _m, K = _k;return ::solve();}};

map<pair<ll, int>, double> dp;

double dfs(ll n, int k) {
  if(n <= 0) { return 0; }
  if(k == 0) { return 1; }
  if(dp.find({n, k}) != dp.end()) { return dp[{n, k}]; }
  return dp[{n, k}] = dfs(n - n / M, k - 1) * (M - n % M) / M
                    + dfs(n - (n / M + 1), k - 1) * (n % M) / M;
}

double solve() {
  dp.clear();
  return dfs(n, K);
}

// CUT begin
using namespace std;
    char do_test(ll,int,int,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            ll n;
            int m;
            int k;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 3LL;
                                        m = 2;
                                        k = 2;
                              
                    __expected = 0.75;
                    break;
        }
                case 1: {
                              n = 1LL;
                                        m = 3;
                                        k = 3;
                              
                    __expected = 0.2962962962962962;
                    break;
        }
                case 2: {
                              n = 4LL;
                                        m = 3;
                                        k = 2;
                              
                    __expected = 1.0;
                    break;
        }
                case 3: {
                              n = 5LL;
                                        m = 4;
                                        k = 5;
                              
                    __expected = 0.87109375;
                    break;
        }
                case 4: {
                              n = 1000000000000LL;
                                        m = 2;
                                        k = 40;
                              
                    __expected = 0.9094947017729282;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
                  m = ;
                  k = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, m, k, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(ll n, int m, int k, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n) << ","<< pretty_print(m) << ","<< pretty_print(k)  << "]" << endl;
   
    JanuszInTheCasino *__instance = new JanuszInTheCasino();
  double __result = __instance->findProbability(n, m, k);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "JanuszInTheCasino: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
