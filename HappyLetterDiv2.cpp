#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

struct HappyLetterDiv2 {
  char getHappyLetter(string letters){
    map<char, int> mp;
    for(int i=0; i<letters.size(); i++) {
      mp[letters[i]] ++;
    }

    for(auto& e: mp) {
      if(e.second > letters.size() / 2) {
        return e.first;
      }
    }
    return '.';
  }
};
