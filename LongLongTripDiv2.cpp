#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
ll D;
ll T; // convert from int
ll B; // convert from int
// ------------- input end  --------------
string solve(); struct LongLongTripDiv2 {string isAble(ll _D, int _T, int _B){D = _D, T = _T, B = _B;return solve();}};

string solve() {

  ll L = 0, R = 1e9+1, OL, OR;
  do {
    OL = L, OR = R;
    ll M = (L + R) / 2;
    if(T < M || (D - M * B) < T - M) {
      R = M;
    }
    else if((D - M * B) == T - M) {
      return "Possible";
    }
    else {
      L = M;
    }
  } while(!(OL == L && OR == R));

  return "Impossible";
}

// CUT begin
using namespace std;
    char do_test(ll,int,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            ll D;
            int T;
            int B;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              D = 10LL;
                                        T = 6;
                                        B = 3;
                              
                    __expected = "Possible";
                    break;
        }
                case 1: {
                              D = 10LL;
                                        T = 5;
                                        B = 3;
                              
                    __expected = "Impossible";
                    break;
        }
                case 2: {
                              D = 50LL;
                                        T = 100;
                                        B = 2;
                              
                    __expected = "Impossible";
                    break;
        }
                case 3: {
                              D = 120LL;
                                        T = 10;
                                        B = 11;
                              
                    __expected = "Impossible";
                    break;
        }
                case 4: {
                              D = 10LL;
                                        T = 10;
                                        B = 9999;
                              
                    __expected = "Possible";
                    break;
        }
                case 5: {
                              D = 1000LL;
                                        T = 100;
                                        B = 10;
                              
                    __expected = "Possible";
                    break;
        }
                case 6: {
                              D = 1000010000100001LL;
                                        T = 1100011;
                                        B = 1000000000;
                              
                    __expected = "Possible";
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    D = ;
                  T = ;
                  B = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(D, T, B, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(ll D, int T, int B, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(D) << ","<< pretty_print(T) << ","<< pretty_print(B)  << "]" << endl;
   
    LongLongTripDiv2 *__instance = new LongLongTripDiv2();
  string __result = __instance->isAble(D, T, B);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "LongLongTripDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
