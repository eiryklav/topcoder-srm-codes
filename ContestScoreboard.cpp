#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> scores;
// ------------- input end  --------------
vector<int> solve(); struct ContestScoreboard {vector<int> findWinner(vector<string> _scores){scores = _scores;return ::solve();}};

vector<string> names;
vector<int> Ss[111], Ts[111];

vector<int> solve() {

  map<string, int> name_to_idx;
  rep(i, scores.size()) {
    auto& e = scores[i];
    stringstream ss(e); string s; ss >> s; name_to_idx[s] = i;
  }

  sort(all(scores));

  map<int, int> to_raw_idx;
  rep(i, scores.size()) {
    auto& e = scores[i];
    stringstream ss(e); string s; ss >> s; to_raw_idx[i] = name_to_idx[s];
  }

  names.clear();
  rep(i, 111) Ss[i].clear(), Ts[i].clear();

  set<int> allts = {1};

  rep(i, scores.size()) {
    auto& e = scores[i];                                
    stringstream ss(e);
    string name; ss >> name;
    names.push_back(name);
    string s;
    while(ss >> s) {
      int si, ti; char dum; stringstream ss2(s);
      ss2 >> si >> dum >> ti;
      Ss[i].push_back(si), Ts[i].push_back(ti);
      allts.insert(ti + 1);
    }
  }

  int N = scores.size();

  vector<int> ret(N);

  for(auto && D: allts) {

    vector<pair<int, int>> kekka(N);

    rep(i, N) {
      int sum = 0;
      rep(j, Ts[i].size()) {
        if(Ts[i][j] < D) {
          sum += Ss[i][j];
        }
      }
      kekka[i] = {-sum, i};
    }

    sort(all(kekka));
    ret[to_raw_idx[kekka[0].second]] = 1;


  }

  return ret;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,vector<int>,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> scores;
            vector<int> __expected = vector<int>();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              scores = { "TVG 1/1 1/2 1/3","AJI 1/4 1/5 1/6" };
                              
                    __expected = { 1, 1 };
                    break;
        }
                case 1: {
                              scores = { "GLP 1/114 1/195 1/171 1/19 1/146 1/29","BKPF 1/57 1/187 1/277 1/21 1/223 1/35" };
                              
                    __expected = { 1, 1 };
                    break;
        }
                case 2: {
                              scores = { "AAA 248/2 495/5 993/7","BBB 244/6 493/7 990/10","CCC 248/2 495/5 993/10" };
                              
                    __expected = { 1, 0, 0 };
                    break;
        }
                case 3: {
                              scores = { "UBA 10/2 30/4 25/3 999/1000","UNC 1/3 3/20 40/50","UNLP 2/2 3/3 4/4 100/100","UNR 999/1000000 999/999999","UNS 999/100000000" };
                              
                    __expected = { 1, 0, 1, 1, 0 };
                    break;
        }
                case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    scores = {"A 1/1", "B 2/1"};
              
          __unknownAnswer = true; 
          break;
        }
    
    default: return 'm';
  }
  return do_test(scores, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> scores, vector<int> __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << scores  << "]" << endl;
   
    ContestScoreboard *__instance = new ContestScoreboard();
  vector<int> __result = __instance->findWinner(scores);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  __expected  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  __result  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ContestScoreboard: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
