#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> A;
vector<int> numA;
vector<int> B;
vector<int> numB;
// ------------- input end  --------------
int solve(); struct TwoNumberGroupsEasy {int solve(vector<int> _A, vector<int> _numA, vector<int> _B, vector<int> _numB){A = _A, numA = _numA, B = _B, numB = _numB;return ::solve();}};

template<class T> vector<T> divisors(T x) {
  vector<T> ret;
  int p = 1;
  while((ll)p * p <= x) {
    if(x % p == 0) {
      ret.push_back(p);
      ret.push_back(x/p);
    }
    p ++;
  }
  return ret;
}

int solve() {

  // A_i ≡ B_j (mod M) <=> A_i - B_j ≡ 0 (mod M) より、A_i - B_j の約数を全探索する。
  // 負になる場合があるので、実際は |A_i - B_j| の約数を調べれば良い。

  set<int> Ms = {2};
  for(auto && e: A) for(auto && u: B)
    for(auto && m: divisors(abs(e - u)))
      if(m > 1) { Ms.insert(m); }

  int ans = inf;
  for(auto && M: Ms) {
    auto a = A; for(auto && e: a) e %= M;
    auto b = B; for(auto && e: b) e %= M;
    map<int, ll> ma, mb;
    rep(i, a.size()) ma[a[i]] += numA[i];
    rep(i, b.size()) mb[b[i]] += numB[i];

    set<int> s;
    for(auto && e: ma) s.insert(e.first);
    for(auto && e: mb) s.insert(e.first);

    int cnt = 0;
    for(auto && e: s) {
      cnt += abs(ma[e] - mb[e]);
    }
    minimize(ans, cnt);
  }
  return ans;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> A;
            vector<int> numA;
            vector<int> B;
            vector<int> numB;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = { 1,2,3,4 };
                                        numA = { 2,1,1,1 };
                                        B = { 5,6,7,8 };
                                        numB = { 1,1,1,2 };
                              
                    __expected = 2;
                    break;
        }
                case 1: {
                              A = { 5,7 };
                                        numA = { 1,1 };
                                        B = { 12,14 };
                                        numB = { 1,1 };
                              
                    __expected = 0;
                    break;
        }
                case 2: {
                              A = { 100 };
                                        numA = { 2 };
                                        B = { 1 };
                                        numB = { 1 };
                              
                    __expected = 1;
                    break;
        }
                case 3: {
                              A = { 1 };
                                        numA = { 1 };
                                        B = { 1 };
                                        numB = { 1 };
                              
                    __expected = 0;
                    break;
        }
                case 4: {
                              A = { 5 };
                                        numA = { 1 };
                                        B = { 6 };
                                        numB = { 1 };
                              
                    __expected = 2;
                    break;
        }
                case 5: {
                              A = { 733815053,566264976,984867861,989991438,407773802,701974785,599158121,713333928,530987873,702824160 };
                                        numA = { 8941,4607,1967,2401,495,7654,7078,4213,5485,1026 };
                                        B = { 878175560,125398919,556001255,570171347,643069772,787443662,166157535,480000834,754757229,101000799 };
                                        numB = { 242,6538,7921,2658,1595,3049,655,6945,7350,6915 };
                              
                    __expected = 7;
                    break;
        }
                case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = {728668149, 48864810, 853242445, 752162484, 428192564, 342166021, 600326538, 698564774, 441142645};
                    numA = {65954, 79668, 48332, 76379, 59116, 19715, 74033, 19945, 4618};
                    B = {550583671, 865303766, 730480317, 283028704, 400637025, 500481567, 151192227, 91534494, 142314786, 785137776};
                    numB = {83567, 81038, 25933, 47410, 51843, 63201, 98008, 41625, 38996, 28125};
              
                    __expected = 111986; 
          break;
        }
    
    default: return 'm';
  }
  return do_test(A, numA, B, numB, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> A, vector<int> numA, vector<int> B, vector<int> numB, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << A << ","<< numA << ","<< B << ","<< numB  << "]" << endl;
   
    TwoNumberGroupsEasy *__instance = new TwoNumberGroupsEasy();
  int __result = __instance->solve(A, numA, B, numB);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TwoNumberGroupsEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
