#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> duration;
vector<int> tone;
int T;
// ------------- input end  --------------
int solve(); struct GUMIAndSongsDiv2 { int maxSongs(vector<int> _duration, vector<int> _tone, int _T) { duration = _duration, tone = _tone, T = _T; return solve(); }};

int dp[1<<16][16];

int solve() {

  int N = duration.size();

  rep(i, 1<<16) rep(j, 16) dp[i][j] = inf;
  rep(i, N) dp[1<<i][i] = duration[i];

  rep(S, 1<<N) {
    rep(i, N) {
      if(dp[S][i] == inf) { continue; }
      if(!(S >> i & 1)) { continue; }
      rep(j, N) {
        if(S >> j & 1) { continue; }
        minimize(dp[S|(1<<j)][j], dp[S][i] + abs(tone[j]-tone[i]) + duration[j]);
      }
    }
  }

  int ans = 0;
  rep(i, 1<<N) rep(j, N) {
    if(dp[i][j] <= T) {
      maximize(ans, __builtin_popcount(i));
    }
  }

  return ans;
}

int main() {

  vector<vector<int>> durations = {
    { 3, 5, 4, 11 },
    { 100, 200, 300 },
    { 1, 2, 3, 4 },
    { 10, 10, 10 },
    { 8, 11, 7, 15, 9, 16, 7, 9 },
    { 5611, 39996, 20200, 56574, 81643, 90131, 33486, 99568, 48112, 97168, 5600, 49145, 73590, 3979, 94614 },
  };

  vector<vector<int>> tones = {
    { 2, 1, 3, 1 },
    { 1, 2, 3 },
    { 1, 1, 1, 1 },
    { 58, 58, 58 },
    { 3, 8, 5, 4, 2, 7, 4, 1 },
    { 2916, 53353, 64924, 86481, 44803, 61254, 99393, 5993, 40781, 2174, 67458, 74263, 69710, 40044, 80853 }
  };

  vector<int> Ts = {
    17,
    10,
    100,
    30,
    14,
    302606
  };

  vector<int> answers = {
    3,
    0,
    4,
    3,
    1,
    8
  };

  rep(i, durations.size()) {
    cout << "Case " << i << ":\n";
    if(GUMIAndSongsDiv2().maxSongs(durations[i], tones[i], Ts[i]) == answers[i]) {
      cout << "OK\n";
    }
    else {
      cout << "NG\n";
    }
  }

  return 0;
}