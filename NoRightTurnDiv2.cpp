#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> x;
vector<int> y;
// ------------- input end  --------------
vector<int> solve(); struct NoRightTurnDiv2 {vector<int> findPath(vector<int> _x, vector<int> _y){x = _x, y = _y;return ::solve();}};

typedef complex<double> P;

inline double cross(P const& a, P const& b) { return imag(conj(a)*b); }
inline double dot(P const& a, P const& b) { return real(conj(a)*b); }

enum ccw_result {
  counter_clockwise = +1, clockwise = -1, online_back = +2, online_front = -2, on_segment = 0
};

ccw_result ccw(P a, P b, P c) {
  b -= a, c -= a;
  if(cross(b, c) > 0)   { return ccw_result::counter_clockwise; }
  if(cross(b, c) < 0)   { return ccw_result::clockwise; }
  if(dot(b, c) < 0)     { return ccw_result::online_back; }
  if(norm(b) < norm(c)) { return ccw_result::online_front; }
  return ccw_result::on_segment;
}

vector<int> solve() {

  int N = x.size();
  vector<P> pts;

  rep(i, N) {
    pts.emplace_back(x[i], y[i]);
  }

  int s = 0;
  rep(i, N) {
    if(x[i] > x[s] || (x[i] == x[s] && y[i] > y[s])) { s = i; }
  }

  vector<int> ret(N);
  ret[0] = s;
  set<int> used = {s};

  int prev = s;

  REP(i, 1, N) {
    int next = -1;
    rep(j, N) {
      if(used.count(j)) { continue; }
      if(next == -1 || ccw(pts[prev], pts[next], pts[j]) == clockwise) {
        next = j;
      }
    }
    ret[i] = next;
    used.insert(next);
    prev = next;
  }
  return ret;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,vector<int>,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> x;
            vector<int> y;
            vector<int> __expected = vector<int>();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              x = { -10,0,10 };
                                        y = { 10,-10,10 };
                              
                    __expected = { 0, 1, 2 };
                    break;
        }
                case 1: {
                              x = { 0,0,-3,-3,3,3 };
                                        y = { -1,1,-3,3,-3,3 };
                              
                    __expected = { 0, 4, 5, 3, 2, 1 };
                    break;
        }
                case 2: {
                              x = { 10,9,8,7,6,5,4,3,2,1 };
                                        y = { 1,4,9,16,25,36,49,64,81,100 };
                              
                    __expected = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
                    break;
        }
                case 3: {
                              x = { 0,2,-2,4,-4,2,-2,0 };
                                        y = { 1,2,2,4,4,6,6,5 };
                              
                    __expected = { 4, 2, 0, 1, 3, 5, 6, 7 };
                    break;
        }
                case 4: {
                              x = { -76,98,83,58,-15,94,21,55,80,84,-39,-90,-46,100,-80,-49,-2,-70,36,48,88,10,55,-56,22,67,31,81,100,-39,64,-62,-7,45,-82,-24,51,-33,53,11,20,-74,-83,47,9,39,42,63,-97,94 };
                                        y = { -90,68,91,-92,-6,88,99,10,39,-69,-61,-4,71,-5,90,-51,21,-53,-21,-86,41,-9,42,-23,-4,12,94,-59,55,18,70,-88,-86,-17,-97,-33,87,80,91,-80,-79,-79,-78,-99,57,67,-52,-46,61,-10 };
                              
                    __expected = { 39, 32, 40, 31, 19, 27, 47, 46, 0, 34, 43, 3, 9, 13, 28, 1, 5, 2, 6, 14, 48, 42, 41, 49, 20, 38, 26, 37, 12, 11, 17, 10, 33, 25, 8, 30, 36, 44, 29, 23, 15, 18, 7, 22, 45, 16, 4, 35, 24, 21 };
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    x = ;
                  y = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(x, y, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> x, vector<int> y, vector<int> __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << x << ","<< y  << "]" << endl;
   
    NoRightTurnDiv2 *__instance = new NoRightTurnDiv2();
  vector<int> __result = __instance->findPath(x, y);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  __expected  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  __result  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "NoRightTurnDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
