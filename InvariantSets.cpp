#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> f;
// ------------- input end  --------------

ll solve(); struct InvariantSets {ll countSets(vector<int> _f){f = _f;return solve();}};

int N;
bool in_comp[55], vis[55];
int uf[55];
vector<int> rev[55];

void dfs(int curr, int tar) {
  if(curr == tar) {
    in_comp[tar] = 1;
  }
  else {
    if(!vis[curr]) {
      vis[curr] = 1;
      dfs(f[curr], tar);
    }
  }
}

ll count(int curr) {
  ll ret = 1;
  for(auto && e: rev[curr]) {
    ret *= 1 + count(e);
  }
  return ret;
}

ll solve() {
  N = f.size();
  zero(in_comp);
  rep(i, N) {
    zero(vis);
    dfs(f[i], i);
    uf[i] = i;
    if(in_comp[i]) {
      rep(j, N) {
        if(vis[j]) {
          minimize(uf[i], j);
        }
      }
    }
    rev[i].clear();
  }

  rep(i, N) {
    if(!in_comp[i]) {
      rev[uf[f[i]]].push_back(i);
    }
  }

  ll res = 1;
  rep(i, N) {
    if(in_comp[i] && uf[i] == i) {
      res *= 1 + count(i);
    }
  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> f;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              f = { 1,0,0,0 };
                              
                    __expected = 5LL;
                    break;
        }
                case 1: {
                              f = { 1,2,0 };
                              
                    __expected = 2LL;
                    break;
        }
                case 2: {
                              f = { 0,0,1,2 };
                              
                    __expected = 5LL;
                    break;
        }
                case 3: {
                              f = { 0,1,2,3,4,5 };
                              
                    __expected = 64LL;
                    break;
        }
                case 4: {
                              f = { 12,10,0,4,0,6,3,8,9,14,1,5,6,12,5 };
                              
                    __expected = 90LL;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    f = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(f, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> f, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << f  << "]" << endl;
   
    InvariantSets *__instance = new InvariantSets();
  ll __result = __instance->countSets(f);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "InvariantSets: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
