#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> sweets;
// ------------- input end  --------------
string solve(); struct EllysCandyGame {string getWinner(vector<int> _sweets){sweets = _sweets;return solve();}};

int N;

int dfs(vector<int>& v, int kris = 0, int elly = 0, bool krisTurn = 0) {  // ellyから

  int ret = krisTurn ? +1 : -1; // wasureruna~~ initial value is worst case.
  bool ok = 0;

  rep(i, N) {
    if(v[i] == 0) { continue; }
    ok = 1;
    if(i-1 >= 0) v[i-1] *= 2;
    if(i+1 < N)  v[i+1] *= 2;
    int t = v[i]; v[i] = 0;
    if(krisTurn)  minimize(ret, dfs(v, kris + t, elly, !krisTurn));
    else          maximize(ret, dfs(v, kris, elly + t, !krisTurn));
    if(i-1 >= 0) v[i-1] /= 2;
    if(i+1 < N)  v[i+1] /= 2;
    v[i] = t;
  }

  if(!ok) {
    if(kris > elly) { return -1; }
    if(kris < elly) { return +1; }
    return 0;
  }
  return ret;
}

string solve() {

  N = sweets.size();

  int r = dfs(sweets);
  if(r == -1) { return "Kris"; }
  if(r ==  0) { return "Draw"; }
  if(r == +1) { return "Elly"; }
  assert(false);
}

// CUT begin
using namespace std;
    char do_test(vector<int>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> sweets;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              sweets = { 20,50,70,0,30 };
                              
                    __expected = "Kris";
                    break;
        }
                case 1: {
                              sweets = { 42,13,7 };
                              
                    __expected = "Elly";
                    break;
        }
                case 2: {
                              sweets = { 10,20 };
                              
                    __expected = "Draw";
                    break;
        }
                case 3: {
                              sweets = { 3,1,7,11,1,1 };
                              
                    __expected = "Kris";
                    break;
        }
                case 4: {
                              sweets = { 41,449,328,474,150,501,467,329,536,440 };
                              
                    __expected = "Kris";
                    break;
        }
                case 5: {
                              sweets = { 177,131,142,171,411,391,17,222,100,298 };
                              
                    __expected = "Elly";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    sweets = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(sweets, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> sweets, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << sweets  << "]" << endl;
   
    EllysCandyGame *__instance = new EllysCandyGame();
  string __result = __instance->getWinner(sweets);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "EllysCandyGame: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
