#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct ArcadeManao {int shortestLadder(vector <string> level, int coinRow, int coinColumn);};

int N, M;
struct node {
	int pos, cost;
	node(int p, int c): pos(p), cost(c) {}
	bool operator < (node const& r) const {
		return cost > r.cost;
	}
};
vector<node> graph[2555];

int num(int x, int y) {
	return x + y * M;
}

pair<int, int> toXY(int num) {
	return {num % M, num / M};
}

int ArcadeManao::shortestLadder(vector <string> level, int coinRow, int coinColumn) {

//	rep(i, 2555) graph[i].clear();

	N = level.size();
	M = level[0].size();

	coinColumn--;
	coinRow--;

	rep(i, N) rep(j, M) {
		if(j + 1 < M && level[i][j] == 'X' && level[i][j+1] == 'X') {
			graph[num(j, i)].emplace_back(num(j+1, i), 0);
			graph[num(j+1, i)].emplace_back(num(j, i), 0);
		}
	}
	rep(i, N) rep(j, M) {
		if(level[i][j] == 'X') {
			for(int k=i-1; k>=0; k--) {
				if(level[k][j] == 'X') {
					graph[num(j, i)].emplace_back(num(j, k), i-k);
					break;
				}
			}
			for(int k=i+1; k<N; k++) {
				if(level[k][j] == 'X') {
					graph[num(j, i)].emplace_back(num(j, k), k-i);
					break;
				}
			}
		}
	}

	priority_queue<node> pq;
	pq.emplace(num(0, N-1), 0);

	static int dp[2555];
	rep(i, 2555) dp[i] = inf;
	dp[num(0, N-1)] = 0;

	while(!pq.empty()) {
		auto p = pq.top(); pq.pop();
		int num = p.pos;
		int x = toXY(num).first;
		int y = toXY(num).second;
		int cost = p.cost;

		if(x == coinColumn && y == coinRow) {
			return cost;
		}

		rep(i, graph[num].size()) {
			int nextnum = graph[num][i].pos;
			int nextcost = max(cost, graph[num][i].cost);
			if(dp[nextnum] <= nextcost) {
				continue;
			}
			dp[nextnum] = nextcost;
			pq.emplace(nextnum, nextcost);
		}
	}

	assert(false);
}
