#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int R;
int T;
int Vrat;
int Vcat;
// ------------- input end  --------------
double solve(); struct CatAndRat {double getTime(int _R, int _T, int _Vrat, int _Vcat){R = _R, T = _T, Vrat = _Vrat, Vcat = _Vcat;return solve();}};

double solve() {

  if(Vrat >= Vcat) { return -1.0; }
  double len = 2 * R * M_PI;
  double t = T * Vrat;
  if(t > len / 2) {
    t = len / 2;
  }

  return t / abs(Vrat - Vcat);
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int R;
            int T;
            int Vrat;
            int Vcat;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              R = 10;
                                        T = 1;
                                        Vrat = 1;
                                        Vcat = 1;
                              
                    __expected = -1.0;
                    break;
        }
                case 1: {
                              R = 10;
                                        T = 1;
                                        Vrat = 1;
                                        Vcat = 2;
                              
                    __expected = 1.0;
                    break;
        }
                case 2: {
                              R = 10;
                                        T = 1;
                                        Vrat = 2;
                                        Vcat = 1;
                              
                    __expected = -1.0;
                    break;
        }
                case 3: {
                              R = 1000;
                                        T = 1000;
                                        Vrat = 1;
                                        Vcat = 1000;
                              
                    __expected = 1.001001001001001;
                    break;
        }
                case 4: {
                              R = 1;
                                        T = 1000;
                                        Vrat = 1;
                                        Vcat = 2;
                              
                    __expected = 3.141592653589793;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    R = ;
                  T = ;
                  Vrat = ;
                  Vcat = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(R, T, Vrat, Vcat, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int R, int T, int Vrat, int Vcat, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(R) << ","<< pretty_print(T) << ","<< pretty_print(Vrat) << ","<< pretty_print(Vcat)  << "]" << endl;
   
    CatAndRat *__instance = new CatAndRat();
  double __result = __instance->getTime(R, T, Vrat, Vcat);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "CatAndRat: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
