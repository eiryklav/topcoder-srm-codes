#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct DrawingPointsDivTwo {int maxSteps(vector <string> points);};

bool A[50][100][100], B[50][100][100];

bool check(int tar) {
	memcpy(B[tar], A[tar], sizeof A[tar]);

	for(int k=tar-1; k>=0; k--) {
		zero(B[k]);
		REP(i, 1, 100) {
			REP(j, 1, 100) {
				B[k][i][j] =
					 B[k+1][i-1][j-1]
				&& B[k+1][i-1][j]
				&& B[k+1][i][j-1]
				&& B[k+1][i][j];
			}
		}
	}

	rep(i, 100) rep(j, 100) {
		if(A[0][i][j] ^ B[0][i][j]) { return false; }
	}

	return true;
}

int DrawingPointsDivTwo::maxSteps(vector <string> points) {
  zero(A[0]);

  rep(i, points.size()) {
  	rep(j, points[0].size()) {
  		A[0][i+50][j+50] = (points[i][j] == '*');
  	}
  }

  REP(k, 1, 50) {
		zero(A[k]);
		rep(i, 100) rep(j, 100) {
			if(A[k-1][i][j]) {
				A[k][i-1][j-1] = A[k][i-1][j] = A[k][i][j-1] = A[k][i][j] = 1;
			}
		}
  }

  if(check(49)) { return -1; }

  for(int i=48; i>=0; i--) {
  	if(check(i)) { return i; }
  }
  assert(false);
}