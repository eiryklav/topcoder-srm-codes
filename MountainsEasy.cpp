#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

int const MOD9 = 1e9+9;

struct MountainsEasy {int countPlacements(vector <string> picture, int N);};

//int cmemo[51][2501][51];
int const MaxComb = 2501;
vector<vector<ll>> comb(MaxComb, vector<ll>(MaxComb));

int cmemo[55][55];
int xcnt;

int calc(int a, int k) {
	if(a < 0 || k < 0) { return 0; }
	if(a == 0 && k == 0) { return 1; }
	int& ret = cmemo[a][k];
	if(ret >= 0) return ret;
	return ret = ((ll)a * calc(a-1, k-1) % MOD9 + (ll)(xcnt - a) * calc(a, k-1) % MOD9) % MOD9;
}

int MountainsEasy::countPlacements(vector <string> picture, int N) {
	rep(i, MaxComb) comb[i][0] = comb[i][i] = 1;
	REP(i, 1, MaxComb) REP(j, 1, i) {
		if(j)	comb[i][j] = comb[i-1][j-1];
		(comb[i][j] += comb[i-1][j]) %= MOD9;
	}

	int H = picture.size();
	int W = picture[0].size();
	vector<pair<int, int>> peaks;
	rep(i, H) {
		rep(j, W) {
			if((!i || picture[i-1][j] == '.') &&
				picture[i][j] == 'X' &&
				(!i || (
					(!j || picture[i-1][j-1] == '.') &&
					(j == W-1 || picture[i-1][j+1] == '.')
					)
				)) {
				peaks.emplace_back(j, i);
			}
		}
	}

	int PSize = peaks.size();

	xcnt = 0;
	rep(i, H) rep(j, W) {
		if(picture[i][j] == 'X') {
			xcnt ++;
		}
	}
	minus(cmemo);
	return calc(PSize, N);
}
