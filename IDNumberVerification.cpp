#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string id;
vector<string> regionCodes;
// ------------- input end  --------------
string solve(); struct IDNumberVerification { string verify(string _id, vector<string> _regionCodes) { id = _id, regionCodes = _regionCodes; return solve(); }};

// A: It is a multiple of 4, but not a multiple of 100.
// B: It is a multiple of 400. Therefore, 1904 and 2000 are leap years, while 1900 and 2011 are not.

bool isLeap(int year) {
  if(year % 4 == 0 && year % 100 > 0) return true;
  return year % 400 == 0;
}

bool validateRegionCode() {
  bool valid = 0;
  rep(i, regionCodes.size()) {
    bool ok = 1;
    rep(j, regionCodes[i].size()) {
      if(id[j] != regionCodes[i][j]) { ok = 0; break; }
    }
    if(ok) {
      valid = 1; break;
    }
  }
  return valid;
}

bool validateDate() {
  int days[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

  auto date = string(id.begin() + 6, id.begin() + 14);
  auto year = string(date.begin(), date.begin() + 4); int y = stoi(year);
  auto month = string(date.begin() + 4, date.begin() + 6); int m = stoi(month);
  auto day = string(date.begin() + 6, date.begin() + 8); int d = stoi(day);

  cout << y << ' ' << m << ' ' << d << endl;

  if(1<=m&&m<=12);else{ return false; }
  if(1<=d&&d<=31);else{ return false; }
  if(d<=days[m-1]);else{ return false; }
  if(!isLeap(y) && m == 2 && d == 29){ return false; }
  if(y<1900 || y>2011){ return false; }
  return true;
}

bool validateSex() {
  auto sex = string(id.end() - 4, id.end() - 1); int s = stoi(sex);
  if(s == 0) { return false; }
  return true;
}

string sex() {
  auto sex = string(id.end() - 4, id.end() - 1); int s = stoi(sex);
  return s % 2 ? "Male" : "Female";
}

bool validateCheckSum() {
  int x = (*(id.end()-1)) == 'X' ? 10 : (*(id.end()-1))-'0';
  rep(i, id.size()-1) {
    (x += (id[i]-'0') * pow(2, 17-i)) %= 11;
  }
  return x == 1;
}

string solve() {
  if(!validateRegionCode() ||
     !validateDate() ||
     !validateSex() ||
     !validateCheckSum()
  ) { return "Invalid"; }
  return sex();
}
