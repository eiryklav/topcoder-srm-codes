#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int h;
vector<int> a;
vector<int> b;
// ------------- input end  --------------
string solve(); struct TheKingsRoadsDiv2 {string getAnswer(int _h, vector<int> _a, vector<int> _b){h = _h, a = _a, b = _b;return ::solve();}};

tuple<int, int, int> unuse;
vector<vector<pair<int, int>>> g(1111);

int deg[4];
int ecount;
int N;

int dfs(int curr, int par) {

  int d = 0;
  int cnum = -1;

  for(auto && e: g[curr]) {
    int tar, id; tie(tar, id) = e;
    if(unuse == make_tuple(curr, tar, id) || unuse == make_tuple(tar, curr, id)) { continue; }
    d ++; if(d > 3) { return -1; }
    if(tar == par) { continue; }
    if(ecount++ > N-1) { return -1; }
    int r = dfs(tar, curr);
    if(r == -1) { return -1; }
    if(cnum == -1) { cnum = r; }
    else if(cnum != r) { return -1; }
  }

  deg[d] ++;

  return d;
}

string solve() {

  N = a.size();

  rep(i, 1111) g[i].clear();

  rep(i, N) {
    g[a[i]-1].emplace_back(b[i]-1, i);
    g[b[i]-1].emplace_back(a[i]-1, i);
  }

  rep(i, N) {

    unuse = make_tuple(a[i]-1, b[i]-1, i);
    ecount = 0;

    int root = -1;
    rep(r, N) {
      int rccnt = 0;
      for(auto && e: g[r]) {
        int f, t, id; tie(f, t, id) = unuse;
        if(r == f && e.first == t && e.second == id) { continue; }
        if(e.first == f && r == t && e.second == id) { continue; }
        rccnt ++;
      }
      if(rccnt == 2 && root == -1) { root = r; }
      else if(rccnt == 2 && root != r) { root = -1; break; }
    }
    if(root == -1) { continue; }

    zero(deg);
    if(dfs(root, -1) == -1) { continue; }
    if(deg[1] == 1 << (h-1) &&
      deg[3] == (1 << h) - deg[1] - 2) {
      return "Correct";
    }
  }

  return "Incorrect";
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int h;
            vector<int> a;
            vector<int> b;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              h = 3;
                                        a = { 1,2,3,7,1,5,4 };
                                        b = { 6,7,4,3,3,1,7 };
                              
                    __expected = "Correct";
                    break;
        }
                case 1: {
                              h = 2;
                                        a = { 1,2,3 };
                                        b = { 2,1,3 };
                              
                    __expected = "Incorrect";
                    break;
        }
                case 2: {
                              h = 3;
                                        a = { 7,1,1,2,2,3,1 };
                                        b = { 7,1,7,4,5,2,6 };
                              
                    __expected = "Incorrect";
                    break;
        }
                case 3: {
                              h = 2;
                                        a = { 1,3,3 };
                                        b = { 2,1,2 };
                              
                    __expected = "Correct";
                    break;
        }
                case 4: {
                              h = 3;
                                        a = { 6,5,3,3,5,5,6 };
                                        b = { 1,5,5,6,4,7,2 };
                              
                    __expected = "Correct";
                    break;
        }
                case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                  h = 2;
                  a = {2, 2, 2};
                  b ={1, 1, 3};
              
          __unknownAnswer = true; 

          case 6: {
            h = 3;
            a = {4, 7, 7, 1, 1, 1, 4};
            b = {6, 5, 1, 7, 4, 3, 2};
          }
          break;
        }
    
    default: return 'm';
  }
  return do_test(h, a, b, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int h, vector<int> a, vector<int> b, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(h) << ","<< a << ","<< b  << "]" << endl;
   
    TheKingsRoadsDiv2 *__instance = new TheKingsRoadsDiv2();
  string __result = __instance->getAnswer(h, a, b);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TheKingsRoadsDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
