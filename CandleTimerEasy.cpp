#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> A;
vector<int> B;
vector<int> len;
// ------------- input end  --------------
int solve(); struct CandleTimerEasy {int differentTime(vector<int> _A, vector<int> _B, vector<int> _len){A = _A, B = _B, len = _len;return ::solve();}};

int G[22][22];

int solve() {

  rep(i, 22) rep(j, 22) G[i][j] = i == j ? 0 : inf;

  int N = A.size() + 1;

  vector<int> deg(N);
  vector<int> leaves;
  rep(i, N-1) {
    G[A[i]][B[i]] = G[B[i]][A[i]] = len[i];
    deg[A[i]] ++, deg[B[i]] ++;
  }

  rep(i, N) {
    if(deg[i] == 1) {
      leaves.push_back(i);
    }
  }

//  cout << "leaves: "; for(auto && e: leaves) { cout << e << " "; }cout << endl;

  rep(k, N) rep(i, N) rep(j, N) {
    if(G[i][k] == inf || G[k][j] == inf) { continue; }
    minimize(G[i][j], G[i][k] + G[k][j]);
  }

  set<double> st;

  REP(S, 1, 1<<(int)leaves.size()) {
    double cand = 0.0;
    rep(i, N-1) {
      int a = A[i], b = B[i], l = len[i];
      int speedA = inf, speedB = inf;
      bool same = 0;

      rep(u, leaves.size()) {
        if(!(S >> u & 1)) { continue; }
        int v = leaves[u];
        if(speedA > G[v][a]) {
          speedA = G[v][a];
        }
        if(speedB > G[v][b]) {
          speedB = G[v][b];
        }
        
        if(speedA == G[v][a] && speedB == G[v][b]) {
          same = 1;
        }
        else {
          same = 0;
        }
      }

      if(same) {
        maximize(cand, (double)max(speedA, speedB));
      }
      else {
        int diff = abs(speedA - speedB);
        maximize(cand, (double)(max(speedA, speedB) + (l - diff) / 2.0));
      }
    }
    st.insert(cand);
  }

//  cout << "times: "; for(auto && e: st) { printf("%.5f ", e);}puts("");

  return st.size();
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> A;
            vector<int> B;
            vector<int> len;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = { 0,1 };
                                        B = { 1,2 };
                                        len = { 10,1 };
                              
                    __expected = 2;
                    break;
        }
                case 1: {
                              A = { 0,0,0 };
                                        B = { 1,2,3 };
                                        len = { 1,1,1 };
                              
                    __expected = 2;
                    break;
        }
                case 2: {
                              A = { 0,0,0 };
                                        B = { 1,2,3 };
                                        len = { 1,2,3 };
                              
                    __expected = 4;
                    break;
        }
                case 3: {
                              A = { 0,1,1,2,3,3,2,4 };
                                        B = { 1,2,3,4,5,6,7,8 };
                                        len = { 5,3,2,4,6,8,7,1 };
                              
                    __expected = 7;
                    break;
        }
                case 4: {
                              A = { 0,0,0,0 };
                                        B = { 1,2,3,4 };
                                        len = { 123,456,789,1000 };
                              
                    __expected = 8;
                    break;
        }
                case 5: {
                              A = { 0 };
                                        B = { 1 };
                                        len = { 1000 };
                              
                    __expected = 2;
                    break;
        }
                case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                    B = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
                    len = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
                    __expected = 2;
          break;
        }
    
    default: return 'm';
  }
  return do_test(A, B, len, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> A, vector<int> B, vector<int> len, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << A << ","<< B << ","<< len  << "]" << endl;
   
    CandleTimerEasy *__instance = new CandleTimerEasy();
  int __result = __instance->differentTime(A, B, len);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "CandleTimerEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
