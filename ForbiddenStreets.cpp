#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
vector<int> A;
vector<int> B;
vector<int> times;
// ------------- input end  --------------
vector<int> solve(); struct ForbiddenStreets {vector<int> find(int _N, vector<int> _A, vector<int> _B, vector<int> _time){N = _N, A = _A, B = _B, times = _time;return ::solve();}};

vector<pair<int, int>> G[101];
int dp[101][101];

constexpr array<int, 3> mods = {{1000000007, 1000000009, 1000000021}};
ll min_pattern[mods.size()][101][101];

vector<int> solve() {

  rep(i, 101) G[i].clear();

  rep(i, A.size()) {
    G[A[i]].emplace_back(B[i], times[i]);
    G[B[i]].emplace_back(A[i], times[i]);
  }

  // 各頂点からダイクストラをして、最小経路についてその場合の数を数え上げる。
  rep(i, 101) rep(j, 101) dp[i][j] = i == j ? 0 : inf;
  zero(min_pattern);

  priority_queue<tuple<int, int, int>> pq;  // cost, start position, current position

  rep(i, N) {
    pq.emplace(0, i, i);
    rep(m, mods.size()) {
      min_pattern[m][i][i] = 1;
    }
  }

  while(!pq.empty()) {
    int cost, start, curr; tie(cost, start, curr) = pq.top(); pq.pop();
    cost *= -1;
    if(dp[start][curr] < cost) { continue; }
    for(auto && e: G[curr]) {
      int next, weight; tie(next, weight) = e;
      if(dp[start][next] > cost + weight) {
        dp[start][next] = cost + weight;
        pq.emplace(-cost-weight, start, next);
        rep(i, mods.size()) min_pattern[i][start][next] = min_pattern[i][start][curr];
      }
      else if(dp[start][next] == cost + weight) {
        rep(i, mods.size()) min_pattern[i][start][next] += min_pattern[i][start][curr], min_pattern[i][start][next] %= mods[i];
      }
    }
  }

  vector<int> ret(A.size());

  /*
    辺e = {a, b} がsrc->destの最短経路上に無くてはならない辺である
    <=> src->destの最短距離 = src->aの最短距離 + 辺a<->bの重み + b->destの最短距離
        かつ src->dest の最短経路パターン数 = src->a の最短経路パターン数 * b->dest の最短経路パターン数
    最短経路が巨大になるのでMODをとるが、恣意的なテストケースにやられないように、複数の巨大なMOD値を選ぶ。
   */
  rep(i, A.size()) {
    rep(src, 101) rep(dest, 101) {
      // 片側からの移動制限をすることで、非順序対を数え上げることになる。
      if(dp[src][dest] != dp[src][A[i]] + times[i] + dp[B[i]][dest]) { continue; }
      bool ok = 1;
      rep(m, mods.size()) {
        ok &= min_pattern[m][src][dest] == (ll)min_pattern[m][src][A[i]] * min_pattern[m][B[i]][dest] % mods[m];
      }
      if(ok) {
        ret[i] ++;
      }
    }
  }

  return ret;
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,vector<int>,vector<int>,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            vector<int> A;
            vector<int> B;
            vector<int> time;
            vector<int> __expected = vector<int>();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 3;
                                        A = { 0,1 };
                                        B = { 1,2 };
                                        time = { 1,1 };
                              
                    __expected = { 2, 2 };
                    break;
        }
                case 1: {
                              N = 4;
                                        A = { 0,1,2 };
                                        B = { 1,2,3 };
                                        time = { 2,4,6 };
                              
                    __expected = { 3, 4, 3 };
                    break;
        }
                case 2: {
                              N = 5;
                                        A = { 0,0,0,0,1,1,1,2,2,3 };
                                        B = { 1,2,3,4,2,3,4,3,4,4 };
                                        time = { 1,2,3,4,5,6,7,8,9,10 };
                              
                    __expected = { 4, 4, 4, 4, 0, 0, 0, 0, 0, 0 };
                    break;
        }
                case 3: {
                              N = 5;
                                        A = { 0,0,0,0,1,1,1,2,2,3 };
                                        B = { 1,2,3,4,2,3,4,3,4,4 };
                                        time = { 1,2,1,2,1,2,1,2,1,2 };
                              
                    __expected = { 1, 0, 1, 0, 1, 0, 1, 1, 1, 1 };
                    break;
        }
                case 4: {
                              N = 3;
                                        A = { 0,1,2 };
                                        B = { 1,2,0 };
                                        time = { 1,2,3 };
                              
                    __expected = { 1, 1, 0 };
                    break;
        }
                case 5: {
                              N = 13;
                                        A = { 12,12,12,12,6,6,0,6,9,1,6,6,9,11,4,0,1,4,11,2,4,0,0,10,12,3,9,7,7,2,9,12,1,10,6,9,0,3,1,1,7,3,2,2 };
                                        B = { 9,1,6,7,0,8,11,10,5,4,3,2,11,3,5,4,11,2,6,10,11,12,2,7,2,7,0,4,5,11,8,3,10,12,4,2,5,4,5,2,8,10,3,7 };
                                        time = { 764,541,173,212,568,783,863,68,930,23,70,394,230,920,199,371,92,874,997,328,314,896,815,751,789,652,933,369,227,602,318,281,445,730,98,928,498,20,341,724,847,351,35,492 };
                              
                    __expected = { 0, 0, 12, 4, 0, 0, 0, 12, 0, 35, 26, 0, 21, 0, 9, 10, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 3, 0, 11, 0, 0, 0, 0, 0, 2, 36, 0, 0, 1, 0, 12, 0 };
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  A = ;
                  B = ;
                  time = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, A, B, time, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, vector<int> A, vector<int> B, vector<int> time, vector<int> __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< A << ","<< B << ","<< time  << "]" << endl;
   
    ForbiddenStreets *__instance = new ForbiddenStreets();
  vector<int> __result = __instance->find(N, A, B, time);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  __expected  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  __result  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ForbiddenStreets: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
