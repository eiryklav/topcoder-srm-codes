#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> board;
// ------------- input end  --------------
int solve(); struct TheTilesDivTwo {int find(vector<string> _board){board = _board;return ::solve();}};

int dp[5][50][(1<<4) + 5];
int N, M;

constexpr bool isblack(int i, int j) {
	return (i + j) % 2 == 0;
}

inline bool isnone(int i, int j) {
	return board[i][j] == '.';
}

constexpr bool isnone_mask(int i, int mask) {
	return !(mask >> i & 1);
}

int dfs(int i, int j, int mask) {

	if(j >= M) { return 0; }

	auto& ret = dp[i][j][mask];

	if(ret + 1) { return ret; }

	if(i + 1 < N) {

		ret = dfs(i + 1, j, (mask & ~(1 << i)));

		int ni = i + 2 < N ? i + 2 : 0;
		int nj = i + 2 < N ? j : j + 1;

		if(isblack(i + 1, j) &&
			isnone(i, j) &&
			isnone(i + 1, j + 1) &&
			isnone(i + 1, j) &&

			isnone_mask(i, mask) &&
			isnone_mask(i + 1, mask)) {

			int nmask = (mask & ~(1 << i)) | (1 << (i + 1));
			maximize(ret, 1 + dfs(ni, nj, nmask));
		}

		if(isblack(i, j) &&
			isnone(i, j) &&
			isnone(i, j + 1) &&
			isnone(i + 1, j) &&

			isnone_mask(i, mask) &&
			isnone_mask(i + 1, mask)) {

			int nmask = (mask & ~(1 << (i + 1))) | (1 << i);
			maximize(ret, 1 + dfs(ni, nj, nmask));
		}

		if(isblack(i, j + 1) &&
			isnone(i, j) &&
			isnone(i, j + 1) &&
			isnone(i + 1, j + 1) &&

			isnone_mask(i, mask)) {

			int nmask = (mask | (1 << i)) | (1 << (i + 1));
			maximize(ret, 1 + dfs(ni, nj, nmask));
		}

		if(isblack(i + 1, j + 1) &&
			isnone(i, j + 1) &&
			isnone(i + 1, j + 1) &&
			isnone(i + 1, j) &&

			isnone_mask(i + 1, mask)) {

			int nmask = (mask | (1 << i)) | (1 << (i + 1));
			maximize(ret, 1 + dfs(ni, nj, nmask));
		}

	} else {
		ret = dfs(0, j + 1, mask & ~(1 << i));
	}

	return ret;
}

int solve() {
	N = board.size(), M = board[0].size();
	minus(dp);
  return dfs(0, 0, 0);
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> board;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              board = { "X.X","...","X.X" };
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              board = { "...","...","..." };
                              
                    __expected = 2;
                    break;
        }
                case 2: {
                              board = { "......X.X.XXX.X.XX." };
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              board = { "X.....XXX.XX..XXXXXXXXX...X.XX.XX....X",".XXXX..X..XXXXXXXX....XX.X.X.X.....XXX","....XX....X.XX..X.X...XX.X..XXXXXXX..X","XX.XXXXX.X.X..X..XX.XXX..XX...XXX.X..." };
                              
                    __expected = 13;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    board = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(board, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> board, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << board  << "]" << endl;
   
    TheTilesDivTwo *__instance = new TheTilesDivTwo();
  int __result = __instance->find(board);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TheTilesDivTwo: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
