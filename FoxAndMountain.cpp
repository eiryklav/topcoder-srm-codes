#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int n;
string history;
// ------------- input end  --------------
int solve(); struct FoxAndMountain {int count(int _n, string _history){n = _n, history = _history;return ::solve();}};

int trans[55][2];
int dp[55][55][33];

int const MOD9 = 1e9+9;

int solve() {

  int N = history.size();

  zero(trans);

  rep(i, N) rep(up, 2) {
    if(history[i] != "DU"[up]) { continue; }
    trans[i][up] = i + 1;
    int maxIdx = -1;
    REP(backto, 1, i+1) {
      if(history.substr(0, backto) == (history.substr(0, i) + "DU"[up^1]).substr(i-backto+1)) {
        maximize(maxIdx, backto);
      }
    }
    if(maxIdx + 1) {
      trans[i][up^1] = maxIdx;
    }
  }

  trans[N][0] = trans[N][1] = N;

  zero(dp);
  dp[0][0][0] = 1;
  rep(i, n) rep(j, N+1) rep(k, 30) {
    if(!dp[i][j][k]) { continue; }
    if(k > 0) {
      dp[i+1][trans[j][0]][k-1] += dp[i][j][k];
      dp[i+1][trans[j][0]][k-1] %= MOD9;
    }
    if(k < 25) {
      dp[i+1][trans[j][1]][k+1] += dp[i][j][k];
      dp[i+1][trans[j][1]][k+1] %= MOD9;
    }
  }

  return dp[n][N][0];
}

// CUT begin
using namespace std;
    char do_test(int,string,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int n;
            string history;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 4;
                                        history = "UUDD";
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              n = 4;
                                        history = "DUUD";
                              
                    __expected = 0;
                    break;
        }
                case 2: {
                              n = 4;
                                        history = "UU";
                              
                    __expected = 1;
                    break;
        }
                case 3: {
                              n = 49;
                                        history = "U";
                              
                    __expected = 0;
                    break;
        }
                case 4: {
                              n = 20;
                                        history = "UUUDUUU";
                              
                    __expected = 990;
                    break;
        }
                case 5: {
                              n = 30;
                                        history = "DUDUDUDUDUDUDUDU";
                              
                    __expected = 3718;
                    break;
        }
                case 6: {
                              n = 50;
                                        history = "U";
                              
                    __expected = 946357703;
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
                  history = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, history, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int n, string history, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n) << ","<< pretty_print(history)  << "]" << endl;
   
    FoxAndMountain *__instance = new FoxAndMountain();
  int __result = __instance->count(n, history);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FoxAndMountain: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
