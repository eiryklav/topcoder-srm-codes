#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct CuttingBitString {int getmin(string S);};

vector<string> five_facts;

int CuttingBitString::getmin(string S) {
  int res = 0;

  ll five = 1;
  rep(_, 27) {
  	auto x = five;
  	string s;
  	while(x > 0) {
  		s += (x % 2) + '0';
  		x /= 2;
  	}
  	reverse(all(s));
  	five_facts.push_back(s);
  	five *= 5;
  }

  int N = S.size();

  // i文字目までの文字列で、分割が最小
  // 答え: dp[N]

  int inf = 1<<29;
  int dp[N+1];
  rep(i, N+1) dp[i] = inf;
  dp[0] = 0;

  REP(i, 1, N+1) {
  	rep(j, i) {
			if(dp[j] == inf) { continue; }
  		auto substr = S.substr(j, i-j);
//  		cout << ":" << substr << endl;
  		if(find(all(five_facts), substr) != five_facts.end()) {
//  			cout << "ok\n";
	  		dp[i] = min(dp[i], dp[j] + 1);
  		}
  	}
  }

  res = dp[N] == inf ? -1 : dp[N];

  return res;
}
