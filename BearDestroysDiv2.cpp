#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct BearDestroysDiv2 {int sumUp(int W, int H, int MOD);};

int BearDestroysDiv2::sumUp(int W, int H, int MOD) {
  int res = 0;
  return res;
}

// BEGIN CUT HERE
#include <cstdio>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
namespace moj_harness {
	using std::string;
	using std::vector;
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				std::cerr << "Illegal input! Test case " << casenum << " does not exist." << std::endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			std::cerr << "No test cases run." << std::endl;
		} else if (correct < total) {
			std::cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << std::endl;
		} else {
			std::cerr << "All " << total << " tests passed!" << std::endl;
		}
	}
	
	int verify_case(int casenum, const int &expected, const int &received, std::clock_t elapsed) { 
		std::cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			std::sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		std::cerr << verdict;
		if (!info.empty()) {
			std::cerr << " (";
			for (size_t i=0; i<info.size(); ++i) {
				if (i > 0) std::cerr << ", ";
				std::cerr << info[i];
			}
			std::cerr << ")";
		}
		std::cerr << std::endl;
		
		if (verdict == "FAILED") {
			std::cerr << "    Expected: " << expected << std::endl; 
			std::cerr << "    Received: " << received << std::endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum__) {
		switch (casenum__) {
		case 0: {
			int W                     = 4;
			int H                     = 3;
			int MOD                   = 999999937;
			int expected__            = 24064;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 1: {
			int W                     = 3;
			int H                     = 4;
			int MOD                   = 999999937;
			int expected__            = 24576;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 2: {
			int W                     = 2;
			int H                     = 20;
			int MOD                   = 584794877;
			int expected__            = 190795689;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 3: {
			int W                     = 5;
			int H                     = 10;
			int MOD                   = 3;
			int expected__            = 2;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 4: {
			int W                     = 1;
			int H                     = 1;
			int MOD                   = 19;
			int expected__            = 0;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 5: {
			int W                     = 7;
			int H                     = 40;
			int MOD                   = 312880987;
			int expected__            = 256117818;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 6: {
			int W                     = ;
			int H                     = ;
			int MOD                   = ;
			int expected__            = ;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
/*      case 7: {
			int W                     = ;
			int H                     = ;
			int MOD                   = ;
			int expected__            = ;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
/*      case 8: {
			int W                     = ;
			int H                     = ;
			int MOD                   = ;
			int expected__            = ;

			std::clock_t start__      = std::clock();
			int received__            = BearDestroysDiv2().sumUp(W, H, MOD);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}


#include <cstdlib>
int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(std::atoi(argv[i]));
	}
}
// END CUT HERE
