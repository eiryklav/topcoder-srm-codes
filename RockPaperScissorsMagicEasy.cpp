#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
using ll = long long;
vector<int> card;
int score;
int solve(); struct RockPaperScissorsMagicEasy {int count(vector<int> _card, int _score){card = _card, score = _score;return ::solve();}};
int const MOD = 1e9+7;

int const max_comb = 2020;
ll dp[max_comb][max_comb];

int solve() {
  
  rep(i, max_comb) {
    dp[i][0] = 1;
    REP(j, 1, i+1) {
      dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
      dp[i][j] %= MOD;
    }
  }

  if(card.size() < score) { return 0; }
  int lose = card.size() - score;
  int ans = dp[card.size()][score];
  rep(i, lose) {
    ans *= 2; ans %= MOD;
  }
  return ans;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> card;
            int score;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              card = { 0,1,2 };
                                        score = 2;
                              
                    __expected = 6;
                    break;
        }
                case 1: {
                              card = { 1,2 };
                                        score = 0;
                              
                    __expected = 4;
                    break;
        }
                case 2: {
                              card = { 2,2,1,0,0 };
                                        score = 10;
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              card = { 0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2 };
                                        score = 7;
                              
                    __expected = 286226628;
                    break;
        }
                case 4: {
                              card = { 0,1,2,0,1,2,2,1,0 };
                                        score = 8;
                              
                    __expected = 18;
                    break;
        }
                case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    card = {2, 2, 1, 1, 0, 2, 2, 0, 1, 1, 2, 0, 2, 2, 0, 0, 1, 1, 2, 2, 1, 2, 2, 0, 2, 2, 1, 1, 1, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 0, 2, 0, 2, 0, 1, 2, 1, 0, 1, 0, 2, 0, 0, 0, 2, 1, 0, 0, 2, 1, 2, 1, 0, 2, 0, 2, 1, 1, 0, 2, 0, 0, 0, 1, 1, 1, 2, 0, 2, 2, 2, 1, 2, 2, 1, 1, 1, 1, 0, 0, 2, 0, 1, 2, 1, 1, 2, 0, 2, 0, 1, 2, 1, 2, 1, 0, 0, 2, 2, 2, 2, 1, 1, 0, 2, 0, 2, 1, 1, 2, 2, 2, 1, 2, 1, 0, 2, 2, 2, 1, 2, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 2, 2, 0, 0, 2, 0, 2, 2, 1, 0, 0, 2, 0, 1, 0, 1, 0, 0, 2, 1, 0, 0, 1, 2, 1, 1, 1, 2, 2, 1, 0, 0, 1, 0, 1, 1, 2, 2, 0, 1, 0, 2, 0, 0, 1, 0, 2, 0, 1, 0, 0, 0, 0, 0, 2, 1, 1, 0, 0, 2, 1, 2, 0, 0, 0, 0, 0, 2, 0, 2, 2, 2, 2, 1, 2, 2, 0, 1, 1, 0, 1, 0, 1, 2, 0, 0, 0, 1, 2, 0, 0, 1, 1, 2, 1, 0, 1, 0, 0, 0, 2, 1, 1, 1, 1, 2, 1, 0, 1, 2, 0, 2, 1, 1, 0, 1, 1, 2, 1, 1, 0, 2, 2, 2, 1, 2, 1, 0, 1, 1, 1, 0, 0, 2, 0, 0, 0, 1, 2, 2, 0, 2, 2, 2, 0, 1, 0, 1, 2, 2, 0, 0, 0, 2, 2, 2, 2, 0, 1, 2, 1, 2, 2, 2, 2, 0, 1, 2, 0, 1, 2, 2, 2, 2, 0, 1, 2, 1, 1, 0, 2, 2, 0, 0, 0, 0, 2, 2, 1, 2, 1, 0, 2, 2, 0, 1, 0, 1, 1, 0, 2, 2, 2, 2, 0, 1, 2, 0, 2, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 2, 0, 0, 2, 0, 0, 1, 1, 0, 2, 2, 2, 2, 2, 0, 1, 1, 1, 0, 2, 2, 1, 1, 2, 1, 2, 0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 2, 1, 1, 1, 1, 1, 1, 0, 1, 2, 1, 2, 0, 2, 0, 2, 0, 1, 1, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0, 2, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 2, 1, 2, 1, 1, 1, 0, 1, 2, 1, 2, 0, 2, 2, 1, 1, 0, 1, 2, 1, 2, 1, 0, 1, 2, 1, 1, 2, 1, 2, 0, 0, 0, 0, 0, 2, 0, 0, 1, 2, 2, 2, 0, 2, 2, 0, 2, 1, 0, 0, 2, 2, 2, 2, 1, 2, 0, 2, 1, 0, 2, 2, 0, 0, 1, 1, 1, 0, 2, 0, 2, 1, 1, 1, 2, 2, 0, 0, 0, 2, 1, 2, 1, 1, 1, 0, 2, 1, 2, 1, 2, 2, 1, 1, 0, 0, 0, 2, 2, 2, 2, 0, 1, 2, 1, 1, 0, 2, 2, 2, 2, 0, 1, 2, 0, 0, 2, 2, 2, 0, 1, 1, 1, 2, 1, 2, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 1, 1, 0};
                    score = 329;
              
          __unknownAnswer = true; 
          break;
        }
    
    default: return 'm';
  }
  return do_test(card, score, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> card, int score, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << card << ","<< pretty_print(score)  << "]" << endl;
   
    RockPaperScissorsMagicEasy *__instance = new RockPaperScissorsMagicEasy();
  int __result = __instance->count(card, score);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "RockPaperScissorsMagicEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
