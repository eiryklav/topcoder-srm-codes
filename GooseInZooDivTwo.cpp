#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

struct GooseInZooDivTwo {int count(vector <string> field, int dist);};

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int N, M;
int comp[55][55];
int K;

int const MOD = 1e9+7;

vector<string> F;

void solve_manhattan(int y, int x, int dist) {
	static deque<pair<int, int>> q;
	q.clear();
	q.emplace_back(y, x);
	int memo[55][55]; rep(i, 55) rep(j, 55) memo[i][j] = inf;
	memo[y][x] = 0;
	while(!q.empty()) {
		auto p = q.front(); q.pop_front();
		int y = p.first, x = p.second;

		if(memo[y][x] == dist) {
			continue;
		}

		rep(i, 4) {
			int ny = y+dy[i], nx = x+dx[i];
			if(!in_range(ny, nx, N, M)) { continue; }
			if(memo[ny][nx] < memo[y][x] + 1) { continue; }
			memo[ny][nx] = memo[y][x] + 1;

			if(F[ny][nx] == 'v') {
				comp[ny][nx] = K;
				memo[ny][nx] = 0;
			}

			q.emplace_back(ny, nx);
		}
	}
}

int mod_pow(ll n, ll p) {
	ll res = 1;
	while(p > 0) {
		if(p & 1) res *= n, res %= MOD;
	 	n *= n; n %= MOD;
		p /= 2;
	}
	return res % MOD;
}

int GooseInZooDivTwo::count(vector <string> field, int dist) {
	F = field;
	N = field.size();
	M = field[0].size();
	K = 0;
	minus(comp);
	rep(i, N) {
		rep(j, M) {
			if(field[i][j] == 'v' && comp[i][j] == -1) {
				comp[i][j] = K;
				solve_manhattan(i, j, dist);
				K++;
			}
		}
	}
	return mod_pow(2, K) - 1;
}
