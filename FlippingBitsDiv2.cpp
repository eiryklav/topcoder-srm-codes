#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> vecS;
int M;
// ------------- input end  --------------
int solve(); struct FlippingBitsDiv2 { int getmin(vector<string> _S, int _M) { vecS = _S, M = _M; return solve(); }};

typedef unsigned long long ull;

int N;
string rawS;
map<string, int> memo;

int rec(string const& str) {

  if(memo.find(str) != memo.end()) { return memo[str]; }
  if(str.empty()) { return 0; }
  if(!count(str.begin(), str.end(), '0')) { return 0; }

  auto& ret = memo[str];

  int ret1 = count(str.end() - M, str.end(), '0');
  ret1 += rec(string(str.begin(), str.end()-M));

  auto T = str;
  int n = T.size();
  rep(i, n) {
    T[i] = '1'-T[i]+'0';
  }
  int ret2 = 1 + count(T.end() - M, T.end(), '0');
  ret2 += rec(string(T.begin(), T.end()-M));

  return ret = min(ret1, ret2);
}

int solve() {

  memo.clear();
  rawS.clear();
  rep(i, vecS.size()) rawS += vecS[i];
  N = rawS.size();
  int res = inf;
  rep(i, N/M) {
    auto S = rawS.substr(0, i*M);
    auto T = rawS.substr(i*M);
    reverse(all(T));
    cout << S << ", " << T << endl;
    minimize(res, rec(S) + rec(T));
  }

  return res;
}
