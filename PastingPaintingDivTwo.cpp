#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct PastingPaintingDivTwo {long long countColors(vector <string> clipboard, int T);};

long long PastingPaintingDivTwo::countColors(vector <string> clipboard, int T) {
  int N = clipboard.size();
  int M = clipboard[0].size();

  int NM = max(N, M);

  vector<string> B(2*NM+1);
  rep(i, 2*NM+1) B[i].resize(2*NM+1);

  if(T < NM) {
		rep(k, T) {
		  rep(i, N) rep(j, M) {
		  	if(clipboard[i][j] == 'B') {
		  		B[i+k][j+k] = 'B';
		  	}
	  	}
	  }
	  ll res = 0;
	  rep(i, B.size()) rep(j, B[0].size()) {
	  	res += B[i][j] == 'B';
	  }
	  return res;
  }

	rep(k, NM) {
	  rep(i, N) rep(j, M) {
	  	if(clipboard[i][j] == 'B') {
	  		B[i+k][j+k] = 'B';
	  	}
  	}
  }

  int base = 0;
  rep(i, B.size()) rep(j, B[0].size()) {
  	if(B[i][j] == 'B') {
  		base ++;
  	}
  }

  rep(i, N) rep(j, M) {
  	if(clipboard[i][j] == 'B') {
	  	B[i+NM][j+NM] = 'B';
	  }
  }

  int next = 0;
  rep(i, B.size()) rep(j, B[0].size()) {
  	if(B[i][j] == 'B') {
  		next ++;
  	}
  }

  int diff = next - base;
  return base + (ll)diff * (T - NM);
}
