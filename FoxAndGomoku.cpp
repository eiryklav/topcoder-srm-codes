#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> board;
// ------------- input end  --------------
string solve(); struct FoxAndGomoku { string win(vector<string> _board) { board = _board; return solve(); }};

int N, M;

string solve() {

  N = board.size(), M = board[0].size();

  rep(i, N) rep(j, M) {
    if(board[i][j] == 'o') {
      bool ok = 1;
      rep(k, 5) {
        if(j+k >= M) { ok = 0; break; }
        if(board[i][j+k] == '.') { ok = 0; break; }
      }
      if(ok) { return "found"; }

      ok = 1;
      rep(k, 5) {
        if(i+k >= N) { ok = 0; break; }
        if(board[i+k][j] == '.') { ok = 0; break; }
      }
      if(ok) { return "found"; }

      ok = 1;
      rep(k, 5) {
        if(j+k >= M) { ok = 0; break; }
        if(i+k >= N) { ok = 0; break; }
        if(board[i+k][j+k] == '.') { ok = 0; break; }
      }
      if(ok) { return "found"; }

      ok = 1;
      rep(k, 5) {
        if(j-k <  0) { ok = 0; break; }
        if(i+k >= N) { ok = 0; break; }
        if(board[i+k][j-k] == '.') { ok = 0; break; }
      }
      if(ok) { return "found"; }
    }
  }

  return "not found";
}
