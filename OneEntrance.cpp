#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> a;
vector<int> b;
int s;
// ------------- input end  --------------
int solve(); struct OneEntrance {int count(vector<int> _a, vector<int> _b, int _s){a = _a, b = _b, s = _s;return ::solve();}};

vector<vector<int>> g;
int N;

int solve() {

  g.clear(); g.resize(a.size() + 1);

  rep(i, a.size()) {
    g[a[i]].push_back(b[i]);
    g[b[i]].push_back(a[i]);
  }

  N = a.size() + 1;

  vector<int> ord; rep(i, N) if(s != i) ord.push_back(i);

  int ans = 0;
  do {

    auto leaf = [](vector<vector<int>>& g, int i) {
      return g[i].size() <= 1;
    };

    auto remove_leaf = [](vector<vector<int>>& g, int i) {
      int t = g[i][0];
      g[i].clear();
      rep(k, g[t].size()) {
        if(g[t][k] == i) {
          g[t].erase(g[t].begin() + k);
          break;
        }
      }
    };

    ord.push_back(s);

    auto t = g;
    bool ok = 1;
    rep(i, N) {
      if(!leaf(t, ord[i])) { ok = 0; break; }
      if(i == N-1) { break; }
      remove_leaf(t, ord[i]);
    }

    ord.erase(ord.end()-1);

    ans += ok;
  } while(next_permutation(ord.begin(), ord.end()));

  return ans;
}
// CUT begin
#include <fstream>
auto write_graph(vector<vector<int>> const& tree, string const& pngfname) -> void {

  ofstream ofs("tree.dot");
  ofs << "digraph G {\n";
  rep(i, tree.size()) {
    for(auto const& tar: tree[i]) {
      ofs << "  " << i << " -> " << tar << ";\n";
    }
  }
  ofs << "}\n";
  ofs.close();

  int ret, state;
  ret = system(("dot -Tpng tree.dot > " + pngfname + ".png").c_str());
  if(WIFEXITED(ret)){
    state = WEXITSTATUS(ret);
  }
  else{
    state = -1;
  }
  printf("STATUS = %d\n", state);
  system(("open " + pngfname + ".png").c_str());
//  exit(ret);
}

using namespace std;
    char do_test(vector<int>,vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> a;
            vector<int> b;
            int s;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              a = { 0,1,2 };
                                        b = { 1,2,3 };
                                        s = 0;
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              a = { 0,1,2 };
                                        b = { 1,2,3 };
                                        s = 2;
                              
                    __expected = 3;
                    break;
        }
                case 2: {
                              a = { 0,0,0,0 };
                                        b = { 1,2,3,4 };
                                        s = 0;
                              
                    __expected = 24;
                    break;
        }
                case 3: {
                              a = { 7,4,1,0,1,1,6,0 };
                                        b = { 6,6,2,5,0,3,8,4 };
                                        s = 4;
                              
                    __expected = 896;
                    break;
        }
                case 4: {
                              a = {  };
                                        b = {  };
                                        s = 0;
                              
                    __expected = 1;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    a = ;
                  b = ;
                  s = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(a, b, s, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> a, vector<int> b, int s, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << a << ","<< b << ","<< pretty_print(s)  << "]" << endl;
   
    OneEntrance *__instance = new OneEntrance();
  int __result = __instance->count(a, b, s);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "OneEntrance: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
