#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <cstring>
#include <numeric>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> kind;
vector<int> found;
int K;
// ------------- input end  --------------
ll solve(); struct Excavations2 { ll count(vector<int> _kind, vector<int> _found, int _K) { kind = _kind, found = _found, K = _K; return solve(); }};

map<int, int> kindMap;

ll comb[1010][1010];
void make_comb(int n) {
  rep(i, n+1) {
    comb[i][0] = 1;
    REP(j, 1, i+1) {
      comb[i][j] = comb[i-1][j-1] + comb[i-1][j];
    }
  }
}

ll solve() {

  make_comb(100);

  kindMap.clear();
  rep(i, kind.size()) {
    kindMap[kind[i]]++;
  }

  static ll dp[2][51]; zero(dp);
  dp[0][K] = 1;
  int f = 0;

  for(auto type: found) {
    int const MaxUse = kindMap[type];
    zero(dp[f^1]);
    auto curr = dp[f], next = dp[f^1];
    // type を 1個以上 kindMap[type]以下 用いる
    rep(remain, K+1) {
      if(!curr[remain]) { continue; }
      REP(useCnt, 1, MaxUse + 1) {
        if(remain - useCnt < 0) { continue; }
        next[remain - useCnt] += curr[remain] * comb[MaxUse][useCnt];
      }
    }
    f ^= 1;
  }

  return dp[f][0];
}
