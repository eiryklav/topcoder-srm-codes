#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> values;
// ------------- input end  --------------
string solve(); struct ColorfulCoinsEasy {string isPossible(vector<int> _values){values = _values;return solve();}};

string solve() {

  vector<int> v;
  rep(i, values.size()-1) {
    v.push_back(values[i+1] / values[i] - 1);
  }

  sort(all(v));

  rep(i, v.size()) {
    if(i >= v[i]) {
      return "Impossible";
    }
  }

  return "Possible";
}

// CUT begin
using namespace std;
    char do_test(vector<int>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> values;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              values = { 1 };
                              
                    __expected = "Possible";
                    break;
        }
                case 1: {
                              values = { 1,3 };
                              
                    __expected = "Possible";
                    break;
        }
                case 2: {
                              values = { 1,2,4 };
                              
                    __expected = "Impossible";
                    break;
        }
                case 3: {
                              values = { 1,5,15,90 };
                              
                    __expected = "Possible";
                    break;
        }
                case 4: {
                              values = { 1,4,20,60,180,1440,5760 };
                              
                    __expected = "Impossible";
                    break;
        }
                case 5: {
                              values = { 1,7,21,105,630,3150,18900 };
                              
                    __expected = "Possible";
                    break;
        }
                case 6: {
                              values = { 1,10,30,300,900,9000,18000 };
                              
                    __expected = "Impossible";
                    break;
        }
                case 7: {
                              values = { 1,2,10,40,200,1000,4000,20000 };
                              
                    __expected = "Impossible";
                    break;
        }
                /*case 8: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    values = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(values, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 8;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> values, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << values  << "]" << endl;
   
    ColorfulCoinsEasy *__instance = new ColorfulCoinsEasy();
  string __result = __instance->isPossible(values);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ColorfulCoinsEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
