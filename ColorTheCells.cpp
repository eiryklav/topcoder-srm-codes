#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = min(a, x)
#define maximize(a, x) a = max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> dryingTime;
// ------------- input end  --------------
int solve(); struct ColorTheCells { int minimalTime(vector<int> _dryingTime) { dryingTime = _dryingTime; return solve(); }};

int N;
vector<int> nextDraw;

int summation(vector<int> const& v, int from, int to, vector<bool> const& use) {
  int res = 0;
  if(from > to) { swap(from, to); }
  REP(i, from, to+1) {
    if(use[i]) {
      res += v[i];
    }
  }
  return res;
}

void move(int& time, int const pos, int const tar, vector<int>& v, bitset<55> const& use) {
  int add = 0;
  for(int i=pos < tar ? pos + 1 : pos - 1; pos < tar ? i<=tar : i>=tar; pos < tar ? i++ : i--) {//REP(i, pos, tar) {
    if(use[i] && v[i] > 0) {
      if(add < v[i]) {
        v[i] -= add;
        add += v[i];
        v[i] = 0;
      }
      else {
        v[i] = 0;
      }
    }
    add ++;
  }
  rep(i, min(pos, tar)) {
    if(use[i]) { v[i] = max(0, v[i] - add); }
  }
  REP(i, max(pos, tar)+1, N) {
    if(use[i]) { v[i] = max(0, v[i] - add); }
  }
  time += add;
}

void stay(int& time, int add, vector<int>& v, bitset<55> const& use) {
  time += add;
  rep(i, v.size()) {
    if(use[i]) {
      v[i] = max(0, v[i] - add);
    }
  }
}

int requiredTimeForS(int S) {
  auto v = dryingTime;
  bitset<55> use;
  int pos = 0;
  int time = 0;
  if(!(S >> 0 & 1)) { return inf; }
  if((S >> (N-1) & 1)) { return inf; }
  rep(i_, N) {
    int i = nextDraw[i_];
    if(S >> i & 1) {
      // 右から
      move(time, pos, i+1, v, use);
      stay(time, 1, v, use);
      use[i] = 1; // after finished painting
      pos = i+1;
    }
    else {
      // 左から
      move(time, pos, i-1, v, use);
      stay(time, 1, v, use);
      use[i] = 1;
      pos = i-1;
    }
  }
  return time;
}

int solve() {

  N = dryingTime.size();
  int ans = inf;

  nextDraw.clear(); nextDraw.resize(N); iota(all(nextDraw), 0);
  do {
    rep(S, 1<<N) {  // 0: 左から塗る 1: 右から塗る
      minimize(ans, requiredTimeForS(S));
    } // for S
  } while(next_permutation(all(nextDraw)));

  return ans;
}
