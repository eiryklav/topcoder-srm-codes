#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct IncubatorEasy {int maxMagicalGirls(vector <string> love);};

int N;
vector<string> G;
vector<bool> magical, prot;

void prot_dfs(int i) {
	prot[i] = 1;
	rep(j, N) {
		if(G[i][j] == 'Y' && !prot[j]) { prot_dfs(j); }
	}
}

int IncubatorEasy::maxMagicalGirls(vector <string> love) {
  int res = 0;

  N = love.size(); G = love;

  vector<int> order(N);
  iota(all(order), 0);

  do {
  	magical.clear(), prot.clear();
  	magical.resize(N), prot.resize(N);

  	rep(i, N) {
  		int tar = order[i];
 			if(!magical[tar] && !prot[tar]) {
 				magical[tar] = 1;
 				rep(j, N) {
 					if(G[tar][j] == 'Y' && !prot[j]) {
 						prot_dfs(j);
 					}
 				}
			}

	  	int ans = 0;
	  	rep(k, i+1) {
	  		ans += magical[order[k]] && !prot[order[k]];
	  	}
	  	res = max(res, ans);
  	}


  } while(next_permutation(all(order)));

  return res;
}
