#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int A;
int B;
int C;
// ------------- input end  --------------
ll solve(); struct LittleElephantAndXor {ll getNumber(int _A, int _B, int _C){A = _A, B = _B, C = _C;return solve();}};

deque<int> getBits(int x) {
  deque<int> ret;
  while(x) {
    ret.push_back(x&1);
    x >>= 1;
  }
  reverse(all(ret));
  return ret;
}

void adjustLeading0(deque<int>& d, int n) {
  assert(d.size() <= n);
  while(d.size() < n) d.push_front(0);
}

ll solve() {

  auto ABits = getBits(A);
  auto BBits = getBits(B);
  auto CBits = getBits(C);

  int n = max({ABits.size(), BBits.size(), CBits.size()}) + 2;
  adjustLeading0(ABits, n);
  adjustLeading0(BBits, n);
  adjustLeading0(CBits, n);

  ll dp[40][2][2][2] = {};
  dp[0][1][1][1] = 1;

  REP(index, 1, n+1) {
    auto prev = dp[index-1], curr = dp[index];
    int amax = ABits[index-1];
    int bmax = BBits[index-1];
    int cmax = CBits[index-1];

    rep(pa, 2) rep(pb, 2) rep(pc, 2) {
      rep(x, (pa == 1 ? amax+1 : 2)) rep(y, (pb == 1 ? bmax+1 : 2)) {
        int z = x ^ y;
        if(pc == 1 && z > cmax) { continue; }
        int a = pa == 1 ? ( x == amax ? 1 : 0 ) : 0;
        int b = pb == 1 ? ( y == bmax ? 1 : 0 ) : 0;
        int c = pc == 1 ? ( z == cmax ? 1 : 0 ) : 0;
        curr[a][b][c] += prev[pa][pb][pc];
      }
    }
  }

  ll res = 0;
  rep(i, 2) rep(j, 2) rep(k, 2) {
    res += dp[n][i][j][k];
  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int A;
            int B;
            int C;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = 2;
                                        B = 2;
                                        C = 1;
                              
                    __expected = 5LL;
                    break;
        }
                case 1: {
                              A = 4;
                                        B = 7;
                                        C = 3;
                              
                    __expected = 20LL;
                    break;
        }
                case 2: {
                              A = 10;
                                        B = 10;
                                        C = 5;
                              
                    __expected = 57LL;
                    break;
        }
                case 3: {
                              A = 774;
                                        B = 477;
                                        C = 447;
                              
                    __expected = 214144LL;
                    break;
        }
                case 4: {
                              A = 1000000000;
                                        B = 1000000000;
                                        C = 500000000;
                              
                    __expected = 468566946385621507LL;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = ;
                  B = ;
                  C = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(A, B, C, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int A, int B, int C, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(A) << ","<< pretty_print(B) << ","<< pretty_print(C)  << "]" << endl;
   
    LittleElephantAndXor *__instance = new LittleElephantAndXor();
  ll __result = __instance->getNumber(A, B, C);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "LittleElephantAndXor: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
