#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
int M;
int goalX;
int goalY;
// ------------- input end  --------------
double solve(); struct TorusSailingEasy {double expectedTime(int _N, int _M, int _goalX, int _goalY){N = _N, M = _M, goalX = _goalX, goalY = _goalY;return solve();}};

double dp[100001][301];

double solve() {

  int x = 0, y = 0;
  int a = 0;
  while(!(x == goalX && y == goalY) && a <= 111) { x++, y++; x %= N, y %= M; a++; }  
  if(a >= 111) { return -1.0; }

  x = 0, y = 0;
  int b = 0;
  while(!(x == goalX && y == goalY) && b <= 111) { x--, y--; x += N, y += M; x %= N, y %= M; b++; }
  if(b >= 111) { return -1.0; }

  zero(dp);
  a += b;
  dp[0][b] = 1.0;
  rep(i, 100000) {
    rep(j, 300) {
      if(j == a || j == 0) { continue; } // STOP !!!
      if(j) dp[i+1][j-1] += dp[i][j] * 0.5;
      dp[i+1][j+1] += dp[i][j] * 0.5;
    }
  }

  double ans = 0.0;
  rep(i, 100000) {
    ans += i * (dp[i][0] + dp[i][a]);
  }
  return ans;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            int M;
            int goalX;
            int goalY;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 2;
                                        M = 2;
                                        goalX = 1;
                                        goalY = 1;
                              
                    __expected = 1.0;
                    break;
        }
                case 1: {
                              N = 2;
                                        M = 2;
                                        goalX = 0;
                                        goalY = 1;
                              
                    __expected = -1.0;
                    break;
        }
                case 2: {
                              N = 3;
                                        M = 3;
                                        goalX = 1;
                                        goalY = 1;
                              
                    __expected = 2.0;
                    break;
        }
                case 3: {
                              N = 4;
                                        M = 6;
                                        goalX = 1;
                                        goalY = 3;
                              
                    __expected = 27.0;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  M = ;
                  goalX = ;
                  goalY = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, M, goalX, goalY, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, int M, int goalX, int goalY, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< pretty_print(M) << ","<< pretty_print(goalX) << ","<< pretty_print(goalY)  << "]" << endl;
   
    TorusSailingEasy *__instance = new TorusSailingEasy();
  double __result = __instance->expectedTime(N, M, goalX, goalY);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TorusSailingEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
