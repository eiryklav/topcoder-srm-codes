#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct PolygonTraversal2 {int count(int N, vector <int> points);};

int N, start;
int dp[1<<15][15];

bool check(int S, int from, int to) {
	int mn = min(from, to);
	int mx = max(from, to);
	bool ok1 = 0, ok2 = 0;
	rep(i, N) {
		if(mn < i && i < mx && (S >> i & 1)) {
			ok1 = 1;
		}
		if((mx < i || i < mn) && (S >> i & 1)) {
			ok2 = 1;
		}
	}
	return ok1 && ok2;
}

int dfs(int S, int v) {
	auto& ret = dp[S][v];

	if(ret >= 0) { return ret; }
	if(__builtin_popcount(S) == N) {
		if(!check(S, v, start)) { return 0; }
		return 1;
	}

	ret = 0;

	rep(i, N) {
		if(!(S >> i & 1)) {
			if(!check(S, v, i)) { continue; }
			int nextS = S | (1 << i);
			if(dp[nextS][i] >= 0) {
				ret += dp[nextS][i];
			}
			else {
				ret += dfs(nextS, i);
			}
		}
	}

	return ret;
}

int PolygonTraversal2::count(int N, vector <int> points) {
	for(auto& e: points) e--;
	::start = points[0];
	::N = N;
	minus(dp);
	int S = 0;
	rep(i, points.size()) {
		S |= 1 << points[i];
	}
	return dfs(S, points.back());
}