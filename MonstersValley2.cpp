#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct MonstersValley2 {int minimumPrice(vector <int> dread, vector <int> price);};

int MonstersValley2::minimumPrice(vector <int> dread, vector <int> price) {
	int N = dread.size();
	int ret = inf;
	rep(S, 1<<N) {
		int cost = 0;
		ll myDread = 0;
		bool invalid = 0;
		rep(i, N) {
			if(S >> i & 1) {
				myDread += dread[i];
				cost += price[i];
			}
			else {
				if(myDread < dread[i]) {
					invalid = 1; break;
				}
			}
		}
		if(invalid) { continue; }
		ret = min(ret, cost);
	}
	return ret;
}
