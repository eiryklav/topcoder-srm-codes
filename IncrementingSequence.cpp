#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int k;
vector<int> A;
// ------------- input end  --------------
string solve(); struct IncrementingSequence {string canItBeDone(int _k, vector<int> _A){k = _k, A = _A;return solve();}};

string solve() {

  int N = A.size();
  sort(all(A));
  bool okk = 1;
  REP(i, 1, N+1) if(A[i-1] != i) okk = 0;
  if(okk) return "POSSIBLE";

  bool ks[11] = {};
  rep(i, A.size()) {
    if(A[i] < k) {
      ks[A[i]] = 1;
    }
  }
  bool ok = 1;
  REP(i, 1, k) {
    ok = ok && ks[i];
  }
  if(!ok) return "IMPOSSIBLE";

  vector<int> B;
  for(auto& e: A) {
    if(e < k && ks[e]) {
      ks[e] = 0;
    }
    else {
      B.push_back(e);
    }
  }

  sort(all(B));
  set<int> st;

  for(auto& e: B) {
    for(int u=e; u<=N; u+=k) {
      if(u >= k && !st.count(u)) {
        st.insert(u);
        break;
      }
    }
  }

  for(int i=k; i<=N; i++) {
    if(!st.count(i)) { return "IMPOSSIBLE"; }
  }

  return "POSSIBLE";
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int k;
            vector<int> A;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              k = 3;
                                        A = { 1,2,4,3 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                case 1: {
                              k = 5;
                                        A = { 2,2 };
                              
                    __expected = "IMPOSSIBLE";
                    break;
        }
                case 2: {
                              k = 1;
                                        A = { 1,1,1,1,1,1,1,1 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                case 3: {
                              k = 2;
                                        A = { 5,3,3,2,1 };
                              
                    __expected = "IMPOSSIBLE";
                    break;
        }
                case 4: {
                              k = 9;
                                        A = { 1,2,3,1,4,5,6,7,9,8 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                case 5: {
                              k = 2;
                                        A = { 1,1,1,1,1,1,2,2,2,2,2,2 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                case 6: {
                              k = 1;
                                        A = { 1 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    k = ;
                  A = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(k, A, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int k, vector<int> A, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(k) << ","<< A  << "]" << endl;
   
    IncrementingSequence *__instance = new IncrementingSequence();
  string __result = __instance->canItBeDone(k, A);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "IncrementingSequence: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
