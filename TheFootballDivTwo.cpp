#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int yourScore;
vector<int> scores;
vector<int> numberOfTeams;
// ------------- input end  --------------
int solve(); struct TheFootballDivTwo {int find(int _yourScore, vector<int> _scores, vector<int> _numberOfTeams){yourScore = _yourScore, scores = _scores, numberOfTeams = _numberOfTeams;return ::solve();}};

int solve() {

  int winMatchesNum = 1;
  rep(i, numberOfTeams.size()) winMatchesNum += numberOfTeams[i];

  yourScore += 6;
  winMatchesNum -= 2;

  vector<pair<int, int>> others; // score, remain // 50万
  rep(i, numberOfTeams.size()) {
    numberOfTeams[i] --;
    others.emplace_back(scores[i], 2);
    if(numberOfTeams[i] > 0) { i --; }
  }

  int bestRank = 1;

  rep(i, others.size()) {
    if(others[i].first >= yourScore + 1) {
      bestRank ++;
    }
  }

  rep(i, others.size()) {
    while(others[i].first >= yourScore + 1 && others[i].second) {
      others[i].first += 3;
      others[i].second --;
      winMatchesNum --;
    }
    while(others[i].first <= yourScore - 3 && others[i].second) {
      others[i].first += 3;
      others[i].second --;
      winMatchesNum --;
    }
  }

  if(winMatchesNum <= 0) { return bestRank; }

  sort(all(others), [](pair<int, int> const& a, pair<int, int> const& b) { return a.second > b.second; });

  rep(i, others.size()) {
    if(others[i].second) {
      bestRank ++;
      winMatchesNum -= others[i].second;
      others[i].second = 0;
      if(winMatchesNum <= 0) { return bestRank; }
    }
  }

  return bestRank;
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int yourScore;
            vector<int> scores;
            vector<int> numberOfTeams;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              yourScore = 4;
                                        scores = { 7 };
                                        numberOfTeams = { 1 };
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              yourScore = 1;
                                        scores = { 7 };
                                        numberOfTeams = { 2 };
                              
                    __expected = 2;
                    break;
        }
                case 2: {
                              yourScore = 1;
                                        scores = { 7,1 };
                                        numberOfTeams = { 2,1 };
                              
                    __expected = 1;
                    break;
        }
                case 3: {
                              yourScore = 11;
                                        scores = { 5,12,17,19,99,13,15,14 };
                                        numberOfTeams = { 2,4,8,2,1,3,25,3 };
                              
                    __expected = 18;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    yourScore = ;
                  scores = ;
                  numberOfTeams = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(yourScore, scores, numberOfTeams, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int yourScore, vector<int> scores, vector<int> numberOfTeams, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(yourScore) << ","<< scores << ","<< numberOfTeams  << "]" << endl;
   
    TheFootballDivTwo *__instance = new TheFootballDivTwo();
  int __result = __instance->find(yourScore, scores, numberOfTeams);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TheFootballDivTwo: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
