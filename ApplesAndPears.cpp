#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> board;
int K;
// ------------- input end  --------------
int solve(); struct ApplesAndPears {int getArea(vector<string> _board, int _K){board = _board, K = _K;return solve();}};

map<char, int> num;
int cnt[3][55][55];
int N;

int solve() {

  num['.'] = 0;
  num['A'] = 1;
  num['P'] = 2;
  
  N = board.size();
  rep(i, N) rep(j, N) {
    int n = num[board[i][j]];
    rep(k, 3) {
      cnt[k][i][j] = n == k;
      if(i) cnt[k][i][j] += cnt[k][i-1][j];
      if(j) cnt[k][i][j] += cnt[k][i][j-1];
      if(i && j) cnt[k][i][j] -= cnt[k][i-1][j-1];
    }
  }

  auto accumulate = [](int d, int i, int j, int k, int l) {
    int ret = cnt[d][k][l];
    if(i) ret -= cnt[d][i-1][l];
    if(j) ret -= cnt[d][k][j-1];
    if(i && j) ret += cnt[d][i-1][j-1];
    return ret;
  };

  int sum[3]; rep(d, 3) sum[d] = accumulate(d, 0, 0, N-1, N-1);
//  for(int i=0; i<3; i++) cout << sum[i] << endl; cout << endl;
  int res = 1;
  rep(i, N) rep(j, N) REP(k, i, N) REP(l, j, N) {
    int emptyCellsInRect = accumulate(0, i, j, k, l);
    int emptyCellsOutOfRect = sum[0] - emptyCellsInRect;
    REP(d, 1, 3) {
      int fruitInRect = accumulate(d, i, j, k, l);
      int fruitOutOfRect = sum[d] - fruitInRect;
      int notUseF = !(d-1) + 1;
      int notUseFruitInRect = accumulate(notUseF, i, j, k, l);
      int needK = emptyCellsInRect + 2 * notUseFruitInRect;
      if(emptyCellsInRect + notUseFruitInRect > fruitOutOfRect) continue;
      if(notUseFruitInRect > 0 && emptyCellsInRect + emptyCellsOutOfRect == 0) continue;
      if(needK > K) continue;
      maximize(res, (k-i+1)*(l-j+1));
    }

    // empty cell
    int allFruitsInRect = accumulate(1, i, j, k, l) + accumulate(2, i, j, k, l);
    if(emptyCellsOutOfRect >= allFruitsInRect && allFruitsInRect <= K) {
//      cout << i <<","<<j<<","<<k<<","<<l<<endl; cout << emptyCellsOutOfRect << "::\n";
      maximize(res, (k-i+1)*(l-j+1));
    }
  }
//  exit(0);

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> board;
            int K;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              board = { ".A","P." };
                                        K = 0;
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              board = { ".A","P." };
                                        K = 1;
                              
                    __expected = 2;
                    break;
        }
                case 2: {
                              board = { ".PP","PPA","PAP" };
                                        K = 3;
                              
                    __expected = 6;
                    break;
        }
                case 3: {
                              board = { "A.P.PAAPPA","PPP..P.PPP","AAP.A.PAPA","P.PA.AAA.A","...PA.P.PA","P..A.A.P..","PAAP..A.A.","PAAPPA.APA",".P.AP.P.AA","..APAPAA.." };
                                        K = 10;
                              
                    __expected = 21;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    board = ;
                  K = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(board, K, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> board, int K, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << board << ","<< pretty_print(K)  << "]" << endl;
   
    ApplesAndPears *__instance = new ApplesAndPears();
  int __result = __instance->getArea(board, K);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ApplesAndPears: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
