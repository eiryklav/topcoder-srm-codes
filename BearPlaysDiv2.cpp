#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <array>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct BearPlaysDiv2 {string equalPiles(int A, int B, int C);};

int All;

map<pair<int, int>, bool> memo;

bool dfs(int a, int b) {
	int c = All - (a + b);
	if(a > b) swap(a, b);
	if(b > c) swap(b, c);
	if(a > b) swap(a, b);

	const int a_ = a, b_ = b;

	if(memo.find({a, b}) != memo.end()) return memo[{a, b}];
	auto& ret = memo[{a, b}];
	ret = 0;
	if(a == b && b == c) { return ret = 1; }

	if(a < b) {
		a *= 2, b -= a_;
		ret = max(ret, dfs(a, b));
		a = a_, b = b_;
	}
	if(a < c) {
		a *= 2;
		ret = max(ret, dfs(a, b));
		a = a_;
	}
	if(b < c) {
		b *= 2;
		ret = max(ret, dfs(a, b));
		b = b_;
	}

	return ret;
}

string BearPlaysDiv2::equalPiles(int A, int B, int C) {
	All = A + B + C;
	return dfs(A, B) ? "possible" : "impossible";
}
