#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int n;
// ------------- input end  --------------
vector<string> solve(); struct Target {vector<string> draw(int _n){n = _n;return ::solve();}};
/*
Passed System Test
vector<string> solve() {

  vector<string> res;
  rep(i, n) res.push_back(string(n, ' '));

  int dx[4] = {-1,0,1,0};
  int dy[4] = {0,1,0,-1};

  int y = 0, x = n-1;
  while(n > 0) {
    if(n == 1) res[y][x] = '#';
    rep(d, 4) {
      rep(k, n-1) {
        res[y + dy[d]][x + dx[d]] = '#', y += dy[d], x += dx[d];
      }
    }
    y += 2, x -= 2;
    n -= 4;
  }

  return res;
}
*/

vector<string> solve() {
  vector<string> res; rep(i, n) res.push_back(string(n, ' '));
  rep(i, n) rep(j, n) res[i][j] = max(abs(i-n/2), abs(j-n/2)) % 2 ? ' ' : '#';
  return res;
}

// CUT begin
using namespace std;
    char do_test(int,vector<string>,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int n;
            vector<string> __expected = vector<string>();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 5;
                              
                    __expected = {
                    "#####",
                    "#   #",
                    "# # #",
                    "#   #",
                    "#####"
                  };
                    break;
        }
                case 1: {
                              n = 9;
                              
                    __expected = {
                    "#########",
                    "#       #",
                    "# ##### #",
                    "# #   # #",
                    "# # # # #",
                    "# #   # #",
                    "# ##### #",
                    "#       #",
                    "#########" };
                    break;
        }
                case 2: {
                              n = 13;
                              
                    __expected = { "#############", "#           #", "# ######### #", "# #       # #", "# # ##### # #", "# # #   # # #", "# # # # # # #", "# # #   # # #", "# # ##### # #", "# #       # #", "# ######### #", "#           #", "#############" };
                    break;
        }
                case 3: {
                              n = 17;
                              
                    __expected = { "#################", "#               #", "# ############# #", "# #           # #", "# # ######### # #", "# # #       # # #", "# # # ##### # # #", "# # # #   # # # #", "# # # # # # # # #", "# # # #   # # # #", "# # # ##### # # #", "# # #       # # #", "# # ######### # #", "# #           # #", "# ############# #", "#               #", "#################" };
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int n, vector<string> __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n)  << "]" << endl;
   
    Target *__instance = new Target();
  vector<string> __result = __instance->draw(n);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  __expected  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  __result  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "Target: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
