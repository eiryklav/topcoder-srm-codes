#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct TheNumberGameDivTwo {string find(int n);};

int wins[1010];

bool dfs(int remain) {
	if(wins[remain] >= 0) return wins[remain];
	wins[remain] = 0;
	for(int i=2; i<remain; i++) {
		if(remain % i == 0) {
			wins[remain] = wins[remain] || !dfs(remain-i);
		}
	}
	return wins[remain];
}

bool is_prime(int n) {
	for(int i=2; i*i<=n; i++) {
		if(n % i == 0) return false;
	}
	return true;
}

string TheNumberGameDivTwo::find(int n) {
	minus(wins);
	wins[1] = 0;
	for(int i=2; i<1010; i++) {
		if(is_prime(i)) {
			wins[i] = 0;
		}
	}
	return dfs(n) ? "John" : "Brus";
}

