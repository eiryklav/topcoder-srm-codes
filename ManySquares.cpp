#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

vector<int> sticks;
int solve(); struct ManySquares {int howManySquares(vector<int> _sticks){sticks = _sticks;return solve();}};

int solve() {
  map<int, int> mp;
  for(auto& e: sticks) {
    mp[e]++;
  }

  int ans = 0;
  for(auto& e: mp) {
    ans += e.second / 4;
  }

  return ans;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> sticks;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              sticks = { 1,1,2,2,1,1,2 };
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              sticks = { 3,1,4,4,4,10,10,10,10 };
                              
                    __expected = 1;
                    break;
        }
                case 2: {
                              sticks = { 1,2,3,4,1,2,3,4,1,2,3,1,2,3,4,1,2,3,3,3 };
                              
                    __expected = 3;
                    break;
        }
                case 3: {
                              sticks = { 1,1,1,2,2,2,3,3,3,4,4,4 };
                              
                    __expected = 0;
                    break;
        }
                case 4: {
                              sticks = { 1,1,1,2,1,1,1,3,1,1,1 };
                              
                    __expected = 2;
                    break;
        }
                case 5: {
                              sticks = { 2,2,4,4,8,8 };
                              
                    __expected = 0;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    sticks = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(sticks, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> sticks, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << sticks  << "]" << endl;
   
    ManySquares *__instance = new ManySquares();
  int __result = __instance->howManySquares(sticks);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ManySquares: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
