#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> G;
// ------------- input end  --------------
string solve(); struct RGBTree {string exist(vector<string> _G){G = _G;return ::solve();}};

vector<pair<int, int>> graph[15];
int N, K;
int memo[1<<13][5][5][5];

int dfs(int mask, int red, int green, int blue) {
  if(red >= 5 || green >= 5 || blue >= 5) { return 0; }
  auto& ret = memo[mask][red][green][blue];
  if(ret + 1) { return ret; }
  ret = 0;
  if(red == K / 3 && green == K / 3 && blue == K / 3) { return ret = 1; }
  rep(pos, N) {
    if(!(mask >> pos & 1)) { continue; }
    for(auto& e: graph[pos]) {
      int to, col; tie(to, col) = e;
      int nred    = red   + (col == 0);
      int ngreen  = green + (col == 1);
      int nblue   = blue  + (col == 2);
      if(mask >> to & 1) { continue; }
      int nmask = mask | (1 << to);
      ret |= dfs(nmask, nred, ngreen, nblue);
    }
  }
  return ret;
}

string solve() {

  rep(i, 15) graph[i].clear();
  N = G.size();
  rep(i, N) rep(j, N) {
    int col;
    if(G[i][j] == 'R') col = 0;
    else if(G[i][j] == 'G') col = 1;
    else if(G[i][j] == 'B') col = 2;
    else { continue; }
    graph[i].emplace_back(j, col);
  }

  K = N - 1;

  minus(memo);
  return dfs(1<<0, 0, 0, 0) ? "Exist" : "Does not exist";
}

// CUT begin
using namespace std;
    char do_test(vector<string>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> G;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              G = { ".RGB","R...","G...","B..." };
                              
                    __expected = "Exist";
                    break;
        }
                case 1: {
                              G = { ".RRB","R...","R...","B..." };
                              
                    __expected = "Does not exist";
                    break;
        }
                case 2: {
                              G = { ".R..BG..G..RG","R...GG..BR.G.","...G.GG.RR.BB","..G.RR.B..GRB","BG.R.G.BRRR.G","GGGRG.R....RR","..G..R.BGRR..","...BB.B.RB.G.","GBR.R.GR.B.R.",".RR.R.RBB.BRB","...GR.R..B...","RGBR.R.GRR...","G.BBGR...B..." };
                              
                    __expected = "Exist";
                    break;
        }
                case 3: {
                              G = { ".............",".......BB.R..",".......RR....",".....G.G....R","........BB...","...G.........","........B...R",".BRG.......G.",".BR.B.B...GB.","....B......GR",".R......G....",".......GBG..B","...R..R..R.B." };
                              
                    __expected = "Does not exist";
                    break;
        }
                case 4: {
                              G = { "..B.BB...RB..","......R..B.G.","B.......BB...",".......R...G.","B....GRB..R..","B...G.RG.R...",".R..RR..B.RB.","...RBG...G...","..B...B......","RBB..R.G....R","B...R.R......",".G.G..B.....R",".........R.R." };
                              
                    __expected = "Exist";
                    break;
        }
                case 5: {
                              G = { "....","....","....","...." };
                              
                    __expected = "Does not exist";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    G = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(G, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> G, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << G  << "]" << endl;
   
    RGBTree *__instance = new RGBTree();
  string __result = __instance->exist(G);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "RGBTree: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
