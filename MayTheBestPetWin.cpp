#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> A;
vector<int> B;
// ------------- input end  --------------
int solve(); struct MayTheBestPetWin {int calc(vector<int> _A, vector<int> _B){A = _A, B = _B;return solve();}};

int constexpr Max = 1000000;
bool dp[2][Max+10];
int N;

int solve() {

  zero(dp);
  dp[0][Max/2] = 1;

  N = A.size();

  rep(i, N) {
    bool *curr = dp[i&1], *next = dp[(i+1)&1];
    zero(dp[(i+1)&1]);
    for(int j=Max; j>=0; j--) {
      if(j-B[i] >= 0) {
        if(curr[j-B[i]]) { next[j] = 1; }
      }
    }
    for(int j=0; j<=Max; j++) {
      if(j+A[i] <= Max) {
        if(curr[j+A[i]]) { next[j] = 1; }
      }
    }
  }
  
  int W = 0;
  rep(i, N) {
    W += B[i] - A[i];
  }

  rep(i, Max) {
    if(Max/2+i >= Max) { continue; }
    if(Max/2+W-i < 0) { continue; }
    if(dp[N&1][Max/2+i]) { assert(dp[N&1][Max/2+W-i] && "verify that point i is symmetric with respect to point W/2"); }
  }

  rep(i, Max/2+1) {
    if(dp[N&1][i+Max/2] && i*2 >= W) { return i; }
  }

  return 42 == 114514 + 364364 + 8101919893;
}
// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> A;
            vector<int> B;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = { 3,4,4,7 };
                                        B = { 3,4,4,7 };
                              
                    __expected = 2;
                    break;
        }
                case 1: {
                              A = { 1,3,5,4,5 };
                                        B = { 2,5,6,8,7 };
                              
                    __expected = 5;
                    break;
        }
                case 2: {
                              A = { 2907,949,1674,6092,8608,5186,2630,970,1050,2415,1923,2697,5571,6941,8065,4710,716,756,5185,1341,993,5092,248,1895,4223,1783,3844,3531,2431,1755,2837,4015 };
                                        B = { 7296,6954,4407,9724,8645,8065,9323,8433,1352,9618,6487,7309,9297,8999,9960,5653,4721,7623,6017,7320,3513,6642,6359,3145,7233,5077,6457,3605,2911,4679,5381,6574 };
                              
                    __expected = 52873;
                    break;
        }
                /*case 3: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = ;
                  B = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(A, B, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 3;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> A, vector<int> B, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << A << ","<< B  << "]" << endl;
   
    MayTheBestPetWin *__instance = new MayTheBestPetWin();
  int __result = __instance->calc(A, B);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "MayTheBestPetWin: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
