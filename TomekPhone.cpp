#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct TomekPhone {int minKeystrokes(vector <int> frequencies, vector <int> keySizes);};

int TomekPhone::minKeystrokes(vector <int> frequencies, vector <int> keySizes) {
  int res = 0;

  int allsize = accumulate(all(keySizes), 0);
  if(allsize < frequencies.size()) { return -1; }

  priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> pq;
  rep(i, keySizes.size()) {
  	pq.emplace(1, i);
  }

  sort(all(frequencies), greater<int>());
  int fidx = 0;
 
  while(!pq.empty()) {
  	auto p = pq.top(); pq.pop();
  	int mul = p.first;
  	int const index = p.second;
  	cout << frequencies[fidx] << ' ' << mul << endl;
  	res += frequencies[fidx++] * mul;
  	if(fidx == frequencies.size()) { break; }
  	mul++;
  	if(keySizes[index] >= mul) {
  		pq.emplace(mul, index);
  	}
  }

  return res;
}

// BEGIN CUT HERE
#include <cstdio>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
namespace moj_harness {
	using std::string;
	using std::vector;
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				std::cerr << "Illegal input! Test case " << casenum << " does not exist." << std::endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			std::cerr << "No test cases run." << std::endl;
		} else if (correct < total) {
			std::cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << std::endl;
		} else {
			std::cerr << "All " << total << " tests passed!" << std::endl;
		}
	}
	
	int verify_case(int casenum, const int &expected, const int &received, std::clock_t elapsed) { 
		std::cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			std::sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		std::cerr << verdict;
		if (!info.empty()) {
			std::cerr << " (";
			for (size_t i=0; i<info.size(); ++i) {
				if (i > 0) std::cerr << ", ";
				std::cerr << info[i];
			}
			std::cerr << ")";
		}
		std::cerr << std::endl;
		
		if (verdict == "FAILED") {
			std::cerr << "    Expected: " << expected << std::endl; 
			std::cerr << "    Received: " << received << std::endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum__) {
		switch (casenum__) {
		case 0: {
			int frequencies[]         = {7,3,4,1};
			int keySizes[]            = {2,2};
			int expected__            = 19;

			std::clock_t start__      = std::clock();
			int received__            = TomekPhone().minKeystrokes(vector <int>(frequencies, frequencies + (sizeof frequencies / sizeof frequencies[0])), vector <int>(keySizes, keySizes + (sizeof keySizes / sizeof keySizes[0])));
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 1: {
			int frequencies[]         = {13,7,4,20};
			int keySizes[]            = {2,1};
			int expected__            = -1;

			std::clock_t start__      = std::clock();
			int received__            = TomekPhone().minKeystrokes(vector <int>(frequencies, frequencies + (sizeof frequencies / sizeof frequencies[0])), vector <int>(keySizes, keySizes + (sizeof keySizes / sizeof keySizes[0])));
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 2: {
			int frequencies[]         = {11,23,4,50,1000,7,18};
			int keySizes[]            = {3,1,4};
			int expected__            = 1164;

			std::clock_t start__      = std::clock();
			int received__            = TomekPhone().minKeystrokes(vector <int>(frequencies, frequencies + (sizeof frequencies / sizeof frequencies[0])), vector <int>(keySizes, keySizes + (sizeof keySizes / sizeof keySizes[0])));
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 3: {
			int frequencies[]         = {100,1000,1,10};
			int keySizes[]            = {50};
			int expected__            = 1234;

			std::clock_t start__      = std::clock();
			int received__            = TomekPhone().minKeystrokes(vector <int>(frequencies, frequencies + (sizeof frequencies / sizeof frequencies[0])), vector <int>(keySizes, keySizes + (sizeof keySizes / sizeof keySizes[0])));
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 4: {
			int frequencies[]         = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50};
			int keySizes[]            = {10,10,10,10,10,10,10,10};
			int expected__            = 3353;

			std::clock_t start__      = std::clock();
			int received__            = TomekPhone().minKeystrokes(vector <int>(frequencies, frequencies + (sizeof frequencies / sizeof frequencies[0])), vector <int>(keySizes, keySizes + (sizeof keySizes / sizeof keySizes[0])));
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 5: {
			int frequencies[]         = ;
			int keySizes[]            = ;
			int expected__            = ;

			std::clock_t start__      = std::clock();
			int received__            = TomekPhone().minKeystrokes(vector <int>(frequencies, frequencies + (sizeof frequencies / sizeof frequencies[0])), vector <int>(keySizes, keySizes + (sizeof keySizes / sizeof keySizes[0])));
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
/*      case 6: {
			int frequencies[]         = ;
			int keySizes[]            = ;
			int expected__            = ;

			std::clock_t start__      = std::clock();
			int received__            = TomekPhone().minKeystrokes(vector <int>(frequencies, frequencies + (sizeof frequencies / sizeof frequencies[0])), vector <int>(keySizes, keySizes + (sizeof keySizes / sizeof keySizes[0])));
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
/*      case 7: {
			int frequencies[]         = ;
			int keySizes[]            = ;
			int expected__            = ;

			std::clock_t start__      = std::clock();
			int received__            = TomekPhone().minKeystrokes(vector <int>(frequencies, frequencies + (sizeof frequencies / sizeof frequencies[0])), vector <int>(keySizes, keySizes + (sizeof keySizes / sizeof keySizes[0])));
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}


#include <cstdlib>
int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(std::atoi(argv[i]));
	}
}
// END CUT HERE
