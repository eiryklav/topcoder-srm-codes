#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> X;
vector<int> Y;
vector<int> Z;
// ------------- input end  --------------
vector<ll> solve(); struct CandyAddict {vector<ll> solve(vector<int> _X, vector<int> _Y, vector<int> _Z){X = _X, Y = _Y, Z = _Z;return ::solve();}};

ll solve(ll allowance, ll price, ll days) {
  // same as editorial
  if(allowance == price) {
    return 0;
  }

  ll money = 0;
  while(days > 0) {
    ll num = (money + allowance) / price;
    if(num >= days) {
      money = (money + allowance) % price + allowance * (days - 1);
      days = 0;
    }
    else {
      ll money_bound  = price * (num + 1) - allowance;
      ll increment    = (allowance - price) * num;
      ll steps = min((money_bound - money - 1) / increment, (days - num + 1) / num);
      if(steps < 1) {
        steps = 1;
      }
      days -= num * steps;
      money += increment * steps;
    }
  }
  return money;
}

vector<ll> solve() {
  vector<ll> ret(X.size());
  rep(i, X.size()) {
    ret[i] = solve(X[i], Y[i], Z[i]);
  }
   return ret;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,vector<int>,vector<ll>,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> X;
            vector<int> Y;
            vector<int> Z;
            vector<ll> __expected = vector<ll>();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              X = { 7 };
                              Y = { 5 };
                              Z = { 2000 };
                              __unknownAnswer = 1;
//                    __expected = { 6LL };
                    break;
        }

                case 4: {
                              X = { 5 };
                                        Y = { 3 };
                                        Z = { 3 };
                              
                    __expected = { 6LL };
                    break;
        }
                case 1: {
                              X = { 5,5,5,5,5 };
                                        Y = { 3,3,3,3,3 };
                                        Z = { 1,2,3,4,5 };
                              
                    __expected = { 2LL, 1LL, 6LL, 2LL, 7LL };
                    break;
        }
                case 2: {
                              X = { 1000000000,1000000000,1000000000,1000000000,1000000000 };
                                        Y = { 1,2,3,999999998,999999999 };
                                        Z = { 342568368,560496730,586947396,386937583,609483745 };
                              
                    __expected = { 342568367000000000LL, 60496729000000000LL, 253614062000000001LL, 773875166LL, 609483745LL };
                    break;
        }
                case 3: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
X = {1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000};
Y = {1, 999999999, 2, 999999998, 3, 999999997, 4, 999999996, 5, 1000000000};
Z = {1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000};
              
          __unknownAnswer = true; 
          break;
        }
    
    default: return 'm';
  }
  return do_test(X, Y, Z, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 3;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> X, vector<int> Y, vector<int> Z, vector<ll> __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << X << ","<< Y << ","<< Z  << "]" << endl;
   
    CandyAddict *__instance = new CandyAddict();
  vector<ll> __result = __instance->solve(X, Y, Z);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  __expected  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  __result  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "CandyAddict: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
