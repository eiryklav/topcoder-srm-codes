#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()

int r1;
int c1;
int r2;
int c2;
int solve(); struct BishopMove {int howManyMoves(int _r1, int _c1, int _r2, int _c2){r1 = _r1, c1 = _c1, r2 = _r2, c2 = _c2;return solve();}};
int solve() {
  if((r1 + c1) % 2 != (r2 + c2) % 2)  return -1;
  if(r1 == r2 && c1 == c2)            return 0;
  if(abs(r2 - r1) == abs(c2 - c1))    return 1;
  return 2;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int r1;
            int c1;
            int r2;
            int c2;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              r1 = 4;
                                        c1 = 6;
                                        r2 = 7;
                                        c2 = 3;
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              r1 = 2;
                                        c1 = 5;
                                        r2 = 2;
                                        c2 = 5;
                              
                    __expected = 0;
                    break;
        }
                case 2: {
                              r1 = 1;
                                        c1 = 3;
                                        r2 = 5;
                                        c2 = 5;
                              
                    __expected = 2;
                    break;
        }
                case 3: {
                              r1 = 4;
                                        c1 = 6;
                                        r2 = 7;
                                        c2 = 4;
                              
                    __expected = -1;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    r1 = ;
                  c1 = ;
                  r2 = ;
                  c2 = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(r1, c1, r2, c2, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int r1, int c1, int r2, int c2, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(r1) << ","<< pretty_print(c1) << ","<< pretty_print(r2) << ","<< pretty_print(c2)  << "]" << endl;
   
    BishopMove *__instance = new BishopMove();
  int __result = __instance->howManyMoves(r1, c1, r2, c2);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BishopMove: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
