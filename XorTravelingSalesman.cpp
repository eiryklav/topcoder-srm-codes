#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

struct XorTravelingSalesman {int maxProfit(vector <int> cityValues, vector <string> roads);};

bool vis[55][1024];

int XorTravelingSalesman::maxProfit(vector <int> cityValues, vector <string> roads) {
  int res = 0;

  zero(vis);

  int N = cityValues.size();

  queue<pair<int, int>> q;
  q.emplace(0, cityValues[0]);
  vis[0][cityValues[0]] = 1;

  while(!q.empty()) {
  	auto p = q.front(); q.pop();
  	int curr = p.first, cost = p.second;
  	res = max(res, cost);
  	rep(i, N) {
  		if(roads[curr][i] == 'Y') {
  			int ncost = cost ^ cityValues[i];
  			if(vis[i][ncost]) { continue; }
  			vis[i][ncost] = 1;
  			q.emplace(i, ncost);
  		}
  	}
  }

  return res;
}
