#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

struct RobotHerbDiv2 {int getdist(int T, vector <int> a);};

int RobotHerbDiv2::getdist(int T, vector <int> a) {
	int N = a.size();
	int x = 0, y = 0, d = 0;
	rep(_, T) {
		rep(i, N) {
			x += a[i] * dx[d], y += a[i] * dy[d];
			(d += a[i]) %= 4;
		}
	}
	return abs(x) + abs(y);
}
