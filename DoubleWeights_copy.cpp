#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> weight1;
vector<string> weight2;
// ------------- input end  --------------
int solve(); struct DoubleWeights {int minimalCost(vector<string> _weight1, vector<string> _weight2){weight1 = _weight1, weight2 = _weight2;return ::solve();}};

vector<int> g[22];
int N;

bool validw2(ll const W2, ll& ansCost) {
  ansCost = inf;

  priority_queue<tuple<int, int, int>, vector<tuple<int, int, int>>, greater<tuple<int, int, int>>> q; // costw1, pos, currw2;
  q.emplace(0, 0, 0);
  vector<int> dist(N, inf);
  dist[0] = 0;
  while(!q.empty()) {
    int w1, pos, cw2; tie(w1, pos, cw2) = q.top(); q.pop();

    if(pos == 1 && cw2 <= W2) { minimize(ansCost, (ll)w1 * cw2); continue; }

    rep(i, N) {
      if(weight1[pos][i] == '.') { continue; }
      int nw1 = w1 + weight1[pos][i] - '0';
      int nw2 = cw2 + weight2[pos][i] - '0';
      if(nw2 > W2) { continue; }
      if(dist[i] <= nw1) { continue; }
      dist[i] = nw1;
      q.emplace(nw1, i, nw2);
    }

  }
  if(ansCost == inf) { return false; }
  return true;
}

int solve() {

  rep(i, 22) g[i].clear();
  N = weight1.size();

  ll ans = inf;

  int ok = 1e6;  // 解が存在する値
  int ng = 0;    // 解が存在しない値
  
  while(abs(ok-ng) > 1) {
    int mid = (ok + ng) / 2;
    ll cost;
    if(validw2(mid, cost)) {
      ok = mid;
      minimize(ans, cost);
    } else {
      ng = mid;
    }
  }
  return ans == inf ? -1 : ans;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> weight1;
            vector<string> weight2;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              weight1 = { "99","99" };
                              weight2 = { "99","99" };
                              
                    __unknownAnswer = 1;
                    break;
        }
                case 1: {
                              weight1 = { "..14","..14","11..","44.." };
                                        weight2 = { "..94","..94","99..","44.." };
                              
                    __expected = 36;
                    break;
        }
                case 2: {
                              weight1 = { "..",".." };
                                        weight2 = { "..",".." };
                              
                    __expected = -1;
                    break;
        }
                case 3: {
                              weight1 = { ".....9","..9...",".9.9..","..9.9.","...9.9","9...9." };
                                        weight2 = { ".....9","..9...",".9.9..","..9.9.","...9.9","9...9." };
                              
                    __expected = 2025;
                    break;
        }
                case 4: {
                              weight1 = { ".4...1","4.1...",".1.1..","..1.1.","...1.1","1...1." };
                                        weight2 = { ".4...1","4.1...",".1.1..","..1.1.","...1.1","1...1." };
                              
                    __expected = 16;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    weight1 = ;
                  weight2 = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(weight1, weight2, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> weight1, vector<string> weight2, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << weight1 << ","<< weight2  << "]" << endl;
   
    DoubleWeights *__instance = new DoubleWeights();
  int __result = __instance->minimalCost(weight1, weight2);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "DoubleWeights: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
