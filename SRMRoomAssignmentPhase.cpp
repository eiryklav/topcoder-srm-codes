#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct SRMRoomAssignmentPhase {int countCompetitors(vector <int> ratings, int K);};

int SRMRoomAssignmentPhase::countCompetitors(vector <int> ratings, int K) {

  int N = ratings.size();
  int me = ratings[0];

  sort(all(ratings));
  reverse(all(ratings));

  vector<vector<int>> kvec(K, vector<int>());

  rep(i, N) {
  	kvec[i%K].push_back(ratings[i]);
  }

  rep(i, K) {
  	rep(j, kvec[i].size()) {
  		if(kvec[i][j] == me) {
  			return j;
  		}
  	}
  }

}