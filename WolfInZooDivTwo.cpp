#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
vector<string> L;
vector<string> R;
// ------------- input end  --------------
int solve(); struct WolfInZooDivTwo {int count(int _N, vector<string> _L, vector<string> _R){N = _N, L = _L, R = _R;return ::solve();}};

vector<int> to_ints(vector<string> const& v) {
  stringstream ss;
  vector<int> ret;
  for(auto && e: v) ss << e;
  for(string s;ss >> s;) ret.push_back(stoi(s)+1);
  return ret;
}

int dp[333][333]; // [iマス目][最後に置いた座標j>=1]

int const MOD = 1e9+7;

int solve() {

  auto ls = to_ints(L);
  auto rs = to_ints(R);

  int M = ls.size();

  zero(dp);
  dp[0][0] = 1;

  REP(i, 1, N+1) {
    rep(j, i) {

      // 置く
      dp[i][i] += dp[i-1][j];
      dp[i][i] %= MOD;

      // 置かない
      bool ng = 0;
      rep(k, M) {
        if(j+1 <= ls[k] && rs[k] <= i) {
          ng = 1;
        }
      }
      if(!ng) {
        dp[i][j] = dp[i-1][j];
      }
      else {
        dp[i][j] = 0;
      }
    }
  }

  int ans = 0;
  rep(i, N+1) {
    ans += dp[N][i];
    ans %= MOD;
  }
  return ans;
}

// CUT begin
using namespace std;
    char do_test(int,vector<string>,vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            vector<string> L;
            vector<string> R;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 5;
                                        L = { "0 1" };
                                        R = { "2 4" };
                              
                    __expected = 27;
                    break;
        }
                case 1: {
                              N = 10;
                                        L = { "0 4 2 7" };
                                        R = { "3 9 5 9" };
                              
                    __expected = 798;
                    break;
        }
                case 2: {
                              N = 100;
                                        L = { "0 2 2 7 10 1","3 16 22 30 33 38"," 42 44 49 51 57 60 62"," 65 69 72 74 77 7","8 81 84 88 91 93 96" };
                                        R = { "41 5 13 22 12 13 ","33 41 80 47 40 ","4","8 96 57 66 ","80 60 71 79"," 70 77 ","99"," 83 85 93 88 89 97 97 98" };
                              
                    __expected = 250671525;
                    break;
        }
                case 3: {
                              N = 3;
                                        L = { "1" };
                                        R = { "2" };
                              
                    __expected = 6;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  L = ;
                  R = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, L, R, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, vector<string> L, vector<string> R, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< L << ","<< R  << "]" << endl;
   
    WolfInZooDivTwo *__instance = new WolfInZooDivTwo();
  int __result = __instance->count(N, L, R);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "WolfInZooDivTwo: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
