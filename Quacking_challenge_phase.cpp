#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(c) memset(c, 0, sizeof c)
#define minus(c) memset(c, -1, sizeof c)

using ll  = long long;
int const inf = 1<<29;

template<class T>
string make_vector_string(vector<T> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += to_string(v[i]);
  }
  res += '}';
  return res;
}

template<>
string make_vector_string(vector<string> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += "\"" + v[i] + "\"";  // need quote?
  }
  res += '}';
  return res;
}

template<class T>
string make_2d_vector_string(vector<vector<T>> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += make_vector_string(v[i]);
  }
  res += '}';
  return res;
}

string make_string_string(string const& str) {
  return str;
//  return "\"" + str + "\"";
}

template<class T> ostream& operator << (ostream& ost, vector<T>& v) {
  return ost << make_vector_string(v);
}

template<class T> ostream& operator << (ostream& ost, vector<vector<T>>& v) {
  return ost << make_2d_vector_string(v);
}

random_device random_engine;

int make_random_int(int lower, int upper) {
  mt19937 mt(random_engine());
  uniform_int_distribution<> res(lower, upper);
  return res(mt);
}

int make_random_double(double lower, double upper) {
  mt19937 mt(random_engine());
  uniform_real_distribution<> res(lower, upper);
  return res(mt);
}

// Here making challenge case code.

int main() {

  string S = string(500,'q') + string(500,'u') + string(500,'a') + string(500,'c') + string(500,'k');
  cout << S << endl;
  cout << endl;
  srand(unsigned(time(NULL)));
  while(1) {
    random_shuffle(all(S));

    int n = S.size();
    const string quack = "quack";
    int state[4] = {};
    int depth = 0;
    int maxdepth = 0;
    rep(i, n) {
      int pos = quack.find(S[i]);
      if(pos == 4 && !state[3]) {
        goto ng;
      }
      if(pos == 4) {
        state[3] --;
        state[2] --;
        state[1] --;
        state[0] --;
        depth --;
        continue;
      }
      if(pos == 0) {
        depth ++;
        maxdepth = max(maxdepth, depth);
        state[0] ++;
      }
      else if(state[pos-1] > state[pos]) {
        state[pos] ++;
      }
      else {
        REP(k, i+1, n) {
          if(S[k] == quack[pos-1]) {
            swap(S[i], S[k]);
            break;
          }
        }
        i --;
      }
    }

    cout << S << endl;
    break;

    ng:;
  }

  return 0;
}

