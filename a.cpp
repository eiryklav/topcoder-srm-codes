#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(c) memset(c, 0, sizeof c)
#define minus(c) memset(c, -1, sizeof c)

using ll  = long long;
int const inf = 1<<29;

int const MOD9 = 1e9+9;

ll comb[1111][1111];
void make_comb(int n) {
  rep(i, n+1) {
    comb[i][0] = 1;
    REP(j, 1, i+1) {
      comb[i][j] = comb[i-1][j-1] + comb[i-1][j];
      comb[i][j] %= MOD9;
    }
  }
}

int main() {


  make_comb(1100);

  ll f[27]; f[0] = 1;
  REP(i, 1, 27) {
    f[i] = f[i-1] * i;
    f[i] %= MOD9;
  }

  ll L;
  while(cin >> L) {
    if(L < 27) {
      cout << (comb[26][L] * f[L]) % MOD9 << endl;
    }
    else {
      cout << ((f[26] * comb[L-26+25][25]) % MOD9) << endl;
    }
  }

  return 0;
}