#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define chmin(a, x) a = min(a, x)
#define chmax(a, x) a = max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> magicalGirlStrength;
vector<int> enemyStrength;
vector<int> enemyCount;
// ------------- input end  --------------
int solve(); struct SpaceWarDiv2 { int minimalFatigue(vector<int> _magicalGirlStrength, vector<int> _enemyStrength, vector<int> _enemyCount) { magicalGirlStrength = _magicalGirlStrength, enemyStrength = _enemyStrength, enemyCount = _enemyCount; return solve(); }};

int N, M;

int solve() {

  N = magicalGirlStrength.size();
  M = enemyStrength.size();

  sort(magicalGirlStrength.rbegin(), magicalGirlStrength.rend());

  vector<pair<int, int>> v_;
  rep(i, M) {
    v_.emplace_back(enemyStrength[i], enemyCount[i]);
  }

  sort(v_.rbegin(), v_.rend());

  REP(time, 1, 5001) {
    auto v = v_;
    int enemy = 0;
    int T = time;
    int girl = 0;
    while(girl < N && enemy < M) {
      if(v[enemy].first > magicalGirlStrength[girl]) { goto ng; }
      int remain = max(0, v[enemy].second - T);
      if(remain == 0) {
        T -= v[enemy].second;
        v[enemy].second = 0;
      }
      else if(remain > 0) {
        v[enemy].second -= T;
        T = 0;
      }
      if(v[enemy].second == 0) {
        enemy ++;
      }
      if(T == 0) {
        T = time;
        girl++;
      }
    }

    if(v.back().second == 0) {
      return time;
    }
    
    ng:;
  }

  return -1;
}