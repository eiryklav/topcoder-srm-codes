#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> A;
vector<int> B;
int k;
// ------------- input end  --------------
int solve(); struct FoxConnection2 {int ways(vector<int> _A, vector<int> _B, int _k){A = _A, B = _B, k = _k;return solve();}};

int const MOD = 1e9+7;

int N;
vector<int> G[55];
ll memo[55][55][55];

ll dfs(int curr, int par, int leftMost, int K) {
  auto& ret = memo[curr][leftMost][K];
  if(ret >= 0) { return ret; }
  if(K == 0) { return 1; }
  if(leftMost >= G[curr].size()) { return K == 1; }
  int next = G[curr][leftMost];
  if(next == par) { return ret = dfs(curr, par, leftMost+1, K); }

  ret = 0;
  rep(k, K) {
    ret += dfs(next, curr, 0, k) * dfs(curr, par, leftMost+1, K-k) % MOD;
    ret %= MOD;
  }

  return ret;
}


ll decideRootOfTree(int curr, int par, int K) {
  ll ret = dfs(curr, par, 0, K);
  for(auto next: G[curr]) {
    if(next == par) { continue; }
    ret += decideRootOfTree(next, curr, K);
    ret %= MOD;
  }
  return ret;
}

int solve() {

  rep(i, 55) G[i].clear();

  N = A.size();
  rep(i, N) {
    A[i] --, B[i] --;
    G[A[i]].push_back(B[i]);
    G[B[i]].push_back(A[i]);
  }

  minus(memo);

  return decideRootOfTree(0, -1, k);
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> A;
            vector<int> B;
            int k;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = { 1,2,3 };
                                        B = { 2,3,4 };
                                        k = 2;
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              A = { 1,1,1,1 };
                                        B = { 2,3,4,5 };
                                        k = 3;
                              
                    __expected = 6;
                    break;
        }
                case 2: {
                              A = { 1,2,3,4 };
                                        B = { 2,3,4,5 };
                                        k = 3;
                              
                    __expected = 3;
                    break;
        }
                case 3: {
                              A = { 1,2,2,4,4 };
                                        B = { 2,3,4,5,6 };
                                        k = 3;
                              
                    __expected = 6;
                    break;
        }
                case 4: {
                              A = { 1,2,2,4,4 };
                                        B = { 2,3,4,5,6 };
                                        k = 5;
                              
                    __expected = 4;
                    break;
        }
                case 5: {
                              A = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
                                        B = { 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40 };
                                        k = 20;
                              
                    __expected = 923263934;
                    break;
        }
                case 6: {
                              A = { 2 };
                                        B = { 1 };
                                        k = 1;
                              
                    __expected = 2;
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = ;
                  B = ;
                  k = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(A, B, k, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> A, vector<int> B, int k, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << A << ","<< B << ","<< pretty_print(k)  << "]" << endl;
   
    FoxConnection2 *__instance = new FoxConnection2();
  int __result = __instance->ways(A, B, k);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FoxConnection2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
