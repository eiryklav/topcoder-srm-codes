#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(c) memset(c, 0, sizeof c)
#define minus(c) memset(c, -1, sizeof c)

using ll  = long long;
int const inf = 1<<29;

template<class T>
string make_vector_string(vector<T> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += to_string(v[i]);
  }
  res += '}';
  return res;
}

template<>
string make_vector_string(vector<string> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += "\"" + v[i] + "\"";
  }
  res += '}';
  return res;
}

template<class T>
string make_2d_vector_string(vector<vector<T>> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += make_vector_string(v[i]);
  }
  res += '}';
  return res;
}

string make_string_string(string const& str) {
  return "\"" + str + "\"";
}

random_device random_engine;

int make_random_int(int lower, int upper) {
  mt19937 mt(random_engine());
  uniform_int_distribution<> res(lower, upper);
  return res(mt);
}

int make_random_double(double lower, double upper) {
  mt19937 mt(random_engine());
  uniform_real_distribution<> res(lower, upper);
  return res(mt);
}

#ifdef CHALLENGE_PHASE_UNIT_TEST

string argStringToString(string const& argString) {
  string res;
  int openParPos = argString.find("(");
  int firstDoubleQuotePos = argString.find("\"");
  if(openParPos != string::npos && firstDoubleQuotePos != string::npos && firstDoubleQuotePos < openParPos) {
    return argString;
  }
  else if(openParPos != string::npos) {
    bool ok = false;
    rep(i, argString.size()) {
      if(ok) {
        res += argString[i];
      }
      if(argString[i] == '(') {
        ok = 1;
      }
    }
    if(ok) {
      while(res.back() != ')') res = res.substr(0, res.size()-1);
      res = res.substr(0, res.size()-1);
    }
    else {
      return argString;
    }
  }
  else {
    return argString;
  }
  return res;
}

#define TEST(func, arg) { \
  cout << "Case: " << argStringToString(#arg); \
  if(func(arg) == argStringToString(#arg)) {  \
    cout << "\t\t... passed" << endl;  \
  } \
  else {  \
    cout << "\t\t... failed" << endl; \
    cout << func(arg) << endl;  \
    assert(false);  \
  } \
}

int main() {

  TEST(make_vector_string, vector<string>({"3","3","4","114514"}))
  TEST(make_vector_string, vector<int>({114514,364364,334}))
  TEST(make_vector_string, vector<double>({114.514000,364364.000000,8.100000}))
  TEST(make_2d_vector_string, vector<vector<string>>({{"114514","810"},{"1919","893"}}))
  TEST(make_2d_vector_string, vector<vector<int>>({{114514},{810,1919,364364}}))

  TEST(make_string_string, "114514")
  TEST(make_string_string, "(()()()())))))))(((((((()")
  TEST(make_string_string, ("()())())))))))))))"))
  
  {
    cout << "make_random_int(1, 6): ";
    int num[7] = {};
    rep(i, 100) {
      auto random_int = make_random_int(1, 6);
      assert(1 <= random_int && random_int <= 6);
      num[random_int] ++;
    }
    assert(num[0] == 0);
    REP(i, 1, 7) assert(num[i] > 0), cout << num[i] << " ";
    cout << "\t\t... passed" << endl;
  }

  {
    cout << "make_random_double(1.0, 6.0): ";
    rep(i, 100) {
      auto random_double = make_random_double(1, 6);
      assert(1 <= random_double && random_double <= 6);
    }
    cout << "\t\t... passed" << endl;
  }

  return 0;
}
#else





// Here making challenge case code.

int main() {

  return 0;
}










#endif
