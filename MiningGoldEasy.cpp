#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
int M;
vector<int> event_i;
vector<int> event_j;
// ------------- input end  --------------
int solve(); struct MiningGoldEasy {int GetMaximumGold(int _N, int _M, vector<int> _event_i, vector<int> _event_j){N = _N, M = _M, event_i = _event_i, event_j = _event_j;return solve();}};

int solve() {

  int size = event_i.size();
  int dp[55][55][55] = {};
  // 必ずどこかの金がある位置に行または列を揃える。
  // 金のある行と列のみ考慮した座標圧縮をして、50*50マスのどこかに必ずいるべきと考える。
  // あとは普通のグリッドDPをすればよい。
  rep(i, size) rep(j, size) {
    dp[0][i][j] = N + M - abs(event_i[0] - event_i[i]) - abs(event_j[0] - event_j[j]);
  }

  rep(n, size-1) rep(k, size) rep(i, size) rep(j, size) {
    maximize(dp[n+1][k][j], dp[n][i][j] + N + M - abs(event_i[n+1] - event_i[k]) - abs(event_j[n+1] - event_j[j]));
    maximize(dp[n+1][i][k], dp[n][i][j] + N + M - abs(event_i[n+1] - event_i[i]) - abs(event_j[n+1] - event_j[k]));
  }

  int max = 0;
  rep(i, size) rep(j, size) {
    maximize(max, dp[size-1][i][j]);
  }

  return max;
}

// CUT begin
using namespace std;
    char do_test(int,int,vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            int M;
            vector<int> event_i;
            vector<int> event_j;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 2;
                                        M = 2;
                                        event_i = { 0 };
                                        event_j = { 0 };
                              
                    __expected = 4;
                    break;
        }
                case 1: {
                              N = 2;
                                        M = 2;
                                        event_i = { 0,2 };
                                        event_j = { 0,1 };
                              
                    __expected = 7;
                    break;
        }
                case 2: {
                              N = 3;
                                        M = 3;
                                        event_i = { 0,3,3 };
                                        event_j = { 0,3,0 };
                              
                    __expected = 15;
                    break;
        }
                case 3: {
                              N = 3;
                                        M = 4;
                                        event_i = { 0,3 };
                                        event_j = { 4,1 };
                              
                    __expected = 11;
                    break;
        }
                case 4: {
                              N = 5;
                                        M = 6;
                                        event_i = { 0,4,2,5,0 };
                                        event_j = { 3,4,5,6,0 };
                              
                    __expected = 48;
                    break;
        }
                case 5: {
                              N = 557;
                                        M = 919;
                                        event_i = { 81,509,428,6,448,149,77,142,40,405,109,247 };
                                        event_j = { 653,887,770,477,53,637,201,863,576,393,512,243 };
                              
                    __expected = 16255;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  M = ;
                  event_i = ;
                  event_j = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, M, event_i, event_j, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, int M, vector<int> event_i, vector<int> event_j, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< pretty_print(M) << ","<< event_i << ","<< event_j  << "]" << endl;
   
    MiningGoldEasy *__instance = new MiningGoldEasy();
  int __result = __instance->GetMaximumGold(N, M, event_i, event_j);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "MiningGoldEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
