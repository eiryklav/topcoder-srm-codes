#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
int K;
// ------------- input end  --------------
int solve(); struct AlienAndSetDiv2 {int getNumber(int _N, int _K){N = _N, K = _K;return solve();}};

int const MOD = 1e9+7;

int Trim;
int dp[55][55][1<<10];

int push(int S, int x) {
  S <<= 1; S += x;
  S &= Trim;
  return S;
};

int get(int S, int i, int j) {

}

int solve() {

  Trim = (1<<K) - 1;

  zero(dp);

  rep(i, N) rep(j, N) rep(hist, 1<<K) {
    if(i == j) {
      int nhist;
      nhist = push(hist, 0); dp[i+1][j][nhist] += dp[i][j][hist]; dp[i+1][j][nhist] %= MOD;
      nhist = push(hist, 1); dp[i][j+1][nhist] += dp[i][j][hist]; dp[i][j+1][nhist] %= MOD;
    }
    if(i < j) {
      int nhist;
      {
        nhist = push(hist, 0);
        if()
      }
      nhist = push(hist, 1); dp[i][j+1][nhist] += dp[i][j][hist]; dp[i][j+1][nhist] %= MOD;
    }
    if(i > j) {

    }
  }

  ll res = 0;
  rep(i, 1<<10) {
    res += dp[N][N][i];
    res %= MOD;
  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            int K;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 2;
                                        K = 1;
                              
                    __expected = 4;
                    break;
        }
                case 1: {
                              N = 3;
                                        K = 1;
                              
                    __expected = 8;
                    break;
        }
                case 2: {
                              N = 4;
                                        K = 2;
                              
                    __expected = 44;
                    break;
        }
                case 3: {
                              N = 10;
                                        K = 10;
                              
                    __expected = 184756;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  K = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, K, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, int K, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< pretty_print(K)  << "]" << endl;
   
    AlienAndSetDiv2 *__instance = new AlienAndSetDiv2();
  int __result = __instance->getNumber(N, K);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "AlienAndSetDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
