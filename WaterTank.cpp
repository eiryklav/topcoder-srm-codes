#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> t;
vector<int> x;
int C;
// ------------- input end  --------------
double solve(); struct WaterTank {double minOutputRate(vector<int> _t, vector<int> _x, int _C){t = _t, x = _x, C = _C;return solve();}};

double solve() {
  return 0.0;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> t;
            vector<int> x;
            int C;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              t = { 3,3 };
                                        x = { 1,2 };
                                        C = 3;
                              
                    __expected = 0.9999999999999999;
                    break;
        }
                case 1: {
                              t = { 1,2,3,4,5 };
                                        x = { 5,4,3,2,1 };
                                        C = 10;
                              
                    __expected = 1.9999999999999996;
                    break;
        }
                case 2: {
                              t = { 5949,3198,376,3592,4019,3481,5609,3840,6092,4059 };
                                        x = { 29,38,96,84,10,2,39,27,76,94 };
                                        C = 1000000000;
                              
                    __expected = 0.0;
                    break;
        }
                case 3: {
                              t = { 9,3,4,8,1,2,5,7,6 };
                                        x = { 123,456,789,1011,1213,1415,1617,1819,2021 };
                                        C = 11;
                              
                    __expected = 2019.1666666666665;
                    break;
        }
                case 4: {
                              t = { 100 };
                                        x = { 1000 };
                                        C = 12345;
                              
                    __expected = 876.55;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    t = ;
                  x = ;
                  C = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(t, x, C, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> t, vector<int> x, int C, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << t << ","<< x << ","<< pretty_print(C)  << "]" << endl;
   
    WaterTank *__instance = new WaterTank();
  double __result = __instance->minOutputRate(t, x, C);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "WaterTank: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
