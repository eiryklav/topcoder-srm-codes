#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct MarblePositioning {double totalWidth(vector <int> radius);};

double MarblePositioning::totalWidth(vector <int> radius) {
	sort(all(radius));
	int N = radius.size();
	long double ans = Inf;
	do {
		vector<long double> positions;
		rep(i, N) {
			long double x = 0.0;
			rep(j, i) {
				long double px = positions[j];
				long double s = radius[i] + radius[j]; s *= s;
				long double h = abs(radius[i] - radius[j]); h *= h;
				long double w = sqrt(s - h);
				x = max(x, px + w);
			}
			positions.push_back(x);
		}
		ans = min(ans, positions.back());
	} while(next_permutation(all(radius)));
	return ans;
}
