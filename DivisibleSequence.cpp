#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

int const MOD9 = 1e9+9;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct DivisibleSequence {int count(int N, int H);};

ll mod_pow(ll x, ll n, ll mod) {
	ll ret = 1;
	while(n > 0) {
		if(n & 1) (ret *= x) %= mod;
		x = x * x % mod;
		n >>= 1;
	}
	return ret;
}

/*
  mod_inverseは、通常extgcdを用いて計算する。
  modが素数の時はフェルマーの小定理より、
  a^(p-1) ≡ 1 (mod p) i.e. a^(-1) ≡ a^(p-2) (mod p)
  よって、逆元をMOD-2を次数とする繰り返し二乗法で求めることが出来る
*/
ll mod_inverse(ll x, ll mod) {
	return mod_pow(x, mod-2, mod);
}

ll mod_comb(ll n, int k, ll mod) {
	if(k > n) { return 0; }
	ll p = 1, q = 1;
	REP(i, 1, k+1) {
		(q *= i) %= mod;
		(p *= n-i+1) %= mod;
	}
	return (p * mod_inverse(q, mod)) % mod;
}

int DivisibleSequence::count(int N, int H) {
  ll ret = 1;
  for(int i=2; (ll)i*i<=N; i++) {
  	int c = 0;
  	while(N % i == 0) {
  		N /= i;
  		c++;
  	}
  	(ret *= mod_comb(H-1+c, c, MOD9)) %= MOD9;
  }
  if(N > 1) {
  	(ret *= H) %= MOD9;
  }
  return ret;
}
