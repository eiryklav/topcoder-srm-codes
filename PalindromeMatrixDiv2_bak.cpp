#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> A;
int rowCount;
int columnCount;
// ------------- input end  --------------
int solve(); struct PalindromeMatrixDiv2 {int minChange(vector<string> _A, int _rowCount, int _columnCount){A = _A, rowCount = _rowCount, columnCount = _columnCount;return solve();}};

struct UnionFind
{
  vector<int> rank;
  vector<int> rid;

  UnionFind(int n) {
    rank.resize(n);
    rid.assign(n, -1);
  }

  void unite(int u, int v) {
    u = root(u), v = root(v);
    if(u == v) { return ; }
    if(rank[u] < rank[v]) {
      rid[u] = v;
    }
    else {
      rid[v] = u;
      if(rank[u] == rank[v]) {
        rank[u]++;
      }
    }
  }

  bool same(int u, int v) {
    return root(u) == root(v);
  }

  int root(int x) {
    if(rid[x] < 0) return x;
    else return rid[x] = root(rid[x]);
  }
};

int solve() {

  int H = A.size(), W = A[0].size();
  int res = inf;

  rep(SH, 1<<H) rep(SW, 1<<W) {
    if(__builtin_popcount(SH) < rowCount) { continue; }
    if(__builtin_popcount(SW) < columnCount) { continue; }

    UnionFind uf(H * W + 1);
    int zero[64] = {}, one[64] = {};

    int cand = 0;
    rep(i, H) {
      if(SH >> i & 1) {
        rep(j, W) {
          uf.unite(i * W + j, i * W + (W - 1 - j));
        }
      }
    }
    rep(i, W) {
      if(SW >> i & 1) {
        rep(j, H) {
          uf.unite(j * W + i, (H - 1 - j) * W + i);
        }
      }
    }

    rep(i, H) rep(j, W) {
      int r = uf.root(i * W + j);
      if(A[i][j] == '0') { zero[r]++; }
      else { one[r] ++; }
    }

    rep(i, 64) {
      cand += min(zero[i], one[i]);
    }

    minimize(res, cand);

  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> A;
            int rowCount;
            int columnCount;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = { "0000","1000","1100","1110" };
                                        rowCount = 2;
                                        columnCount = 2;
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              A = { "0000","1000","1100","1110" };
                                        rowCount = 3;
                                        columnCount = 2;
                              
                    __expected = 3;
                    break;
        }
                case 2: {
                              A = { "01","10" };
                                        rowCount = 1;
                                        columnCount = 1;
                              
                    __expected = 1;
                    break;
        }
                case 3: {
                              A = { "1110","0001" };
                                        rowCount = 0;
                                        columnCount = 0;
                              
                    __expected = 0;
                    break;
        }
                case 4: {
                              A = { "01010101","01010101","01010101","01010101","01010101","01010101","01010101","01010101" };
                                        rowCount = 2;
                                        columnCount = 2;
                              
                    __expected = 8;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = ;
                  rowCount = ;
                  columnCount = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(A, rowCount, columnCount, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> A, int rowCount, int columnCount, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << A << ","<< pretty_print(rowCount) << ","<< pretty_print(columnCount)  << "]" << endl;
   
    PalindromeMatrixDiv2 *__instance = new PalindromeMatrixDiv2();
  int __result = __instance->minChange(A, rowCount, columnCount);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "PalindromeMatrixDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
