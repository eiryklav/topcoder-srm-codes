#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> chocolate;
// ------------- input end  --------------
int solve(); struct ChocolateDividingHard {int findBest(vector<string> _chocolate){chocolate = _chocolate;return ::solve();}};

int solve() {
  int N = chocolate.size();
  int M = chocolate[0].size();
  vector<vector<int>> G(N, vector<int>(M));
  rep(i, N) rep(j, M) {
    G[i][j] = chocolate[i][j] - '0';
  }

  int sum[100][100] = {};
  rep(i, N) rep(j, M) {
    sum[i][j] = G[i][j] + (j ? sum[i][j-1] : 0) + (i ? sum[i-1][j] : 0) - (i && j ? sum[i-1][j-1] : 0);
  }

  auto csum = [&](int i, int j, int k, int l) {
    return sum[k][l] - (i ? sum[i-1][l] : 0) - (j ? sum[k][j-1] : 0) + (i && j ? sum[i-1][j-1] : 0);
  };

//  while(1) { int i, j, k, l; cin >> i >> j >> k >> l; cout << csum(i, j, k, l) << endl; }

  int L = 0, R = 1000000;
  while(1) {
    int value = (L + R) / 2;
    int OL = L, OR = R;
    bool good = 0;

    REP(a, 1, N) REP(b, a+1, N) REP(c, b+1, N) {
      int pos = 0;
      int turn = 0;
      while(1) {
        int X = 0, Y = 0, Z = 0, W = 0;
        bool ok2 = 0;
        for(; pos < M; pos++) {
          bool ok = 1;
          X += csum(0, pos, a-1, pos);
          ok = ok && X >= value;
          Y += csum(a, pos, b-1, pos);
          ok = ok && Y >= value;
          Z += csum(b, pos, c-1, pos);
          ok = ok && Z >= value;
          W += csum(c, pos, N-1, pos);
          ok = ok && W >= value;
          if(ok) {
            ok2 = 1;
            pos++;
            break;
          }
        } // for pos
        if(ok2) {
          turn ++;
          if(turn == 4) { good = 1; break; }
        }
        else {
          break;
        }
      } // while
    } // reps

    if(good) {
      L = value;
    }
    else {
      R = value;
    }
    if(OL == L && OR == R) { break; }
  }

  return L;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> chocolate;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              chocolate = { "95998","21945","23451","99798","74083" };
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              chocolate = { "12942","23456","99798","98998","67675" };
                              
                    __expected = 5;
                    break;
        }
                case 2: {
                              chocolate = { "129420","234560","997980","989980","676760" };
                              
                    __expected = 6;
                    break;
        }
                case 3: {
                              chocolate = { "75356291270936062","61879202375922897","36129319478450361","06320615547656937","45254744307868843","14920689266495048","71727226106159490","91771159776736563","94812939088509638","56115984810304444","76317596217857418","59753883189643338" };
                              
                    __expected = 44;
                    break;
        }
                case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    chocolate = {
                    "1111",
                    "1111",
                    "1111",
                    "1111"
                    };
              
          __unknownAnswer = true; 
          break;
        }
    
    default: return 'm';
  }
  return do_test(chocolate, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> chocolate, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << chocolate  << "]" << endl;
   
    ChocolateDividingHard *__instance = new ChocolateDividingHard();
  int __result = __instance->findBest(chocolate);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ChocolateDividingHard: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
