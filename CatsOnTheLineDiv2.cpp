#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> position;
vector<int> cnt;
int T;
// ------------- input end  --------------
string solve(); struct CatsOnTheLineDiv2 {string getAnswer(vector<int> _position, vector<int> _count, int _time){position = _position, cnt = _count, T = _time;return ::solve();}};

string solve() {

  vector<pair<int, int>> vs;
  rep(i, position.size()) {
    vs.emplace_back(position[i], cnt[i]);
  }

  sort(all(vs));

  int center = (vs[0].first + vs.back().second) / 2;  // odd / 2 のとき？
  int L = -inf, R = inf;
  rep(i, vs.size()) {
    if(vs[i].first < center) {
      int lmost = -T + vs[i].second - 1;
      maximize(L, lmost);
    }
    else {
      int rmost = T - vs[i].second + 1;
      minimize(R, rmost);
    }
  }

  return "";
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> position;
            vector<int> count;
            int time;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              position = { 0 };
                                        count = { 7 };
                                        time = 3;
                              
                    __expected = "Possible";
                    break;
        }
                case 1: {
                              position = { 0 };
                                        count = { 8 };
                                        time = 2;
                              
                    __expected = "Impossible";
                    break;
        }
                case 2: {
                              position = { 0,1 };
                                        count = { 3,1 };
                                        time = 0;
                              
                    __expected = "Impossible";
                    break;
        }
                case 3: {
                              position = { 5,0,2 };
                                        count = { 2,3,5 };
                                        time = 2;
                              
                    __expected = "Impossible";
                    break;
        }
                case 4: {
                              position = { 5,1,-10,7,12,2,10,20 };
                                        count = { 3,4,2,7,1,4,3,4 };
                                        time = 6;
                              
                    __expected = "Possible";
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    position = ;
                  count = ;
                  time = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(position, count, time, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> position, vector<int> count, int time, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << position << ","<< count << ","<< pretty_print(time)  << "]" << endl;
   
    CatsOnTheLineDiv2 *__instance = new CatsOnTheLineDiv2();
  string __result = __instance->getAnswer(position, count, time);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "CatsOnTheLineDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
