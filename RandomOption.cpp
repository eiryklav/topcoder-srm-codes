#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int keyCount;
vector<int> badLane1;
vector<int> badLane2;
// ------------- input end  --------------
double solve(); struct RandomOption {double getProbability(int _keyCount, vector<int> _badLane1, vector<int> _badLane2){keyCount = _keyCount, badLane1 = _badLane1, badLane2 = _badLane2;return ::solve();}};

double dp[1<<15][15];
bool invalid[15][15];

double dfs(int used, int last) {
  if(last == -1) { last = 14; }
  auto& ret = dp[used][last];

  if(ret >= 0) { return ret; }
  if(used == (1 << keyCount) - 1) { return ret = 1.0; }

  ret = 0.0;

  rep(tap, keyCount) {
    if(used >> tap & 1) { continue; }
    if(invalid[last][tap]) { continue; }
    ret += dfs(used | (1 << tap), tap);
  }

  ret /= keyCount - __builtin_popcount(used);

  return ret;
}

double solve() {

  minus(dp);
  zero(invalid);
  rep(i, badLane1.size()) {
    invalid[badLane1[i]][badLane2[i]] = invalid[badLane2[i]][badLane1[i]] = 1;
  }

  return dfs(0, -1);
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int keyCount;
            vector<int> badLane1;
            vector<int> badLane2;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              keyCount = 5;
                                        badLane1 = { 0 };
                                        badLane2 = { 3 };
                              
                    __expected = 0.6;
                    break;
        }
                case 1: {
                              keyCount = 5;
                                        badLane1 = { 0,1,2 };
                                        badLane2 = { 1,2,0 };
                              
                    __expected = 0.1;
                    break;
        }
                case 2: {
                              keyCount = 5;
                                        badLane1 = { 2,2,2,2 };
                                        badLane2 = { 0,1,3,4 };
                              
                    __expected = 0.0;
                    break;
        }
                case 3: {
                              keyCount = 7;
                                        badLane1 = { 0,1,2 };
                                        badLane2 = { 6,5,4 };
                              
                    __expected = 0.3904761904761904;
                    break;
        }
                case 4: {
                              keyCount = 7;
                                        badLane1 = { 3,5,1,0,2,6,4 };
                                        badLane2 = { 0,2,4,6,1,5,3 };
                              
                    __expected = 0.09166666666666667;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    keyCount = ;
                  badLane1 = ;
                  badLane2 = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(keyCount, badLane1, badLane2, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int keyCount, vector<int> badLane1, vector<int> badLane2, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(keyCount) << ","<< badLane1 << ","<< badLane2  << "]" << endl;
   
    RandomOption *__instance = new RandomOption();
  double __result = __instance->getProbability(keyCount, badLane1, badLane2);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "RandomOption: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
