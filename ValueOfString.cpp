#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string s;
// ------------- input end  --------------
int solve(); struct ValueOfString {int findValue(string _s){s = _s;return ::solve();}};

int solve() {

  int N = s.size();
  map<char, int> mp;
  rep(i, N) {
    mp[s[i]] ++;
  }

  int ans = 0;
  rep(i, N) {
    int cnt = 0;
    REP(j, 'a', s[i] + 1) {
      cnt += mp[char(j)];
    }
    ans += cnt * (s[i] - 'a' + 1);
  }

  return ans;
}

// CUT begin
using namespace std;
    char do_test(string,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            string s;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              s = "babca";
                              
                    __expected = 35;
                    break;
        }
                case 1: {
                              s = "zz";
                              
                    __expected = 104;
                    break;
        }
                case 2: {
                              s = "y";
                              
                    __expected = 25;
                    break;
        }
                case 3: {
                              s = "aaabbc";
                              
                    __expected = 47;
                    break;
        }
                case 4: {
                              s = "topcoder";
                              
                    __expected = 558;
                    break;
        }
                case 5: {
                              s = "thequickbrownfoxjumpsoverthelazydog";
                              
                    __expected = 11187;
                    break;
        }
                case 6: {
                              s = "zyxwvutsrqponmlkjihgfedcba";
                              
                    __expected = 6201;
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    s = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(s, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(string s, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(s)  << "]" << endl;
   
    ValueOfString *__instance = new ValueOfString();
  int __result = __instance->findValue(s);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ValueOfString: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
