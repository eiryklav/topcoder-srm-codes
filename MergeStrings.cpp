#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string S;
string A;
string B;
// ------------- input end  --------------
string solve(); struct MergeStrings {string getmin(string _S, string _A, string _B){S = _S, A = _A, B = _B;return solve();}};

int N, M;

string dp[55][55];

constexpr bool equal_to_c(char x, char c) {
  return c == '?' || x == c;
}

const string undef = string(1, 'z'+1);

string rec(int i, int j) {

  auto& ret = dp[i][j];
  if(!ret.empty()) { return ret; }

  ret = undef;

  if(i == N) {
    string t;
    while(j < M) { if(!equal_to_c(B[j], S[i+j])) { return undef; } t += B[j]; j++; }
    return ret = t;
  }

  if(j == M) {
    string t;
    while(i < N) { if(!equal_to_c(A[i], S[i+j])) { return undef; } t += A[i]; i++; }
    return ret = t;
  }

  if(equal_to_c(A[i], S[i+j])) {
    auto r = rec(i+1, j);
    if(r != undef) {
      minimize(ret, A[i] + r);
    }
  }
  if(equal_to_c(B[j], S[i+j])) {
    auto r = rec(i, j+1);
    if(r != undef) {
      minimize(ret, B[j] + r);
    }
  }

  return ret;
}

string solve() {
  N = A.size(), M = B.size();
  rep(i, 55) rep(j, 55) dp[i][j].clear();
  auto r = rec(0, 0);
  return r == undef ? "" : r;
}

// CUT begin
using namespace std;
    char do_test(string,string,string,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            string S;
            string A;
            string B;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              S = "??CC??";
                                        A = "ABC";
                                        B = "BCC";
                              
                    __expected = "ABCCBC";
                    break;
        }
                case 1: {
                              S = "WHAT?";
                                        A = "THE";
                                        B = "WA";
                              
                    __expected = "";
                    break;
        }
                case 2: {
                              S = "PARROT";
                                        A = "PARROT";
                                        B = "";
                              
                    __expected = "PARROT";
                    break;
        }
                case 3: {
                              S = "???????????";
                                        A = "AZZAA";
                                        B = "AZAZZA";
                              
                    __expected = "AAZAZZAAZZA";
                    break;
        }
                case 4: {
                              S = "????K??????????????D???K???K????????K?????K???????";
                                        A = "KKKKKDKKKDKKDDKDDDKDKK";
                                        B = "KDKDDKKKDDKDDKKKDKDKKDDDDDDD";
                              
                    __expected = "KDKDKDKKKDDKDDKKKDKDKKDKDDDKDDDKKDKKKDKKDDKDDDKDKK";
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    S = ;
                  A = ;
                  B = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(S, A, B, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(string S, string A, string B, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(S) << ","<< pretty_print(A) << ","<< pretty_print(B)  << "]" << endl;
   
    MergeStrings *__instance = new MergeStrings();
  string __result = __instance->getmin(S, A, B);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "MergeStrings: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
