#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct AlternateColors {string getColor(long long r, long long g, long long b, long long k);};

vector<string> ans = {"RED", "GREEN", "BLUE"};

string AlternateColors::getColor(long long r, long long g, long long b, long long k) {
	auto mn = min({r, g, b});
	if(mn * 3 < k) {
		k -= mn * 3;
		r -= mn, g -= mn, b -= mn;
		if((r == 0) + (g == 0) + (b == 0) == 2) {
			if(r > 0) return ans[0];
			if(g > 0) return ans[1];
			return ans[2];
		}

		if(r == 0) {
			mn = min(g, b);
			if(mn * 2 < k) {
				g -= mn, b -= mn;
				if(g < b) return ans[2];
				else return ans[1];
			}
			else return k % 2 ? ans[1] : ans[2];
		}
		else if(g == 0) {
			mn = min(r, b);
			if(mn * 2 < k) {
				r -= mn, b -= mn;
				if(r < b) return ans[2];
				else return ans[0];
			}
			else return k % 2 ? ans[0] : ans[2];
		}
		else {
			mn = min(r, g);
			if(mn * 2 < k) {
				r -= mn, g -= mn;
				if(r < g) return ans[1];
				else return ans[0];
			}
			else return k % 2 ? ans[0] : ans[1];
		}
	}
	else {
		return ans[(k-1)%3];
	}
}
