#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct TheNumberGameDiv2 {int minimumMoves(int A, int B);};

int TheNumberGameDiv2::minimumMoves(int A, int B) {
	string a, b;
	{stringstream ss; ss << A;a = ss.str();}
	{stringstream ss; ss << B;b = ss.str();}

	queue<pair<string, int>> q;
	q.emplace(a, 0);
	set<string> st;
	while(!q.empty()) {
		auto p = q.front(); q.pop();
		if(p.first == b) { return p.second; }
		if(st.count(p.first)) { continue; }
		st.insert(p.first);
		if(p.first.size() == 1) { continue; }
		if(p.first.size() > 1) {
			q.emplace(p.first.substr(0, p.first.size()-1), p.second+1);
		}
		reverse(all(p.first));
		q.emplace(p.first, p.second+1);
	}
	return -1;
}