#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> names;
int L;
// ------------- input end  --------------
int solve(); struct SimilarNames2 {int count(vector<string> _names, int _L){names = _names, L = _L;return solve();}};

bool is_prefix(string const& s, string const& t) {
//  if(s.empty()) { return true; }
  if(s.size() >= t.size()) { return false; }
  if(t.substr(0, s.size()) == s) {
    return true;
  }
  return false;
}

int const MOD = 1e9+7;

ll fact[51];

int N;
ll dp[55][55];

ll dfs(int curr, int l) {
  auto& ret = dp[curr][l];
  if(ret >= 0) { return ret; }

  if(l == 1) { return ret = 1; }

  ret = 0;

  rep(i, N) {
    if(curr == i) { continue; }
    if(is_prefix(names[curr], names[i])) {
      ret += dfs(i, l-1);
      ret %= MOD;
    }
  }

  return ret;
}

int solve() {

  N = names.size();
  minus(dp);

  fact[0] = 1;
  rep(i, 50) {
    fact[i+1] = fact[i] * (i+1);
    fact[i+1] %= MOD;
  }

  ll res = 0;

  rep(i, N) {
    dfs(i, L);
  }

  rep(i, N) {
    if(dp[i][L] > 0) {
      res += dp[i][L] * fact[N-L] % MOD;
      res %= MOD;
    }
  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> names;
            int L;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              names = { "kenta","kentaro","ken" };
                                        L = 2;
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              names = { "hideo","hideto","hideki","hide" };
                                        L = 2;
                              
                    __expected = 6;
                    break;
        }
                case 2: {
                              names = { "aya","saku","emi","ayane","sakura","emika","sakurako" };
                                        L = 3;
                              
                    __expected = 24;
                    break;
        }
                case 3: {
                              names = { "taro","jiro","hanako" };
                                        L = 2;
                              
                    __expected = 0;
                    break;
        }
                case 4: {
                              names = { "alice","bob","charlie" };
                                        L = 1;
                              
                    __expected = 6;
                    break;
        }
                case 5: {
                              names = { "ryota","ryohei","ryotaro","ryo","ryoga","ryoma","ryoko","ryosuke","ciel","lun","ryuta","ryuji","ryuma","ryujiro","ryusuke","ryutaro","ryu","ryuhei","ryuichi","evima" };
                                        L = 3;
                              
                    __expected = 276818566;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    names = ;
                  L = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(names, L, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> names, int L, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << names << ","<< pretty_print(L)  << "]" << endl;
   
    SimilarNames2 *__instance = new SimilarNames2();
  int __result = __instance->count(names, L);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "SimilarNames2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
