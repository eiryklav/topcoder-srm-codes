#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string S;
string T;
// ------------- input end  --------------
int solve(); struct CombinationLockDiv2 {int minimumMoves(string _S, string _T){S = _S, T = _T;return solve();}};

static const int MAX_N = 50;
static const int MAX_OP = 450;
vector<int> d;
int n;
int dp[MAX_N + 1][MAX_OP + 1][2];
int N;

int rec(int index, int numberOfChanges, int direction) {
  auto& ret = dp[index][numberOfChanges][direction];
  if(ret >= 0) { return ret; }
  if(index == N) { return 0; }

  ret = inf;

  rep(nextNumberOfChanges, MAX_OP + 1) rep(nextDirection, 2) {
    if(nextDirection == 0 && (T[index] - S[index] + nextNumberOfChanges) % 10) {
      continue;
    }
    if(nextDirection == 1 && (S[index] - T[index] + nextNumberOfChanges) % 10) {
      continue;
    }

    if(nextDirection == direction) {
      minimize(ret, rec(index + 1, nextNumberOfChanges, nextDirection) + max(0, nextNumberOfChanges - numberOfChanges));
    }
    else {
      minimize(ret, rec(index + 1, nextNumberOfChanges, nextDirection) + nextNumberOfChanges);
    }
  }

  return ret;
}

int solve() {
  N = S.size();
  minus(dp);
  return rec(0, 0, 0);
}

// CUT begin
using namespace std;
    char do_test(string,string,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            string S;
            string T;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              S = "123";
                                        T = "112";
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              S = "1";
                                        T = "7";
                              
                    __expected = 4;
                    break;
        }
                case 2: {
                              S = "607";
                                        T = "607";
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              S = "1234";
                                        T = "4567";
                              
                    __expected = 3;
                    break;
        }
                case 4: {
                              S = "020";
                                        T = "909";
                              
                    __expected = 2;
                    break;
        }
                case 5: {
                              S = "4423232218340";
                                        T = "6290421476245";
                              
                    __expected = 18;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    S = ;
                  T = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(S, T, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(string S, string T, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(S) << ","<< pretty_print(T)  << "]" << endl;
   
    CombinationLockDiv2 *__instance = new CombinationLockDiv2();
  int __result = __instance->minimumMoves(S, T);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "CombinationLockDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
