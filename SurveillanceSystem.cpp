#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define chmin(a) a = min(a, x)
#define chmax(a) a = max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string containers;
vector<int> reports;
int L;
// ------------- input end  --------------
string solve(); struct SurveillanceSystem { string getContainerInfo(string _containers, vector<int> _reports, int _L) { containers = _containers, reports = _reports, L = _L; return solve(); }};

int N, M;
map<int, int> reportsMap;
string res;

void draw(int xnum, vector<int> const& marks, int diff) {
  rep(i, N-L+1) {
    if(xnum != count(containers.begin()+i, containers.begin()+i+L, 'X')) { continue; }
    REP(j, i, i+L) {
      if(res[j] == '+') { continue; }
      res[j] = '?';
      if(marks[j] >= diff+1) {
        res[j] = '+';
      }
    }
  }
}

string solve() {

  N = containers.size();
  M = reports.size();

  res.clear();
  res.resize(N, '-');

  reportsMap.clear();
  rep(i, M) reportsMap[reports[i]]++;

  for(auto& e: reportsMap) {
    int xnum = e.first, xcnt = e.second;
    int realXCnt = 0;
    vector<int> marks(N);
    rep(i, N-L+1) {
      if(xnum == count(containers.begin() + i, containers.begin() + i+L, 'X')) {
        realXCnt++;
        REP(j, i, i+L) marks[j] ++;
      }
    }
    draw(xnum, marks, realXCnt-xcnt);
  }

  return res;
}
