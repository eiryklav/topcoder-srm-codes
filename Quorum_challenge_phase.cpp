#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(c) memset(c, 0, sizeof c)
#define minus(c) memset(c, -1, sizeof c)

using ll  = long long;
int const inf = 1<<29;

template<class T>
string make_vector_string(vector<T> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += to_string(v[i]);
  }
  res += '}';
  return res;
}

template<>
string make_vector_string(vector<string> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += "\"" + v[i] + "\"";  // need quote?
  }
  res += '}';
  return res;
}

template<class T>
string make_2d_vector_string(vector<vector<T>> const& v) {
  string res;
  res += '{';
  rep(i, v.size()) {
    if(i) res += ',';
    res += make_vector_string(v[i]);
  }
  res += '}';
  return res;
}

string make_string_string(string const& str) {
  return str;
//  return "\"" + str + "\"";
}

template<class T> ostream& operator << (ostream& ost, vector<T>& v) {
  return ost << make_vector_string(v);
}

template<class T> ostream& operator << (ostream& ost, vector<vector<T>>& v) {
  return ost << make_2d_vector_string(v);
}

random_device random_engine;

int make_random_int(int lower, int upper) {
  mt19937 mt(random_engine());
  uniform_int_distribution<> res(lower, upper);
  return res(mt);
}

int make_random_double(double lower, double upper) {
  mt19937 mt(random_engine());
  uniform_real_distribution<> res(lower, upper);
  return res(mt);
}

// Here making challenge case code.

int main() {
  vector<int> v(50);
  rep(i, 50) v[i] = (std::random_device()() % 50) + 1;
  cout << v << endl;

  return 0;
}

