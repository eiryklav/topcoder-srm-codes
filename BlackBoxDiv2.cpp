#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string front;
string side;
// ------------- input end  --------------
int solve(); struct BlackBoxDiv2 {int count(string _front, string _side){front = _front, side = _side;return solve();}};

int const MOD = 1e9+7;

ll comb[1010][1010];
void make_comb(int n) {
  rep(i, n+1) {
    comb[i][0] = 1;
    REP(j, 1, i+1) {
      comb[i][j] = comb[i-1][j-1] + comb[i-1][j];
      comb[i][j] %= MOD;
    }
  }
}

ll pow2[2555];

int solve() {

  make_comb(1000);
  pow2[0] = 1;
  rep(i, 2510) pow2[i+1] = pow2[i] * 2 % MOD;

  int R = 0, C = 0;

  rep(i, front.size())  { C += front[i] == 'B'; }
  rep(i, side.size())   { R += side[i]  == 'B'; }

  ll res = 0;
  rep(rnum, R+1) rep(cnum, C+1) {
    ll pat = pow2[(R - rnum) * (C - cnum)];
    pat = pat * comb[R][rnum] % MOD;
    pat = pat * comb[C][cnum] % MOD;

    bool sign = (rnum + cnum) % 2;
    if(sign) {
      res += MOD - pat;
    }
    else {
      res += pat;
    }
      res %= MOD;
  }
  return res;
}

// CUT begin
using namespace std;
    char do_test(string,string,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            string front;
            string side;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              front = "BB";
                                        side = "BB";
                              
                    __expected = 7;
                    break;
        }
                case 1: {
                              front = "...";
                                        side = ".....";
                              
                    __expected = 1;
                    break;
        }
                case 2: {
                              front = "...";
                                        side = "BBB";
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              front = "...BB.B....";
                                        side = "B.B.B.B.B";
                              
                    __expected = 16081;
                    break;
        }
                case 4: {
                              front = "BBBBB.BBBBBBBBB.BBBBBBB.BBBB.B.BB..BB.B.BBBBBBBBBB";
                                        side = ".B.BBB..BBBBBB.BBBB.B...BBBB.BB.BBBBBBBBBB.....BBB";
                              
                    __expected = 771030153;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    front = ;
                  side = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(front, side, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(string front, string side, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(front) << ","<< pretty_print(side)  << "]" << endl;
   
    BlackBoxDiv2 *__instance = new BlackBoxDiv2();
  int __result = __instance->count(front, side);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BlackBoxDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
