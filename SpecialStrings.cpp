#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string current;
// ------------- input end  --------------
string solve(); struct SpecialStrings {string findNext(string _current){current = _current;return ::solve();}};

bool is_special(string const& s) {
  int n = s.size();
  rep(i, n-1) {
    if(s.substr(0, i+1) >= s.substr(i+1)) { return false; }
  }
  return true;
}

string solve() {
  int N = current.size();
  if(current == "0") { return "1"; }

  // S = U_i V_i を総当りし、'0'の場所をみつける。
  for(int i=N-1; i>0; i--) {  // 先頭は必ず'0' (, 末尾は必ず'0')
    if(current[i] == '1') { continue; }
    current[i] = '1';
    // V_iを最小化する。上から縮められるだけ縮める
    REP(j, i+1, N) {
      if(current[j] == '0') { continue; }
      current[j] = '0';
      if(!is_special(current)) {
        current[j] = '1';
      }
    }
    return current;
  }

  return "";
}

// CUT begin
using namespace std;
    char do_test(string,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            string current;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              current = "01";
                              
                    __expected = "";
                    break;
        }
                case 1: {
                              current = "00101";
                              
                    __expected = "00111";
                    break;
        }
                case 2: {
                              current = "0010111";
                              
                    __expected = "0011011";
                    break;
        }
                case 3: {
                              current = "000010001001011";
                              
                    __expected = "000010001001101";
                    break;
        }
                case 4: {
                              current = "01101111011110111";
                              
                    __expected = "01101111011111111";
                    break;
        }
                case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    current = "0";
              
                    __expected = "1";
          break;
        }
    
    default: return 'm';
  }
  return do_test(current, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(string current, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(current)  << "]" << endl;
   
    SpecialStrings *__instance = new SpecialStrings();
  string __result = __instance->findNext(current);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "SpecialStrings: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
