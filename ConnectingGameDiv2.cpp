#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> board;
// ------------- input end  --------------
int solve(); struct ConnectingGameDiv2 {int getmin(vector<string> _board){board = _board;return ::solve();}};

vector<int> G[2555];
int cWeights[2555];
int comp[200][200];

int dx[] = {-1,0,1,0,-1,1,1,-1};
int dy[] = {0,-1,0,1,-1,-1,1,1};
int H, W;
int V;

void dfs(int sy, int sx) {
  char col = board[sy][sx];
  queue<pair<int, int>> q;
  q.emplace(sy, sx);
  comp[sy][sx] = V;
  int nextY = -1, nextX = -1;
  while(!q.empty()) {
    auto p = q.front(); q.pop();
    int y, x; tie(y, x) = p;
    rep(k, 4) {
      int ny = y + dy[k], nx = x + dx[k];
      if(!in_range(ny, nx, H, W)) { continue; }
      if(comp[ny][nx] >= 0) { continue; }
      if(board[ny][nx] == col) {
        comp[ny][nx] = V;
        q.emplace(ny, nx);
      }
      else {
        nextY = ny, nextX = nx;
      }
    }
  }
  V++;
  if(nextY >= 0) {
    dfs(nextY, nextX);
  }
}

struct state {
  int pos, weight;
  state(int p, int w): pos(p), weight(w) {}
  bool operator < (state const& rhs) const {
    return weight > rhs.weight;
  }
};

int solve() {
  H = board.size(), W = board[0].size();
  rep(i, 2555) G[i].clear();
  minus(comp);
  zero(cWeights);
  V = 0;
  rep(i, H) rep(j, W) {
    if(comp[i][j] < 0) {
      dfs(i, j);
    }
  }

  set<pair<int, int>> st;
  rep(i, H) rep(j, W) {
    cWeights[comp[i][j]] ++;
    rep(k, 8) {
      int ni = i + dy[k], nj = j + dx[k];
      if(!in_range(ni, nj, H, W)) { continue; }
      if(comp[i][j] == comp[ni][nj]) { continue; }
      if(st.count({comp[i][j], comp[ni][nj]})) { continue; }
      st.emplace(comp[i][j], comp[ni][nj]);
      G[comp[i][j]].push_back(comp[ni][nj]);
    }
  }
  
  int S = 2520, T = 2530;
  rep(i, H) {
    if(find(all(G[S]), comp[i][0]) == G[S].end()) {
      G[S].push_back(comp[i][0]);
    }
    if(find(all(G[comp[i][W-1]]), T) == G[comp[i][W-1]].end()) {
      G[comp[i][W-1]].push_back(T);
    }
  }

  priority_queue<state> pq;

  vector<int> dist(2555, inf);
  dist[S] = 0;
  pq.emplace(S, 0);

  while(!pq.empty()) {
    const auto p = pq.top(); pq.pop();
    const int pos = p.pos, weight = p.weight;
    for(auto && to: G[pos]) {
      if(dist[to] <= weight + cWeights[to]) { continue; }
      dist[to] = weight + cWeights[to];
      pq.emplace(to, dist[to]);
    }
  }

  return dist[T];
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> board;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              board = { "AA","BC" };
                              
                    __expected = 2;
                    break;
        }
                case 1: {
                              board = { "AAB","ACD","CCD" };
                              
                    __expected = 4;
                    break;
        }
                case 2: {
                              board = { "iii","iwi","iii" };
                              
                    __expected = 8;
                    break;
        }
                case 3: {
                              board = { "rng58","Snuke","Sothe" };
                              
                    __expected = 6;
                    break;
        }
                case 4: {
                              board = { "yyAArJqjWTH5","yyEEruYYWTHG","hooEvutpkkb2","OooNgFFF9sbi","RRMNgFL99Vmm","R76XgFF9dVVV","SKnZUPf88Vee" };
                              
                    __expected = 14;
                    break;
        }
                case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    board = {{"YYPPPPbbbbv5JSSRp", "YYLPPPPbbbW5JSSbb", "ybbbPPP3bbWWMbbbb", "yybbb000bbbbbbbbb", "Eybbb0000bbbbbbbb", "Eybbbbwbbbbbbbbbb", "EyQbbbttbbbbbbbbb", "bbbbbbbbbbqqqoooo", "bbbbbbbbbqqqooooo", "bbbbbbbbqqqBooooo", "bbbbbbbbqqqBoom66", "bbbbXXbbbb9zZ6666", "xbbbbbbbbb9bb6666", "bbbibbbbbbbb66666", "bbbbbbabbbbbUfffC", "1bbbbbabbbbbUfrrr", "bbbbbbbbbbbbbbbrr", "bbbbbbbbbbbbbbbrr", "bbHbbbbbbbhbbbbrr", "bbbbbeesbbbbbbbbb", "bbbbbeesbbbbbbbbb", "nbbnbbbbbbbbbbbbb", "nnnnbbbbbbbbbb8bb", "nnnbbbGGbbbbbbbbb", "nnndbGGGbbbbbbbbb", "nnnbbbGObbbbTlbDj", "nnbbbbbbbbbbb777g", "nnnbbbbbbbbbb22Kg", "nnnbbbbNbcbbbbbKg"}};
                    __unknownAnswer = 1;
          break;
        }
    
    default: return 'm';
  }
  return do_test(board, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> board, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << board  << "]" << endl;
   
    ConnectingGameDiv2 *__instance = new ConnectingGameDiv2();
  int __result = __instance->getmin(board);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ConnectingGameDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
