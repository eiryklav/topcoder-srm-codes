#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>
#include <stack>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string expression;
// ------------- input end  --------------
string solve(); struct BracketExpressions {string ifPossible(string _expression){expression = _expression;return solve();}};

/*
Passed System Test

string solve() {

  int N = expression.size();
  vector<int> xpos;
  rep(i, N) {
    if(expression[i] == 'X') {
      xpos.push_back(i);
    }
  }

  map<char, char> open = {{']','['},{')','('},{'}','{'}};
  string v = "[](){}";
  rep(S, pow(6, xpos.size())) {
    auto str = expression;
    rep(i, xpos.size()) {
      int T = S; rep(_, i) T /= 6;
      str[xpos[i]] = v[T%6];
    }

    deque<char> stk;
    bool ok = 1;
    rep(i, N) {
      stk.push_back(str[i]);
      if(v.find(stk.back()) % 2) {
        if(stk.size() == 1) { ok = 0; break; }
        if(open[stk.back()] != stk[stk.size()-2]) { ok = 0; break; }
        stk.pop_back(), stk.pop_back();
      }
    }
    if(!stk.empty()) { ok = 0; }
    if(ok) { return "possible"; }
  }

  return "impossible";
}
*/
#include <regex>

string solve() {
  int N = expression.size();
  int dp[N+1][N+1]; zero(dp);
  REP(w, 2, N+1) REP(i, 2, N+1) {
    int j = i + w;
    if(j <= N) {
      std::regex("[X\\(]")
      //("([Mm]icrosoft)?\\s*([Ww]indows)\\s*(8\\.1)\\s*([Uu]pdate)?.*");
    }
  }
}

// CUT begin
using namespace std;
    char do_test(string,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            string expression;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              expression = "([]{})";
                              
                    __expected = "possible";
                    break;
        }
                case 1: {
                              expression = "(())[]";
                              
                    __expected = "possible";
                    break;
        }
                case 2: {
                              expression = "({])";
                              
                    __expected = "impossible";
                    break;
        }
                case 3: {
                              expression = "[]X";
                              
                    __expected = "impossible";
                    break;
        }
                case 4: {
                              expression = "([]X()[()]XX}[])X{{}}]";
                              
                    __expected = "possible";
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    expression = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(expression, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(string expression, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(expression)  << "]" << endl;
   
    BracketExpressions *__instance = new BracketExpressions();
  string __result = __instance->ifPossible(expression);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BracketExpressions: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
