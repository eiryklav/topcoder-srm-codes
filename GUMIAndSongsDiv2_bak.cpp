#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> duration;
vector<int> tone;
int T;
// ------------- input end  --------------
int solve(); struct GUMIAndSongsDiv2 { int maxSongs(vector<int> _duration, vector<int> _tone, int _T) { duration = _duration, tone = _tone, T = _T; return solve(); }};

int N;

int solve() {
  N = duration.size();
  vector<pair<int, int>> v;
  rep(i, N) v.emplace_back(tone[i], duration[i]);
  sort(all(v));
  int res = 0;
  rep(S, 1<<N) {
    ll sum = 0;
    int last = 0;
    int usecnt = 0;
    bool first = true;
    rep(i, N) {
      int tone = v[i].first;
      int duration = v[i].second;
      if(S >> i & 1) {
        if(!first) { sum += abs(tone - last); }
        first = false;
        last = tone;
        sum += duration;
        usecnt++;
      }
    }
    if(sum <= T) { maximize(res, usecnt); }
  }

  return res;
}
