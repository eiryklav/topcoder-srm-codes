#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> level;
vector<int> damage;
// ------------- input end  --------------
int solve(); struct SpellCardsEasy {int maxDamage(vector<int> _level, vector<int> _damage){level = _level, damage = _damage;return ::solve();}};

/*
Passed System Test
しかし、どういう理由で3重ループしてるか忘れた

int dp[55][55];

int SpellCardsEasy::maxDamage(vector <int> level, vector <int> damage) {
  int N = level.size();
  rep(i, N) level[i] --;
  minus(dp);
  dp[N][0] = 0;
  for(int i=N-1; i>=0; i--) {
    rep(j, N) {
      if(dp[i+1][j] >= 0) { dp[i][j+1] = max(dp[i][j+1], dp[i+1][j]); }
      REP(k, i+1, N+1) {
        if(dp[k][j] < 0) { continue; }
        dp[i][j] = max(dp[i][j], dp[k][j]);
        if(j-level[i] < 0) { continue; }
        dp[i][j-level[i]] = max(dp[i][j-level[i]], dp[k][j] + damage[i]);
      }
    }
  }
  return *max_element(dp[0], dp[0] + N);
}
*/


int N;
int dp[55][55];

int dfs(int idx, int needcons) {
  auto& ret = dp[idx][needcons];
  if(ret + 1) { return ret; }
  if(idx == N && needcons > 0) { return ret = -inf; }
  if(idx == N) { return ret = 0; }

  ret = dfs(idx + 1, max(0, needcons - 1));
  if(N - idx - 1 >= needcons + level[idx] - 1) {
    maximize(ret, dfs(idx + 1, needcons + level[idx] - 1) + damage[idx]);
  }

  return ret;
}

int solve() {
  N = level.size();
  minus(dp);
  return dfs(0, 0);
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> level;
            vector<int> damage;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              level = { 1,1,1 };
                                        damage = { 10,20,30 };
                              
                    __expected = 60;
                    break;
        }
                case 1: {
                              level = { 3,3,3 };
                                        damage = { 10,20,30 };
                              
                    __expected = 10;
                    break;
        }
                case 2: {
                              level = { 4,4,4 };
                                        damage = { 10,20,30 };
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              level = { 50,1,50,1,50 };
                                        damage = { 10,20,30,40,50 };
                              
                    __expected = 60;
                    break;
        }
                case 4: {
                              level = { 2,1,1 };
                                        damage = { 40,40,10 };
                              
                    __expected = 80;
                    break;
        }
                case 5: {
                              level = { 1,2,1,1,3,2,1 };
                                        damage = { 10,40,10,10,90,40,10 };
                              
                    __expected = 150;
                    break;
        }
                case 6: {
                              level = { 1,2,2,3,1,4,2 };
                                        damage = { 113,253,523,941,250,534,454 };
                              
                    __expected = 1577;
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    level = ;
                  damage = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(level, damage, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> level, vector<int> damage, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << level << ","<< damage  << "]" << endl;
   
    SpellCardsEasy *__instance = new SpellCardsEasy();
  int __result = __instance->maxDamage(level, damage);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "SpellCardsEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
