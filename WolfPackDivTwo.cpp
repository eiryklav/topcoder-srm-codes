#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct WolfPackDivTwo {int calc(vector <int> x, vector <int> y, int m);};

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int const MOD = 1e9+7;

int N;
ll dp[55][200][200];

int WolfPackDivTwo::calc(vector <int> x, vector <int> y, int m) {
	zero(dp);
	dp[0][100][100] = 1;
	rep(t, m) {
		rep(i, 200) rep(j, 200) {
			rep(k, 4) {
				dp[t+1][i+dy[k]][j+dx[k]] += dp[t][i][j];
				dp[t+1][i+dy[k]][j+dx[k]] %= MOD;
			}
		}
	}

	ll res = 1;
	N = x.size();
	REP(k, -100, 100) REP(l, -100, 100) {
		ll cnt = 1;
		rep(i, N) {
			cnt *= dp[m][y[i]+100 + k][x[i]+100 + l] % MOD;
			cnt %= MOD;
		}
		res += cnt;
		res %= MOD;
	}
	return res - 1;
}
