#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> field;
// ------------- input end  --------------
int solve(); struct ElephantDrinkingEasy {int maxElephants(vector<string> _map){field = _map;return solve();}};

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int N;

bool doElephant(int y, int x, bool used[][5], bool invalid[][5], int dir) {
  while(in_range(y, x, N, N)) {
    if(used[y][x]) { return false; }
    if(invalid[y][x]) { return false; }
    if(field[y][x] == 'Y') { used[y][x] = 1; return true; }
    invalid[y][x] = true;
    x += dx[dir], y += dy[dir];
  }
  return false;
}

int solve() {

  N = field.size();

  int res = 0;
  rep(S, 1<<(4*N)) {
    bool used[5][5]; zero(used);
    bool invalid[5][5]; zero(invalid);
    bool ng = 0;
    rep(i, 4*N) {
      if(!(S >> i & 1)) { continue; }
      if(i < N) {
        int y = N - 1;
        int x = N - i - 1;
        if(!doElephant(y, x, used, invalid, 1)) { ng = 1; break; }
      }
      else if(i < 2*N) {
        int y = N - (i - N) - 1;
        int x = 0;
        if(!doElephant(y, x, used, invalid, 2)) { ng = 1; break; }
      }
      else if(i < 3*N) {
        int y = 0;
        int x = (i - 2 * N);
        if(!doElephant(y, x, used, invalid, 3)) { ng = 1; break; }
      }
      else {
        int y = (i - 3 * N);
        int x = N - 1;
        if(!doElephant(y, x, used, invalid, 0)) { ng = 1; break; }
      }
    }
    if(ng) { continue; }

    int cand = 0;
    rep(i, N) rep(j, N) {
      cand += used[i][j];
    }
    maximize(res, cand);
  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> map;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              map = { "NNNNN","NNYYN","NYNNN","NNYNN","NNNNN" };
                              
                    __expected = 4;
                    break;
        }
                case 1: {
                              map = { "YYY","YYY","YYY" };
                              
                    __expected = 8;
                    break;
        }
                case 2: {
                              map = { "YNYN","NNYY","YYNN","YYYY" };
                              
                    __expected = 10;
                    break;
        }
                case 3: {
                              map = { "YNYN","YNYY","YYNN","YYYY" };
                              
                    __expected = 10;
                    break;
        }
                case 4: {
                              map = { "YYNYN","NYNNY","YNYNN","YYNYY","YYNNN" };
                              
                    __expected = 12;
                    break;
        }
                case 5: {
                              map = { "YYNYN","NYNYY","YNYYN","YYNYY","YYNNN" };
                              
                    __expected = 13;
                    break;
        }
                case 6: {
                              map = { "NN","NN" };
                              
                    __expected = 0;
                    break;
        }
                case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    map = {"NYYYN", "YNYNY", "YYNNN", "NYNYY", "YNNNN"};
              
          __expected = 11;
          break;
        }
    
    default: return 'm';
  }
  return do_test(map, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> map, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << map  << "]" << endl;
   
    ElephantDrinkingEasy *__instance = new ElephantDrinkingEasy();
  int __result = __instance->maxElephants(map);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ElephantDrinkingEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
