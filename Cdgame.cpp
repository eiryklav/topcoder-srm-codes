#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct Cdgame {int rescount(vector <int> a, vector <int> b);};

int Cdgame::rescount(vector <int> a, vector <int> b) {
  int res = 0;

  int A = accumulate(all(a), 0);
  int B = accumulate(all(b), 0);

  set<int> st;
  rep(i, a.size()) rep(j, b.size()) {
  	st.insert((A-a[i]+b[j])*(B-b[j]+a[i]));
  }
  res = st.size();
  return res;
}
