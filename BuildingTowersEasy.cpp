#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
vector<int> x;
vector<int> t;
// ------------- input end  --------------
int solve(); struct BuildingTowersEasy {int maxHeight(int _N, vector<int> _x, vector<int> _t){N = _N, x = _x, t = _t;return ::solve();}};

int solve() {

  vector<int> H(N, N);
  H[0] = 0;
  rep(i, x.size()) {
    minimize(H[x[i]-1], t[i]);
  }

  rep(i, N) {
    if(i) { minimize(H[i], H[i-1] + 1); }
    if(i+1 < N) { minimize(H[i], H[i+1] + 1); }
  }

  for(int i=N-1; i>=0; i--) {
    if(i) { minimize(H[i], H[i-1] + 1); }
    if(i+1 < N) { minimize(H[i], H[i+1] + 1); }
  }

  return *max_element(all(H));
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            vector<int> x;
            vector<int> t;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 10;
                                        x = { 3,8 };
                                        t = { 1,1 };
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              N = 100000;
                                        x = {  };
                                        t = {  };
                              
                    __expected = 99999;
                    break;
        }
                case 2: {
                              N = 2718;
                                        x = { 1,30,400,1300,2500 };
                                        t = { 100000,100000,100000,100000,100000 };
                              
                    __expected = 2717;
                    break;
        }
                case 3: {
                              N = 20;
                                        x = { 4,7,13,15,18 };
                                        t = { 3,8,1,17,16 };
                              
                    __expected = 8;
                    break;
        }
                case 4: {
                              N = 447;
                                        x = { 32,35,55,60,61,88,91,97,128,151,181,186,192,196,198,237,259,268,291,314,341,367,389,390,391,428,435 };
                                        t = { 81,221,172,641,25,953,330,141,123,440,692,394,200,649,78,726,50,810,501,4,216,407,2,172,0,29,14 };
                              
                    __expected = 120;
                    break;
        }
                case 5: {
                              N = 97638;
                                        x = { 8,1749,4550,5388,6752,7089,9737,14891,16671,16821,17691,19753,24589,25348,30114,32213,34376,36467,37699,41490,44784,44893,57316,58275,58567,61122,61489,63195,64776,65905,68788,69908,72853,78152,78784,82779,84488,86277,88611,92793,93214,97583 };
                                        t = { 16610,6,497,14,42892,31,79,1203,518,31147,597,7846,1396,8309,12,14,1148,433,23693,13,1939,244,19,46,27,611,412,10,27023,19141,34,15667,588,10,229,83,390,14,38441,16021,4,39386 };
                              
                    __expected = 6343;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  x = ;
                  t = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, x, t, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, vector<int> x, vector<int> t, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< x << ","<< t  << "]" << endl;
   
    BuildingTowersEasy *__instance = new BuildingTowersEasy();
  int __result = __instance->maxHeight(N, x, t);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BuildingTowersEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
