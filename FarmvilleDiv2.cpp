#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> times;
vector<int> cost;
int budget;
// ------------- input end  --------------
int solve(); struct FarmvilleDiv2 {int minTime(vector<int> _time, vector<int> _cost, int _budget){times = _time, cost = _cost, budget = _budget;return solve();}};

int solve() {

  int N = times.size();

  vector<pair<int, int>> v;
  rep(i, N) {
    v.emplace_back(cost[i], times[i]);
  }

  sort(all(v));

  rep(i, N) {
    int M = min(v[i].second * v[i].first, budget);
    v[i].second -= M / v[i].first;
    budget -= M;
  }

  int acc = 0;
  rep(i, N) {
    acc += v[i].second;
  }
  return acc;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> time;
            vector<int> cost;
            int budget;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              time = { 100 };
                                        cost = { 1 };
                                        budget = 26;
                              
                    __expected = 74;
                    break;
        }
                case 1: {
                              time = { 100 };
                                        cost = { 1 };
                                        budget = 101;
                              
                    __expected = 0;
                    break;
        }
                case 2: {
                              time = { 2,1 };
                                        cost = { 1,1 };
                                        budget = 3;
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              time = { 1,2,3,4,5 };
                                        cost = { 5,4,3,2,1 };
                                        budget = 15;
                              
                    __expected = 6;
                    break;
        }
                case 4: {
                              time = { 100,100,100,100,100,100,100,100,100,100 };
                                        cost = { 2,4,6,8,10,1,3,5,7,9 };
                                        budget = 5000;
                              
                    __expected = 50;
                    break;
        }
                case 5: {
                              time = { 30,40,20,10 };
                                        cost = { 10,20,30,40 };
                                        budget = 5;
                              
                    __expected = 100;
                    break;
        }
                case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    time = {1};
                  cost = {1};
                  budget = 5000;
              
          __unknownAnswer = true; 
          break;
        }
    
    default: return 'm';
  }
  return do_test(time, cost, budget, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> time, vector<int> cost, int budget, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << time << ","<< cost << ","<< pretty_print(budget)  << "]" << endl;
   
    FarmvilleDiv2 *__instance = new FarmvilleDiv2();
  int __result = __instance->minTime(time, cost, budget);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FarmvilleDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
