#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int n;
int a0;
int b;
int c;
int m;
// ------------- input end  --------------
int solve(); struct SubtreesCounting {int sumOfSizes(int _n, int _a0, int _b, int _c, int _m){n = _n, a0 = _a0, b = _b, c = _c, m = _m;return ::solve();}};

template<class value_type> value_type mod_mul(value_type x, value_type k, value_type m) { if(k == 0) { return 0; } if(k % 2 == 0) { return mod_mul((x+x) % m, k/2, m); } else { return (x + mod_mul(x, k-1, m)) % m; } }
template<class value_type> value_type mod_pow(value_type x, value_type n, value_type mod) { if(n == 0) { return 1; } if(n % 2 == 0) { return mod_pow(mod_mul(x, x, mod) % mod, n / 2, mod); } else { return mod_mul(x, mod_pow(x, n - 1, mod), mod); } }
template<class value_type> value_type extgcd(value_type a, value_type b, value_type& x, value_type& y) { value_type d = a; if(b != 0) { d = extgcd(b, a%b, y, x); y -= (a / b) * x;} else { x = 1, y = 0; } return d; }
template<class value_type> value_type mod_inverse(value_type x, value_type mod) { return mod_pow(x, mod-2, mod); /* use fermat */ }
template<class value_type> value_type mod_inverse_unusual_mod(value_type a, value_type mod) { value_type x, y; extgcd(a, mod, x, y); return (mod + x % mod) % mod; }

vector<int> g[100001];
int deg[100001];

void make_tree() {

  vector<ll> a(n);
  a[0] = a0;
  REP(i, 1, n-1)
    a[i] = (mod_mul((ll)b, (ll)a[i-1], (ll)m) + (ll)c) % m;


  REP(i, 1, n) {

//    cout << "a[" << i-1 << "] = " << a[i-1] << endl;

    int j = i == 1 ? 0 : a[i-1] % i;

    // 多重辺はないとする
    g[i].push_back(j);
    g[j].push_back(i);

    deg[i] ++, deg[j] ++;
  }

}

int const MOD = 1e9+7;

ll tsize[100001];

ll size_dfs(int curr, int prev) {

  ll& ret = tsize[curr];

  if(g[curr].size() == 1 && prev != -1) {
    return ret = 1;
  }

  ret = 1;

  for(auto && next: g[curr]) {
    if(next == prev) { continue; }
    ret += size_dfs(next, curr);
    ret %= MOD;
  }

  return ret;
}

int solve() {

  rep(i, 100001) { g[i].clear(); deg[i] = tsize[i] = 0; }

  make_tree();

  int root = -1;

  rep(i, n) {
    if(deg[i] == 1) {
      root = i;
      break;
    }

    if(deg[i] == 0) {
      return 1;
    }

  }

  size_dfs(root, -1);
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int n;
            int a0;
            int b;
            int c;
            int m;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 3;
                                        a0 = 1;
                                        b = 1;
                                        c = 1;
                                        m = 1;
                              
                    __expected = 10;
                    break;
        }
                case 1: {
                              n = 5;
                                        a0 = 1;
                                        b = 2;
                                        c = 3;
                                        m = 100;
                              
                    __expected = 52;
                    break;
        }
                case 2: {
                              n = 1;
                                        a0 = 1;
                                        b = 1;
                                        c = 1;
                                        m = 1;
                              
                    __expected = 1;
                    break;
        }
                case 3: {
                              n = 2;
                                        a0 = 5;
                                        b = 6;
                                        c = 7;
                                        m = 8;
                              
                    __expected = 4;
                    break;
        }
                case 4: {
                              n = 100000;
                                        a0 = 123;
                                        b = 46645;
                                        c = 4564579;
                                        m = 1000000000;
                              
                    __expected = 769840633;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
                  a0 = ;
                  b = ;
                  c = ;
                  m = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, a0, b, c, m, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int n, int a0, int b, int c, int m, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n) << ","<< pretty_print(a0) << ","<< pretty_print(b) << ","<< pretty_print(c) << ","<< pretty_print(m)  << "]" << endl;
   
    SubtreesCounting *__instance = new SubtreesCounting();
  int __result = __instance->sumOfSizes(n, a0, b, c, m);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "SubtreesCounting: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
