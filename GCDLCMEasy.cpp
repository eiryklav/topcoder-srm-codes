#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int n;
vector<int> A;
vector<int> B;
vector<int> G;
vector<int> L;
// ------------- input end  --------------
string solve(); struct GCDLCMEasy {string possible(int _n, vector<int> _A, vector<int> _B, vector<int> _G, vector<int> _L){n = _n, A = _A, B = _B, G = _G, L = _L;return ::solve();}};

template<class T> constexpr T gcd(T a, T b) {return __gcd(a, b);}
template<class T> constexpr T lcm(T a, T b) {return a * b / __gcd(a, b);}

vector<tuple<int, int, int>> graph[555];
int X[555];

bool dfs(int curr, int val) {
  if(X[curr] != -1) {
    return X[curr] == val;
  }

  X[curr] = val;

  for(auto && e: graph[curr]) {
    int to, g, l; tie(to, g, l) = e;
    // val * tar = x[A[i]] * x[B[i]] = G[i] * L[i] を満たすか？
    if(g * l % val > 0) { return false; } 
    int tar = g * l / val;
    if(lcm(val, tar) != l) { return false; }
    if(gcd(val, tar) != g) { return false; }
    if(!dfs(to, tar)) { return false; }
  }

  return true;
}

string solve() {

  rep(i, 555) graph[i].clear();
  minus(X);

  rep(i, A.size()) {
    graph[A[i]].emplace_back(B[i], G[i], L[i]);
    graph[B[i]].emplace_back(A[i], G[i], L[i]);
  }

  rep(i, n) {
    bool ok = 0;
    REP(val, 1, 10001) {
      minus(X);
      if(dfs(i, val)) {
        ok = 1;
        break;
      }
    }
    if(!ok) {
      return "Solution does not exist";
    }
  }
  return "Solution exists";
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,vector<int>,vector<int>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int n;
            vector<int> A;
            vector<int> B;
            vector<int> G;
            vector<int> L;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 4;
                                        A = { 0,1,2,3 };
                                        B = { 1,2,3,0 };
                                        G = { 6,6,6,6 };
                                        L = { 12,12,12,12 };
                              
                    __expected = "Solution exists";
                    break;
        }
                case 1: {
                              n = 5;
                                        A = { 0,1,2,3,4 };
                                        B = { 1,2,3,4,0 };
                                        G = { 6,6,6,6,6 };
                                        L = { 12,12,12,12,12 };
                              
                    __expected = "Solution does not exist";
                    break;
        }
                case 2: {
                              n = 2;
                                        A = { 0,0 };
                                        B = { 1,1 };
                                        G = { 123,123 };
                                        L = { 456,789 };
                              
                    __expected = "Solution does not exist";
                    break;
        }
                case 3: {
                              n = 2;
                                        A = { 0 };
                                        B = { 1 };
                                        G = { 1234 };
                                        L = { 5678 };
                              
                    __expected = "Solution does not exist";
                    break;
        }
                case 4: {
                              n = 6;
                                        A = { 0,0,0,0,0,1,1,1,1,2,2,2,3,3,4 };
                                        B = { 1,2,3,4,5,2,3,4,5,3,4,5,4,5,5 };
                                        G = { 2,2,3,3,1,2,5,1,5,1,7,7,3,5,7 };
                                        L = { 30,42,30,42,210,70,30,210,70,210,42,70,105,105,105 };
                              
                    __expected = "Solution exists";
                    break;
        }
                case 5: {
                              n = 500;
                                        A = { 0 };
                                        B = { 1 };
                                        G = { 10000 };
                                        L = { 10000 };
                              
                    __expected = "Solution exists";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
                  A = ;
                  B = ;
                  G = ;
                  L = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, A, B, G, L, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int n, vector<int> A, vector<int> B, vector<int> G, vector<int> L, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n) << ","<< A << ","<< B << ","<< G << ","<< L  << "]" << endl;
   
    GCDLCMEasy *__instance = new GCDLCMEasy();
  string __result = __instance->possible(n, A, B, G, L);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "GCDLCMEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
