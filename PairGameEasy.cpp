#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int a;
int b;
int c;
int d;
// ------------- input end  --------------
string solve(); struct PairGameEasy {string able(int _a, int _b, int _c, int _d){a = _a, b = _b, c = _c, d = _d;return solve();}};

bool dfs(int a, int b) {
  if(a == c && b == d) { return true; }
  if(a > c || b > d) { return false; }
  bool ret = 0;
  if(dfs(a+b, b)) { return true; }
  if(dfs(a, a+b)) { return true; }
  return ret;
}

string solve() {
  return dfs(a, b) ? "Able to generate" : "Not able to generate";
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int a;
            int b;
            int c;
            int d;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              a = 1;
                                        b = 2;
                                        c = 3;
                                        d = 5;
                              
                    __expected = "Able to generate";
                    break;
        }
                case 1: {
                              a = 1;
                                        b = 2;
                                        c = 2;
                                        d = 1;
                              
                    __expected = "Not able to generate";
                    break;
        }
                case 2: {
                              a = 2;
                                        b = 2;
                                        c = 2;
                                        d = 999;
                              
                    __expected = "Not able to generate";
                    break;
        }
                case 3: {
                              a = 2;
                                        b = 2;
                                        c = 2;
                                        d = 1000;
                              
                    __expected = "Able to generate";
                    break;
        }
                case 4: {
                              a = 47;
                                        b = 58;
                                        c = 384;
                                        d = 221;
                              
                    __expected = "Able to generate";
                    break;
        }
                case 5: {
                              a = 1000;
                                        b = 1000;
                                        c = 1000;
                                        d = 1000;
                              
                    __expected = "Able to generate";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    a = ;
                  b = ;
                  c = ;
                  d = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(a, b, c, d, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int a, int b, int c, int d, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(a) << ","<< pretty_print(b) << ","<< pretty_print(c) << ","<< pretty_print(d)  << "]" << endl;
   
    PairGameEasy *__instance = new PairGameEasy();
  string __result = __instance->able(a, b, c, d);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "PairGameEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
