#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> A;
int K;
// ------------- input end  --------------
int solve(); struct BubbleSortWithReversals {int getMinSwaps(vector<int> _A, int _K){A = _A, K = _K;return solve();}};

int naive_inversion_count(int x, vector<int> const& a) {
  int ret = 0;
  REP(i, x, a.size()) rep(j, i) {
    ret += a[j] > a[i];
  }
  return ret;
}

int solve() {

  int dp[55][55] = {};
  int N = A.size();

  for(int x=N-1; x>=0; x--) {
    rep(k, K+1) {
      vector<int> B(A.begin(), A.begin() + x + 1);
      dp[x][k] = naive_inversion_count(x, B) + dp[x + 1][k];

      if(k >= 1) {
        REP(y, x+1, N) {
          vector<int> B(A.begin(), A.begin() + y + 1);
          reverse(B.begin() + x, B.begin() + y + 1);
          minimize(dp[x][k], naive_inversion_count(x, B) + dp[y + 1][k - 1]);
        }
      }
    }
  }

  return dp[0][K];
}

// CUT begin
using namespace std;
    char do_test(vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> A;
            int K;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = { 6,8,8,7,7 };
                                        K = 1;
                              
                    __expected = 0;
                    break;
        }
                case 1: {
                              A = { 7,2,2,13,5,5,2 };
                                        K = 2;
                              
                    __expected = 3;
                    break;
        }
                case 2: {
                              A = { 12,5,1,10,12,6,6,10,6,8 };
                                        K = 2;
                              
                    __expected = 12;
                    break;
        }
                case 3: {
                              A = { 2,3,1 };
                                        K = 2;
                              
                    __expected = 1;
                    break;
        }
                case 4: {
                              A = { 482,619,619,601,660,660,691,691,77,77,96,77 };
                                        K = 9;
                              
                    __expected = 22;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = ;
                  K = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(A, K, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> A, int K, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << A << ","<< pretty_print(K)  << "]" << endl;
   
    BubbleSortWithReversals *__instance = new BubbleSortWithReversals();
  int __result = __instance->getMinSwaps(A, K);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BubbleSortWithReversals: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
