#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct ChocolateBar {int maxLength(string letters);};

int cnt[26];

map<string, int> memo;

int dfs(string const& s, int curr) {
	bool end = true;
	rep(i, 26) {
		if(cnt[i] >= 2) end = false;
	}
	if(end) { return curr; }

	if(memo.find(s) != memo.end()) { return memo[s]; }
	int& ret = memo[s];

	int n = s.size();
	cnt[s[n-1]-'a']--;
	int A = dfs(s.substr(0, n-1), curr+1);
	cnt[s[n-1]-'a']++;
	cnt[s[0]-'a']--;
	int B = dfs(s.substr(1), curr+1);
	cnt[s[0]-'a']++;
	return ret = min(A, B);
}

int ChocolateBar::maxLength(string letters) {

	memset(cnt, 0, sizeof cnt);
	memo.clear();

	rep(i, letters.size()) {
		cnt[letters[i]-'a'] ++;
	}

  return letters.size() - dfs(letters, 0);
}
