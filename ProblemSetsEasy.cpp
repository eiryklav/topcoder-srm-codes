#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int E;
int EM;
int M;
int MH;
int H;
// ------------- input end  --------------
int solve(); struct ProblemSetsEasy {int maxSets(int _E, int _EM, int _M, int _MH, int _H){E = _E, EM = _EM, M = _M, MH = _MH, H = _H;return ::solve();}};

int solve() {

  for(int x=166666; x>=0; x--) {
    int em = EM, mh = MH;
    if(E - x < 0) { em -= x - E; }
    if(H - x < 0) { mh -= x - H; }
    if(em < 0) { continue; }
    if(mh < 0) { continue;; }
    int remain = em + mh;
    if(M - x < 0 && x - M > remain) { continue; }
    return x;
  }
  return 11451481019192431;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int E;
            int EM;
            int M;
            int MH;
            int H;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              E = 2;
                                        EM = 2;
                                        M = 1;
                                        MH = 2;
                                        H = 2;
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              E = 100;
                                        EM = 100;
                                        M = 100;
                                        MH = 0;
                                        H = 0;
                              
                    __expected = 0;
                    break;
        }
                case 2: {
                              E = 657;
                                        EM = 657;
                                        M = 657;
                                        MH = 657;
                                        H = 657;
                              
                    __expected = 1095;
                    break;
        }
                case 3: {
                              E = 1;
                                        EM = 2;
                                        M = 3;
                                        MH = 4;
                                        H = 5;
                              
                    __expected = 3;
                    break;
        }
                case 4: {
                              E = 100000;
                                        EM = 100000;
                                        M = 100000;
                                        MH = 100000;
                                        H = 100000;
                              
                    __expected = 166666;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    E = ;
                  EM = ;
                  M = ;
                  MH = ;
                  H = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(E, EM, M, MH, H, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int E, int EM, int M, int MH, int H, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(E) << ","<< pretty_print(EM) << ","<< pretty_print(M) << ","<< pretty_print(MH) << ","<< pretty_print(H)  << "]" << endl;
   
    ProblemSetsEasy *__instance = new ProblemSetsEasy();
  int __result = __instance->maxSets(E, EM, M, MH, H);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ProblemSetsEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
