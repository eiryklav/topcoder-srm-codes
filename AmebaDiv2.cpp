#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> X;
int A;
// ------------- input end  --------------
int solve(); struct AmebaDiv2 {int simulate(vector<int> _X, int _A){X = _X, A = _A;return solve();}};

int solve() {

  int N = X.size();
  rep(i, N) {
    if(A == X[i]) { A *= 2; }
  }

  return A;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> X;
            int A;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              X = { 2,1,3,1,2 };
                                        A = 1;
                              
                    __expected = 4;
                    break;
        }
                case 1: {
                              X = { 1,4,9,16,25,36,49 };
                                        A = 10;
                              
                    __expected = 10;
                    break;
        }
                case 2: {
                              X = { 1,2,4,8,16,32,64,128,256,1024,2048 };
                                        A = 1;
                              
                    __expected = 512;
                    break;
        }
                case 3: {
                              X = { 817,832,817,832,126,817,63,63,126,817,832,287,823,817,574 };
                                        A = 63;
                              
                    __expected = 252;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    X = ;
                  A = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(X, A, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> X, int A, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << X << ","<< pretty_print(A)  << "]" << endl;
   
    AmebaDiv2 *__instance = new AmebaDiv2();
  int __result = __instance->simulate(X, A);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "AmebaDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
