#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct ICPCBalloons {int minRepaintings(vector <int> balloonCount, string balloonSize, vector <int> maxAccepted);};

int ICPCBalloons::minRepaintings(vector <int> balloonCount, string balloonSize, vector <int> maxAccepted) {
  int res = inf;
  int N = balloonCount.size();
  vector<int> Ms, Ls;
  rep(i, N) {
  	if(balloonSize[i] == 'M') {
  		Ms.push_back(balloonCount[i]);
  	}
  	else {
  		Ls.push_back(balloonCount[i]);
  	}
  }

  int bmscount = accumulate(all(Ms), 0);
  int blscount = accumulate(all(Ls), 0);

  sort(all(Ms), greater<int>());
  sort(all(Ls), greater<int>());

  rep(S, 1<<maxAccepted.size()) {
  	vector<int> givenMs, givenLs;
  	rep(i, maxAccepted.size()) {
  		if(S >> i & 1) {
  			givenMs.push_back(maxAccepted[i]);
  		}
  		else {
  			givenLs.push_back(maxAccepted[i]);
  		}
  	}

  	int gmscount = accumulate(all(givenMs), 0);
  	int glscount = accumulate(all(givenLs), 0);

  	if(gmscount > bmscount || glscount > blscount) {
  		continue;
  	}

  	sort(all(givenMs), greater<int>());
  	sort(all(givenLs), greater<int>());

  	int cand = 0;
  	rep(i, givenMs.size()) {
  		if(i < Ms.size()) { cand += max(0, givenMs[i]-Ms[i]); }
  		else { cand += givenMs[i]; }
  	}
  	rep(i, givenLs.size()) {
  		if(i < Ls.size()) { cand += max(0, givenLs[i]-Ls[i]); }
  		else { cand += givenLs[i]; }
  	}
  	res = min(res, cand);
  }

  return res == inf ? -1 : res;
}
