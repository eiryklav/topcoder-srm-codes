#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = std::numeric_limits<int>::max();
constexpr ll Inf = std::numeric_limits<ll>::max();

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> x;
vector<int> y;
// ------------- input end  --------------
ll solve(); struct MinimumSquareEasy {ll minArea(vector<int> _x, vector<int> _y){x = _x, y = _y;return solve();}};

int N;

bool pointsInArea(ll len, int xl, int yl, int i, int j) {

  if(len > Inf / len) { return true; }

  rep(k, N) {
    if(i == k || j == k) { continue; }
    if((xl < x[k] && (ll)x[k] < (ll)xl + len) &&
       (yl < y[k] && (ll)y[k] < (ll)yl + len)); else { return false; }
  }
  return true;
}

ll solve() {

  N = x.size();

  ll ans = Inf;

  rep(i, N) REP(j, i+1, N) {
    int minx = inf, miny = inf;
    rep(k, N) {
      if(i == k || j == k) { continue; }
      minimize(minx, x[k]);
      minimize(miny, y[k]);
    }

    int xl = minx - 1, yl = miny - 1;

    ll L = 0, R = Inf;
    while(1) {
      ll OL = L, OR = R;
      ll M = (L + R) / 2;
      if(pointsInArea(M, xl, yl, i, j)) {
        R = M;
      }
      else {
        L = M;
      }
      if(OL == L && OR == R) { break; }
    }
    minimize(ans, R);
  }
  
  return ans * ans;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> x;
            vector<int> y;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              x = { 0,1,2 };
                                        y = { 0,1,5 };
                              
                    __expected = 4LL;
                    break;
        }
                case 1: {
                              x = { -1,-1,0,2,0 };
                                        y = { -2,-1,0,-1,-2 };
                              
                    __expected = 9LL;
                    break;
        }
                case 2: {
                              x = { 1000000000,-1000000000,1000000000,-1000000000 };
                                        y = { 1000000000,1000000000,-1000000000,-1000000000 };
                              
                    __expected = 4000000008000000004LL;
                    break;
        }
                case 3: {
                              x = { 93,34,12,-11,-7,-21,51,-22,59,74,-19,29,-56,-95,-96,9,44,-37,-54,-21 };
                                        y = { 64,12,-43,20,55,74,-20,-54,24,20,-18,77,86,22,47,-24,-33,-57,5,7 };
                              
                    __expected = 22801LL;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    x = ;
                  y = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(x, y, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> x, vector<int> y, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << x << ","<< y  << "]" << endl;
   
    MinimumSquareEasy *__instance = new MinimumSquareEasy();
  ll __result = __instance->minArea(x, y);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "MinimumSquareEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
