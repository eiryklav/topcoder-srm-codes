#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int A;
int B;
int C;
int D;
// ------------- input end  --------------
string solve(); struct BigFatInteger2 {string isDivisible(int _A, int _B, int _C, int _D){A = _A, B = _B, C = _C, D = _D;return solve();}};

map<int, int> divisor(int n) {
  map<int, int> ret;
  for(int i=2; (ll)i*i<=n; i++) {
    while(n % i == 0) {
      ret[i] ++;
      n /= i;
    }
  }
  if(n > 1) { ret[n] ++; }
  return ret;
}

string solve() {

  auto amap = divisor(A);
  auto cmap = divisor(C);

  for(auto& e: cmap) {
    if((ll)amap[e.first] * B >= (ll)e.second * D) {

    }
    else {
      return "not divisible";
    }
  }

  return "divisible";
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int A;
            int B;
            int C;
            int D;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = 6;
                                        B = 1;
                                        C = 2;
                                        D = 1;
                              
                    __expected = "divisible";
                    break;
        }
                case 1: {
                              A = 2;
                                        B = 1;
                                        C = 6;
                                        D = 1;
                              
                    __expected = "not divisible";
                    break;
        }
                case 2: {
                              A = 1000000000;
                                        B = 1000000000;
                                        C = 1000000000;
                                        D = 200000000;
                              
                    __expected = "divisible";
                    break;
        }
                case 3: {
                              A = 8;
                                        B = 100;
                                        C = 4;
                                        D = 200;
                              
                    __expected = "not divisible";
                    break;
        }
                case 4: {
                              A = 999999937;
                                        B = 1000000000;
                                        C = 999999929;
                                        D = 1;
                              
                    __expected = "not divisible";
                    break;
        }
                case 5: {
                              A = 2;
                                        B = 2;
                                        C = 4;
                                        D = 1;
                              
                    __expected = "divisible";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = ;
                  B = ;
                  C = ;
                  D = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(A, B, C, D, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int A, int B, int C, int D, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(A) << ","<< pretty_print(B) << ","<< pretty_print(C) << ","<< pretty_print(D)  << "]" << endl;
   
    BigFatInteger2 *__instance = new BigFatInteger2();
  string __result = __instance->isDivisible(A, B, C, D);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BigFatInteger2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
