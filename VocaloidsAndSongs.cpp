#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int S;
int gumi;
int ia;
int mayu;
// ------------- input end  --------------
int solve(); struct VocaloidsAndSongs {int count(int _S, int _gumi, int _ia, int _mayu){S = _S, gumi = _gumi, ia = _ia, mayu = _mayu;return solve();}};

int const MOD = 1e9+7;

int dp[55][55][55][55];

int solve() {

  zero(dp);

  dp[0][gumi][ia][mayu] = 1;
  rep(i, S) rep(j, gumi+1) rep(k, ia+1) rep(l, mayu+1) {
    if(!dp[i][j][k][l]) { continue; }
    if(j) dp[i+1][j-1][k][l] += dp[i][j][k][l], dp[i+1][j-1][k][l] %= MOD;
    if(k) dp[i+1][j][k-1][l] += dp[i][j][k][l], dp[i+1][j][k-1][l] %= MOD;
    if(l) dp[i+1][j][k][l-1] += dp[i][j][k][l], dp[i+1][j][k][l-1] %= MOD;
    if(j && k) dp[i+1][j-1][k-1][l] += dp[i][j][k][l], dp[i+1][j-1][k-1][l] %= MOD;
    if(k && l) dp[i+1][j][k-1][l-1] += dp[i][j][k][l], dp[i+1][j][k-1][l-1] %= MOD;
    if(j && l) dp[i+1][j-1][k][l-1] += dp[i][j][k][l], dp[i+1][j-1][k][l-1] %= MOD;
    if(j && k && l) dp[i+1][j-1][k-1][l-1] += dp[i][j][k][l], dp[i+1][j-1][k-1][l-1] %= MOD;
  }

  return dp[S][0][0][0];
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int S;
            int gumi;
            int ia;
            int mayu;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              S = 3;
                                        gumi = 1;
                                        ia = 1;
                                        mayu = 1;
                              
                    __expected = 6;
                    break;
        }
                case 1: {
                              S = 3;
                                        gumi = 3;
                                        ia = 1;
                                        mayu = 1;
                              
                    __expected = 9;
                    break;
        }
                case 2: {
                              S = 50;
                                        gumi = 10;
                                        ia = 10;
                                        mayu = 10;
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              S = 18;
                                        gumi = 12;
                                        ia = 8;
                                        mayu = 9;
                              
                    __expected = 81451692;
                    break;
        }
                case 4: {
                              S = 50;
                                        gumi = 25;
                                        ia = 25;
                                        mayu = 25;
                              
                    __expected = 198591037;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    S = ;
                  gumi = ;
                  ia = ;
                  mayu = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(S, gumi, ia, mayu, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int S, int gumi, int ia, int mayu, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(S) << ","<< pretty_print(gumi) << ","<< pretty_print(ia) << ","<< pretty_print(mayu)  << "]" << endl;
   
    VocaloidsAndSongs *__instance = new VocaloidsAndSongs();
  int __result = __instance->count(S, gumi, ia, mayu);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "VocaloidsAndSongs: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
