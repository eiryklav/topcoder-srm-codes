#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define chmin(a) a = min(a, x)
#define chmax(a) a = max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct BlackAndWhiteSolitaire {int minimumTurns(string cardFront);};

int BlackAndWhiteSolitaire::minimumTurns(string cardFront) {
	int a = 0, b = 0;
	bool f = 0;
	rep(i, cardFront.size()) {
		a += cardFront[i] == "BW"[f^1];
		b += cardFront[i] == "BW"[f];
		f ^= 1;
	}
	return min(a, b);
}
