#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> cells;
// ------------- input end  --------------
string solve(); struct ThreeColorabilityEasy { string isColorable(vector<string> _cells) { cells = _cells; return solve(); }};

int N, M;
// a, b
// c, d
tuple<int, int, int> select(int a, int b, int c, int d, bool isZ) {
  bool used[4] = {};
  used[a] = used[b] = used[c] = used[d] = 1;
  if(isZ) {
    
  }
  else {
    
  }
}

string solve() {
  int N = cells.size();
  int M = cells[0].size();

  int G[N+1][M+1]; zero(G);
  G[0][0] = 1, G[0][1] = 2;
  rep(i, N) rep(j, M) {
    auto col = select(G[i][j], G[i][j+1], G[i+1][j], G[i+1][j+1], true);
    int b, c, d; tie(b,c,d) = col;
    if(b == -1) { return "No"; }
    G[i][j+1] = b;
    G[i+1][j] = c;
    G[i+1][j+1] = d;
  }

  return "Yes";
}

int main() {

  vector<vector<string>> tests = {
    {"Z"},
    {"NZ"
    ,"NZ"},
    {"ZZZ"
    ,"ZNZ"},
    {"NZNZNNN"
    ,"NNZNNNZ"
    ,"NNNNZZZ"
    ,"ZZZNZZN"
    ,"ZZNZNNN"
    ,"NZZZZNN"
    ,"ZZZNZNN"},
    {"ZZZZ"
    ,"ZZZZ"
    ,"ZZZZ"},
  };

  vector<string> answers = {
    "Yes", "Yes", "No", "No", "Yes"
  };

  rep(i, tests.size()) {
    cout << "Case " << i << ":\n";
    if(ThreeColorabilityEasy().isColorable(tests[i]) == answers[i]) {
      cout << "OK\n";
    }
    else {
      cout << "NG\n";
    }
  }

  return 0;
}