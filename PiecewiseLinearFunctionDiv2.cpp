#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> Y;
vector<int> query;
// ------------- input end  --------------
vector<int> solve(); struct PiecewiseLinearFunctionDiv2 { vector<int> countSolutions(vector<int> _Y, vector<int> _query) { Y = _Y, query = _query; return solve(); }};

bool infCheck(int i) {
  int num = query[i];
  REP(i, 1, Y.size()) {
    if(num == Y[i-1] && Y[i-1] == Y[i]) { return true; }
  }
  return false;
}

bool cover(int y1, int y2, int num) {
  if(y1 > y2) { swap(y1, y2); }
  return y1 < num && num < y2;
}

vector<int> solve() {

  vector<int> res;
  rep(i, query.size()) {
    if(infCheck(i)) { res.push_back(-1); continue; }

    int cnt = 0;
    REP(j, 1, Y.size()) {
      if(cover(Y[j-1], Y[j], query[i])) {
        cnt ++;
      }
    }
    rep(j, Y.size()) {
      if(Y[j] == query[i]) {
        cnt ++;
      }
    }

    res.push_back(cnt);
  }

  return res;
}
