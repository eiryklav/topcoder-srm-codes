#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> graph;
vector<int> winprob;
vector<int> looseprob;
int Start;
// ------------- input end  --------------
double solve(); struct GraphWalkWithProbabilities {double findprob(vector<string> _graph, vector<int> _winprob, vector<int> _looseprob, int _Start){graph = _graph, winprob = _winprob, looseprob = _looseprob, Start = _Start;return solve();}};

typedef pair<int, int> pii;

int N;

struct node {
  int pos, depth;
  double loseprob;
  double winprob;
  node(int pos, int depth, double loseprob, double winprob)
  : pos(pos), depth(depth), loseprob(loseprob), winprob(winprob)
  {}

  bool operator < (const node& rhs) const {
    return loseprob != rhs.loseprob ? loseprob > rhs.loseprob : winprob < rhs.winprob;
  }
};

double solve() {

  N = graph.size();
  rep(i, N) rep(j, N) graph[i][j] -= '0';

  priority_queue<node> pq;
  pq.emplace(Start, 0, 0.0, 0.0);

  vector<pair<double, double>> dist(N, {inf, -inf});
  dist[Start] = {0.0, 0.0}; // losep, winp

  double ans = -1.0;

  while(!pq.empty()) {
    auto p = pq.top(); pq.pop();
    if(p.depth >= 2000) { ans = p.winprob; break; }
    rep(i, N) {
      if(graph[p.pos][i]) {
        double nloseprob = (p.loseprob + 1.0) / (p.depth + 1.0);
        double nwinprob = (p.winprob + 1.0) / (p.depth + 1.0);
        if(dist[i].first < nloseprob) { continue; }
        if(dist[i].first == nloseprob) {
          if(dist[i].second < nwinprob) { continue; }
        }
        pq.emplace(i, p.depth + 1, nloseprob, nwinprob);
      }
    }
  }

  return ans;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,vector<int>,vector<int>,int,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> graph;
            vector<int> winprob;
            vector<int> looseprob;
            int Start;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              graph = { "1" };
                                        winprob = { 1 };
                                        looseprob = { 1 };
                                        Start = 0;
                              
                    __expected = 0.5;
                    break;
        }
                case 1: {
                              graph = { "11","11" };
                                        winprob = { 60,40 };
                                        looseprob = { 40,60 };
                                        Start = 0;
                              
                    __expected = 0.6;
                    break;
        }
                case 2: {
                              graph = { "11","11" };
                                        winprob = { 2,3 };
                                        looseprob = { 3,4 };
                                        Start = 0;
                              
                    __expected = 0.4285714285714286;
                    break;
        }
                case 3: {
                              graph = { "110","011","001" };
                                        winprob = { 2,1,10 };
                                        looseprob = { 20,20,10 };
                                        Start = 0;
                              
                    __expected = 0.405;
                    break;
        }
                case 4: {
                              graph = { "111","111","011" };
                                        winprob = { 100,1,1 };
                                        looseprob = { 0,50,50 };
                                        Start = 2;
                              
                    __expected = 0.5;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    graph = ;
                  winprob = ;
                  looseprob = ;
                  Start = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(graph, winprob, looseprob, Start, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> graph, vector<int> winprob, vector<int> looseprob, int Start, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << graph << ","<< winprob << ","<< looseprob << ","<< pretty_print(Start)  << "]" << endl;
   
    GraphWalkWithProbabilities *__instance = new GraphWalkWithProbabilities();
  double __result = __instance->findprob(graph, winprob, looseprob, Start);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "GraphWalkWithProbabilities: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
