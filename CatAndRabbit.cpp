#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

struct CatAndRabbit {string getWinner(string tiles);};

string CatAndRabbit::getWinner(string tiles) {
	int w = 0;
	int nim = 0;
	bool sh = 0;
	rep(i, tiles.size()) {
		if(tiles[i] == '.') { w++; }
		else {
			sh = 1;
			nim ^= w;
			w = 0;
		}
	}

	if(sh) nim ^= w;
  return nim ? "Cat" : "Rabbit";
}
