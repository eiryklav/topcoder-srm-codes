#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int w;
int h;
int a;
int b;
// ------------- input end  --------------
ll solve(); struct KnightCircuit {ll maxSize(int _w, int _h, int _a, int _b){w = _w, h = _h, a = _a, b = _b;return ::solve();}};

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

ll solve() {

  if(w < 100 || h < 100) {
    ll maxi = 1;
    rep(sy, max(a, b) + 1) rep(sx, max(a, b) + 1) {
      ll cnt = 1;
      if(!in_range(sy, sx, h, w)) { continue; }
      vector<vector<bool>> visited(h, vector<bool>(w));
      queue<pair<int, int>> q;
      q.emplace(sy, sx);
      visited[sy][sx] = 1;
      while(!q.empty()) {
        int y = q.front().first, x = q.front().second; q.pop();
        rep(i, 4) {
          int ny = y + dy[i], nx = x + dx[i];
          if(!in_range(ny, nx, h, w)) { continue; }
          if(visited[ny][nx]) { continue; }
          visited[ny][nx] = 1;
          cnt ++;
          q.emplace(ny, nx);
        }
      }
      maximize(maxi, cnt);
    }
    return maxi;
  }
  else {
    return (ll)w * h;
  }

  return 0;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int w;
            int h;
            int a;
            int b;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              w = 1;
                                        h = 1;
                                        a = 2;
                                        b = 1;
                              
                    __expected = 1LL;
                    break;
        }
                case 1: {
                              w = 3;
                                        h = 20;
                                        a = 1;
                                        b = 3;
                              
                    __expected = 11LL;
                    break;
        }
                case 2: {
                              w = 100000;
                                        h = 100000;
                                        a = 1;
                                        b = 2;
                              
                    __expected = 10000000000LL;
                    break;
        }
                case 3: {
                              w = 3;
                                        h = 3;
                                        a = 1;
                                        b = 2;
                              
                    __expected = 8LL;
                    break;
        }
                case 4: {
                              w = 30;
                                        h = 30;
                                        a = 8;
                                        b = 4;
                              
                    __expected = 64LL;
                    break;
        }
                case 5: {
                              w = 32;
                                        h = 34;
                                        a = 6;
                                        b = 2;
                              
                    __expected = 136LL;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    w = ;
                  h = ;
                  a = ;
                  b = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(w, h, a, b, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int w, int h, int a, int b, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(w) << ","<< pretty_print(h) << ","<< pretty_print(a) << ","<< pretty_print(b)  << "]" << endl;
   
    KnightCircuit *__instance = new KnightCircuit();
  ll __result = __instance->maxSize(w, h, a, b);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "KnightCircuit: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
