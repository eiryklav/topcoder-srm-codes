#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int k;
vector<ll> table;
// ------------- input end  --------------
ll solve(); struct NumberGameAgain {ll solve(int _k, vector<ll> _table){k = _k, table = _table;return ::solve();}};

ll solve() {

  set<ll> st; for(auto && e: table) st.insert(e);
  sort(all(table), greater<ll>());
  for(auto && u: table) {
    if(!st.count(u)) { continue; }
    auto cnt = 0;
    auto e = u;
    while(e > 0) {
      cnt += st.count(e);
      e /= 2;
    }
    if(cnt > 1) {
      st.erase(u);
    }
  }

  ll ans = (1LL<<k) - 2;
  for(auto && e: st)
    ans -= (1LL<<(k-(int)log2(e))) - 1;

  return ans;
}

// CUT begin
using namespace std;
    char do_test(int,vector<ll>,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int k;
            vector<ll> table;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              k = 3;
                                        table = { 2LL,4LL,6LL };
                              
                    __expected = 2LL;
                    break;
        }
                case 1: {
                              k = 5;
                                        table = { 2LL,3LL };
                              
                    __expected = 0LL;
                    break;
        }
                case 2: {
                              k = 5;
                                        table = {  };
                              
                    __expected = 30LL;
                    break;
        }
                case 3: {
                              k = 40;
                                        table = { 2LL,4LL,8LL,16LL,32141531LL,2324577LL,1099511627775LL,2222222222LL,33333333333LL,4444444444LL,2135LL };
                              
                    __expected = 549755748288LL;
                    break;
        }
                case 4: {
                              k = 40;
                                        table = {  };
                              
                    __expected = 1099511627774LL;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    k = ;
                  table = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(k, table, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int k, vector<ll> table, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(k) << ","<< table  << "]" << endl;
   
    NumberGameAgain *__instance = new NumberGameAgain();
  ll __result = __instance->solve(k, table);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "NumberGameAgain: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
