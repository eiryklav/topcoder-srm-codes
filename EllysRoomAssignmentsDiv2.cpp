#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct EllysRoomAssignmentsDiv2 {double getProbability(vector <string> ratings);};

double EllysRoomAssignmentsDiv2::getProbability(vector <string> ratings) {
	string rating;
	rep(i, ratings.size()) rating += ratings[i];
	stringstream ss(rating);
	vector<int> ratvec;
	int MyRate; ss >> MyRate;
	int rat;
	while(ss >> rat) {
		ratvec.push_back(rat);
	}

	sort(ratvec.rbegin(), ratvec.rend());

	if(ratvec[0] <= MyRate) { return 1.0; }

	int rank = 1;
	rep(i, ratvec.size()) {
		if(ratvec[i] > MyRate) { rank++; }
	}

	int N = ratvec.size() + 1;
	int R = N / 20 + (N % 20 > 0);
	cout << ":" << N << endl;
	cout << ":" << R << endl;

	if(rank <= R) { return 0.0; }
	if(R == 1) { return 1.0; }

	return 1.0 / R;
}
