#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> str;
// ------------- input end  --------------
vector<int> solve(); struct LostCharacter {vector<int> getmins(vector<string> _str){str = _str;return ::solve();}};

vector<int> solve() {
  int N = str.size();
  vector<int> res(N);
  rep(rem, N) {
    vector<string> v;
    string temp;
    rep(k, N) {
      auto s = str[k];
      if(k == rem) {
        rep(l, s.size()) {
          if(s[l] == '?') { s[l] = 'a'; }
        }
        temp = s;
      }
      else {
        rep(l, s.size()) {
          if(s[l] == '?') { s[l] = 'z'; }
        }
      }
      v.emplace_back(s);
    }

    sort(all(v));

    rep(i, N) {
      if(v[i] == temp) {
        res[rem] = i;
        break;
      }
    }

  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,vector<int>,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> str;
            vector<int> __expected = vector<int>();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              str = { "abc","bcd","cde","cdf","bbc" };
                              
                    __expected = { 0, 2, 3, 4, 1 };
                    break;
        }
                case 1: {
                              str = { "?ala","ara","baba" };
                              
                    __expected = { 0, 0, 1 };
                    break;
        }
                case 2: {
                              str = { "a?","a","a","ab","aa" };
                              
                    __expected = { 2, 0, 0, 3, 2 };
                    break;
        }
                case 3: {
                              str = { "s?nu?ke","sm??eke","?sna?ke","so?th?e","shake??" };
                              
                    __expected = { 0, 1, 0, 2, 0 };
                    break;
        }
                case 4: {
                              str = { "?","z?","zz?","zzz?","zzzz?","zzzzz?","zzzzzz?" };
                              
                    __expected = { 0, 1, 2, 3, 4, 5, 6 };
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    str = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(str, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> str, vector<int> __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << str  << "]" << endl;
   
    LostCharacter *__instance = new LostCharacter();
  vector<int> __result = __instance->getmins(str);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  __expected  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  __result  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "LostCharacter: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
