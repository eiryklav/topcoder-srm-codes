#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct DistinctRemainders {int howMany(long long N, int M);};

int const MOD = 1e9+7;

ll mod_pow(ll n, ll p) {
	ll res = 1;
	while(p > 0) {
		if(p & 1) { (res *= n) %= MOD; }
		n = (n * n) % MOD;
		p >>= 1;
	}
	return res % MOD;
}

//*
ll mod_comb(ll n, ll k) {
	if(n < k) { return 0; }
	if(n < 0) { return 0; }
	ll p = 1, q = 1;
	rep(i, k) {
		q *= i+1; q %= MOD;
		p *= n % MOD - i; p %= MOD;// % MOD;
		// p *= (n-i) % MOD; p %= MOD;
		/*
		(q *= i+1) %= MOD;
		(p *= (n-i) % MOD ) %= MOD;
		こういう書き方をすると
		(p *= n-i) %= MOD;
		とか書いてオーバーフローするのでやめましょう
		*/
	}
	return (p * mod_pow(q, MOD-2)) % MOD;
}

ll fact(ll n) {
	if(n == 0) return 1;
	return (fact(n-1) * n) % MOD;
}

int DistinctRemainders::howMany(long long N, int M) {

//	vector<vector<ll>> dp(M+1, vector<ll>(M*M));
	static ll dp[55][2500];	// 手前環境だと配列は死ぬ
	dp[0][0] = 1;

	rep(i, M) {
		for(int j=M*M/2+1; j>=0; j--) {
			for(int k=M-1; k>=0; k--) {
				if(j+i >= M*M) { continue; }
				dp[k+1][j+i] += dp[k][j];
				dp[k+1][j+i] %= MOD;
			}
		}
	}

  ll ret = 0;
  REP(i, 1, M+1) rep(j, M*M) {
  	if((N - j) % M) { continue; }
  	ll x = fact(i) % MOD;
  	x *= dp[i][j] % MOD;
  	x %= MOD;
  	x *= mod_comb((N-j) / M + i - 1, i - 1) % MOD;
  	x %= MOD;
  	ret += x % MOD;
  	ret %= MOD;
  }

	return ret;
}
