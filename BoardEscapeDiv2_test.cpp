#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> s;
int K;
// ------------- input end  --------------
string solve(); struct BoardEscapeDiv2 {string findWinner(vector<string> _s, int _k){s = _s, K = _k;return solve();}};

int dp[101][55][55];
int N, M;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

string solve() {

  int si, sj;
  N = s.size(), M = s[0].size();
  rep(i, N) rep(j, M) {
    if(s[i][j] == 'T') {
      si = i, sj = j;
      break;
    }
  }

  bool valid = 0;
  rep(i, 4) {
    int ni = si + dy[i], nj = sj + dx[i];
    if(in_range(ni, nj, N, M)) {
      if(s[ni][nj] == '.') {
        valid = 1;
        bool ok = 0;
        rep(d, 4) {
          int nni = ni + dy[d], nnj = nj + dx[d];
          if(!in_range(nni, nnj, N, M)) {
            ok = 1;
          }
        }
        if(ok) {
          return "Bob";
        }
      }
    }
  }
  if(valid) { return "Alice"; }
  return "Bob";
/*
  zero(dp);
  dp[0][si][sj] = 1;

  rep(k, K) rep(i, N) rep(j, M) {
    rep(d, 4) {
      int ni = i + dy[d], nj = j + dx[d];
      if(!in_range(ni, nj, N, M)) { continue; }
      if(s[ni][nj] == '#') { continue; }
      if(s[ni][nj] == 'E') {
        dp[k+1][ni][nj] = inf;
      }
      else {
        dp[k+1][ni][nj] += dp[k][i][j];
      }
    }
  }

  rep(i, N) rep(j, M) {
    if(dp[K][i][j])
  }
*/
  return "";
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> s;
            int k;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              s = { "T.#","#.E" };
                                        k = 3;
                              
                    __expected = "Alice";
                    break;
        }
                case 1: {
                              s = { "E#E","#T#","E#E" };
                                        k = 99;
                              
                    __expected = "Bob";
                    break;
        }
                case 2: {
                              s = { "#E...","#...E","E.T#.","..#.." };
                                        k = 13;
                              
                    __expected = "Alice";
                    break;
        }
                case 3: {
                              s = { "TE" };
                                        k = 50;
                              
                    __expected = "Alice";
                    break;
        }
                case 4: {
                              s = { ".T." };
                                        k = 1;
                              
                    __expected = "Alice";
                    break;
        }
                case 5: {
                              s = {
                                "..........................",
                                "......EEE..EEE..E...E.....",
                                ".....E.....E..E.EE.EE.....",
                                "......EEE..EEE..E.E.E.....",
                                ".........E.E.E..E...E.....",
                                "......EEE..E..E.E...E.....",
                                "..........................",
                                "...E#E#E#E#E#E#E#E#E#E#...",
                                "..........................",
                                "......EEE..EEE...EEE......",
                                ".....E........E.E.........",
                                "......EEE.....E..EEE......",
                                ".....E...E...E..E...E.....",
                                "......EEE....E...EEE......",
                                "..........................",
                                "...#E#E#E#E#E#E#E#E#E#E...",
                                "..........................",
                                "....EE...E...E..E.EEE.E...",
                                "...E.....E...E..E.E...E...",
                                "...E.EE..E...EEEE.EE..E...",
                                "...E..E..E...E..E.E.......",
                                "....EE...EEE.E..E.E...E...",
                                "T........................." };
                                        k = 100;
                              
                    __expected = "Bob";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    s = ;
                  k = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(s, k, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> s, int k, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << s << ","<< pretty_print(k)  << "]" << endl;
   
    BoardEscapeDiv2 *__instance = new BoardEscapeDiv2();
  string __result = __instance->findWinner(s, k);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BoardEscapeDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
