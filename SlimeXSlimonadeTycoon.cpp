#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> morning;
vector<int> customers;
int stale_limit;
// ------------- input end  --------------
int solve(); struct SlimeXSlimonadeTycoon {int sell(vector<int> _morning, vector<int> _customers, int _stale_limit){morning = _morning, customers = _customers, stale_limit = _stale_limit;return solve();}};

int solve() {

  int N = morning.size();

  int inc[200]; zero(inc);
  int dec[200]; zero(dec);
  rep(i, N) {
    inc[i] += morning[i];
    dec[i+stale_limit] += morning[i];
  }

  int sold = 0;
  int curr = 0;
  rep(i, N) {
    curr += inc[i] - dec[i];
    int change = min(curr, customers[i]);
    curr -= change;
    sold += change;
    int rem = change;
    REP(j, i+1, N) {
      if(dec[j] > 0) {
        if(dec[j] - rem < 0) {
          rem -= dec[j];
          dec[j] = 0;
        }
        else {
          dec[j] -= rem;
          rem = 0; break;
        }
      }
    }

  }

  return sold;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> morning;
            vector<int> customers;
            int stale_limit;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              morning = { 5,1,1 };
                                        customers = { 1,2,3 };
                                        stale_limit = 2;
                              
                    __expected = 5;
                    break;
        }
                case 1: {
                              morning = { 10,20,30 };
                                        customers = { 30,20,10 };
                                        stale_limit = 1;
                              
                    __expected = 40;
                    break;
        }
                case 2: {
                              morning = { 1,2,3,4,5 };
                                        customers = { 5,5,5,5,5 };
                                        stale_limit = 5;
                              
                    __expected = 15;
                    break;
        }
                case 3: {
                              morning = { 10000,0,0,0,0,0,0 };
                                        customers = { 1,2,4,8,16,32,64 };
                                        stale_limit = 4;
                              
                    __expected = 15;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    morning = ;
                  customers = ;
                  stale_limit = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(morning, customers, stale_limit, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> morning, vector<int> customers, int stale_limit, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << morning << ","<< customers << ","<< pretty_print(stale_limit)  << "]" << endl;
   
    SlimeXSlimonadeTycoon *__instance = new SlimeXSlimonadeTycoon();
  int __result = __instance->sell(morning, customers, stale_limit);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "SlimeXSlimonadeTycoon: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
