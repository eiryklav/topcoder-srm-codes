#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
int K;
vector<int> x;
vector<int> t;
// ------------- input end  --------------
ll solve(); struct BuildingTowers {ll maxHeight(int _N, int _K, vector<int> _x, vector<int> _t){N = _N, K = _K, x = _x, t = _t;return ::solve();}};

int RSize;

ll solve() {

  RSize = x.size();

  if(RSize == 0) {
    return (ll)(N-1) * K;
  }

  vector<pair<int, int>> vv;
  rep(i, RSize) vv.emplace_back(x[i], t[i]);
  sort(all(vv));

  vector<ll> H(RSize, N);
  H[0] = 0;
  rep(i, RSize) {
    minimize(H[i], (ll)vv[i].second);
  }

  rep(i, RSize) {
    if(i)           minimize(H[i], H[i-1] + K * (vv[i].first - vv[i-1].first));
    if(i+1 < RSize) minimize(H[i], H[i+1] + K * (vv[i+1].first - vv[i].first));
  }

  for(int i=RSize-1; i>=0; i--) {
    if(i)           minimize(H[i], H[i-1] + K * (vv[i].first - vv[i-1].first));
    if(i+1 < RSize) minimize(H[i], H[i+1] + K * (vv[i+1].first - vv[i].first));
  }

  ll max = 0;

  rep(i, RSize - 1) {
    ll top = (abs(vv[i+1].first - vv[i].first) + abs(H[i+1] - H[i]) / K) / 2 * K + std::min(H[i], H[i+1]);
    maximize(max, top);
  }
  
  return max;
}

// CUT begin
using namespace std;
    char do_test(int,int,vector<int>,vector<int>,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            int K;
            vector<int> x;
            vector<int> t;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 10;
                                        K = 1;
                                        x = { 3,8 };
                                        t = { 1,1 };
                              
                    __expected = 3LL;
                    break;
        }
                case 1: {
                              N = 1000000000;
                                        K = 1000000000;
                                        x = {  };
                                        t = {  };
                              
                    __expected = 999999999000000000LL;
                    break;
        }
                case 2: {
                              N = 20;
                                        K = 3;
                                        x = { 4,7,13,15,18 };
                                        t = { 8,22,1,55,42 };
                              
                    __expected = 22LL;
                    break;
        }
                case 3: {
                              N = 780;
                                        K = 547990606;
                                        x = { 34,35,48,86,110,170,275,288,313,321,344,373,390,410,412,441,499,525,538,568,585,627,630,671,692,699,719,752,755,764,772 };
                                        t = { 89,81,88,42,55,92,19,91,71,42,72,18,86,89,88,75,29,98,63,74,45,11,68,34,94,20,69,33,50,69,54 };
                              
                    __expected = 28495511604LL;
                    break;
        }
                case 4: {
                              N = 7824078;
                                        K = 2374;
                                        x = { 134668,488112,558756,590300,787884,868112,1550116,1681439,1790994,1796091,1906637,2005485,2152813,2171716,2255697,2332732,2516853,2749024,2922558,2930163,3568190,3882735,4264888,5080550,5167938,5249191,5574341,5866912,5936121,6142348,6164177,6176113,6434368,6552905,6588059,6628843,6744163,6760794,6982172,7080464,7175493,7249044 };
                                        t = { 8,9,171315129,52304509,1090062,476157338,245,6,253638067,37,500,29060,106246500,129,22402,939993108,7375,2365707,40098,10200444,3193547,55597,24920935,905027,1374,12396141,525886,41,33,3692,11502,180,3186,5560,778988,42449532,269666,10982579,48,3994,1,9 };
                              
                    __expected = 1365130725LL;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  K = ;
                  x = ;
                  t = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, K, x, t, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, int K, vector<int> x, vector<int> t, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< pretty_print(K) << ","<< x << ","<< t  << "]" << endl;
   
    BuildingTowers *__instance = new BuildingTowers();
  ll __result = __instance->maxHeight(N, K, x, t);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BuildingTowers: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
