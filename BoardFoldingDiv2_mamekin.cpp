#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> paper;
// ------------- input end  --------------
int solve(); struct BoardFoldingDiv2 {int howMany(vector<string> _paper){paper = _paper;return ::solve();}};

vector<bool> g(vector<vector<bool>> const& a) {
  vector<bool> ret(a.size());
  ret[0] = 1;
  rep(i, a.size()) {
    if(!ret[i]) { continue; }
    for(int j=1; 2*j<=a.size()-i;j++) {
      bool ok = 1;
      rep(k, j) if(a[i+k] != a[i+2*j-1-k]) { ok = 0; break; }
      ret[i + j] = ok;
    }
  }

  return ret;
}

int f(vector<vector<bool>> a) {
  auto a1 = g(a);
  reverse(all(a));
  auto a2 = g(a);
  int ret = 0;
  rep(i, a.size()) for(int j=0; i+j<a.size(); j++)
    ret += a1[i] && a2[j];
  return ret;
}

int solve() {
  int N = paper.size();
  int M = paper[0].size();
  vector<vector<bool>> a(N, vector<bool>(M));
  vector<vector<bool>> b(M, vector<bool>(N));
  rep(i, N) rep(j, M) {
    a[i][j] = b[j][i] = paper[i][j] == '1';
  }
  return f(a) * f(b);
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> paper;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              paper = { "10","11" };
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              paper = { "1111111","1111111" };
                              
                    __expected = 84;
                    break;
        }
                case 2: {
                              paper = { "0110","1001","1001","0110" };
                              
                    __expected = 9;
                    break;
        }
                case 3: {
                              paper = { "0","0","0","1","0","0" };
                              
                    __expected = 6;
                    break;
        }
                case 4: {
                              paper = { "000","010","000" };
                              
                    __expected = 1;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    paper = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(paper, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> paper, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << paper  << "]" << endl;
   
    BoardFoldingDiv2 *__instance = new BoardFoldingDiv2();
  int __result = __instance->howMany(paper);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BoardFoldingDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
