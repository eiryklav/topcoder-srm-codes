#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct VerySecureEncryption {string encrypt(string message, vector <int> key, int K);};

string VerySecureEncryption::encrypt(string message, vector <int> key, int K) {
  string res = 0;
  return res;
}

// BEGIN CUT HERE
#include <cstdio>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
namespace moj_harness {
	using std::string;
	using std::vector;
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				std::cerr << "Illegal input! Test case " << casenum << " does not exist." << std::endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			std::cerr << "No test cases run." << std::endl;
		} else if (correct < total) {
			std::cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << std::endl;
		} else {
			std::cerr << "All " << total << " tests passed!" << std::endl;
		}
	}
	
	int verify_case(int casenum, const string &expected, const string &received, std::clock_t elapsed) { 
		std::cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			std::sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		std::cerr << verdict;
		if (!info.empty()) {
			std::cerr << " (";
			for (size_t i=0; i<info.size(); ++i) {
				if (i > 0) std::cerr << ", ";
				std::cerr << info[i];
			}
			std::cerr << ")";
		}
		std::cerr << std::endl;
		
		if (verdict == "FAILED") {
			std::cerr << "    Expected: \"" << expected << "\"" << std::endl; 
			std::cerr << "    Received: \"" << received << "\"" << std::endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum__) {
		switch (casenum__) {
		case 0: {
			string message            = "abc";
			int key[]                 = {1,2,0};
			int K                     = 1;
			string expected__         = "cab";

			std::clock_t start__      = std::clock();
			string received__         = VerySecureEncryption().encrypt(message, vector <int>(key, key + (sizeof key / sizeof key[0])), K);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 1: {
			string message            = "abcde";
			int key[]                 = {4, 3, 2, 1, 0};
			int K                     = 1;
			string expected__         = "edcba";

			std::clock_t start__      = std::clock();
			string received__         = VerySecureEncryption().encrypt(message, vector <int>(key, key + (sizeof key / sizeof key[0])), K);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 2: {
			string message            = "abcde";
			int key[]                 = {4, 3, 2, 1, 0};
			int K                     = 2;
			string expected__         = "abcde";

			std::clock_t start__      = std::clock();
			string received__         = VerySecureEncryption().encrypt(message, vector <int>(key, key + (sizeof key / sizeof key[0])), K);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}
		case 3: {
			string message            = "uogcodlk";
			int key[]                 = {4, 3, 6, 2, 5, 1, 0, 7};
			int K                     = 44;
			string expected__         = "goodluck";

			std::clock_t start__      = std::clock();
			string received__         = VerySecureEncryption().encrypt(message, vector <int>(key, key + (sizeof key / sizeof key[0])), K);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 4: {
			string message            = ;
			int key[]                 = ;
			int K                     = ;
			string expected__         = ;

			std::clock_t start__      = std::clock();
			string received__         = VerySecureEncryption().encrypt(message, vector <int>(key, key + (sizeof key / sizeof key[0])), K);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
/*      case 5: {
			string message            = ;
			int key[]                 = ;
			int K                     = ;
			string expected__         = ;

			std::clock_t start__      = std::clock();
			string received__         = VerySecureEncryption().encrypt(message, vector <int>(key, key + (sizeof key / sizeof key[0])), K);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
/*      case 6: {
			string message            = ;
			int key[]                 = ;
			int K                     = ;
			string expected__         = ;

			std::clock_t start__      = std::clock();
			string received__         = VerySecureEncryption().encrypt(message, vector <int>(key, key + (sizeof key / sizeof key[0])), K);
			return verify_case(casenum__, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}


#include <cstdlib>
int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(std::atoi(argv[i]));
	}
}
// END CUT HERE
