#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
vector<int> X;
vector<int> Y;
vector<int> height;
ll B;
// ------------- input end  --------------
int solve(); struct TallShoes {int maxHeight(int _N, vector<int> _X, vector<int> _Y, vector<int> _height, ll _B){N = _N, X = _X, Y = _Y, height = _height, B = _B;return ::solve();}};

struct state {
  int pos; ll budget;
  state(int p, ll b): pos(p), budget(b) {}
  bool operator < (state const& rhs) const {
    return budget < rhs.budget; // prior to larger one
  }
};

int solve() {

  vector<vector<pair<int, int>>> G(N);
  rep(i, X.size()) {
    G[X[i]].emplace_back(Y[i], height[i]);
    G[Y[i]].emplace_back(X[i], height[i]);
  }

  auto dijkstra = [&](int Height){
    vector<ll> dist(N, -1);
    dist[0] = B;
    priority_queue<state> pq;
    pq.emplace(0, B);

    while(!pq.empty()) {
      auto p = pq.top(); pq.pop();
      const int pos = p.pos; const ll budget = p.budget;

      if(pos == N-1) { return true; }

      for(auto && e: G[pos]) {
        int to, limit; tie(to, limit) = e;
        if(dist[to] >= budget) { continue; }
        if(limit < Height) {
          ll diff = Height - limit;
          if(diff * diff <= budget && dist[to] < budget - diff * diff) {
            dist[to] = budget - diff * diff;
            pq.emplace(to, dist[to]);
          }
        }
        else {
          dist[to] = budget;
          pq.emplace(to, dist[to]);
        }
      }
    }

    return false;
  };

  int L = 0, R = 1e8 + 1;
  while(L + 1 < R) {
    int leastHeight = (L + R) / 2;
    if(dijkstra(leastHeight)) {
      L = leastHeight;
    }
    else {
      R = leastHeight;
    }
  }

  return L;
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,vector<int>,ll,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            vector<int> X;
            vector<int> Y;
            vector<int> height;
            ll B;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 3;
                                        X = { 0,1,0 };
                                        Y = { 1,2,2 };
                                        height = { 3,4,2 };
                                        B = 1LL;
                              
                    __expected = 4;
                    break;
        }
                case 1: {
                              N = 3;
                                        X = { 0,1,0 };
                                        Y = { 1,2,2 };
                                        height = { 3,4,2 };
                                        B = 52LL;
                              
                    __expected = 9;
                    break;
        }
                case 2: {
                              N = 8;
                                        X = { 0,0,3,3,4,4,4,7,7 };
                                        Y = { 1,2,1,2,3,5,6,5,6 };
                                        height = { 1000,1000,1000,1000,1,1000,1000,1000,1000 };
                                        B = 3LL;
                              
                    __expected = 2;
                    break;
        }
                case 3: {
                              N = 10;
                                        X = { 0,1,2,3,4,5,6,7,8 };
                                        Y = { 1,2,3,4,5,6,7,8,9 };
                                        height = { 0,0,0,0,0,0,0,0,0 };
                                        B = 9876543210123LL;
                              
                    __expected = 1047565;
                    break;
        }
                case 4: {
                              N = 6;
                                        X = { 0,0,0,0,0,1,1,1,1,2,2,2,3,3,4 };
                                        Y = { 1,2,3,4,5,2,3,4,5,3,4,5,4,5,5 };
                                        height = { 999999,986588,976757,988569,977678,999999,967675,947856,955856,999999,975956,956687,999999,979687,999999 };
                                        B = 0LL;
                              
                    __expected = 999999;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  X = ;
                  Y = ;
                  height = ;
                  B = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, X, Y, height, B, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, vector<int> X, vector<int> Y, vector<int> height, ll B, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< X << ","<< Y << ","<< height << ","<< pretty_print(B)  << "]" << endl;
   
    TallShoes *__instance = new TallShoes();
  int __result = __instance->maxHeight(N, X, Y, height, B);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TallShoes: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
