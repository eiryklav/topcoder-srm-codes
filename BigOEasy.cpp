#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> graph;
// ------------- input end  --------------
string solve(); struct BigOEasy {string isBounded(vector<string> _graph){graph = _graph;return solve();}};

string solve() {
  vector<string> G = graph;
  int N = graph.size();
  rep(k, N) rep(i, N) rep(j, N) {
    if(G[i][k] == 'Y' && G[k][j] == 'Y') {
      G[i][j] = 'Y';
    }
  }
  rep(i, N) rep(j, N) {
    if(i == j) { continue; }
    int cnt = 0;
    if(graph[i][j] == 'Y') {
      if(G[j][i] == 'Y') {
        cnt ++;
        rep(k, N) {
          if(i == k || j == k) { continue; }
          if(graph[i][k] == 'Y' && G[k][i] == 'Y' && G[j][k] == 'Y' && G[k][j] == 'Y') {
            cnt ++;
          }
        }
      }
    }
    if(cnt > 1) { return "Unbounded"; }
  }
  return "Bounded";
}

// CUT begin
using namespace std;
    char do_test(vector<string>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> graph;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              graph = { "NYY","YNY","YYN" };
                              
                    __expected = "Unbounded";
                    break;
        }
                case 1: {
                              graph = { "NYYN","NNNY","NNNY","NNNN" };
                              
                    __expected = "Bounded";
                    break;
        }
                case 2: {
                              graph = { "NYN","NNY","YNN" };
                              
                    __expected = "Bounded";
                    break;
        }
                case 3: {
                              graph = { "NYYN","YNNN","NNNY","NNYN" };
                              
                    __expected = "Bounded";
                    break;
        }
                case 4: {
                              graph = { "NYY","YNN","YNN" };
                              
                    __expected = "Unbounded";
                    break;
        }
                case 5: {
                              graph = { "NYNYN","NNYNY","YNNNN","NNNNN","YNNNN" };
                              
                    __expected = "Unbounded";
                    break;
        }
                case 6: {
                              graph = { "NYYN","NNYN","NNNY","YNNN" };
                              
                    __expected = "Unbounded";
                    break;
        }
                case 7: {
                              graph = { "NYYNNNNYNNYNYNNNNYYNNYNYNNNNNNNNNNYNNNNNNNYYNYNNNN","NNNNNNNNNNNNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN","NNNNNNNNNNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN","YYYNYYNYYYYYYYYYYYYYYYYYYNYYYYYYYYYYYYYYNYYYYYNYYN","YYYNNNNYNNYNYNNNNYYNNNNYNNNNNNNNNNYNNNNNNNYYNYNNNN","YYYNYNNYNNYYYNNNYYYNNYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","YYYYYYNYYYYYYYYYYYYYYYYYYNYNYYYYYYYYYYYYNYYYYYNYYN","NYYNNNNNNNYNYNNNNYNNNNNYNNNNNNNNNNNNNNNNNNYNNYNNNN","YYYNYNNYNNYYYNNNNYYNNYNYNNYNYYYYYYYYYYNYNYYYNYNYNN","YYYNYNNYNNYYYNNNNYYNYYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","NNNNNNNNNNNNNNNNNNNNNNNYNNNNNNNNNNNNNNNNNNNNNNNNNN","NYYNNNNYNNYNYNNNNYYNNNNYNNNNNNNNNNYYNNNNNNYYNYNNNN","NNNNNNNNNNNNNNNNNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN","YYYNYNNYYNYYYNNNNYYNNYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","YYYNYNNYNNYYYNNNNYYYNYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","YYYNYNNYNNYYYNNNNYYNNYYYNNYNYYYNYYYYYYNYNYYYNYNYNN","YYYNYNNYNYYYYNNNNYYNNYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","NNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN","NYYNNNNNNNYNYNNNNYNNNNNYNNNNNNNNNNYNNNNNNNNNNYNNNN","YYYNYNNYNNYYYNNNNYYNNYNYNNYNYYYNYYYYYYYYNYYYNYNYNN","YYYNYNNYNNYYYNNNNYYNNYNYYNYNYYYNYYYYYYNYNYYYNYNYNN","NYYNNNNYNNYNYNNNNYYNNNNYNNNNNNYNNNYNNNNNNNYYNYNNNN","YYYNYYNYNNYYYNNNNYYNNYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNYNNNN","YYYNYNNYNNYYYYNNNYYNNYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","YYYNYYNYYYYYYYYYYYYYYYYYYNYNYYYYYYYYYYYYNYYYYYNYYY","NYYNNNNYNNYNYNNNNYYNNNNYNNNNYNNNNNYNNNNNNNYYNYNNNN","YYYNYYNYYYYYYYYYYYYYYYYYYNYNYYYYYYYYYYYYYYYYYYNYYN","NYYNNNNYNNYNYNNNNYYNNNNYNNNNNNNNNNYNNNNNNNYYNYNYNN","NYYNNNNYNNYYYNNNNYYNNNNYNNNNNNNNNNYNNNNNNNYYNYNNNN","NYYNNNNYNNYNYNNNNYYNNNNYNNYNNNNNNNYNNNNNNNYYNYNNNN","YYYNYNNYNNYYYNYNNYYNNYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","YYYNYNNYNNYYYNNNNYYNNYNYNNYNYYYNNYYYNYNYNYYYNYNYNN","NYYNNNNYNNYNYNNNNYYNNNNYNNNNNNNNNNYNNNNYNNYYNYNNNN","NYYNNNNYNNYNYNNNNYNNNNNYNNNNNNNNNNNNNNNNNNNNNYNNNN","NYYNNNNYNNYNYNNNNYYNNNNYNNNNNNNNNYYNNNNNNNYYNYNNNN","YYYNYNNYNNYYYNNNNYYNNYNYNNYNYYYNYYYYNNNYNYYYNYNYNN","YYYNYNNYNNYYYNNNNYYNNYNYNNYNYYYNNYYYYNNYNYYYNYNYNN","YYYNYNNYNNYYYNNYNYYNNYNYNNYNYYYNYYYYYYNYNYYYNYNYNN","NYYNNNNYNNYNYNNNNYYNNNNYNNNNNNNNNNYNNNNNNYYYNYNNNN","YYYNYYNYYYYYYYYYYYYYYYYYYNYNYYYYYYYYYYYYNYYYYYYYYN","NYYNYNNYNNYNYNNNNYYNNNNYNNNNNNNNNNYNNNNNNNYYNYNNNN","NYYNNNNNNNYNYNNNNYNNNNNYNNNNNNNNNNNNNNNNNNNYNYNNNN","NYYNNNNNNNYNYNNNNYYNNNNYNNNNNNNNNNNNNNNNNNNNNYNNNN","YYYNYYNYYYYYYYYYYYYYYYYYYNYNYYYYYYYYYYYYNYYYNYNYNN","NYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN","YYYNYYNYYYYYYYYYYYYYYYYYYYYNYYYYYYYYYYYYNYYYYYNYYN","NYYNNNNYNNYNYNNNNYYNNNNYNNNNNYNNNNYNNNNNNNYYNYNNNN","YYYNYYNYYYYYYYYYYYYYYYYYYNYNYYYYYYYYYYYYNYYYYYNYNN","YYYNYYYYYYYYYYYYYYYYYYYYYNYNYYYYYYYYYYYYNYYYYYNYYN" };
                              
                    __expected = "Bounded";
                    break;
        }
                /*case 8: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    graph = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(graph, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 8;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> graph, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << graph  << "]" << endl;
   
    BigOEasy *__instance = new BigOEasy();
  string __result = __instance->isBounded(graph);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "BigOEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
