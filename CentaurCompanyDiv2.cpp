#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> a;
vector<int> b;
// ------------- input end  --------------
ll solve(); struct CentaurCompanyDiv2 {ll count(vector<int> _a, vector<int> _b){a = _a, b = _b;return ::solve();}};

int N;

vector<int> g[55];

ll dp[55];
set<int> forbid_set;

ll dfs(int curr, int prev) {
	auto& ret = dp[curr];

	if(ret + 1) { return ret; }

	ret = 1;

	for(auto && next: g[curr]) {
		if(next == prev) { continue; }
		if(forbid_set.count(next)) { continue; }
		ret *= dfs(next, curr) + 1;
	}

	return ret;
}

ll solve() {

	N = a.size() + 1;
	rep(i, 55) g[i].clear();
	rep(i, N-1) {
		g[a[i]-1].push_back(b[i]-1);
		g[b[i]-1].push_back(a[i]-1);
	}

	ll ans = 1;

	forbid_set.clear();

	rep(root, N) {
		minus(dp);
		ans += dfs(root, -1);
		forbid_set.insert(root);
	}

  return ans;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> a;
            vector<int> b;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              a = { 1 };
                                        b = { 2 };
                              
                    __expected = 4LL;
                    break;
        }
                case 1: {
                              a = { 2,2 };
                                        b = { 1,3 };
                              
                    __expected = 7LL;
                    break;
        }
                case 2: {
                              a = { 1,2,3,4,5,6,7,8,9 };
                                        b = { 2,3,4,5,6,7,8,9,10 };
                              
                    __expected = 56LL;
                    break;
        }
                case 3: {
                              a = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
                                        b = { 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51 };
                              
                    __expected = 1125899906842675LL;
                    break;
        }
                case 4: {
                              a = { 10,7,2,5,6,2,4,9,7 };
                                        b = { 8,10,10,4,1,6,2,2,3 };
                              
                    __expected = 144LL;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    a = ;
                  b = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(a, b, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> a, vector<int> b, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << a << ","<< b  << "]" << endl;
   
    CentaurCompanyDiv2 *__instance = new CentaurCompanyDiv2();
  ll __result = __instance->count(a, b);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "CentaurCompanyDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
