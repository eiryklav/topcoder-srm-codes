#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> board;
// ------------- input end  --------------
int solve(); struct FoxAndShogi { int differentOutcomes(vector<string> _board) { board = _board; return solve(); }};

int const MOD = 1e9+7;

int memo[55][55];
int N, M;
vector<int> komaPos;
//string debug;

int rec(string const& B, int rIdx, int const rightPos) {

  if(rIdx < 0) {
//    cout << debug << endl;
    return 1;
  }
  
  if(memo[rIdx][rightPos] >= 0) { return memo[rIdx][rightPos]; }

  ll ret = 0;
  int pos = komaPos[rIdx];
  bool LDir = B[pos] == 'L';
  if(pos >= rightPos) {
    if(B[pos] == 'R') { return 0; }
    pos = rightPos - 1;
  }

  if(pos < 0) { return 0; }

  if(LDir) {
    while(1) {
      if(pos < 0) {
        break;
      }
//      debug[pos] = 'L';
      ret += rec(B, rIdx-1, pos);
//      debug[pos] = '.';
      ret %= MOD;
      pos--;
    }
  }
  else {
    while(pos < rightPos) {
//      debug[pos] = 'R';
      ret += rec(B, rIdx-1, pos);
//      debug[pos] = '.';
      ret %= MOD;
      pos++;
    }
  }

  return memo[rIdx][rightPos] = ret % MOD;
}

int solve() {

  N = board.size(), M = board[0].size();

  ll res = 1;
  rep(i, M) {
    string B;
    komaPos.clear();
    int maxRightPos = -1;
    bool rightIsRight = 0;
    rep(j, N) {
      if(board[j][i] == 'D') { B += 'R'; }
      if(board[j][i] == 'U') { B += 'L'; }
      if(board[j][i] == '.') { B += '.'; }

      if(B.back() == 'R') {
        maxRightPos = j;
        rightIsRight = 1;
        komaPos.push_back(j);
      }
      else if(B.back() == 'L') {
        maxRightPos = j;
        rightIsRight = 0;
        komaPos.push_back(j);
      }
    }

    if(maxRightPos < 0) { continue; }

    ll cnt = 0;
    int rightPos = maxRightPos;
    if(!rightIsRight) {
      while(rightPos >= 0) {
//        debug.clear(); debug.resize(N, '.');
//        debug[rightPos] = 'L';
        minus(memo);
        cnt += rec(B, (int)komaPos.size()-2, rightPos);
        cnt %= MOD;
        rightPos--;
      }
    }
    else {
      while(rightPos < N) {
//        debug.clear(); debug.resize(N, '.');
//        debug[rightPos] = 'R';
        minus(memo);
        cnt += rec(B, (int)komaPos.size()-2, rightPos);
        cnt %= MOD;
        rightPos++;
      }
    }
    if(cnt) { res *= cnt; }
    res %= MOD;
  }

  return (int)(res % MOD);
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> board;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              board = { ".D.","...","..." };
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              board = { ".D.","...",".U." };
                              
                    __expected = 3;
                    break;
        }
                case 2: {
                              board = { "DDDDD",".....",".....",".....","....." };
                              
                    __expected = 3125;
                    break;
        }
                case 3: {
                              board = { "DDDDD","U....",".U...","..U..","...U." };
                              
                    __expected = 900;
                    break;
        }
                case 4: {
                              board = { "....D..","U....D.","D.D.U.D","U.U...D","....U..","D.U...D","U.U...." };
                              
                    __expected = 2268;
                    break;
        }
                case 5: {
                              board = { "DDDDDDDDDD","..........","..........","..........","..........","..........","..........","..........","..........",".........." };
                              
                    __expected = 999999937;
                    break;
        }
                case 6: {
                              board = { "..",".." };
                              
                    __expected = 1;
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    board = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(board, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> board, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << board  << "]" << endl;
   
    FoxAndShogi *__instance = new FoxAndShogi();
  int __result = __instance->differentOutcomes(board);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FoxAndShogi: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
