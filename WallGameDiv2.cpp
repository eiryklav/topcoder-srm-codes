#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define chmax(a,x) a=max(a,x)
#define chmin(a,x) a=min(a,x)

typedef long long ll;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct WallGameDiv2 {int play(vector <string> costs);};

int WallGameDiv2::play(vector <string> costs) {
	int N = costs.size();
	int M = costs[0].size();

	int dp[55][55]; minus(dp);

	dp[0][0] = 0;
	REP(i, 1, M) {
		if(costs[0][i] == 'x') break;
		dp[0][i] = dp[0][i-1] + (costs[0][i] - '0');
	}
	REP(i, 1, N) {
		rep(j, M) {
			if(costs[i][j] == 'x') { continue; }
			rep(k, M) {
				if(dp[i-1][k] < 0) { continue; }
				int sum = 0;
				bool ok = 1;
				int left 	= min(j, k), right = max(j, k);
				REP(l, left, right + 1) {
					if(isdigit(costs[i][l])) {
						sum += costs[i][l] - '0';
					}
					else {
						ok = 0;
					}
				}
				if(ok) {
					chmax(dp[i][j], dp[i-1][k] + sum);
				}
			}
		}
	}
	int res = 0;
	rep(i, M) {
		if(costs[N-1][i] == 'x') { continue; }
		chmax(res, dp[N-2][i] + costs[N-1][i] - '0');
	}
	return res;
}
