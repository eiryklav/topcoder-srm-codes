#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> A;
int N;
// ------------- input end  --------------
int solve(); struct XorSequenceEasy {int getmax(vector<int> _A, int _N){A = _A, N = _N;return ::solve();}};

int solve() {

  int N = A.size();
  int res = 0;
  int score[33] = {};
  rep(i, N) REP(j, i+1, N) {
    if(A[i] < A[j]) { res ++; }
    for(int k=30; k>=0; k--) {
      int l = A[i] >> k & 1;
      int r = A[j] >> k & 1;
      if(l < r) {
        score[k] --;
      }
      if(l > r) {
        score[k] ++;
      }
    }
  }

  rep(i, 33) {
    if(score[i] > 0) { res += score[i]; }
  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> A;
            int N;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = { 3,2,1,0,3,2 };
                                        N = 4;
                              
                    __expected = 8;
                    break;
        }
                case 1: {
                              A = { 3,0,4,6,1,5,7,6 };
                                        N = 8;
                              
                    __expected = 21;
                    break;
        }
                case 2: {
                              A = { 3,1,4,1,5,9,2,6,5,3,5,8,9,7,9 };
                                        N = 16;
                              
                    __expected = 76;
                    break;
        }
                case 3: {
                              A = { 5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5 };
                                        N = 8;
                              
                    __expected = 0;
                    break;
        }
                case 4: {
                              A = { 408024109,11635919,196474438,117649705,812669700,553475508,445349752,271145432,730417256,738416295,147699711,880268351,816031019,686078705,1032012284,182546393,875376506,220137366,906190345,16216108,799485093,715669847,413196148,122291044,777206980,68706223,769896725,212567592,809746340,964776169,928126551,228208603,918774366,352800800,849040635,941604920,326686120,920977486,964528038,659998484,207195539,607901477,725914710,655525412,949610052,142750431,766838105,1024818573,836758851,97228667 };
                                        N = 1073741824;
                              
                    __expected = 720;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = ;
                  N = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(A, N, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> A, int N, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << A << ","<< pretty_print(N)  << "]" << endl;
   
    XorSequenceEasy *__instance = new XorSequenceEasy();
  int __result = __instance->getmax(A, N);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "XorSequenceEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
