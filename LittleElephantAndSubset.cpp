#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
// ------------- input end  --------------
int solve(); struct LittleElephantAndSubset {int getNumber(int _N){N = _N;return solve();}};

int const MOD = 1e9+7;

ll dp[1024][1024];

struct scnt {
  int S; ll cnt;
  scnt(int S, ll cnt): S(S), cnt(cnt) {}
};

vector<scnt> sc;

ll rec(int S, int index) {
  if(index == sc.size()) { return S > 0; }
  ll& ret = dp[S][index];
  if(ret >= 0) { return ret; }
  ret = 0;
  if(!(S & sc[index].S)) {
    // sc[index].S の集合に含まれる数字を全て使った数を「使う」
    ret += sc[index].cnt % MOD * rec(S | sc[index].S, index+1) % MOD;
  }
  {
    // sc[index].S の集合に含まれる数字を全て使った数を「使わない」
    ret += rec(S, index + 1);
  }
  return ret % MOD;
}

int solve() {

  sc.clear();

  REP(S, 1, 1024) {
    string s; rep(i, 10) if(S >> i & 1) { s += char('0' + i); }
    ll cnt = 0;
    do {
      if(s[0] == '0') { continue; }
      try {
        if(stoll(s) <= (ll)N) { cnt += 1; cnt %= MOD; }
        else { break; }
      } catch(...) {
        cout << s << endl;
      }
    } while(next_permutation(all(s)));
    if(cnt) { sc.emplace_back(S, cnt); }
  }

  minus(dp);
  return rec(0, 0) % MOD;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 3;
                              
                    __expected = 7;
                    break;
        }
                case 1: {
                              N = 10;
                              
                    __expected = 767;
                    break;
        }
                case 2: {
                              N = 47;
                              
                    __expected = 25775;
                    break;
        }
                case 3: {
                              N = 4777447;
                              
                    __expected = 66437071;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N)  << "]" << endl;
   
    LittleElephantAndSubset *__instance = new LittleElephantAndSubset();
  int __result = __instance->getNumber(N);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "LittleElephantAndSubset: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
