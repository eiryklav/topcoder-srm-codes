#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct BallsSeparating {int minOperations(vector <int> red, vector <int> green, vector <int> blue);};

int BallsSeparating::minOperations(vector <int> red, vector <int> green, vector <int> blue) {
	int N = red.size();

	if(N < 3) { return -1; }	// R[i], G[i], B[i] >= 1

	int res = 1<<29;
	rep(r, N) rep(g, N) rep(b, N) {
		if(r == g || g == b || b == r) { continue; }
		int cnt = 0;
		rep(i, N) {
			if(r == i) { cnt += green[i] + blue[i]; }
			else if(g == i) { cnt += red[i] + blue[i]; }
			else if(b == i) { cnt += red[i] + green[i]; }
			else {
				cnt += red[i] + green[i] + blue[i] - max({red[i], green[i], blue[i]});
			}
		}
		res = min(res, cnt);
	}

	return res;
}
