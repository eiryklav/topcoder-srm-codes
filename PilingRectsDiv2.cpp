#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> X;
vector<int> Y;
int limit;
// ------------- input end  --------------
int solve(); struct PilingRectsDiv2 {int getmax(vector<int> _X, vector<int> _Y, int _limit){X = _X, Y = _Y, limit = _limit;return solve();}};

int N;

int solve() {

  N = X.size();

  vector<pair<int, int>> ps;

  rep(i, N) if(X[i] < Y[i]) { swap(X[i], Y[i]); }
  rep(i, N) ps.emplace_back(X[i], Y[i]);
  sort(all(ps));

  int res = -1;

  rep(i, N) {
    int cand = 0;
    REP(j, i, N) cand += ps[i].first * ps[j].second >= limit;
    if(cand) { maximize(res, cand); }
  }
  
  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> X;
            vector<int> Y;
            int limit;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              X = { 1,2,3,1 };
                                        Y = { 3,2,4,4 };
                                        limit = 3;
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              X = { 4,7 };
                                        Y = { 7,4 };
                                        limit = 25;
                              
                    __expected = 2;
                    break;
        }
                case 2: {
                              X = { 10 };
                                        Y = { 10 };
                                        limit = 9999;
                              
                    __expected = -1;
                    break;
        }
                case 3: {
                              X = { 10 };
                                        Y = { 3 };
                                        limit = 30;
                              
                    __expected = 1;
                    break;
        }
                case 4: {
                              X = { 3,6,5,8,2,9,14 };
                                        Y = { 14,6,13,8,15,6,3 };
                                        limit = 27;
                              
                    __expected = 4;
                    break;
        }
                case 5: {
                              X = { 121,168,86,106,36,10,125,97,53,26,148,129,41,18,173,55,13,73,91,174,177,190,28,164,122,92,5,26,58,188,14,67,48,196,41,94,24,89,54,117,12,6,155,103,200,128,184,29,92,149 };
                                        Y = { 199,182,43,191,2,145,15,53,38,37,61,45,157,129,119,123,177,178,183,188,132,108,112,137,92,59,75,196,102,152,114,121,181,93,32,3,24,116,4,163,96,159,196,43,59,150,79,113,20,146 };
                                        limit = 5345;
                              
                    __expected = 24;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    X = ;
                  Y = ;
                  limit = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(X, Y, limit, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> X, vector<int> Y, int limit, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << X << ","<< Y << ","<< pretty_print(limit)  << "]" << endl;
   
    PilingRectsDiv2 *__instance = new PilingRectsDiv2();
  int __result = __instance->getmax(X, Y, limit);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "PilingRectsDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
