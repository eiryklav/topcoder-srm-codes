#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> a;
vector<int> p;
vector<int> v;
// ------------- input end  --------------
vector<int> solve(); struct SuccessiveSubtraction2 {vector<int> calc(vector<int> _a, vector<int> _p, vector<int> _v){a = _a, p = _p, v = _v;return ::solve();}};

int dp[2002][3][3];
int N, Q;

vector<int> solve() {
  N = a.size();
  Q = p.size();
  vector<int> ret(Q);

  rep(q, Q) {
    a[p[q]] = v[q];
    rep(i, 2002) rep(j, 3) rep(k, 3) dp[i][j][k] = -inf;
    dp[0][0][0] = a[0];
    REP(i, 1, N) {
      rep(open, 3) rep(close, open+1) {
        if(dp[i-1][open][close] == -inf) { continue; }
        int sign = (open + close) % 2 ? +1 : -1;
        REP(nopen, open, 3) REP(nclose, close, nopen+1) {
          maximize(dp[i][nopen][nclose], dp[i-1][open][close] + a[i] * sign);
        }
      }
    }
    ret[q] = max({dp[N-1][0][0], dp[N-1][1][1], dp[N-1][2][2]});
  }
  return ret;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,vector<int>,vector<int>,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> a;
            vector<int> p;
            vector<int> v;
            vector<int> __expected = vector<int>();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              a = { 1,2,3,4,5 };
                                        p = { 1,2,0,4,3 };
                                        v = { 3,9,-10,5,1 };
                              
                    __expected = { 10, 16, 5, 5, 2 };
                    break;
        }
                case 1: {
                              a = { -100,-100,-100,-100,-100 };
                                        p = { 0,1,2,3,4 };
                                        v = { 0,0,0,0,0 };
                              
                    __expected = { 400, 300, 200, 100, 0 };
                    break;
        }
                case 2: {
                              a = { 83,0,25,21 };
                                        p = { 0,3,2,1,3,1,2 };
                                        v = { 10,-90,33,52,-100,0,45 };
                              
                    __expected = { 56, 125, 133, 81, 91, 143, 155 };
                    break;
        }
                case 3: {
                              a = { 1 };
                                        p = { 0,0,0,0 };
                                        v = { 10,-10,100,-100 };
                              
                    __expected = { 10, -10, 100, -100 };
                    break;
        }
                case 4: {
                              a = { -11,-4,28,38,21,-29,-45,11,-58,-39,92,35,-56,-6,29,-2,61,10,-29,-63 };
                                        p = { 19,5,3,10,4,18,5,2,0,15 };
                                        v = { -19,21,7,-66,38,-39,-22,24,-32,13 };
                              
                    __expected = { 451, 443, 412, 440, 457, 467, 468, 464, 443, 458 };
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    a = ;
                  p = ;
                  v = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(a, p, v, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> a, vector<int> p, vector<int> v, vector<int> __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << a << ","<< p << ","<< v  << "]" << endl;
   
    SuccessiveSubtraction2 *__instance = new SuccessiveSubtraction2();
  vector<int> __result = __instance->calc(a, p, v);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  __expected  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  __result  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "SuccessiveSubtraction2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
