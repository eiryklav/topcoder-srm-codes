#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct UndoHistory {int minPresses(vector <string> lines);};

bool is_prefix_of_string(string const& a, string const& b) {
	int asize = a.size(), bsize = b.size();
	if(asize > bsize) {
		return false;
	}
	rep(i, asize) {
		if(a[i] != b[i]) { return false; }
	}
	return true;
}

int UndoHistory::minPresses(vector <string> lines) {

	int N = lines.size();
	int ret = 0;
	set<string> history;
	rep(i, N) {
		int plus = inf;

		// 現在の状態からそのまま遷移
		if(!i || is_prefix_of_string(lines[i-1], lines[i])) {
			plus = lines[i].size() - (!i ? 0 : lines[i-1].size());
		}

		// historyを活用
		for(auto& e: history) {
			if(is_prefix_of_string(e, lines[i])) {
				plus = min(plus, (int)(lines[i].size()-e.size()+2));
			}
		}
		ret += plus + 1;
		rep(j, lines[i].size()+1) {
			history.insert(lines[i].substr(0, j));
		}
	}

	return ret;
}
