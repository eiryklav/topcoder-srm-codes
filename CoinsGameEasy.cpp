#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct CoinsGameEasy {int minimalSteps(vector <string> board);};

typedef vector<pair<int, int>> vpii;

int N, M;
vector<string> B;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int bfs(vpii const& ps) {
	int ret = 11;
	queue<pair<vpii, int>> q;
	q.emplace(ps, 0);

	set<vpii> st;
	st.insert(ps);
	while(!q.empty()) { 
		auto p = q.front(); q.pop();
		if(p.second == 11) { continue; }
		rep(k, 4) {
			int out_num = 0;
			int nx1 = p.first[0].first + dx[k], ny1 = p.first[0].second + dy[k];
			if(!in_range(nx1, ny1, M, N)) { out_num++; }
			else {
				if(B[ny1][nx1] == '#') { nx1 = p.first[0].first, ny1 = p.first[0].second; }
				else {}
			}

			int nx2 = p.first[1].first + dx[k], ny2 = p.first[1].second + dy[k];
			if(!in_range(nx2, ny2, M, N)) {
				out_num++;
				if(out_num == 2) { continue; }
				if(out_num == 1) { ret = min(ret, p.second+1); }
			}
			else {
				if(out_num == 1) { ret = min(ret, p.second+1); }
				else {
					if(B[ny2][nx2] == '#') { nx2 = p.first[1].first, ny2 = p.first[1].second; }
					auto nvec = vpii{make_pair(nx1,ny1), make_pair(nx2,ny2)};
					if(st.count(nvec)) { continue; }
					st.insert(nvec);
					q.emplace(nvec, p.second+1);
				}
			}
		}
	}

	return ret == 11 ? -1 : ret;
}

int CoinsGameEasy::minimalSteps(vector <string> board) {
	B = board;
	N = board.size(), M = board[0].size();
	vector<pair<int, int>> ps;
	rep(i, N) rep(j, M) {
		if(board[i][j] == 'o') {
			ps.emplace_back(j, i);
		}
	}
	
	return bfs(ps);
}
