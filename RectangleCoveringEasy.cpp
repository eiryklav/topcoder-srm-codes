#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int holeH;
int holeW;
int boardH;
int boardW;
// ------------- input end  --------------
int Solve(); struct RectangleCoveringEasy {int solve(int _holeH, int _holeW, int _boardH, int _boardW){holeH = _holeH, holeW = _holeW, boardH = _boardH, boardW = _boardW;return Solve();}};

int Solve() {
  bool ok1 = holeH <= boardH && holeW <= boardW && (holeH < boardH || holeW < boardW);
  swap(holeH, holeW);
  bool ok2 = holeH <= boardH && holeW <= boardW && (holeH < boardH || holeW < boardW);
  return ok1 || ok2 ? 1 : -1;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int holeH;
            int holeW;
            int boardH;
            int boardW;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              holeH = 1;
                                        holeW = 1;
                                        boardH = 1;
                                        boardW = 1;
                              
                    __expected = -1;
                    break;
        }
                case 1: {
                              holeH = 3;
                                        holeW = 5;
                                        boardH = 4;
                                        boardW = 6;
                              
                    __expected = 1;
                    break;
        }
                case 2: {
                              holeH = 10;
                                        holeW = 20;
                                        boardH = 25;
                                        boardW = 15;
                              
                    __expected = 1;
                    break;
        }
                case 3: {
                              holeH = 3;
                                        holeW = 10;
                                        boardH = 3;
                                        boardW = 12;
                              
                    __expected = 1;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    holeH = ;
                  holeW = ;
                  boardH = ;
                  boardW = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(holeH, holeW, boardH, boardW, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int holeH, int holeW, int boardH, int boardW, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(holeH) << ","<< pretty_print(holeW) << ","<< pretty_print(boardH) << ","<< pretty_print(boardW)  << "]" << endl;
   
    RectangleCoveringEasy *__instance = new RectangleCoveringEasy();
  int __result = __instance->solve(holeH, holeW, boardH, boardW);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "RectangleCoveringEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
