#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> field;
vector<string> moves;
// ------------- input end  --------------
string solve(); struct GameInDarknessDiv2 { string check(vector<string> _field, vector<string> _moves) { field = _field, moves = _moves; return solve(); }};

int N, M;
int ax, ay;
int x, y;

int T;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};
string D = "LURD";

bool inField(int ny, int nx) {
  if(!in_range(ny, nx, N, M)) { return false; }
  if(field[ny][nx] == '#') { return false; }
  return true;
}

void tryAliceMove(char c) {
  int d = D.find(c);
  int ny = ay+dy[d], nx = ax+dx[d];
  if(!inField(ny, nx)) { return; }
  ay = ny, ax = nx;
}

string solve() {
  N = field.size(); M = field[0].size();
  string move; rep(i, moves.size()) move += moves[i];

  rep(i, N) rep(j, M) {
    if(field[i][j] == 'A') {
      ay = i, ax = j;
    }
    if(field[i][j] == 'B') {
      y = i, x = j;
    }
  }

  T = move.size();
  set<pair<int, int>> q;
  q.emplace(y, x);
  rep(step, T) {
    tryAliceMove(move[step]);
    set<pair<int, int>> nq;
    while(!q.empty()) {
      auto p = *q.begin(); q.erase(p);
      int y = p.first, x = p.second;
      if(ay == y && ax == x) { continue; }
      rep(i, 4) {
        int ny = y+dy[i], nx = x+dx[i];
        if(inField(ny, nx)) {
          if(ny == ay && nx == ax) { continue; }
          nq.emplace(ny, nx);
        }
      }
    }
    q = nq;

    if(q.empty()) {
      return "Alice wins";
    }
  }

  return "Bob wins";
}
