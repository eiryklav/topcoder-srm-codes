#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct TheDeviceDiv2 {string identify(vector <string> plates);};

string TheDeviceDiv2::identify(vector <string> plates) {

	int N = plates.size();
	int M = plates[0].size();
	rep(i, M) {
		int zeros = 0, ones = 0;
		rep(j, N) {
			if(plates[j][i] == '0') { zeros ++; }
			if(plates[j][i] == '1') { ones ++; }
		}
		if(zeros < 1 || ones < 2) {
			return "NO";
		}
	}
	return "YES";
}
