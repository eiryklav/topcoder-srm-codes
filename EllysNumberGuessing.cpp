#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> guesses;
vector<int> answers;
// ------------- input end  --------------
int solve(); struct EllysNumberGuessing {int getNumber(vector<int> _guesses, vector<int> _answers){guesses = _guesses, answers = _answers;return solve();}};

int solve() {
  set<int> st;
  if(1 <= guesses[0]-answers[0]) {
    st.insert(guesses[0]-answers[0]);
  }
  if(guesses[0]+answers[0] <= (int)1e9) {
    st.insert(guesses[0]+answers[0]);
  }
  REP(i, 1, guesses.size()) {
    set<int> nst;
    if(1 <= guesses[i]-answers[i] && guesses[i]-answers[i] <= (int)1e9 && st.count(guesses[i]-answers[i])) {
      nst.insert(guesses[i]-answers[i]);
    }
    if(1 <= guesses[i]+answers[i] && guesses[i]+answers[i] <= (int)1e9 && st.count(guesses[i]+answers[i])) {
      nst.insert(guesses[i]+answers[i]);
    }
    swap(st, nst);
  }
  if(st.size() == 1) { return *st.begin(); }
  if(st.size() == 0) { return -2; }
  return -1;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> guesses;
            vector<int> answers;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              guesses = { 600,594 };
                                        answers = { 6,12 };
                              
                    __expected = 606;
                    break;
        }
                case 1: {
                              guesses = { 100,50,34,40 };
                                        answers = { 58,8,8,2 };
                              
                    __expected = 42;
                    break;
        }
                case 2: {
                              guesses = { 500000,600000,700000 };
                                        answers = { 120013,220013,79987 };
                              
                    __expected = -2;
                    break;
        }
                case 3: {
                              guesses = { 500000000 };
                                        answers = { 133742666 };
                              
                    __expected = -1;
                    break;
        }
                case 4: {
                              guesses = { 76938260,523164588,14196746,296286419,535893832,41243148,364561227,270003278,472017422,367932361,395758413,301278456,186276934,316343129,336557549,52536121,98343562,356769915,89249181,335191879 };
                                        answers = { 466274085,20047757,529015599,246925926,7318513,501969197,178651118,273209067,71194923,175279984,147453932,241933889,356935411,226869216,206654796,490676224,444868783,186442430,453963164,208020466 };
                              
                    __expected = 543212345;
                    break;
        }
                case 5: {
                              guesses = { 42 };
                                        answers = { 42 };
                              
                    __expected = 84;
                    break;
        }
                case 6: {
                              guesses = { 999900000 };
                                        answers = { 100001 };
                              
                    __expected = 999799999;
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    guesses = ;
                  answers = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(guesses, answers, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> guesses, vector<int> answers, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << guesses << ","<< answers  << "]" << endl;
   
    EllysNumberGuessing *__instance = new EllysNumberGuessing();
  int __result = __instance->getNumber(guesses, answers);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "EllysNumberGuessing: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
