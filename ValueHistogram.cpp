#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct ValueHistogram {vector <string> build(vector <int> values);};

vector <string> ValueHistogram::build(vector <int> values) {
	int hist[10] = {};
	int max = 0;
	rep(i, values.size()) {
		hist[values[i]]++;
		max = std::max(max, hist[values[i]]);
	}

	const int H = max+1, W = 10;

	vector<string> ret(H, string(W, '.'));

	rep(i, H) rep(j, W) {
		if(hist[j] > i) { ret[H-1-i][j] = 'X'; }
	}

	return ret;
}
