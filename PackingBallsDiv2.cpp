#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int R;
int G;
int B;
// ------------- input end  --------------
int solve(); struct PackingBallsDiv2 {int minPacks(int _R, int _G, int _B){R = _R, G = _G, B = _B;return solve();}};

int solve() {
  int R_ = R, G_ = G, B_ = B;
  int ans = inf;
  rep(numberOfVarietySet, 101) {
    R = R_, G = G_, B = B_;
    int K = numberOfVarietySet;
    int cand = 0;
    int three = min({R, G, B, K});
    cand += three;
    R -= three, G -= three, B -= three, K -= three;
    if(K > 0) {
      int two;
      if(R == 0) two = min({G, B, K});
      if(G == 0) two = min({B, R, K});
      if(B == 0) two = min({R, G, K});
      cand += two;
      if(R > 0) R -= two;
      if(G > 0) G -= two;
      if(B > 0) B -= two;
      if(K > 0) K -= two;
    }
    if(K > 0) { continue; }
    cand += R / 3 + (R % 3 > 0);
    cand += G / 3 + (G % 3 > 0);
    cand += B / 3 + (B % 3 > 0);
    minimize(ans, cand);
  }
  return ans;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int R;
            int G;
            int B;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              R = 4;
                                        G = 2;
                                        B = 4;
                              
                    __expected = 4;
                    break;
        }
                case 1: {
                              R = 1;
                                        G = 7;
                                        B = 1;
                              
                    __expected = 3;
                    break;
        }
                case 2: {
                              R = 2;
                                        G = 3;
                                        B = 5;
                              
                    __expected = 4;
                    break;
        }
                case 3: {
                              R = 78;
                                        G = 53;
                                        B = 64;
                              
                    __expected = 66;
                    break;
        }
                case 4: {
                              R = 100;
                                        G = 100;
                                        B = 100;
                              
                    __expected = 100;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    R = ;
                  G = ;
                  B = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(R, G, B, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int R, int G, int B, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(R) << ","<< pretty_print(G) << ","<< pretty_print(B)  << "]" << endl;
   
    PackingBallsDiv2 *__instance = new PackingBallsDiv2();
  int __result = __instance->minPacks(R, G, B);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "PackingBallsDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
