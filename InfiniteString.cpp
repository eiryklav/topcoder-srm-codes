#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string s;
string t;
// ------------- input end  --------------
string solve(); struct InfiniteString {string equal(string _s, string _t){s = _s, t = _t;return ::solve();}};

string solve() {

  auto get_los = [](string s) {
    int n = s.size();
    string los;
    int p = 0, prev = 0;
    rep(i, n) {
      if(los.empty() || los[p] != s[i]) {
        los += string(s.begin() + prev, s.begin() + i + 1);
        p = 0;
        prev = i + 1;
      } else {
        p ++;
        if(p >= los.size()) { p = 0; }
      }
    }
    if(p != 0) {
      return s;
    }
    return los;
  };

  return get_los(s) == get_los(t) ? "Equal" : "Not equal";
}

// CUT begin
using namespace std;
    char do_test(string,string,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            string s;
            string t;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              s = "ab";
                                        t = "abab";
                              
                    __expected = "Equal";
                    break;
        }
                case 1: {
                              s = "abc";
                                        t = "bca";
                              
                    __expected = "Not equal";
                    break;
        }
                case 2: {
                              s = "abab";
                                        t = "aba";
                              
                    __expected = "Not equal";
                    break;
        }
                case 3: {
                              s = "aaaaa";
                                        t = "aaaaaa";
                              
                    __expected = "Equal";
                    break;
        }
                case 4: {
                              s = "ababab";
                                        t = "abab";
                              
                    __expected = "Equal";
                    break;
        }
                case 5: {
                              s = "a";
                                        t = "z";
                              
                    __expected = "Not equal";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    s = ;
                  t = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(s, t, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(string s, string t, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(s) << ","<< pretty_print(t)  << "]" << endl;
   
    InfiniteString *__instance = new InfiniteString();
  string __result = __instance->equal(s, t);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "InfiniteString: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
