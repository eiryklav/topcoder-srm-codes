#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> par;
vector<int> num;
// ------------- input end  --------------
string solve(); struct TreeCutting {string can(vector<int> _par, vector<int> _num){par = _par, num = _num;return ::solve();}};

vector<int> G[55];
int N;
int dp[55][55][55][55][2];

int f(int curr, int ch, int x, int s, int flag) {
  auto& ret = dp[curr][ch][x][s][flag];
  if(ret + 1) { return ret; }
  ret = 0;

  if(ch == G[curr].size() + 1) {
    ret = s == 0 && flag == 0;
  }
  else if(ch > 0) {
    REP(i, 1, N+1) {
      ret |= f(curr, ch+1, x, s, flag) && f(G[curr][ch-1], 0, i, i, 1);
    }
    REP(i, 1, s+1) rep(j, flag+1) {
      ret |= f(curr, ch+1, x, s-i, flag-j) && f(G[curr][ch-1], 0, x, i, j);
    }
  }
  else {
    if(s > 0) {
      if(num[curr] == -1) {
        ret = f(curr, 1, x, s-1, flag);
      }
      else {
        if(num[curr] == x && flag == 1) {
          ret = f(curr, 1, x, s-1, 0);
        }
      }
    }
  }

  return ret;
}

string solve() {
  N = par.size() + 1;
  rep(i, 55) G[i].clear();
  rep(i, N-1) {
    G[par[i]].push_back(i+1);
  }
  minus(dp);
  REP(i, 1, N+1) {
    if(f(0, 0, i, i, 1)) {
      return "POSSIBLE";
    }
  }
  return "IMPOSSIBLE";
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> par;
            vector<int> num;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              par = { 0,1,2,2,2 };
                                        num = { 2,-1,-1,4,-1,-1 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                case 1: {
                              par = { 0,1,2,2,2 };
                                        num = { 3,-1,-1,3,-1,-1 };
                              
                    __expected = "IMPOSSIBLE";
                    break;
        }
                case 2: {
                              par = { 0,1,2,2,2 };
                                        num = { 2,-1,-1,3,-1,-1 };
                              
                    __expected = "IMPOSSIBLE";
                    break;
        }
                case 3: {
                              par = { 0,1,2,2,1,5,2,6,6 };
                                        num = { -1,-1,2,-1,1,3,-1,1,1,2 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                case 4: {
                              par = { 0,1,2,2,1,5,2,6,6 };
                                        num = { -1,-1,2,-1,1,-1,3,1,1,2 };
                              
                    __expected = "IMPOSSIBLE";
                    break;
        }
                case 5: {
                              par = { 0,0,0,0,1,1,2,3,3,3,4,4,4,5,9,9,14,14,14,16,20 };
                                        num = { -1,3,-1,-1,-1,-1,-1,1,1,-1,-1,-1,3,1,-1,1,8,-1,-1,4,-1,-1 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                case 6: {
                              par = { 0,0,0,0,1,1,2,3,3,3,4,4,4,5,9,9,14,14,14,16,20 };
                                        num = { -1,2,-1,-1,-1,-1,-1,1,1,-1,-1,-1,3,1,-1,1,9,-1,-1,4,-1,-1 };
                              
                    __expected = "IMPOSSIBLE";
                    break;
        }
                case 7: {
                              par = { 0,0,1,1 };
                                        num = { -1,-1,5,-1,-1 };
                              
                    __expected = "POSSIBLE";
                    break;
        }
                /*case 8: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    par = ;
                  num = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(par, num, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 8;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> par, vector<int> num, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << par << ","<< num  << "]" << endl;
   
    TreeCutting *__instance = new TreeCutting();
  string __result = __instance->can(par, num);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TreeCutting: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
