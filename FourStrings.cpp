#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string a;
string b;
string c;
string d;
// ------------- input end  --------------
int solve(); struct FourStrings {int shortestLength(string _a, string _b, string _c, string _d){a = _a, b = _b, c = _c, d = _d;return solve();}};

int A, B, C, D;

int memo[41][11][11][11][11];
vector<char> list;

int dfs(int idx, int i, int j, int k, int l) {

  if(idx == 41) return inf;

  auto& ret = memo[idx][i][j][k][l];

  if(ret + 1) return ret;

  if(i == A && j == B && k == C && l == D) {
    return ret = 0;
  }

  ret = inf;

  for(auto && x: list) {
    rep(ui, 2) rep(uj, 2) rep(uk, 2) rep(ul, 2) {
      int ni = i == A ? A : a[i] == x ? i + 1 : 0;
      int nj = j == B ? B : b[j] == x ? j + 1 : 0;
      int nk = k == C ? C : c[k] == x ? k + 1 : 0;
      int nl = l == D ? D : d[l] == x ? l + 1 : 0;
      if(!ui) ni = 0;
      if(!uj) nj = 0;
      if(!uk) nk = 0;
      if(!ul) nl = 0;
      minimize(ret, dfs(idx+1,ni,nj,nk,nl)+1);
    }
  }

  return ret;
}

int solve() {

  A = a.size();
  B = b.size();
  C = c.size();
  D = d.size();

  list.clear();
  rep(i, A) list.push_back(a[i]);
  rep(i, B) list.push_back(b[i]);
  rep(i, C) list.push_back(c[i]);
  rep(i, D) list.push_back(d[i]);

  sort(all(list));
  list.erase(unique(all(list)), list.end());

  minus(memo);

  return dfs(0, 0, 0, 0, 0);
}

// CUT begin
using namespace std;
    char do_test(string,string,string,string,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            string a;
            string b;
            string c;
            string d;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              a = "abc";
                                        b = "ab";
                                        c = "bc";
                                        d = "b";
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              a = "a";
                                        b = "bc";
                                        c = "def";
                                        d = "ghij";
                              
                    __expected = 10;
                    break;
        }
                case 2: {
                              a = "top";
                                        b = "coder";
                                        c = "opco";
                                        d = "pcode";
                              
                    __expected = 8;
                    break;
        }
                case 3: {
                              a = "thereare";
                                        b = "arelots";
                                        c = "lotsof";
                                        d = "ofcases";
                              
                    __expected = 19;
                    break;
        }
                case 4: {
                              a = "aba";
                                        b = "b";
                                        c = "b";
                                        d = "b";
                              
                    __expected = 3;
                    break;
        }
                case 5: {
                              a = "x";
                                        b = "x";
                                        c = "x";
                                        d = "x";
                              
                    __expected = 1;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    a = ;
                  b = ;
                  c = ;
                  d = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(a, b, c, d, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(string a, string b, string c, string d, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(a) << ","<< pretty_print(b) << ","<< pretty_print(c) << ","<< pretty_print(d)  << "]" << endl;
   
    FourStrings *__instance = new FourStrings();
  int __result = __instance->shortestLength(a, b, c, d);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FourStrings: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
