#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct CentaurCompanyDiv2 {long long count(vector <int> a, vector <int> b);};

int N;
vector<int> tree[55];

ll dp[55];

ll dfs(int curr) {
	ll& res = dp[curr];
	if(res >= 0) return res;

	res = 1;

	rep(i, tree[curr].size()) {
		int tar = tree[curr][i];
		if(dp[tar] >= 0) { continue; }
		res *= 1 + dfs(tar);
	}

	return res;
}

long long CentaurCompanyDiv2::count(vector <int> a, vector <int> b) {

//	rep(i, 55) tree[i].clear();
	N = a.size()+1; // vertex size;
	rep(i, a.size()) {
		a[i]--, b[i]--;
		tree[a[i]].push_back(b[i]);
		tree[b[i]].push_back(a[i]);
	}

	minus(dp);

	dfs(0);

	ll res = 0;
	rep(i, 55) {
		if(dp[i] >= 0) {
			res += dp[i];
		}
	}

	return res + 1;
}
