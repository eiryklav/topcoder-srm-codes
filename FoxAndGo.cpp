#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> board;
// ------------- input end  --------------
int solve(); struct FoxAndGo { int maxKill(vector<string> _board) { board = _board; return solve(); }};

int N, M;
vector<string> B;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int check() {
  int ret = 0;

  vector<vector<bool>> used(N, std::move(vector<bool>(M)));
  rep(i, N) rep(j, M) {
    if(B[i][j] != 'o') { continue; }
    if(used[i][j]) { continue; }
    queue<pair<int, int>> q;
    q.emplace(i, j);
    used[i][j] = 1;
    bool ok = 1;
    int ocnt = 0;
    while(!q.empty()) {
      auto p = q.front(); q.pop();
      ocnt++;
      used[p.first][p.second] = 1;
      rep(i, 4) {
        auto ny = p.first + dy[i], nx = p.second + dx[i];
        if(!in_range(ny, nx, N, M)) { continue; }
        if(B[ny][nx] == '.') { ok = 0; continue; }
        if(B[ny][nx] == 'x') { continue; }
        if(used[ny][nx]) { continue; }
        used[ny][nx] = 1;
        q.emplace(ny, nx);
      }
    }
    if(ok) { ret += ocnt; }
  }

  return ret;
}

int solve() {

  N = board.size(), M = board[0].size();
  B = board;

  int res = 0;

  rep(i, N) rep(j, M) {
    if(board[i][j] == '.') {
      B[i][j] = 'x';
      maximize(res, check());
      B[i][j] = '.';
    }
  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> board;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              board = { ".....","..x..",".xox.",".....","....." };
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              board = { "ooooo","xxxxo","xxxx.","xxxx.","ooooo" };
                              
                    __expected = 6;
                    break;
        }
                case 2: {
                              board = { ".xoxo","ooxox","oooxx","xoxox","oxoox" };
                              
                    __expected = 13;
                    break;
        }
                case 3: {
                              board = { ".......",".......",".......","xxxx...","ooox...","ooox...","ooox..." };
                              
                    __expected = 9;
                    break;
        }
                case 4: {
                              board = { ".......",".xxxxx.",".xooox.",".xo.ox.",".xooox.",".xxxxx.","......." };
                              
                    __expected = 8;
                    break;
        }
                case 5: {
                              board = { "o.xox.o","..xox..","xxxoxxx","ooo.ooo","xxxoxxx","..xox..","o.xox.o" };
                              
                    __expected = 12;
                    break;
        }
                case 6: {
                              board = { ".......","..xx...",".xooox.",".oxxox.",".oxxxo.","...oo..","......." };
                              
                    __expected = 4;
                    break;
        }
                case 7: {
                              board = { ".ox....","xxox...","..xoox.","..xoox.","...xx..",".......","......." };
                              
                    __expected = 5;
                    break;
        }
                case 8: {
                              board = { "...................","...................","...o..........o....","................x..","...............x...","...................","...................","...................","...................","...................","...................","...................","...................","...................","................o..","..x................","...............x...","...................","..................." };
                              
                    __expected = 0;
                    break;
        }
                /*case 9: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    board = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(board, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 9;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> board, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << board  << "]" << endl;
   
    FoxAndGo *__instance = new FoxAndGo();
  int __result = __instance->maxKill(board);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FoxAndGo: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
