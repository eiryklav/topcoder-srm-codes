#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct TypingDistance {int minDistance(string keyboard, string word);};

int TypingDistance::minDistance(string keyboard, string word) {
  int res = 0;

  int dist[256][256] = {};
  rep(i, keyboard.size()) {
  	rep(j, keyboard.size()) {
  		dist[(int)keyboard[i]][(int)keyboard[j]] = abs(j - i);
  	}
  }

  word.erase(unique(all(word)), word.end());

  rep(i, word.size()-1) {
  	res += dist[(int)word[i]][(int)word[i+1]];
  }

  return res;
}