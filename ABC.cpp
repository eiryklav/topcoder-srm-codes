#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int N;
int K;
// ------------- input end  --------------
string solve(); struct ABC {string createString(int _N, int _K){N = _N, K = _K;return ::solve();}};

char dp[33][33][33][500]; // N*(N-1)/2なので、500なら余裕。N*(N+1)/2とかだったら危なかったかも...しかしペアの数はN(N-1)/2であるのはアタリマエ。

string solve() {

  zero(dp);
  dp[0][1][0][0] = 'A';
  dp[0][0][1][0] = 'B';
  dp[0][0][0][0] = 'C';

  REP(i, 1, N) {
    rep(a, 33) rep(b, 33) {
      if(a + b > i) { continue; }
      rep(k, 500) {
        if(!dp[i-1][a][b][k]) { continue; }
        dp[i][a+1][b][k] = 'A';
        if(k+a < 500) dp[i][a][b+1][k+a] = 'B';
        if(k+a+b < 500) dp[i][a][b][k+a+b] = 'C';
      }
    }
  }

  rep(a, 33) rep(b, 33) {
    if(dp[N-1][a][b][K]) {
      string ret;
      int pos = N-1;
      while(pos >= 0) {
        if(dp[pos][a][b][K] == 'A') {
          ret += 'A';
          a --;
        }
        else if(dp[pos][a][b][K] == 'B') {
          ret += 'B';
          K -= a;
          b --;
        }
        else {// assert(dp[pos][a][b][K]);
          ret += 'C';
          K -= a + b;
        }
        pos --;
      }
      reverse(all(ret));
      return ret;
    }
  }

  return "";
}

// CUT begin
using namespace std;
    char do_test(int,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            int K;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 3;
                                        K = 3;
                              
                    __expected = "ABC";
                    break;
        }
                case 1: {
                              N = 3;
                                        K = 0;
                              
                    __expected = "CBA";
                    break;
        }
                case 2: {
                              N = 5;
                                        K = 10;
                              
                    __expected = "";
                    break;
        }
                case 3: {
                              N = 15;
                                        K = 36;
                              
                    __expected = "CABBACCBAABCBBB";
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  K = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, K, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, int K, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< pretty_print(K)  << "]" << endl;
   
    ABC *__instance = new ABC();
  string __result = __instance->createString(N, K);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ABC: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
