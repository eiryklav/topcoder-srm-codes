#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
string Directions;
// ------------- input end  --------------
int solve(); struct GearsDiv2 { int getmin(string _Directions) { Directions = _Directions; return solve(); }};

int N;

int solve() {

  N = Directions.size();

  bool ok = 1;
  rep(i, N+1) {
    if(Directions[i] == Directions[(i+1)%N]) {
      ok = 0;
      break;
    }
  }

  if(ok) { return 0; }
  auto S = Directions;
  auto T = S;

  /*
    S[i-1] == S[i] の比較で、互いに異なるとき、S[i]を取り除くようにする（S[i]=' 'などとする）。
    そうすれば、S[i-1], S[i+1]のどちらも、S[i]が取り除かれたことが考慮されることになる。
    しかし歯車が円形に並んでいるため、このままでは比較開始位置sに対して、S[s-1]の扱いに困る。
    よって、s=1として、S[0]を取り除く場合と取り除かない場合の両方を試して、その小さい方を答えれば良い。
   */

  int cnt1 = 0;
  REP(i, 1, N+1) {
    if(S[i-1] == S[i%N]) cnt1++, S[i] = '~';
  }
  int cnt2 = 1;
  REP(i, 2, N) {
    if(T[i-1] == T[i]) cnt2++, T[i] = '~';
  }
  return min(cnt1, cnt2);
}
