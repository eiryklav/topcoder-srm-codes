#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int m;
vector<int> x;
vector<int> y;
// ------------- input end  --------------
int solve(); struct EnclosingTriangleColorful { int getNumber(int _m, vector<int> _x, vector<int> _y) { m = _m, x = _x, y = _y; return solve(); }};

int solve() {
  return 0;
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int m;
            vector<int> x;
            vector<int> y;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              m = 4;
                                        x = { 1,2,3 };
                                        y = { 1,3,2 };
                              
                    __expected = 8;
                    break;
        }
                case 1: {
                              m = 7;
                                        x = { 1,1,6,6 };
                                        y = { 1,6,1,6 };
                              
                    __expected = 0;
                    break;
        }
                case 2: {
                              m = 4;
                                        x = { 2 };
                                        y = { 2 };
                              
                    __expected = 72;
                    break;
        }
                case 3: {
                              m = 10;
                                        x = { 2,6,7,6 };
                                        y = { 7,7,9,3 };
                              
                    __expected = 52;
                    break;
        }
                case 4: {
                              m = 15;
                                        x = { 7,6,5,4,3 };
                                        y = { 1,4,7,10,13 };
                              
                    __expected = 150;
                    break;
        }
                case 5: {
                              m = 300;
                                        x = { 117,183,181,118,132,128,184,150,168,121,179,132,168,182,119,117,180,120,175,177,116,175,128,163,181,178,123,118,172,143,163,157,179,122,121,119,157,122,150,180,137,177,125,123,172,125,137,143,120,178 };
                                        y = { 157,157,132,163,181,180,150,116,181,125,125,119,119,163,132,143,172,172,179,178,150,121,120,118,168,123,178,137,120,117,182,117,175,177,175,168,183,123,184,128,118,122,179,122,180,121,182,183,128,177 };
                              
                    __expected = 21690886;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    m = ;
                  x = ;
                  y = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(m, x, y, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int m, vector<int> x, vector<int> y, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(m) << ","<< x << ","<< y  << "]" << endl;
   
    EnclosingTriangleColorful *__instance = new EnclosingTriangleColorful();
  int __result = __instance->getNumber(m, x, y);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "EnclosingTriangleColorful: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
