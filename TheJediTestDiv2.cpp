#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct TheJediTestDiv2 {int countSupervisors(vector <int> students, int Y, int J);};

int TheJediTestDiv2::countSupervisors(vector <int> students, int Y, int J) {
	int N = students.size();
	int ans = inf;
	rep(i, N) {
		auto v = students;
		v[i] = max(0, v[i]-Y);
		int cnt = 0;
		rep(j, N) {
			cnt += v[j] / J + (v[j] % J > 0);
		}
		ans = min(ans, cnt);
	}
	return ans;
}
