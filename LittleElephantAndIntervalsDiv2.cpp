#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int M;
vector<int> L;
vector<int> R;
// ------------- input end  --------------
int solve(); struct LittleElephantAndIntervalsDiv2 {int getNumber(int _M, vector<int> _L, vector<int> _R){M = _M, L = _L, R = _R;return solve();}};

set<vector<bool>> st;

void dfs(vector<bool>& v, int depth) {
  if(L.size() == depth) {
    st.insert(v);
    return;
  }

  REP(i, L[depth], R[depth]+1) {
    v[i] = 0;
  }
  dfs(v, depth+1);

  REP(i, L[depth], R[depth]+1) {
    v[i] = 1;
  }
  dfs(v, depth+1);

}

int solve() {
  st.clear();
  rep(i, L.size()) L[i]--, R[i]--;
  vector<bool> v(M);
  dfs(v, 0);
  return st.size();
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int M;
            vector<int> L;
            vector<int> R;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              M = 4;
                                        L = { 1,2,3 };
                                        R = { 1,2,3 };
                              
                    __expected = 8;
                    break;
        }
                case 1: {
                              M = 3;
                                        L = { 1,1,2 };
                                        R = { 3,1,3 };
                              
                    __expected = 4;
                    break;
        }
                case 2: {
                              M = 100;
                                        L = { 47 };
                                        R = { 74 };
                              
                    __expected = 2;
                    break;
        }
                case 3: {
                              M = 100;
                                        L = { 10,20,50 };
                                        R = { 20,50,100 };
                              
                    __expected = 8;
                    break;
        }
                case 4: {
                              M = 42;
                                        L = { 5,23,4,1,15,2,22,26,13,16 };
                                        R = { 30,41,17,1,21,6,28,30,15,19 };
                              
                    __expected = 512;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    M = ;
                  L = ;
                  R = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(M, L, R, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int M, vector<int> L, vector<int> R, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(M) << ","<< L << ","<< R  << "]" << endl;
   
    LittleElephantAndIntervalsDiv2 *__instance = new LittleElephantAndIntervalsDiv2();
  int __result = __instance->getNumber(M, L, R);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "LittleElephantAndIntervalsDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
