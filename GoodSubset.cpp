#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int goodValue;
vector<int> d;
// ------------- input end  --------------
int solve(); struct GoodSubset {int numberOfSubsets(int _goodValue, vector<int> _d){goodValue = _goodValue, d = _d;return ::solve();}};

int const MOD = 1e9+7;

unordered_map<int, int> memo[111];
static int a = 0;
int dfs(int idx, int val) {
  a ++;
  if(idx == d.size()) { return val == 1; }
  if(memo[idx].find(val) != memo[idx].end()) { return memo[idx][val]; }
  return memo[idx][val] = (dfs(idx + 1, val) + (val % d[idx] == 0 ? dfs(idx + 1, val / d[idx]) : 0)) % MOD;
}

int solve() {
  a = 0;
  rep(i, 111) memo[i].clear();
  int k = dfs(0, goodValue) - (goodValue == 1);  // non-empty subsets
  cout << a << endl;
  return k;
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int goodValue;
            vector<int> d;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              goodValue = 10;
                                        d = { 2,3,4,5 };
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              goodValue = 6;
                                        d = { 2,3,4,5,6 };
                              
                    __expected = 2;
                    break;
        }
                case 2: {
                              goodValue = 1;
                                        d = { 1,1,1 };
                              
                    __expected = 7;
                    break;
        }
                case 3: {
                              goodValue = 12;
                                        d = { 1,2,3,4,5,6,7,8,9,10,11,12 };
                              
                    __expected = 6;
                    break;
        }
                case 4: {
                              goodValue = 5;
                                        d = { 1,2,3,4 };
                              
                    __expected = 0;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    goodValue = ;
                  d = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(goodValue, d, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int goodValue, vector<int> d, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(goodValue) << ","<< d  << "]" << endl;
   
    GoodSubset *__instance = new GoodSubset();
  int __result = __instance->numberOfSubsets(goodValue, d);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "GoodSubset: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
