#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int numPosts;
int radius;
vector<int> x;
vector<int> y;
// ------------- input end  --------------
double solve(); struct FencingPenguinsEasy {double calculateMinArea(int _numPosts, int _radius, vector<int> _x, vector<int> _y){numPosts = _numPosts, radius = _radius, x = _x, y = _y;return ::solve();}};

typedef complex<long double> point;
typedef point P;
typedef vector<point> polygon;

constexpr long double EPS = 1e-20;

// NOTICE : DIRECTION IS COUNTER CLOCKWISE

vector<point> penguins;
long double dp[500][500];  // prev, start -> area

point get_point_of_index(int index) {
  return polar((long double)radius, 2.0 * acos((long double)-1) * index / numPosts);
}

inline long double cross(P const& a, P const& b) { return imag(conj(a)*b); }
inline long double dot(P const& a, P const& b) { return real(conj(a)*b); }

enum ccw_result {
  counter_clockwise = +1, clockwise = -1, online_back = +2, online_front = -2, on_segment = 0
};

ccw_result ccw(P a, P b, P c) {
  b -= a, c -= a;
  if(cross(b, c) > EPS)   { return ccw_result::counter_clockwise; }
  if(cross(b, c) < -EPS)  { return ccw_result::clockwise; }
  if(dot(b, c) < -EPS)    { return ccw_result::online_back; }
  if(norm(b) < norm(c))   { return ccw_result::online_front; }
  return ccw_result::on_segment;
}

long double area_of_triangle(point const& a, point const& b, point const& c) {
  return (cross(a, b) + cross(b, c) + cross(c, a)) / 2.0;
}

inline long double area_of_triangle(int p, int c, int s) {
  auto cp = get_point_of_index(p);
  auto cc = get_point_of_index(c);
  auto cs = get_point_of_index(s);
  return area_of_triangle(cp, cc, cs);
}

long double area(int prev, int start) {

  auto& ret = dp[prev][start];
  if(ret >= 0) { return ret; }

  bool yet = 0;
  rep(i, penguins.size()) {
    yet |= ccw(get_point_of_index(start), get_point_of_index(prev), penguins[i]) == ccw_result::counter_clockwise;
  }

  if(!yet) { return ret = 0.0; }

  ret = inf;
  auto prev_p = get_point_of_index(prev);

  REP(curr, prev + 1, numPosts + prev) {
    bool ok = 1;
    auto curr_p = get_point_of_index(curr);
    rep(pe_idx, penguins.size()) {
      ok &= ccw(prev_p, curr_p, penguins[pe_idx]) != ccw_result::clockwise;
    }
    if(ok) {
      minimize(ret, area(curr, start) + area_of_triangle(prev, curr, start));
    }
  }

  return ret;
}

double solve() {

  penguins.clear();
  rep(i, x.size()) {
    penguins.emplace_back(x[i], y[i]);
  }

  minus(dp);

  long double min = inf;
  rep(start, numPosts) {
    auto prev_p = get_point_of_index(start);
    REP(curr, start + 1, numPosts + start) {
      bool ok = 1;
      auto curr_p = get_point_of_index(curr);
      rep(pe_idx, penguins.size()) {
        ok &= ccw(prev_p, curr_p, penguins[pe_idx]) != ccw_result::clockwise;
      }
      if(ok) {
        minimize(min, area(curr, start));
      }
    }
  }
  return min >= inf / 2 ? -1 : min;
}

// CUT begin
using namespace std;
    char do_test(int,int,vector<int>,vector<int>,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int numPosts;
            int radius;
            vector<int> x;
            vector<int> y;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              numPosts = 3;
                                        radius = 5;
                                        x = { -1 };
                                        y = { 0 };
                              
                    __expected = 32.47595264191645;
                    break;
        }
                case 1: {
                              numPosts = 30;
                                        radius = 5;
                                        x = { 6 };
                                        y = { 0 };
                              
                    __expected = -1.0;
                    break;
        }
                case 2: {
                              numPosts = 4;
                                        radius = 5;
                                        x = { 2 };
                                        y = { 1 };
                              
                    __expected = 25.0;
                    break;
        }
                case 3: {
                              numPosts = 4;
                                        radius = 5;
                                        x = { 2,-2 };
                                        y = { 1,-1 };
                              
                    __expected = 50.0;
                    break;
        }
                case 4: {
                              numPosts = 8;
                                        radius = 5;
                                        x = { 8 };
                                        y = { 8 };
                              
                    __expected = -1.0;
                    break;
        }
                case 5: {
                              numPosts = 7;
                                        radius = 10;
                                        x = { 9 };
                                        y = { 1 };
                              
                    __expected = 29.436752637711805;
                    break;
        }
                case 6: {
                              numPosts = 30;
                                        radius = 800;
                                        x = { 8,2,100,120,52,67,19,-36 };
                                        y = { -19,12,88,-22,53,66,-140,99 };
                              
                    __expected = 384778.74757953023;
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    numPosts = ;
                  radius = ;
                  x = ;
                  y = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(numPosts, radius, x, y, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int numPosts, int radius, vector<int> x, vector<int> y, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(numPosts) << ","<< pretty_print(radius) << ","<< x << ","<< y  << "]" << endl;
   
    FencingPenguinsEasy *__instance = new FencingPenguinsEasy();
  double __result = __instance->calculateMinArea(numPosts, radius, x, y);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FencingPenguinsEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
