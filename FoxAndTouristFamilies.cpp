#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct FoxAndTouristFamilies {double expectedLength(vector <int> A, vector <int> B, vector <int> f);};

int V, E;
int dist[55][55];

double FoxAndTouristFamilies::expectedLength(vector <int> A, vector <int> B, vector <int> f) {
  double res = 0.0;
  V = A.size() + 1;
  E = A.size();

  rep(i, V) rep(j, V) dist[i][j] = i != j ? inf : 0;
  rep(i, E) {
  	dist[A[i]][B[i]] = dist[B[i]][A[i]] = 1;
  }

  rep(k, V) rep(i, V) rep(j, V) {
  	if(dist[i][k] == inf || dist[k][j] == inf) { continue; }
  	dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
  }

  rep(e, E) {
  	int a = A[e], b = B[e];
  	double prob = 1.0;
  	rep(i, f.size()) {
  		if(dist[f[i]][a] < dist[f[i]][b]) {
  			int count = 0;
  			rep(k, V) {
  				if(dist[k][b] < dist[k][a]) { count ++; }
  			}
  			prob *= (double) count / (V-1);
  		}
  		else {
  			int count = 0;
  			rep(k, V) {
  				if(dist[k][a] < dist[k][b]) { count ++; }
  			}
  			prob *= (double) count / (V-1);
  		}
  	}
  	res += prob;
  }

  return res;
}
