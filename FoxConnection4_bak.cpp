#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> board;
int K;
// ------------- input end  --------------
int solve(); struct FoxConnection4 {int howManyWays(vector<string> _board, int _k){board = _board, K = _k;return ::solve();}};

vector<vector<bool>> B(10, vector<bool>(10));
set<vector<vector<bool>>> patterns;
set<vector<vector<bool>>> visited;
int N, M;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};
// Cut begin
auto pr = [](vector<vector<bool>> e, int H = -1, int W = -1) {
  if(H == -1 || W == -1) { H = e.size(), W = e[0].size(); }
  rep(i, H) rep(j, W + 1) {
    printf("%c", j == (int)W ? '\n' : e[i][j] ? '#' : '.');
  }
  puts("");
};
// Cut end
vector<vector<bool>> cut_grid(vector<vector<bool>>& v, int ty, int by, int lx, int rx) {
  int H = by - ty + 1, W = rx - lx + 1;
  vector<vector<bool>> ret(H, vector<bool>(W));

  REP(i, ty, by + 1) REP(j, lx, rx + 1) {
    ret[i-ty][j-lx] = v[i][j];
  }
  return move(ret);
}

void make_patterns(vector<vector<bool>>& v, int y, int x, int ty, int by, int lx, int rx, int cnt) {

  // 一列になってるのでWA
  // 枝の分岐が必要

  if(K == cnt) {
    patterns.insert(cut_grid(v, ty, by, lx, rx));
    return;
  }

  rep(y, v.size()) rep(x, v[0].size()) {
    if(!v[y][x]) { continue; }
    rep(i, 4) {
      int ny = y + dy[i], nx = x + dx[i];
      if(v[ny][nx]) { continue; }

      int nty = ty, nby = by, nlx = lx, nrx = rx;
      minimize(nty, ny), maximize(nby, ny);
      minimize(nlx, nx), maximize(nrx, nx);
      if(nby - nty + 1 > 10 || nrx - nlx + 1 > 10) { continue; }
      auto cgrid = cut_grid(v, nty, nby, nlx, nrx);
      if(visited.count(cgrid)) { continue; }
      visited.insert(cgrid);
      v[ny][nx] = 1;
      make_patterns(v, ny, nx, nty, nby, nlx, nrx, cnt + 1);
      v[ny][nx] = 0;
    }
  }

}

int solve() {
  K = 8; board = vector<string>(10, string(10, '.'));

  N = board.size(), M = board[0].size();

  rep(i, N) rep(j, M) {
    B[i][j] = board[i][j] == '#';
  }

  patterns.clear();
  visited.clear();

  vector<vector<bool>> v(22, vector<bool>(22));
  v[11][11] = 1;

  visited.insert(cut_grid(v, 11, 11, 11, 11));
  make_patterns(v, 11, 11, 11, 11, 11, 11, 1);

  int ans = 0;

  for(auto && grid: patterns) {
    int cnt = 0;
    pr(B, N, M); pr(grid);

    int gH = grid.size(), gW = grid[0].size();
    if(gH > N || gW > M) { continue; }
    rep(si, N-gH + 1) rep(sj, M-gW + 1) {
      bool valid = 1;
      REP(i, si, si+gH) {
        REP(j, sj, sj+gW) {
          if(B[i][j] && grid[i-si][j-sj]) { valid = 0; break; }
        }
        if(!valid) { break; }
      }
      ans += valid;
      cnt += valid;
    }
    printf("cnt: %d\n", cnt);
  }

  cout << patterns.size() << endl;

  return ans;

// Cut begin
/*
  for(auto && e: patterns) {

    int sy = -1, sx = -1;

    rep(i, e.size()) rep(j, e[i].size() + 1) {
      printf("%c", j == (int)e[i].size() ? '\n' : e[i][j] ? '#' : '.');
      if(e[i][j]) {
        sy = i, sx = j;
      }
    }
    puts("");
    queue<pair<int, int>> q;
    bool vis[10][10] = {};
    vis[sy][sx] = 1;
    int cnt = 1;
    q.emplace(sy, sx);

    int h = e.size(), w = e[0].size();

    while(!q.empty()) {
      int y, x; tie(y, x) = q.front(); q.pop();
      rep(i, 4) {
        int ny = y + dy[i], nx = x + dx[i];
        if(!in_range(ny, nx, h, w)) { continue; }
        if(!e[ny][nx]) { continue; }
        if(vis[ny][nx]) { continue; }
        vis[ny][nx] = 1;
        cnt ++;
        q.emplace(ny, nx);
      }
    }

    assert(cnt == K);
  }
*/
// Cut end
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> board;
            int k;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              board = { "....." };
                                        k = 3;
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              board = { ".#.#","#.#.",".#.#","#.#." };
                                        k = 2;
                              
                    __expected = 0;
                    break;
        }
                case 2: {
                              board = { "##.","...",".##" };
                                        k = 3;
                              
                    __expected = 3;
                    break;
        }
                case 3: {
                              board = { "....","....","....","...." };
                                        k = 4;
                              
                    __expected = 113;
                    break;
        }
                case 4: {
                              board = { ".....#....",".#........","....#.##..","#......#..","....#.....","..........",".##....#..","..##......","........##","......#.#." };
                                        k = 8;
                              
                    __expected = 22369;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    board = ;
                  k = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(board, k, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> board, int k, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << board << ","<< pretty_print(k)  << "]" << endl;
   
    FoxConnection4 *__instance = new FoxConnection4();
  int __result = __instance->howManyWays(board, k);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FoxConnection4: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
