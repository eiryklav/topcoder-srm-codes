#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define zero(a) memset(a, 0, sizeof a)

int N;
int muddyCount;
int solve(); struct MuddyRoad2 {int theCount(int _N, int _muddyCount){N = _N, muddyCount = _muddyCount;return ::solve();}};

const int MOD = 555555555;

namespace math { namespace combination {

#define COMBINATION_MAX 560
using return_type = int;

namespace memorized { namespace combination { enum { max_value = COMBINATION_MAX }; bool __computed = false; inline bool computed() {if(__computed) { return true; } else { __computed = true; return false; }} return_type dp[max_value][max_value];} }

return_type comb(int N, int K) {
  using memorized::combination::dp;
  if(memorized::combination::computed()) { return dp[N][K]; }

  // Make binomial coefficient
  using memorized::combination::max_value;

  rep(i, max_value) {
    dp[i][0] = 1;
    REP(j, 1, i+1) {
      dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
      dp[i][j] %= MOD;
    }
  }
  return dp[N][K];
}}}

vector<int> odd_dries;

int dp[600][600];  // 幅の合計, 残り沼数 -> パターン数
int fib[600];

int solve() {

  odd_dries.clear();

  zero(fib);

  fib[1] = 1;
  REP(i, 2, N+1) {
    fib[i] = fib[i-1] + fib[i-2];
    fib[i] %= 2;
  }

  REP(i, 1, N+1) {
    if(fib[i] % 2) {
      odd_dries.push_back(i);
    }
  }

  zero(dp);

  const int OFFSET = +1;

  dp[-1 + OFFSET][muddyCount] = 1;

  REP(curr, -1, N+1) rep(mcount, muddyCount+1) {
    for(auto && e: odd_dries) {
      int muddyJump = e + 1;
      int next = curr + muddyJump;
      if(next > N) { continue; }
      if(next == N) {
        dp[N + OFFSET][mcount] += dp[curr + OFFSET][mcount];
        dp[N + OFFSET][mcount] %= MOD;
      }
      else if(mcount > 0) {
        dp[next + OFFSET][mcount - 1] += dp[curr + OFFSET][mcount];
        dp[next + OFFSET][mcount - 1] %= MOD;
      }
    }
  }

  return (math::combination::comb(N - 2, muddyCount) + MOD - dp[N + OFFSET][0]) % MOD;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int N;
            int muddyCount;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              N = 5;
                                        muddyCount = 1;
                              
                    __expected = 2;
                    break;
        }
                case 1: {
                              N = 5;
                                        muddyCount = 2;
                              
                    __expected = 2;
                    break;
        }
                case 2: {
                              N = 10;
                                        muddyCount = 4;
                              
                    __expected = 65;
                    break;
        }
                case 3: {
                              N = 314;
                                        muddyCount = 78;
                              
                    __expected = 498142902;
                    break;
        }
                case 4: {
                              N = 555;
                                        muddyCount = 222;
                              
                    __expected = 541606281;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    N = ;
                  muddyCount = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(N, muddyCount, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int N, int muddyCount, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(N) << ","<< pretty_print(muddyCount)  << "]" << endl;
   
    MuddyRoad2 *__instance = new MuddyRoad2();
  int __result = __instance->theCount(N, muddyCount);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "MuddyRoad2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
