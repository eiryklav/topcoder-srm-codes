#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct BearDartsDiv2 {long long count(vector <int> w);};

long long BearDartsDiv2::count(vector <int> w) {
  long long res = 0;
  int N = w.size();

  map<int, int> mp;
  rep(c, N) {
  	REP(d, c+1, N) {
  		if(w[d] % w[c] == 0) {
  			res += mp[w[d]/w[c]];
	  	}
  	}
  	rep(a, c) {
  		mp[w[a]*w[c]]++;
  	}
  }

  return res;
}