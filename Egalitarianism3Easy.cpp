#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int n;
vector<int> a;
vector<int> b;
vector<int> len;
// ------------- input end  --------------
int solve(); struct Egalitarianism3Easy {int maxCities(int _n, vector<int> _a, vector<int> _b, vector<int> _len){n = _n, a = _a, b = _b, len = _len;return ::solve();}};

int dist[11][11];

int solve() {

  rep(i, n-1) a[i]--, b[i]--;
  rep(i, 11) rep(j, 11) dist[i][j] = i == j ? 0 : inf;
  rep(i, n-1) dist[a[i]][b[i]] = dist[b[i]][a[i]] = len[i];
  rep(k, n) rep(i, n) rep(j, n) {
    if(dist[i][k] == inf || dist[k][j] == inf) continue;
    dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
  }

  int max = 1;
  rep(S, 1<<n) {
    bool ok = 0;
    bool good = 1;
    int length = 0;
    rep(i, n) REP(j, i+1, n) {
      if(!(S >> i & 1)) continue;
      if(!(S >> j & 1)) continue;
      if(!ok) {
        ok = 1;
        length = dist[i][j];
      }
      else {
        if(length != dist[i][j]) {
          good = 0;
        }
      }
    }
    if(good) {
      maximize(max, __builtin_popcount(S));
    }
  }

  return max;
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,vector<int>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int n;
            vector<int> a;
            vector<int> b;
            vector<int> len;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 4;
                                        a = { 1,1,1 };
                                        b = { 2,3,4 };
                                        len = { 1,1,1 };
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              n = 6;
                                        a = { 1,2,3,2,3 };
                                        b = { 2,3,4,5,6 };
                                        len = { 2,1,3,2,3 };
                              
                    __expected = 3;
                    break;
        }
                case 2: {
                              n = 10;
                                        a = { 1,1,1,1,1,1,1,1,1 };
                                        b = { 2,3,4,5,6,7,8,9,10 };
                                        len = { 1000,1000,1000,1000,1000,1000,1000,1000,1000 };
                              
                    __expected = 9;
                    break;
        }
                case 3: {
                              n = 2;
                                        a = { 1 };
                                        b = { 2 };
                                        len = { 3 };
                              
                    __expected = 2;
                    break;
        }
                case 4: {
                              n = 1;
                                        a = {  };
                                        b = {  };
                                        len = {  };
                              
                    __expected = 1;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
                  a = ;
                  b = ;
                  len = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, a, b, len, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int n, vector<int> a, vector<int> b, vector<int> len, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n) << ","<< a << ","<< b << ","<< len  << "]" << endl;
   
    Egalitarianism3Easy *__instance = new Egalitarianism3Easy();
  int __result = __instance->maxCities(n, a, b, len);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "Egalitarianism3Easy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
