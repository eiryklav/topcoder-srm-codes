#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int N, M;

bool in_range(int x, int y) {
	return 0<=x && x<M && 0<=y && y<N;
}

struct SurroundingGameEasy {int score(vector <string> cost, vector <string> benefit, vector <string> stone);};

int SurroundingGameEasy::score(vector <string> cost, vector <string> benefit, vector <string> stone) {
  int res = 0;

  N = cost.size();
  M = cost[0].size();

  rep(i, N) rep(j, M) {
  	if(stone[i][j] == 'o') {
  		res -= cost[i][j] - '0';
  		res += benefit[i][j] - '0';
  	}
  	else {
  		bool ok = true;
  		rep(k, 4) {
	  		int ni = i+dy[k], nj = j+dx[k];
	  		if(!in_range(nj, ni)) { continue; }
	  		if(stone[ni][nj] == '.') { ok = false; }
	  	}
	  	if(ok) {
	  		res += benefit[i][j] - '0';
	  	}
  	}
  }

  return res;
}
