#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> board;
// ------------- input end  --------------
int solve(); struct FoxAndGo2 { int maxKill(vector<string> _board) { board = _board; return solve(); }};

constexpr int dx[4] = {-1,0,1,0};
constexpr int dy[4] = {0,-1,0,1};

//vector<pair<int, int>> cellsForComponent;
bool used[22][22];
int N, M;

bool isEnclosed(int ey, int ex, int wy, int wx) {

  bool empties[22][22] = {};
  empties[ey][ex] = 1;

  queue<pair<int, int>> q;
  q.emplace(ey, ex);
  while(!q.empty()) {
    auto p = q.front(); q.pop();
    const auto y = p.first, x = p.second;
    rep(i, 4) {
      int ny = y+dy[i], nx = x+dx[i];
      if(!in_range(ny, nx, N, M)) { continue; }
      if(empties[ny][nx]) { continue; }
      if(board[ny][nx] != 'o') {
        empties[ny][nx] = 1;
        q.emplace(ny, nx);
      }
    }
  }

  bool whites[22][22] = {};
  whites[wy][wx] = 1;
  q.emplace(wy, wx);

  while(!q.empty()) {
    auto p = q.front(); q.pop();
    const auto y = p.first, x = p.second;
    rep(i, 4) {
      int ny = y+dy[i], nx = x+dx[i];
      if(!in_range(ny, nx, N, M)) { continue; }
      if(whites[ny][nx]) { continue; }
      if(board[ny][nx] == 'o') {
        whites[ny][nx] = 1;
        q.emplace(ny, nx);
      }
      else if(board[ny][nx] == '.') {
//        cout << ny << ", " << nx << endl;
        if(!empties[ny][nx]) { return true; }
      }
    }
  }

  return false;
}

int dfsForComponent(int sy, int sx) {

  set<pair<int, int>> enclosedEmptyCells;
  queue<pair<int, int>> Q;

  Q.emplace(sy, sx);
  used[sy][sx] = 1;

  int compCount = 1;

  while(!Q.empty()) {
    auto p = Q.front(); Q.pop();
    int y = p.first, x = p.second;

    rep(i, 4) {
      const int ny = y+dy[i], nx = x+dx[i];

      if(!in_range(ny, nx, N, M)) { continue; }
      if(used[ny][nx]) { continue; }

      if(board[ny][nx] == '.') {
        if(isEnclosed(ny, nx, y, x)) {
          enclosedEmptyCells.emplace(ny, nx);
        }
      }
      else if(board[ny][nx] == 'o') {
        used[ny][nx] = 1;
        compCount ++;
        Q.emplace(ny, nx);
      }

    }
  }
/*
cout << "compCount: " << compCount << endl;
  rep(i, N) {rep(j, M) {
    cout << board[i][j];
  }cout << endl;
}
cout << enclosedEmptyCells.size() << endl;
cout << "--------------\n";
//*/

  if(enclosedEmptyCells.empty()) {
    return compCount;
  }

  if(enclosedEmptyCells.size() == 1) {
    auto e = *(enclosedEmptyCells.begin());
    board[e.first][e.second] = 'x';
    used[e.first][e.second] = 1;
    return compCount;
  }


  cout << "nothing to be got.\n";
    /*
  else {
    for(auto& e: enclosedEmptyCells) {
      cout << e.first << ',' << e.second << endl;
    }
  }
    */
  return 0;
}

int solve() {

  N = board.size(), M = board[0].size();

  int res = 0;
  while(1) {
    zero(used);
    int cnt = 0;
    rep(i, N) rep(j, M) {
      if(board[i][j] == 'o' && !used[i][j]) {
        cnt += dfsForComponent(i, j);
      }
    }
    if(res < cnt) {
      res = cnt;
    }
    else {
      break;
    }
  }

  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> board;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              board = { "...",".o.","..." };
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              board = { "o.","oo" };
                              
                    __expected = 3;
                    break;
        }
                case 2: {
                              board = { ".o.o.","o.o.o",".o.o.","o.o.o",".o.o." };
                              
                    __expected = 0;
                    break;
        }
                case 3: {
                              board = { ".o.o.","o.o.o",".o.o.","o.o.o","....." };
                              
                    __expected = 10;
                    break;
        }
                case 4: {
                              board = { ".o.o.o.o.o.","o.ooo.ooo.o",".o.......o.","oo.......oo",".o...o...o.","o...o.o...o",".o...o...o.","oo.......oo",".o.......o.","o.ooo.ooo.o",".o.o.o.o.o." };
                              
                    __expected = 4;
                    break;
        }
                case 5: {
                              board = { "...ooo.....","...o.o.....",".ooo.ooo...",".o.....o...",".ooo.ooo...","...o.o.....","...ooo.....","....o......","....o...ooo","....ooooo.o","........ooo" };
                              
                    __expected = 38;
                    break;
        }
                case 6: {
                              board = { "ooooooooooo","o.........o","o...ooo...o","o...o.o...o","o...ooo...o","o....o....o","o....oooooo","o..........","o.......ooo","o.......o.o","ooooooooooo" };
                              
                    __expected = 0;
                    break;
        }
                case 7: {
                              board = { "oo.o.ooo.o..o..","...ooo.o..oo.oo","o..o.o.ooo.o..o","oo.......oo.ooo","..oo.o.o.o.ooo.","..oo..oo..o.ooo","oo.o.oo..o.oooo",".oo.o..ooo.o.oo","o..o.o.o.o.oo..",".oo.oo...o....o","o.o.oo....oo..o",".o.o..o.oo..ooo","o.o.o..o..o....","ooo.oooooooo..o","o..oo.o..o.ooo." };
                              
                    __expected = 60;
                    break;
        }
                case 8: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    board = {{"oo.", "oo.", ".o."}};
                    __expected = 0;
                    break;
          __unknownAnswer = true; 
          break;
        }
    
    default: return 'm';
  }
  return do_test(board, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 9;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> board, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << board  << "]" << endl;
   
    FoxAndGo2 *__instance = new FoxAndGo2();
  int __result = __instance->maxKill(board);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FoxAndGo2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
