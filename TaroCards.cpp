#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min<decltype(a)>(a, x)
#define maximize(a, x) a = std::max<decltype(a)>(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> first;
vector<int> second;
int K;
// ------------- input end  --------------
ll solve(); struct TaroCards {ll getNumber(vector<int> _first, vector<int> _second, int _K){first = _first, second = _second, K = _K;return solve();}};

int const MOD = 1e9+7;

ll dp[55][101][1024];

ll solve() {

  int N = first.size();

  zero(dp);
  dp[0][0][0] = 1;

  rep(i, N) rep(j, K + 1) rep(k, 1<<10) {
    int nk = k | (1 << (second[i] - 1));
    int pp = 0;
    if(first[i] < 11) {
      nk |= 1 << (first[i] - 1);
    }
    else {
      pp ++;
    }
    int nj = j + __builtin_popcount(nk) - __builtin_popcount(k) + pp;
    dp[i + 1][j][k] += dp[i][j][k];
    dp[i + 1][nj][nk] += dp[i][j][k];
  }

  ll ans = 0;
  rep(j, K + 1) rep(k, 1<<10) {
    ans += dp[N][j][k];
  }

  return ans;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,int,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> first;
            vector<int> second;
            int K;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              first = { 1,2 };
                                        second = { 2,3 };
                                        K = 2;
                              
                    __expected = 3LL;
                    break;
        }
                case 1: {
                              first = { 3,1,2 };
                                        second = { 1,1,1 };
                                        K = 3;
                              
                    __expected = 8LL;
                    break;
        }
                case 2: {
                              first = { 4,2,1,3 };
                                        second = { 1,2,3,4 };
                                        K = 5;
                              
                    __expected = 16LL;
                    break;
        }
                case 3: {
                              first = { 1,2,3,4,5,6,7 };
                                        second = { 2,1,10,9,3,2,2 };
                                        K = 3;
                              
                    __expected = 17LL;
                    break;
        }
                case 4: {
                              first = { 1 };
                                        second = { 2 };
                                        K = 1;
                              
                    __expected = 1LL;
                    break;
        }
                case 5: {
                              first = { 6,20,1,11,19,14,2,8,15,21,9,10,4,16,12,17,13,22,7,18,3,5 };
                                        second = { 4,5,10,7,6,2,1,10,10,7,9,4,5,9,5,10,10,3,6,6,4,4 };
                                        K = 14;
                              
                    __expected = 2239000LL;
                    break;
        }
                case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    first = {7, 29, 19, 18, 24, 27, 28, 25, 22, 21, 1, 38, 10, 11, 4, 23, 8, 30, 9, 32, 2, 33, 3, 40, 14, 12, 31, 39, 26, 37, 13, 41, 42, 34, 17, 36, 16, 20, 6, 5, 15, 35};
                    second = {4, 4, 5, 2, 2, 4, 9, 8, 5, 3, 8, 8, 10, 4, 2, 10, 9, 7, 6, 1, 3, 9, 7, 1, 3, 5, 9, 7, 6, 1, 10, 1, 1, 7, 2, 4, 9, 10, 4, 5, 5, 7};
                    K = 72;
                    __expected = 4398046511104LL;
          break;
        }
    
    default: return 'm';
  }
  return do_test(first, second, K, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> first, vector<int> second, int K, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << first << ","<< second << ","<< pretty_print(K)  << "]" << endl;
   
    TaroCards *__instance = new TaroCards();
  ll __result = __instance->getNumber(first, second, K);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TaroCards: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
