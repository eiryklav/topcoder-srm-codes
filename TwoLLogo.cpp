#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

#if __cplusplus >= 201103L
#define minimize(a, x) a = std::min<decltype(remove_reference(a))>(a, x)
#define maximize(a, x) a = std::max<decltype(remove_reference(a))>(a, x)
#else
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)
#endif

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> grid;
// ------------- input end  --------------
ll solve(); struct TwoLLogo {ll countWays(vector<string> _grid){grid = _grid;return solve();}};

int N, M;
int hlen[55][55];
int wlen[55][55];

ll solve() {

  N = grid.size(), M = grid[0].size();

  rep(i, N) for(int j=M-1; j>=0; j--) {
    if(j < M-1 && grid[i][j+1] == '.' && grid[i][j] == '.') {
      wlen[i][j] = wlen[i][j+1] + 1;
    }
    else if(grid[i][j] == '.') {
      wlen[i][j] = 1;
    }
    else {
      wlen[i][j] = 0;
    }
  }

  rep(i, N) rep(j, M) {
    if(i && grid[i-1][j] == '.' && grid[i][j] == '.') {
      hlen[i][j] = hlen[i-1][j] + 1;
    }
    else if(grid[i][j] == '.') {
      hlen[i][j] = 1;
    }
    else {
      hlen[i][j] = 0;
    }
  }

  ll sum = 0;

  REP(ai, 1, N) rep(aj, M-1) {

    if(grid[ai][aj] == '#') { continue; }
    if(grid[ai-1][aj] == '#') { continue; }
    if(grid[ai][aj+1] == '#') { continue; }

    REP(bi, 1, N) REP(bj, aj, M-1) {

      if(grid[bi][bj] == '#') { continue; }
      if(grid[bi-1][bj] == '#') { continue; }
      if(grid[bi][bj+1] == '#') { continue; }

      if(ai == bi && aj == bj) { continue; }

      if(aj < bj) {
        sum += (ll)(hlen[ai][aj]-1) * (wlen[ai][aj]-1) * (hlen[bi][bj]-1) * (wlen[bi][bj]-1);
      }
      else if(aj == bj && ai < bi) {
        sum += (ll)(hlen[ai][aj]-1) * (wlen[ai][aj]-1) * (hlen[bi][bj]-1) * (wlen[bi][bj]-1);        
      }

      int bii = bi - hlen[bi][bj] + 1;
      int ajj = aj + wlen[ai][aj] - 1;

      if(ai < bi && aj < bj && bj <= ajj && bii <= ai) {
        int dj = ajj - bj + 1;
        int di = ai - bii + 1;
        sum -= (ll)di * dj * (hlen[ai][aj] - 1) * (wlen[bi][bj] - 1);
      }

      if(ai == bi && bj <= ajj) {
        int h1 = hlen[ai][aj] - 1;
        int h2 = hlen[bi][bj] - 1;
        int dw = ajj - bj + 1;
        sum -= (ll)dw * (wlen[bi][bj] - 1) * h1 * h2;
      }

      if(aj == bj && bii < ai && ai < bi) {
        int dh = (ai - bii + 1);
        int alen = wlen[ai][aj] - 1;
        int blen = wlen[bi][bj] - 1;
        sum -= (ll)dh * (hlen[ai][aj] - 1) * alen * blen;
      }

    }
  }

  cout << sum << endl;

  return sum;
}

// CUT begin
using namespace std;
    char do_test(vector<string>,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> grid;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              grid = { "....","...." };
                              
                    __expected = 1LL;
                    break;
        }
                case 1: {
                              grid = { ".##..","...#.",".#.#.","#...#" };
                              
                    __expected = 3LL;
                    break;
        }
                case 2: {
                              grid = { "..#.","#.#.","....","..#." };
                              
                    __expected = 4LL;
                    break;
        }
                case 3: {
                              grid = { "..",".." };
                              
                    __expected = 0LL;
                    break;
        }
                case 4: {
                              grid = { ".#.#","....",".#.#","...." };
                              
                    __expected = 34LL;
                    break;
        }
                case 5: {
                              grid = {"##############","##############","#.############","#.############","#.############","#.############","#.############","#.############","#.#####.######","#.#####.######","#.#####.######","#....##.######","#######.######","#######.######","#######.######","#######.######","#######.######","#######.######","#######......#","##############" };
                              
                    __expected = 1350LL;
                    break;
        }
                case 6: {
                              grid = { "#......",".#....#",".#.#...","#....#.",".##..#.",".#.....",".....#.",".#.#...",".#...#.","..##..." };
                              
                    __expected = 2386LL;
                    break;
        }
                case 7: {
                              grid = { "...#..........................","..............................","..............................","..................#...#.......","..................#...........","..............................","...........#..................","..............................",".....#..#.....................",".......................#......","..................#.....#.....","..............................","..............................","..............................","..............................","..#...........................","..............................","..............................","..............................","#............................#","..............................",".....#.........#............#.","..............................",".........................#....",".#............................",".............#................","..............................","..............................",".......................#......",".............#................" };
                              
                    __expected = 5020791386LL;
                    break;
        }
                case 8: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    grid = {
                      "#.#.#",
                      "#...."
                    };
              
                    __expected = 1LL;
          break;
        }

                case 9: {
                   grid = {
                     "#.###.###",
                     "#.###.###",
                     "#...#...."
                   };

                   __expected = 24;
                   break;
       }

                case 10: {
                   grid = {
                     "#.###.###",
                     "#.###.###",
                     "#........"
                   };

//                   __unknownAnswer = 1;
                   __expected = 36;
                   break;
       }

                case 11: {
                   grid = {
                     "#.###.###",
                     "#.###.###",
                     "#.....###",
                     "#####...."
                   };

//                   __unknownAnswer = 1;
                   __expected = 6 * 9;
                   break;
       }

                case 12: {
                   grid = {
                     "#..####",
                     "#......",
                   };

//                   __unknownAnswer = 1;
                   __expected = 0;
                   break;
       }

                case 13: {
                   grid = {
                     ".##",
                     "..#",
                     "#.."
                   };

//                   __unknownAnswer = 1;
                   __expected = 0;
                   break;
       }

                case 14: {
                   grid = {
                     ".###",
                     "...#",
                     "#..."
                   };

//                   __unknownAnswer = 1;
                   __expected = 1;
                   break;
       }

                case 15: {
                   grid = {
                     ".#.#",
                     "....",
                     ".#.#",
                     "...."
                   };

                   __unknownAnswer = 1;
//                   __expected = 3 * 3 + 3 + 3 + 1 + (3*3 * 1) + (3 * 3);
                   break;
       }

                case 16: {
                   grid = {
                     ".#.#",
                     "....",
                     "##.#",
                     "##.."
                   };

//                   __unknownAnswer = 1;
                   __expected = 7;
                   break;
       }

                case 17: {
                   grid = {
                    "#.#",
                    "...",
                    "#.#",
                    "..."
                   };

//                   __unknownAnswer = 1;
                   __expected = 1;
                   break;
       }

                case 18: {
                   grid = {
                    "#.#.#.#",
                    "#......"
                   };

//                   __unknownAnswer = 1;
                   __expected = 7;
                   break;
       }

                case 19: {
                   grid = {
                    "..",
                    "..",
                    "..",
                    "..",
                   };

//                   __unknownAnswer = 1;
                   __expected = 1;
                   break;
       }
    
    default: return 'm';
  }
  return do_test(grid, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 8;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> grid, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << grid  << "]" << endl;
   
    TwoLLogo *__instance = new TwoLLogo();
  ll __result = __instance->countWays(grid);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "TwoLLogo: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
