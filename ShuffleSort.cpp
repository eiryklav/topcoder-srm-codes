#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>
using namespace std;
#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
vector<int> cards;
double solve(); struct ShuffleSort {double shuffle(vector<int> _cards){cards = _cards;return ::solve();}};
int N, K;
double solve() {

	N = cards.size();
	map<int, int> mp;
	for(auto && e: cards) mp[e]++;	

	double ret = 1.0;

	// dp[n] 	= dp[n-1] + (n - t[n]) / t[n]
	// 				= (n - t[n]) / t[n] + (n - 1 - t[n-1]) / t[n-1] + ...
	//				= Σ{0<=i<n} (n - i - t[n-i]) / t[n-i] if n > 0, 0 if n = 0

	auto iter = mp.begin();
	int sum = N;
	for(;sum > 0 || iter != mp.end();) {
		if(iter->second > 0) {
			ret += (double)(sum - iter->second) / iter->second;
			iter->second --;
			sum --;
		}
		else {
			iter ++;
		}
	}

	assert(iter == mp.end());

  return ret;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> cards;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              cards = { 1 };
                              
                    __expected = 1.0;
                    break;
        }
                case 1: {
                              cards = { 1,2 };
                              
                    __expected = 2.0;
                    break;
        }
                case 2: {
                              cards = { 2,3,1 };
                              
                    __expected = 4.0;
                    break;
        }
                case 3: {
                              cards = { 15,16,4,8,42,23 };
                              
                    __expected = 16.0;
                    break;
        }
                case 4: {
                              cards = { 1,1,1,1,1,1,1,1 };
                              
                    __expected = 1.0;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    cards = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(cards, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> cards, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << cards  << "]" << endl;
   
    ShuffleSort *__instance = new ShuffleSort();
  double __result = __instance->shuffle(cards);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "ShuffleSort: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
