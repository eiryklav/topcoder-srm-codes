#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

struct LeftRightDigitsGame {string minNumber(string digits);};

deque<char> res, D;

string LeftRightDigitsGame::minNumber(string digits) {

	res.clear(), D.clear();

  rep(i, digits.size()) D.push_back(digits[i]);

  char firstDigit = '9'+1;
  int firstDigitCount = 0;

  rep(i, digits.size()) {
  	if(digits[i] > '0' && firstDigit > digits[i]) {
  		firstDigit = digits[i];
  		firstDigitCount = 1;
  	}
  	else if(firstDigit == digits[i]) {
  		firstDigitCount ++;
  	}
  }

  cout << "FD: " << firstDigit << ", SIZE: " << firstDigitCount << endl;

  while(!D.empty()) {
  	string ss; rep(i, res.size()) ss += res[i];
  	cout << ":" << ss << endl;
  	char d = D[0]; D.pop_front();
  	if(res.empty()) {
  		if(d == firstDigit) {
  			firstDigitCount --;
 				cout << "SIZE: " << firstDigitCount << endl;
  		}
  		res.push_back(d);
  	}
  	else {
  		if(d == '0') {
  			if(firstDigitCount > 0) {
  				res.push_front(d);
  			}
  			else {
  				res.push_back(d);
  			}
  		}
  		else {
  			if(d == firstDigit) {
  				firstDigitCount --;
  				cout << "SIZE: " << firstDigitCount << endl;
  				if(firstDigitCount == 0) {
  					res.push_front(d);
  					continue;
  				}
  			}
  			if(res[0] > d) {
  				res.push_front(d);
  			}
  			else if(res[0] == d) {
  				if(res.size() == 1) {
  					res.push_back(d);
  				}
  				else {
  					int pos = 1;
  					int n = res.size();
  					while(pos < n && res[pos] == d) {
  						pos++;
  					}
  					if(pos == n) {
  						res.push_back(d);
  					}
  					else if(res[pos] > d) {
  						res.push_front(d);
  					}
  					else {
  						res.push_back(d);
  					}
  				}
  			}
  			else {
  				res.push_back(d);
  			}
  		}
  	}
  }

  string ans;
  rep(i, res.size()) {
  	ans.push_back(res[i]);
  }
  return ans;
}