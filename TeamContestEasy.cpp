#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct TeamContestEasy {int worstRank(vector <int> strength);};

int TeamContestEasy::worstRank(vector <int> strength) {
	int x = strength[0];
	int y = strength[1];
	int z = strength[2];
	int myTeam = x + y + z - min({x, y, z});
	deque<int> v;
	REP(i, 3, strength.size()) {
		v.push_back(strength[i]);
	}
	sort(all(v));
	int N = v.size();
	
	int res = 0;
	while(N > 2) {
		bool ok = false;
		rep(i, N-1) {
			if(myTeam < v[i] + v[N-1]) {
				ok = true;
				res++;
				v.pop_back();
				v.erase(v.begin()+i);
				v.pop_front();
				N-=3;
				break;
			}
		}
		if(!ok) { break; }
	}

	return res + 1;
}