#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct CombiningSlimes {int maxMascots(vector <int> a);};

int CombiningSlimes::maxMascots(vector <int> a) {
  int res = 0;
  priority_queue<int> pq;
  rep(i, a.size()) { pq.push(a[i]); }
  while(pq.size() > 1) {
  	int p = pq.top(); pq.pop();
  	int q = pq.top(); pq.pop();	
  	pq.push(p+q);
  	res += p*q;
  }
  return res;
}
