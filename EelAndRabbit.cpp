#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct EelAndRabbit {int getmax(vector <int> l, vector <int> t);};

int EelAndRabbit::getmax(vector <int> l, vector <int> t) {
	int N = l.size();
	vector<int> v;
	rep(i, N) {
		v.push_back(l[i]);
		v.push_back(t[i] + l[i]);
	}
	int ans = 0;
	rep(i, v.size()) {
		REP(j, i+1, v.size()) {
			int curr = 0;
			rep(k, N) {
				if(
					((t[k] <= v[i]) && (v[i] <= t[k] + l[k]))
					||
					((t[k] <= v[j]) && (v[j] <= t[k] + l[k]))
				) {
					curr ++;
				}
			}
			ans = max(ans, curr);
		}
	}
	return ans;
}
