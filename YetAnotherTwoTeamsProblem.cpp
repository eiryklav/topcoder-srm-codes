#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define repll(i, n) REPll(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> skill;
// ------------- input end  --------------
ll solve(); struct YetAnotherTwoTeamsProblem {ll count(vector<int> _skill){skill = _skill;return solve();}};

int const Max = 50*60000+1;
ll dp[Max];
int N;

ll solve() {
  ll res = 0;
  N = skill.size();
  int sum = accumulate(all(skill), 0);
  sort(skill.rbegin(), skill.rend());
  zero(dp); dp[0] = 1;
  rep(i, N) {
    for(int gr1=Max; gr1>=skill[i]; --gr1) {
      auto& nextGroup1 = dp[gr1]; // add skill[i]
      auto& prevGroup1 = dp[gr1-skill[i]];
      int gr2 = sum - gr1;
      if(gr1 < gr2) {
        nextGroup1 += prevGroup1;
      }
      if(gr1 - skill[i] < gr2 + skill[i] && gr1 > gr2) {
        res += prevGroup1;
      }
    }
  }
  return res;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> skill;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              skill = { 5,4,7,6 };
                              
                    __expected = 2LL;
                    break;
        }
                case 1: {
                              skill = { 1,1,1,1,1 };
                              
                    __expected = 10LL;
                    break;
        }
                case 2: {
                              skill = { 1,2,3,5,10 };
                              
                    __expected = 5LL;
                    break;
        }
                case 3: {
                              skill = { 1,2,3,4,10 };
                              
                    __expected = 0LL;
                    break;
        }
                case 4: {
                              skill = { 999,999,999,1000,1000,1001,999,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,999,1000,512,511,1001,1001,1001,1001,1001,1000 };
                              
                    __expected = 17672631900LL;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    skill = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(skill, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> skill, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << skill  << "]" << endl;
   
    YetAnotherTwoTeamsProblem *__instance = new YetAnotherTwoTeamsProblem();
  ll __result = __instance->count(skill);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "YetAnotherTwoTeamsProblem: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
