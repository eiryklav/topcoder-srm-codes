#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> x;
// ------------- input end  --------------
vector<int> solve(); struct OddEvenTreeHard {vector<int> getTree(vector<string> _x){x = _x;return ::solve();}};

int N;

vector<int> solve() {
  N = x.size();

  auto check = [&]() {
    rep(i, N) {
      if(x[i][i] == 'O') { return false; }
      bool ok = 0;
      rep(j, N) {
        if(x[i][j] == 'O' || x[i][j] == '?') { ok = 1; }
        rep(k, N) {
          if(x[i][j] == 'O' && x[j][k] == 'O' && x[i][k] == 'O') { return false; }
          if(x[i][j] == 'E' && x[j][k] == 'O' && x[i][k] == 'E') { return false; }
          if(x[i][j] == 'O' && x[j][k] == 'E' && x[i][k] == 'E') { return false; }
          if(x[i][j] == 'E' && x[j][k] == 'E' && x[i][k] == 'O') { return false; }
        }
      }
      if(!ok) { return false; }
    }
    return true;
  };

  if(!check()) { return {-1}; }

  rep(i, N) {
    if(x[i][i] == '?') { x[i][i] = 'E'; }
    rep(j, N) {
      rep(k, N) {
        if(x[i][j] == 'O' && x[j][k] == 'O' && x[i][k] == '?') { x[i][k] = 'E'; }
        if(x[i][j] == 'E' && x[j][k] == 'O' && x[i][k] == '?') { x[i][k] = 'O'; }
        if(x[i][j] == 'O' && x[j][k] == 'E' && x[i][k] == '?') { x[i][k] = 'O'; }
        if(x[i][j] == 'E' && x[j][k] == 'E' && x[i][k] == '?') { x[i][k] = 'E'; }
      }
    }
  }

  if(!check()) { return {-1}; }

  vector<bool> vis(N);

  std::function<void(int, vector<int>&)> dfs = [&](int idx, vector<int>& tree) {
    rep(i, N) {
      if(vis[i]) {
        dfs();
      }
    }
  };

  vector<int> ret(2*N-2);
  dfs(0, ret);

  return vector<int>();
}

// CUT begin
using namespace std;
    char do_test(vector<string>,vector<int>,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<string> x;
            vector<int> __expected = vector<int>();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              x = { "EOE","OEO","EOE" };
                              
                    __expected = { 0, 1, 2, 1 };
                    break;
        }
                case 1: {
                              x = { "????","????","????","????" };
                              
                    __expected = { 0, 1, 0, 3, 2, 1 };
                    break;
        }
                case 2: {
                              x = { "????","????","??O?","????" };
                              
                    __expected = { -1 };
                    break;
        }
                case 3: {
                              x = { "??O?","????","E???","????" };
                              
                    __expected = { -1 };
                    break;
        }
                case 4: {
                              x = { "?O??","??O?","???O","????" };
                              
                    __expected = { 0, 1, 0, 3, 2, 1 };
                    break;
        }
                case 5: {
                              x = { "?E??","??E?","???E","????" };
                              
                    __expected = { -1 };
                    break;
        }
                case 6: {
                              x = { "?????????????","??????????OE?","?????????????","?????????????","?????????????","?????????????","?????????????","???????E?????","???????E?????","?????????????","???????????O?","???????????E?","????????????E" };
                              
                    __expected = { 0, 1, 0, 3, 0, 5, 0, 7, 0, 8, 0, 11, 0, 12, 2, 1, 4, 1, 6, 1, 9, 1, 10, 1 };
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    x = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(x, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<string> x, vector<int> __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << x  << "]" << endl;
   
    OddEvenTreeHard *__instance = new OddEvenTreeHard();
  vector<int> __result = __instance->getTree(x);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  __expected  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  __result  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "OddEvenTreeHard: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
