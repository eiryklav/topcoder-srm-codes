#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> doorR;
vector<int> doorG;
vector<int> keys;
// ------------- input end  --------------
int solve(); struct KeyDungeonDiv2 { int countDoors(vector<int> _doorR, vector<int> _doorG, vector<int> _keys) { doorR = _doorR, doorG = _doorG, keys = _keys; return solve(); }};

int solve() {
  int res = 0;
  rep(i, doorR.size()) {
    doorR[i] = max(0, doorR[i]-keys[0]);
    doorG[i] = max(0, doorG[i]-keys[1]);
    res += doorR[i] + doorG[i] <= keys[2];
  }
  return res;
}
