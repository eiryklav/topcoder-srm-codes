#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct BearPaints {long long maxArea(int W, int H, long long M);};

long long BearPaints::maxArea(int W, int H, long long M) {
  long long res = 0;

  REP(i, 1, W+1) {

    res = max(res, i * min((ll)H, M/i));

  }

  return res;
}
