#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int n;
vector<int> a;
vector<int> b;
string c;
// ------------- input end  --------------
int solve(); struct PalindromePath {int shortestLength(int _n, vector<int> _a, vector<int> _b, string _c){n = _n, a = _a, b = _b, c = _c;return solve();}};

int solve() {

  char edge[22][22] = {};

  rep(i, a.size()) {
    edge[a[i]][b[i]] = edge[b[i]][a[i]] = c[i];
  }

  int min[22][22];
  rep(i, 22) rep(j, 22) {
    if(i == j) { min[i][i] = 0; }
    else { min[i][j] = inf; }
  }
  rep(i, a.size()) min[a[i]][b[i]] = min[b[i]][a[i]] = 1;

  rep(_, 22) {
    rep(x, n) rep(y, n) {
      if(min[x][y] == inf) { continue; }
      rep(k, n) rep(l, n) {
        if(edge[k][x] != 0 && edge[k][x] == edge[y][l]) {
          minimize(min[k][l], min[x][y] + 2);
        }
      }
    }
  }
  return min[0][1] == inf ? -1 : min[0][1];
}

// CUT begin
using namespace std;
    char do_test(int,vector<int>,vector<int>,string,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int n;
            vector<int> a;
            vector<int> b;
            string c;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 5;
                                        a = { 2,2,0,3,4 };
                                        b = { 0,1,3,4,1 };
                                        c = "abxyx";
                              
                    __expected = 3;
                    break;
        }
                case 1: {
                              n = 5;
                                        a = { 2,2,0,3,4 };
                                        b = { 0,1,3,4,1 };
                                        c = "abxyz";
                              
                    __expected = -1;
                    break;
        }
                case 2: {
                              n = 7;
                                        a = { 0,0,3,4,5,6 };
                                        b = { 2,3,4,5,6,1 };
                                        c = "abaaaa";
                              
                    __expected = 9;
                    break;
        }
                case 3: {
                              n = 6;
                                        a = { 0,0,3,4,5 };
                                        b = { 2,3,4,5,1 };
                                        c = "abaaa";
                              
                    __expected = -1;
                    break;
        }
                case 4: {
                              n = 2;
                                        a = { 0 };
                                        b = { 1 };
                                        c = "x";
                              
                    __expected = 1;
                    break;
        }
                case 5: {
                              n = 20;
                                        a = { 18 };
                                        b = { 19 };
                                        c = "z";
                              
                    __expected = -1;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
                  a = ;
                  b = ;
                  c = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, a, b, c, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int n, vector<int> a, vector<int> b, string c, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n) << ","<< a << ","<< b << ","<< pretty_print(c)  << "]" << endl;
   
    PalindromePath *__instance = new PalindromePath();
  int __result = __instance->shortestLength(n, a, b, c);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "PalindromePath: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
