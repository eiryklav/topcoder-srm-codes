#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct GreatFairyWar {int minHP(vector <int> dps, vector <int> hp);};

int GreatFairyWar::minHP(vector <int> dps, vector <int> hp) {
  int res = 0;

  int c = 0;
  rep(i, dps.size()) {
  	res += dps[i] * (hp[i] + c);
  	c += hp[i];
  }

  return res;
}