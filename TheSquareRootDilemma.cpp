#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int H) { return 0<=x && x<W && 0<=y && y<H; }

struct TheSquareRootDilemma {int countPairs(int N, int M);};

struct value {
	int integer = 0;
	int squared_integer = 1;
	value(){}
	value(int r, int i): integer(r), squared_integer(i) {}
};

map<int, int> prime_factor(int n) {
	map<int, int> ret;
	for(int i=2; i*i<=n; i++) {
		while(n % i == 0) {
			ret[i] ++;
			n /= i;
		}
	}
	if(n > 1) {
		ret[n] ++;
	}
	return ret;
}

int TheSquareRootDilemma::countPairs(int N, int M) {

	map<int, value> sqrt_value;

	REP(i, 1, 77778) {
		if((int)sqrt(i) * (int)sqrt(i) == i) {
			sqrt_value[i] = value((int)sqrt(i), 1);
		}
		else {
			auto pf = prime_factor(i);
			int re = 1;
			int irr = 1;
			for(auto& e: pf) {
				if(e.second % 2) {
					re *= pow(e.first, e.second / 2);
					irr *= e.first;
				}
				else {
					if(e.second > 0) {
						re *= pow(e.first, e.second / 2);
					}
				}
			}
			sqrt_value[i] = value(re, irr);
		}
	}
	
	// 係数を吐き出したルート部分が、同一の値のルートであるか、
	// A, Bの両方が整数である場合はvalid
	int arr[] = {N, M};
	
	map<int, int> numOfRootK[2];
	int numOfInteger[2] = {};
	rep(k, 2) {
		REP(i, 1, arr[k]+1) {
			if(sqrt_value[i].squared_integer > 1) {
				numOfRootK[k][sqrt_value[i].squared_integer]++;
			}
			else {
				numOfInteger[k] ++;
			}
		}
	}
	
	int root_ans = 0;
	REP(i, 2, 77778) {
		root_ans += numOfRootK[0][i] * numOfRootK[1][i];
	}
	
	return root_ans + numOfInteger[0] * numOfInteger[1];
	
}
