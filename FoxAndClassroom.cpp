#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

// ------------- input start -------------
int n;
int m;
// ------------- input end  --------------
string solve(); struct FoxAndClassroom { string ableTo(int _n, int _m) { n = _n, m = _m; return solve(); }};

string solve() {
  return __gcd(n, m) == 1 ? "Possible" : "Impossible";
}

// CUT begin
using namespace std;
    char do_test(int,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int n;
            int m;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 2;
                                        m = 3;
                              
                    __expected = "Possible";
                    break;
        }
                case 1: {
                              n = 2;
                                        m = 2;
                              
                    __expected = "Impossible";
                    break;
        }
                case 2: {
                              n = 4;
                                        m = 6;
                              
                    __expected = "Impossible";
                    break;
        }
                case 3: {
                              n = 3;
                                        m = 6;
                              
                    __expected = "Impossible";
                    break;
        }
                case 4: {
                              n = 5;
                                        m = 7;
                              
                    __expected = "Possible";
                    break;
        }
                case 5: {
                              n = 10;
                                        m = 10;
                              
                    __expected = "Impossible";
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
                  m = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, m, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int n, int m, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n) << ","<< pretty_print(m)  << "]" << endl;
   
    FoxAndClassroom *__instance = new FoxAndClassroom();
  string __result = __instance->ableTo(n, m);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "FoxAndClassroom: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
