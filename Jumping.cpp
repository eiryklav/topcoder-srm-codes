#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int x;
int y;
vector<int> jumpLengths;
// ------------- input end  --------------
string solve(); struct Jumping {string ableToGet(int _x, int _y, vector<int> _jumpLengths){x = _x, y = _y, jumpLengths = _jumpLengths;return ::solve();}};

string solve() {
  double maxLen = sqrt(x * x + y * y);
  double sum = maxLen;
  rep(i, jumpLengths.size()) {
    maximize(maxLen, (double)jumpLengths[i]);
    sum += jumpLengths[i];
  }
  return maxLen * 2 <= sum ? "Able" : "Not able";
}

// CUT begin
using namespace std;
    char do_test(int,int,vector<int>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int x;
            int y;
            vector<int> jumpLengths;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              x = 5;
                                        y = 4;
                                        jumpLengths = { 2,5 };
                              
                    __expected = "Able";
                    break;
        }
                case 1: {
                              x = 3;
                                        y = 4;
                                        jumpLengths = { 4 };
                              
                    __expected = "Not able";
                    break;
        }
                case 2: {
                              x = 3;
                                        y = 4;
                                        jumpLengths = { 6 };
                              
                    __expected = "Not able";
                    break;
        }
                case 3: {
                              x = 0;
                                        y = 1;
                                        jumpLengths = { 100,100 };
                              
                    __expected = "Able";
                    break;
        }
                case 4: {
                              x = 300;
                                        y = 400;
                                        jumpLengths = { 500 };
                              
                    __expected = "Able";
                    break;
        }
                case 5: {
                              x = 11;
                                        y = 12;
                                        jumpLengths = { 1,2,3,4,5,6,7,8,9,10 };
                              
                    __expected = "Able";
                    break;
        }
                case 6: {
                              x = 11;
                                        y = 12;
                                        jumpLengths = { 1,2,3,4,5,6,7,8,9,100 };
                              
                    __expected = "Not able";
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    x = ;
                  y = ;
                  jumpLengths = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(x, y, jumpLengths, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int x, int y, vector<int> jumpLengths, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(x) << ","<< pretty_print(y) << ","<< jumpLengths  << "]" << endl;
   
    Jumping *__instance = new Jumping();
  string __result = __instance->ableToGet(x, y, jumpLengths);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "Jumping: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
