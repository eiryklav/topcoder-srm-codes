#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> p;
vector<int> l;
vector<int> r;
// ------------- input end  --------------
string solve(); struct SegmentsAndPoints {string isPossible(vector<int> _p, vector<int> _l, vector<int> _r){p = _p, l = _l, r = _r;return ::solve();}};

string solve() {

  typedef pair<int, int> segment;

  int n = l.size();
  vector<segment> segs;
  rep(i, n) {
    segs.emplace_back(r[i], l[i]);
  }

  sort(all(segs));

  rep(i, n) {
    swap(segs[i].first, segs[i].second);
  }

  sort(all(p));

  set<int> usedseg;
  set<int> usedpts;

  rep(i, n) {
    rep(j, n) {
//      cout << "j: " << j << ", " << segs[j].first << ", " << segs[j].second << endl;
      if(!usedseg.count(j) && segs[j].first <= p[i] && p[i] <= segs[j].second) {
        usedpts.insert(i);
        usedseg.insert(j);
        break;
      }
    }
  }

  return usedseg.size() == n ? "Possible" : "Impossible";
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<int>,vector<int>,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> p;
            vector<int> l;
            vector<int> r;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              p = { 1,2 };
                                        l = { 0,0 };
                                        r = { 1,3 };
                              
                    __expected = "Possible";
                    break;
        }
                case 1: {
                              p = { 0 };
                                        l = { 2 };
                                        r = { 3 };
                              
                    __expected = "Impossible";
                    break;
        }
                case 2: {
                              p = { 0,1,2 };
                                        l = { 0,0,1 };
                                        r = { 1,2,1 };
                              
                    __expected = "Possible";
                    break;
        }
                case 3: {
                              p = { 0,1 };
                                        l = { -1,0 };
                                        r = { 0,0 };
                              
                    __expected = "Impossible";
                    break;
        }
                case 4: {
                              p = { 434,63,241,418,-380,-46,397,-205,-262,-282,260,-106,-389,-286,422,-75,127,382,52,-383 };
                                        l = { -447,-226,-411,287,-83,-228,-390,358,422,395,-461,-112,49,75,-160,-152,372,-447,-337,-362 };
                                        r = { -102,348,-70,466,168,-61,-389,469,433,471,-75,-41,52,236,299,-48,383,-353,346,-217 };
                              
                    __expected = "Possible";
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    p = ;
                  l = ;
                  r = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(p, l, r, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> p, vector<int> l, vector<int> r, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << p << ","<< l << ","<< r  << "]" << endl;
   
    SegmentsAndPoints *__instance = new SegmentsAndPoints();
  string __result = __instance->isPossible(p, l, r);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "SegmentsAndPoints: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
