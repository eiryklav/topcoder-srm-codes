#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<string> isFriend;
int d;
// ------------- input end  --------------
int solve(); struct Egalitarianism { int maxDifference(vector<string> _isFriend, int _d) { isFriend = _isFriend, d = _d; return solve(); }};

int dist[55][55];

int N;

int solve() {

  fill(dist[0], &dist[54][54], inf);
  N = isFriend.size();

  rep(i, N) rep(j, N) {
    if(i == j) { dist[i][j] = 0; }
    else {
      dist[i][j] = isFriend[i][j] == 'Y' ? 1 : inf;
    }
  }

  rep(k, N) rep(i, N) rep(j, N) {
    if(dist[i][k] < inf && dist[k][j] < inf) {
      minimize(dist[i][j], dist[i][k] + dist[k][j]);
    }
  }

  int ans = 0;
  rep(i, N) rep(j, N) {
    if(dist[i][j] == inf) {
      return -1;
    }
    else {
      maximize(ans, dist[i][j]);
    }
  }

  return ans * d;
}
