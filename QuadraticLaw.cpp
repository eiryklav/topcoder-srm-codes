#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

using ll = long long;
ll d;

ll solve(); struct QuadraticLaw {ll getTime(ll _d){d = _d;return ::solve();}};

ll solve() {

  ll l = 0, r = sqrt(d) + 10;
  while(1) {
    ll ol_ = l, or_ = r;
    ll t = (l + r) / 2;
    if(t + t * t > d) {
      r = t;
    }
    else {
      l = t;
    }
    if(ol_ == l && or_ == r) { break; }
  }

  return l;
}

// CUT begin
using namespace std;
    char do_test(ll,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            ll d;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              d = 1LL;
                              
                    __expected = 0LL;
                    break;
        }
                case 1: {
                              d = 2LL;
                              
                    __expected = 1LL;
                    break;
        }
                case 2: {
                              d = 5LL;
                              
                    __expected = 1LL;
                    break;
        }
                case 3: {
                              d = 6LL;
                              
                    __expected = 2LL;
                    break;
        }
                case 4: {
                              d = 7LL;
                              
                    __expected = 2LL;
                    break;
        }
                case 5: {
                              d = 1482LL;
                              
                    __expected = 38LL;
                    break;
        }
                case 6: {
                              d = 1000000000000000000LL;
                              
                    __expected = 999999999LL;
                    break;
        }
                case 7: {
                              d = 31958809614643170LL;
                              
                    __expected = 178770270LL;
                    break;
        }
                /*case 8: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    d = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(d, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 8;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(ll d, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(d)  << "]" << endl;
   
    QuadraticLaw *__instance = new QuadraticLaw();
  ll __result = __instance->getTime(d);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "QuadraticLaw: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
