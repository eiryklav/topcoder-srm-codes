#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int x, int y, int W, int  H) { return 0<=x && x<W && 0<=y && y<H; }

struct BlockTower {int getTallest(vector <int> blockHeights);};

int BlockTower::getTallest(vector <int> blockHeights) {
  int res = 0;

  int N = blockHeights.size();

  int dp[55][2];
  minus(dp);
  dp[0][0] = 0;

  REP(i, 1, N+1) {
  	dp[i][0] = dp[i-1][0];
  	dp[i][1] = dp[i-1][1];
  	if(blockHeights[i-1] % 2) {
  		rep(j, i) {
  			if(dp[j][1] >= 0)	dp[i][1] = max(dp[i][1], dp[j][1] + blockHeights[i-1]);
  			if(dp[j][0] >= 0) dp[i][1] = max(dp[i][1], dp[j][0] + blockHeights[i-1]);
  		}
  	}
  	else {
  		rep(j, i) {
  			if(dp[j][0] >= 0) dp[i][0] = max(dp[i][0], dp[j][0] + blockHeights[i-1]);
  		}
  	}
  }

  res = max(dp[N][0], dp[N][1]);

  return res;
}
