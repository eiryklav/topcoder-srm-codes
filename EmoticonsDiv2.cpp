#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <tuple>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int smiles;
// ------------- input end  --------------
int solve(); struct EmoticonsDiv2 {int printSmiles(int _smiles){smiles = _smiles;return solve();}};

auto divisor = [](int x){map<int, int> xfact;for(int i=2; i*i<=x; i++) {while(x % i == 0) {xfact[i]++;x /= i;}}if(x > 1) xfact[x]++;return std::move(xfact);};

int solve() {

  int dp[1001][1001]; // displayed, clipboard -> minimum count
  rep(i, 1001) rep(j, 1001) dp[i][j] = inf;

  queue<tuple<int, int, int>> q;
  q.emplace(1, 0, 0);
  dp[1][0] = 0;
  
  while(!q.empty()) {
    int disp, clip, cost; tie(disp, clip, cost) = q.front(); q.pop();
    if(disp == smiles) { return cost; }
    if(disp + clip <= smiles && dp[disp + clip][clip] > cost + 1) {
      dp[disp + clip][clip] = cost + 1;
      q.emplace(disp + clip, clip, cost + 1);
    }
    if(clip < disp && 2 * disp <= smiles && dp[disp][disp] > cost + 1) {
      dp[disp][disp] = cost + 1;
      q.emplace(disp, disp, cost + 1);
    }
  }
  assert(false);
  /*
  dp[1][0] = 0;
  rep(i, smiles) rep(j, 1001) {
    if(dp[i][j] == inf) { continue; }
    if(i+j <= smiles) {
      minimize(dp[i+j][j], dp[i][j] + 1);
    }
    if(j < i) {
      minimize(dp[i][i], dp[i][j] + 1);
    }
  }
  */

  int min = inf;
  rep(i, 1001) {
    minimize(min, dp[smiles][i]);
  }
  return min;
}

// CUT begin
using namespace std;
    char do_test(int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int smiles;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              smiles = 2;
                              
                    __expected = 2;
                    break;
        }
                case 1: {
                              smiles = 6;
                              
                    __expected = 5;
                    break;
        }
                case 2: {
                              smiles = 11;
                              
                    __expected = 11;
                    break;
        }
                case 3: {
                              smiles = 16;
                              
                    __expected = 8;
                    break;
        }
                case 4: {
                              smiles = 1000;
                              
                    __expected = 21;
                    break;
        }
                /*case 5: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    smiles = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(smiles, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 5;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int smiles, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(smiles)  << "]" << endl;
   
    EmoticonsDiv2 *__instance = new EmoticonsDiv2();
  int __result = __instance->printSmiles(smiles);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "EmoticonsDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
