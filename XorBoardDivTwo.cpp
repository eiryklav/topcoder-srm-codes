#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct XorBoardDivTwo {int theMax(vector <string> board);};

int XorBoardDivTwo::theMax(vector <string> board) {
  int res = 0;
  int N = board.size();
  int M = board[0].size();

  int ans = 0;

	rep(i, N) {
		auto B = board;
		rep(k, M) B[i][k] = 1 - (B[i][k] - '0') + '0';
		rep(j, M) {
			rep(k, N) B[k][j] = 1 - (B[k][j] - '0') + '0';

			int count = 0;
			rep(k, N) rep(l, M) {
				count += B[k][l] == '1';
			}

			ans = max(ans, count);

			rep(k, N) B[k][j] = 1 - (B[k][j] - '0') + '0';

		}
	}

	res = ans;

  return res;
}
