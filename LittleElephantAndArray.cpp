#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
ll A;
int N;
// ------------- input end  --------------
int solve(); struct LittleElephantAndArray {int getNumber(ll _A, int _N){A = _A, N = _N;return solve();}};

int const MOD = 1e9+7;

int dp[2][1<<16];
vector<ll> from, to;

vector<ll> makeNums(string const& raw) {
  vector<ll> nums;
  int const rawsize = raw.size();
  REP(S, 1, 1<<rawsize) {
    /*
    string s; rep(i, rawsize) if(S >> i & 1) { s += raw[i]; }
    stringstream ss(s);
    */
    ll n = 0;
    rep(i, rawsize) {
      if(S >> i & 1) {
        n *= 10;
        n += raw[i] - '0';
      }
    }
    nums.push_back(n);
  }
  sort(all(nums));
  return std::move(nums);
}

int solve() {

  from.clear(); to.clear(); zero(dp);

  vector<string> vs;

  rep(i, N+1) {
    vs.push_back(to_string(A + i));
  }

  from = makeNums(vs[0]);

  rep(i, from.size()) {
    dp[0][i] = i+1;
  }

  REP(i, 1, N+1) {
    to = makeNums(vs[i]);
    rep(j, to.size()) {
      dp[i&1][j] = j ? dp[i&1][j-1] : 0;  // 後に引き継ぐ
      int k = upper_bound(all(from), to[j]) - 1 - from.begin();
      if(k >= 0) {
        dp[i&1][j] += dp[(i&1)^1][k];
        dp[i&1][j] %= MOD;
      }
    }
    from.swap(to);
  }
  return dp[N&1][(int)from.size()-1];
}

// CUT begin
using namespace std;
    char do_test(ll,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            ll A;
            int N;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              A = 1LL;
                                        N = 9;
                              
                    __expected = 1;
                    break;
        }
                case 1: {
                              A = 10LL;
                                        N = 2;
                              
                    __expected = 15;
                    break;
        }
                case 2: {
                              A = 4747774LL;
                                        N = 1;
                              
                    __expected = 8369;
                    break;
        }
                case 3: {
                              A = 6878542150015LL;
                                        N = 74;
                              
                    __expected = 977836619;
                    break;
        }
                /*case 4: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    A = ;
                  N = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(A, N, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 4;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(ll A, int N, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(A) << ","<< pretty_print(N)  << "]" << endl;
   
    LittleElephantAndArray *__instance = new LittleElephantAndArray();
  int __result = __instance->getNumber(A, N);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "LittleElephantAndArray: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
