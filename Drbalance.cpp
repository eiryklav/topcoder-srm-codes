#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<

typedef long long ll;

struct Drbalance {int lesscng(string s, int k);};

int Drbalance::lesscng(string s, int K) {
  int N = s.size();
  int negaNum = 0;
  rep(i, N+1) {
  	int balance = 0;
  	rep(j, i) {
	  	balance -= s[j] == '-';
	  	balance += s[j] == '+';
	  }
	  if(balance < 0) {
	  	negaNum ++;
	  }
  }

  cout << negaNum << ":\n";

  if(negaNum <= K) {
  	return 0;
  }

  rep(k, N) {
  	rep(kk, N) if(s[kk] == '-') { s[kk] = '+'; break; }
  	int num = 0;
  	rep(i, N+1) {
  		int b = 0;
  		rep(j, i) {
  			b += s[j] == '+';
  			b -= s[j] == '-';
  		}
  		if(b < 0) {
  			num++;
  		}
  	}
  	if(num <= K) {
  		return k+1;
  	}
  }
  assert(false);
}