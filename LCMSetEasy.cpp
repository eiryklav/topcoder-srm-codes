#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> S;
int x;
// ------------- input end  --------------
string solve(); struct LCMSetEasy {string include(vector<int> _S, int _x){S = _S, x = _x;return solve();}};

template<class T>
auto lcm(T x, T y){
  return x * y / __gcd(x, y);
};

string solve() {

  ll L = 1;
  for(auto& s: S) {
    if(x % s == 0) {
      L = lcm(L, (ll)s);
    }
  }

  return L != x ? "Impossible" : "Possible";
}

// CUT begin
using namespace std;
    char do_test(vector<int>,int,string,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> S;
            int x;
            string __expected = string();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              S = { 2,3,4,5 };
                                        x = 20;
                              
                    __expected = "Possible";
                    break;
        }
                case 1: {
                              S = { 2,3,4 };
                                        x = 611;
                              
                    __expected = "Impossible";
                    break;
        }
                case 2: {
                              S = { 2,3,4 };
                                        x = 12;
                              
                    __expected = "Possible";
                    break;
        }
                case 3: {
                              S = { 1,2,3,4,5,6,7,8,9,10 };
                                        x = 24;
                              
                    __expected = "Possible";
                    break;
        }
                case 4: {
                              S = { 100,200,300,400,500,600 };
                                        x = 2000;
                              
                    __expected = "Possible";
                    break;
        }
                case 5: {
                              S = { 100,200,300,400,500,600 };
                                        x = 8000;
                              
                    __expected = "Impossible";
                    break;
        }
                case 6: {
                              S = { 1000000000,999999999,999999998 };
                                        x = 999999999;
                              
                    __expected = "Possible";
                    break;
        }
                case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                  S = {8648, 653, 1058, 134, 827, 170, 42, 3, 82150, 5, 74242, 2}, 
                  x = 775726;
              
                  __expected = "Impossible";
          break;
        }
    
    default: return 'm';
  }
  return do_test(S, x, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> S, int x, string __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << S << ","<< pretty_print(x)  << "]" << endl;
   
    LCMSetEasy *__instance = new LCMSetEasy();
  string __result = __instance->include(S, x);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "LCMSetEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
