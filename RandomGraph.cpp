#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
int n;
int p;
// ------------- input end  --------------
double solve(); struct RandomGraph {double probability(int _n, int _p){n = _n, p = _p;return solve();}};

namespace math { namespace combination {

#define COMBINATION_MAX 55

namespace memorized { namespace combination {
  enum { max_value = COMBINATION_MAX };
  bool __computed = false; inline bool computed() {if(__computed) { return true; } else { __computed = true; return false; }}
  ll dp[max_value][max_value];
}}

int comb(int N, int K) {
  using memorized::combination::dp;
  if(memorized::combination::computed()) { return dp[N][K]; }

  // Make binomial coefficient
  using memorized::combination::max_value;

  rep(i, max_value) {
    dp[i][0] = 1;
    REP(j, 1, i+1) {
      dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
//      dp[i][j] %= MOD;
    }
  }
  return dp[N][K];
}

}}

using namespace math::combination;

double dp[55][44][33];

#define unconp ((1000-p)/1000.)
#define conp (p/1000.)

double dfs(int a, int b, int c) {

  auto& ret = dp[a][b][c];
  if(ret + 1) { return ret; }

  ret = 0.0;

  int rem = n - a - 2*b - 3*c;

  if(rem == 0) { return ret = 1.0; }
  if(rem < 0) { return ret = 0.0; }

  // 1
  ret += dfs(a+1, b, c) * rem * pow(unconp, rem-1);

  // 2
  if(rem > 1) {
    ret += dfs(a, b+1, c) * comb(rem, 2) * conp * pow(unconp, 2*rem-4);
  }

  // 3
  if(rem > 2) {
    ret += dfs(a, b, c+1) * comb(rem, 3) * pow(conp, 3.) * pow(unconp, 3*rem-9);
  }

  return ret;
}

double solve() {
  rep(i, 55) rep(j, 44) rep(k, 33) dp[i][j][k] = -1;
  return 1.0 - dfs(0, 0, 0);
}

// CUT begin
using namespace std;
    char do_test(int,int,double,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            int n;
            int p;
            double __expected = double();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              n = 7;
                                        p = 0;
                              
                    __expected = 0.0;
                    break;
        }
                case 1: {
                              n = 3;
                                        p = 620;
                              
                    __expected = 0.0;
                    break;
        }
                case 2: {
                              n = 4;
                                        p = 500;
                              
                    __expected = 0.59375;
                    break;
        }
                case 3: {
                              n = 8;
                                        p = 100;
                              
                    __expected = 0.33566851611343496;
                    break;
        }
                case 4: {
                              n = 15;
                                        p = 50;
                              
                    __expected = 0.5686761670525845;
                    break;
        }
                case 5: {
                              n = 50;
                                        p = 10;
                              
                    __expected = 0.7494276522159893;
                    break;
        }
                case 6: {
                              n = 50;
                                        p = 1000;
                              
                    __expected = 1.0;
                    break;
        }
                /*case 7: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    n = ;
                  p = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(n, p, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 7;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

#include<cmath>
bool double_equal (const double &expected, const double &received) { return abs(expected - received) < 1e-9 || abs(received) > abs(expected) * (1.0 - 1e-9) && abs(received) < abs(expected) * (1.0 + 1e-9); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(int n, int p, double __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(n) << ","<< pretty_print(p)  << "]" << endl;
   
    RandomGraph *__instance = new RandomGraph();
  double __result = __instance->probability(n, p);
    delete __instance;
  
  bool __correct = __unknownAnswer ||   double_equal(__expected, __result);
                    
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "RandomGraph: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
