#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;++i)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<30;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
vector<int> magicPower;
vector<string> magicBond;
int K;
// ------------- input end  --------------
int solve(); struct MagicMoleculeEasy {int maxMagicPower(vector<int> _magicPower, vector<string> _magicBond, int _k){magicPower = _magicPower, magicBond = _magicBond, K = _k;return ::solve();}};

int const MOD = 1e9+7;

int N;

int solve() {

	rep(i, 55) g[i].clear();
	N = magicPower.size();
	rep(i, N) rep(j, N) {
		if(magicBond[i][j] == 'Y') {
			g[i].push_back(j);
		}
	}

	vector<int> atoms;
	rep(i, N) {
		if(g[i].empty()) {
			atoms.push_back(i);
		}
	}

	int ans = 0;

	rep(i, N) {
		if(!g[i].empty()) {
			auto ra = atoms;
			int r;
			r = dfs(1<<i, ra, K, i, true);
			if(r > 0) { ans += MOD; ans %= MOD; }
			ra.push_back(i);
			r = dfs(1<<i, ra, K-1, i, false);
			if(r > 0) { ans += MOD; ans %= MOD; }
		}
	}

  return ans;
}

// CUT begin
using namespace std;
    char do_test(vector<int>,vector<string>,int,int,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            vector<int> magicPower;
            vector<string> magicBond;
            int k;
            int __expected = int();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              magicPower = { 1,2 };
                                        magicBond = { "NY","YN" };
                                        k = 1;
                              
                    __expected = 2;
                    break;
        }
                case 1: {
                              magicPower = { 100,1,100 };
                                        magicBond = { "NYN","YNY","NYN" };
                                        k = 1;
                              
                    __expected = 1;
                    break;
        }
                case 2: {
                              magicPower = { 100,1,100 };
                                        magicBond = { "NYN","YNY","NYN" };
                                        k = 2;
                              
                    __expected = 200;
                    break;
        }
                case 3: {
                              magicPower = { 4,7,5,8 };
                                        magicBond = { "NYNY","YNYN","NYNY","YNYN" };
                                        k = 2;
                              
                    __expected = 15;
                    break;
        }
                case 4: {
                              magicPower = { 46474,60848,98282,58073,42670,50373 };
                                        magicBond = { "NYNNNY","YNNYNY","NNNYYY","NYYNNN","NNYNNN","YYYNNN" };
                                        k = 3;
                              
                    __expected = 209503;
                    break;
        }
                case 5: {
                              magicPower = { 1,1,1,1,1,1,1,1,1,1,1,1,1 };
                                        magicBond = { "NNYYYNYYNYNNY","NNYNYYYYYYYNY","YYNYYNYYYYYYY","YNYNYYNYYYYYY","YYYYNNYYYYYNY","NYNYNNYYYYYNN","YYYNYYNYYYYYY","YYYYYYYNYNYYY","NYYYYYYYNYYYY","YYYYYYYNYNNNN","NYYYYYYYYNNYN","NNYYNNYYYNYNN","YYYYYNYYYNNNN" };
                                        k = 9;
                              
                    __expected = -1;
                    break;
        }
                case 6: {
                              magicPower = { 1,1 };
                                        magicBond = { "NN","NN" };
                                        k = 1;
                              
                    __expected = 1;
                    break;
        }
                case 7: {
                              magicPower = { 1,1,2,5,2,4,2 };
                                        magicBond = { "NNNNNNN","NNYNNNN","NYNNNYN","NNNNNNY","NNNNNNN","NNYNNNN","NNNYNNN" };
                                        k = 3;
                              
                    __expected = 11;
                    break;
        }
                /*case 8: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    magicPower = ;
                  magicBond = ;
                  k = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(magicPower, magicBond, k, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 8;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5, 6, 7 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }
// Vector print
template <typename T> ostream &operator << (ostream &out, vector<T> arr) {
out << "{";
for (int i = 0; i < arr.size(); ++i) out << (i == 0 ? "" : ",") << pretty_print(arr[i]);
  out << "}";
return out;
}

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(vector<int> magicPower, vector<string> magicBond, int k, int __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << magicPower << ","<< magicBond << ","<< pretty_print(k)  << "]" << endl;
   
    MagicMoleculeEasy *__instance = new MagicMoleculeEasy();
  int __result = __instance->maxMagicPower(magicPower, magicBond, k);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "MagicMoleculeEasy: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
