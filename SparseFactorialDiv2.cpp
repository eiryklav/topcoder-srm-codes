#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define REPLL(i,a,b) for(ll i=a;i<(ll)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll = long long;
using pii = pair<int, int>;

constexpr int inf = 1<<28;
constexpr ll Inf = 1e17;

bool in_range(int y, int x, int H, int W) { return 0<=x && x<W && 0<=y && y<H; }

// ------------- input start -------------
ll lo;
ll hi;
ll divisor;
// ------------- input end  --------------
ll solve(); struct SparseFactorialDiv2 {ll getCount(ll _lo, ll _hi, ll _divisor){lo = _lo, hi = _hi, divisor = _divisor;return solve();}};

ll unkocount(ll x, ll i) {if(x % divisor > i) { return x / divisor + 1; }else { return x / divisor; }}ll unko(ll x) {ll ret = 0;vector<ll> dp(divisor, -1);ll k = 0;while(k * k < x - 1) {if(dp[(k * k) % divisor] < 0) {dp[(k * k) % divisor] = k;}k ++;}REPLL(i, 0, divisor) {if(dp[i] < 0) { continue; }ll a = dp[i] * dp[i] + 1;ll b = x;ret += unkocount(b, i) - unkocount(a, i);}return ret;}ll solve() {return unko(hi+1) - unko(lo);}

// CUT begin
using namespace std;
    char do_test(ll,ll,ll,ll,int,bool);
//------------------------------------------------------------------------------
        
    char run_testcase(int __no) {
            ll lo;
            ll hi;
            ll divisor;
            ll __expected = ll();
      bool __unknownAnswer = false;
      
      switch (__no) {
                case 0: {
                              lo = 4LL;
                                        hi = 8LL;
                                        divisor = 3LL;
                              
                    __expected = 3LL;
                    break;
        }
                case 1: {
                              lo = 9LL;
                                        hi = 11LL;
                                        divisor = 7LL;
                              
                    __expected = 1LL;
                    break;
        }
                case 2: {
                              lo = 1LL;
                                        hi = 1000000000000LL;
                                        divisor = 2LL;
                              
                    __expected = 999999999999LL;
                    break;
        }
                case 3: {
                              lo = 16LL;
                                        hi = 26LL;
                                        divisor = 11LL;
                              
                    __expected = 4LL;
                    break;
        }
                case 4: {
                              lo = 10000LL;
                                        hi = 20000LL;
                                        divisor = 997LL;
                              
                    __expected = 1211LL;
                    break;
        }
                case 5: {
                              lo = 123456789LL;
                                        hi = 987654321LL;
                                        divisor = 71LL;
                              
                    __expected = 438184668LL;
                    break;
        }
                /*case 6: { 
        // Your custom testcase goes here (don't forget to add to num/runTests below)
                    lo = ;
                  hi = ;
                  divisor = ;
              
          __unknownAnswer = true; 
          break;
        }*/
    
    default: return 'm';
  }
  return do_test(lo, hi, divisor, __expected, __no, __unknownAnswer);
}

// Tests total:
int      numTests  = 6;
// Tests to run when there are no arguments:
set<int> runTests = { 0, 1, 2, 3, 4, 5 };

//------------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <ctime>

template <typename T> string pretty_print(T t) { stringstream s; typeid(T) == typeid(string) ? s << "\"" << t << "\"" : s << t; return s.str(); }

string colorString(string s, int q)
{
  if (q == 0) {
        //neutral
    return s;
  } else if (q < -1) {
        //bad (score)
    return "\033[1;41m"+s+"\033[0m";
  } else if (q < 0) {
        //bad (single result)
    return "\033[1;31m"+s+"\033[0m";
  } else {
        //good
    return "\033[1;32m"+s+"\033[0m";
  }
}

string colorTestResult(char r)
{
  string s = string(1, r);
  switch(r) {
    case '+' :
    return colorString(s, 1);
    case '?':
    return colorString(s, 0);
    default :
    return colorString(s, -1);
  }
  return "";
}

char do_test(ll lo, ll hi, ll divisor, ll __expected, int __caseNo, bool __unknownAnswer) {
 cout << "\033[1;36mTest " << __caseNo << "\033[0m: [" << pretty_print(lo) << ","<< pretty_print(hi) << ","<< pretty_print(divisor)  << "]" << endl;
   
    SparseFactorialDiv2 *__instance = new SparseFactorialDiv2();
  ll __result = __instance->getCount(lo, hi, divisor);
    delete __instance;
  
  bool __correct = __unknownAnswer || (__result == __expected);
                
        if (! __correct) {
          cout << "Desired answer:" << endl;
          cout << "\t" <<  pretty_print(__expected)  << endl;
        }
        cout << "Your answer:" << endl;
        cout << "\t" <<  pretty_print(__result)  << endl; 
        
        char __res = '-';
        if (! __correct) {
          __res = 'X';
                  } else if (__unknownAnswer) {
          __res = '?';
        } else {
          __res = '+';
        }
        cout << " "<<colorTestResult(__res)<<endl;
        
        cout << "\033[0;2m===============================================================\033[0m" << endl;
          return __res;
        }
        
        int main(int argc, char *argv[]) {
          string result;
          if (argc > 1) {
            runTests.clear();
            for (int i = 1; i < argc; ++i) {
              runTests.insert(atoi(argv[i])); 
            }
          }
          int j = 0;
          for (int i: runTests) {
            while (j < i) {
              result += 'd';
              j++;
            }
            result += run_testcase(i);
            j = i + 1;
          }
          result += string( std::max(0, numTests - j), 'd' );
          cout << "SparseFactorialDiv2: ";
          bool good = true;
          for (char ch: result) {
            good &= ( ch == '?' || ch == '+' );
            cout << colorTestResult(ch);
          }
          
                    cout << endl;
                    return 0;
        }
// CUT end
