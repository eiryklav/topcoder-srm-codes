#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define pr cout <<
#define zero(c) memset(c, 0, sizeof c)

typedef long long ll;

struct FoxAndMountain {int count(int n, string history);};

int const MOD9 = 1e9+9;

int automaton[51][2];	// atm[i文字目まで一致]['U'か'D'か] := 次の遷移先の一致文字数
int dp[51][51][51];		// dp[x=i][historyをj文字使用した][高さ] := 組み合わせの数

int FoxAndMountain::count(int n, string history) {

	int L = history.size();

	zero(automaton);

	rep(i, L) {
		rep(isU, 2) {
			if(history[i] == "DU"[isU]) {
				// 一致; パターンの次の文字に遷移する
				automaton[i][isU] = i+1;

				// 不一致; パターンの直近の最長一致まで遷移する
				auto work = history.substr(0, i);
				work += "DU"[isU^1];
				REP(j, 1, i+1) {
					if(history.substr(0, j) == work.substr(i-j+1)) {
						automaton[i][isU^1] = j;
					}
				}
			}
		}
	}

	automaton[L][0] = automaton[L][1] = L;	// 終端

	zero(dp);
	dp[0][0][0] = 1;
	rep(i, n) rep(j, L+1) rep(k, 30) {
		if(!dp[i][j][k]) { continue; }
		if(k > 0) { (dp[i+1][automaton[j][0]][k-1] += dp[i][j][k]) %= MOD9; }
		if(k <25) { (dp[i+1][automaton[j][1]][k+1] += dp[i][j][k]) %= MOD9; }
	}

  return dp[n][L][0];
}
